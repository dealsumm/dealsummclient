var modulesDependencies = {
    'recent':[
        STATIC_PREFIX + APP_FOLDER + 'recent/controllers/recentCtrl.js'
    ],
    'properties':[
        STATIC_PREFIX + 'common/services/uploadServ.js',
        STATIC_PREFIX + APP_FOLDER + 'properties/controllers/propertiesCtrl.js',
        STATIC_PREFIX + APP_FOLDER + 'properties/controllers/createNewPropertyCtrl.js'
    ],
    'directory':[
        STATIC_PREFIX + 'common/services/uploadServ.js',
        STATIC_PREFIX + 'common/directives/checklist.js',
        STATIC_PREFIX + 'common/directives/ng-context-menu-client.js',
        STATIC_PREFIX + 'common/directives/focusMe.js',
        STATIC_PREFIX + 'common/directives/ng-enter.js',
        STATIC_PREFIX + 'common/directives/datePicker.js',
        STATIC_PREFIX + APP_FOLDER + 'directory/services/advancedSearchServ.js',
        STATIC_PREFIX + APP_FOLDER + 'directory/services/directoryRcmServ.js',
        STATIC_PREFIX + APP_FOLDER + 'directory/services/directoryServ.js',
        STATIC_PREFIX + APP_FOLDER + 'directory/controllers/directoryCtrl.js',
        STATIC_PREFIX + APP_FOLDER + 'directory/controllers/searchModalCtrl.js',
        STATIC_PREFIX + APP_FOLDER + 'directory/controllers/templatesAssignCtrl.js'
    ],
    'rentRoll':[
        STATIC_PREFIX + APP_FOLDER + 'rentRoll/services/rentRollServ.js',
        STATIC_PREFIX + APP_FOLDER + 'rentRoll/controllers/rentRollCtrl.js',
    ]
}
var getModulesDependencies = function(arrs){
    // merge arrays without duplicates
    var ret = [], dependency;
    //console.log("dest",destCopy);
    for(var i=0;i<arrs.length;i++){
        dependency = modulesDependencies[arrs[i]];
        angular.forEach(dependency, function(item){
            //console.log("item",item);
            if(ret.indexOf(item)==-1){
                ret.push(item);
            }
        })
    }
    return ret;
}
/*
 * using a @resolve function that returns a promise in our state definition,
 * this would load all lazy artefacts and resolve the promise once they have been loaded.
 * This ensures that all lazy artefacts will be available before the relevant route is rendered.
 * */

define( function (require) {
    'use strict';

    var angularAMD = require('angularAMD');

    // core
    require('jquery');
    require('angular-ui-router');
    require('angular-resource');

    // extras
    require('httpRequestTracker');
    require('angularFileUpload');
    require('localStorageModule');
    require('notifications');
    require('ncy-angular-breadcrumb');
    require('uiModal');
    require('angular-sanitize');
    require('angular-animate');

    // directives
    require('ngFileUpload');
    require('angular-ui-utils-unique');
    require('angular-ui-bootstrap-transition');
    require('angular-ui-bootstrap-collapse');
    require('angular-ui-bootstrap-dropdownToggle');
    require('angular-ui-bootstrap-bindHtml');
    require('angular-ui-bootstrap-position');
    require('angular-ui-bootstrap-typeahead');
    require('angular-ui-bootstrap-tpls');
    require('angular-ui-utils-scrollfix');
    require('angular-cookies');

    // modules
    require('dealsumm.dashboard');
    require('dealsumm.property');

    var app = angular.module('dealsumm', [
        'ngAnimate',
        'ngSanitize',
        'ngResource',
        'ngCookies',
        'angularFileUpload',
        'services.httpRequestTracker',
        'ui.router',
        'ui.bootstrap.transition',
        'ui.bootstrap.collapse',
        'ui.unique',
        'ui.scrollfix',
        'services.i18nNotifications',
        'ui.bootstrap.dropdownToggle',
        'ui.bootstrap.typeahead',
        'ui.bootstrap',
        'modal',
        'LocalStorageModule',
        'ncy-angular-breadcrumb',
        'ngFileUpload',
        'dealsumm.dashboard',
        'dealsumm.property'
    ]);

    app
        // prevent page from scrolling top each state change
        .value('$anchorScroll', angular.noop)


        // notifications messages
        .constant('I18N.MESSAGES', {
            'error.service.notfound': 'Web service not found {{status}}: {{cause}} ',
            //'error.service.internalError': 'Something went wrong {{status}}: {{cause}}',
            'error.service.internalError': 'Server Error {{status}}',
            'error.service.unauthorizedError': 'Something went wrong {{status}}: This action is not valid.',
            'error.service.unknown': 'Something went wrong {{status}}: {{cause}}',
            'error.service.badRequest': '{{cause}} ({{status}})',
            'errors.route.changeError': 'Route change error',
            'crud.user.save.success': "A user with id '{{id}}' was saved successfully.",
            'crud.user.remove.success': "A user with id '{{id}}' was removed successfully.",
            'crud.user.remove.error': "Something went wrong when removing user with id '{{id}}'.",
            'login.reason.notAuthorized': "You do not have the necessary access permissions.  Do you want to login as someone else?",
            'login.reason.notAuthenticated': "You must be logged in to access this part of the application.",
            'login.error.invalidCredentials': "Login failed.  Please check your credentials and try again.",
            'login.error.serverError': "There was a problem with authenticating: {{exception}}.",
            'support.ticket.edit.success': "A ticket with id {{ticketId}} was updated successfully.",
            'upload.error.server': "{{status}}: {{cause}}.",
            'upload.error.general': "Something went wrong while trying to upload the file. {{status}}: {{cause}}."
        })

        .constant('DEALSUMM_CONFIG', {
            REST_PATH: '/deals/orgs/',
            DSPROD_REST_PATH: '/dsprod/account/',
            GT_REST_PATH: '/gt/account/',
			BASE_PATH:'/dealsumm/',
			BASE_PATH_IMAGES: STATIC_PREFIX+'styles/img/',
			INNER_PATH:'',
            SHOW_BC: true
        })
        .constant('GLOBAL_VARS', {
            rightMenuItems:[],
            maxExtraTabs:3,
            HEADERS:null
        })

        .directive('onFinishRender',['$timeout', function ($timeout) {
            return {
                restrict: 'A',
                link: function (scope, element, attr) {
                    if (scope.$last === true) {
                        $timeout(function () {
                            scope.$emit('ngRepeatFinished', element);
                        });
                    }
                }
            };
        }])
        .config([    '$stateProvider', '$urlRouterProvider', '$locationProvider', '$provide', '$httpProvider', '$breadcrumbProvider',
            function ($stateProvider,   $urlRouterProvider,   $locationProvider,   $provide,   $httpProvider, $breadcrumbProvider) {

                $httpProvider.defaults.xsrfCookieName = 'csrftoken';
                $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';
                //$httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
                $httpProvider.defaults.headers.common = { 'Cache-Control' :'no-cache'};
                if (!$httpProvider.defaults.headers.get) {
                    $httpProvider.defaults.headers.get = {};
                }
                $httpProvider.defaults.headers.get['If-Modified-Since'] = '0';

                // https://github.com/ncuillery/angular-breadcrumb
                $breadcrumbProvider.setOptions({
                    prefixStateName: '',
                    template: 'bootstrap2'
                });

                var httpLogInterceptor = ['$injector', '$q', 'i18nNotifications', '$rootScope', function ($injector, $q, i18nNotifications, $rootScope) {
                    // Lazy load notifications to get around circular dependency
                    //Circular dependency: $rootScope <- notifications <- i18nNotifications <- $exceptionHandler

                    return function (promise) {
                        /*
                         * {param} null - success
                         * {param} @function - error
                         * */
                        return promise.then(null, function (response) {

                            //console.log("response",response.config, response);
                            //used to skip response code validations after switch account process has started to prevent error popups
                            if(response.config.bypassErrorsInterceptor && response.config.bypassErrorsInterceptor==response.status){
                                return response;
                            }

                            var notFound = function () {
                                i18nNotifications.pushForCurrentRoute('error.service.notfound', 'error', {
                                    status: response.status,
                                    cause: response.data
                                });
                            };
                            var internalError = function () {
                                i18nNotifications.pushForCurrentRoute('error.service.internalError', 'error', {
                                    status: response.status,
                                    cause: response.data
                                });
                            };
                            var badRequest = function () {
                                i18nNotifications.pushForCurrentRoute('error.service.badRequest', 'error', {
                                    status: response.status,
                                    cause: response.data.error
                                });
                            };
                            var unauthorisedError = function () {
                                i18nNotifications.pushForCurrentRoute('error.service.unauthorizedError', 'error');
                            };
                            var unknown = function () {
                                i18nNotifications.pushForCurrentRoute('error.service.unknown', 'error', {
                                    status: response.status,
                                    cause: response.data
                                });
                            };
                            var sessionTimeout = function () {
                                //document.location.href = "service/settings/logout?logoutType=session-timeout";
                            };
                            var logOut = function () {
                                //document.location.href = "service/settings/logout";
                            };
                            // do something on error
                            // Push a notification error

                            switch (response.status) {
                                case 0:
                                case 401:
                                    unauthorisedError();
                                    break;
                                case 400:
                                    badRequest();
                                    break;
                                case 403:
                                case 432:
                                    document.location.href = "/deal/";
                                    return $q.resolve(response);
                                    break;
                                case 404:
                                    notFound();
                                    break;
                                case 500:
                                    internalError();
                                    break;
                            }
                            return $q.reject(response);
                        });
                    };

                }];

                $httpProvider.responseInterceptors.push(httpLogInterceptor);

                $httpProvider.interceptors.push(['$q', 'httpRequestTracker', function ($q, httpRequestTracker) {
                    return {
                        'request': function (config) {
                            if (!config.headers['noWait']) {
                                httpRequestTracker.addRequest();
                            }
                            return config || $q.when(config);
                        },
                        'response': function (config) {
                            httpRequestTracker.removeRequest();
                            return config || $q.when(config);
                        },
                        'responseError': function (config) {
                            httpRequestTracker.removeRequest();
                            return $q.reject(config);
                        }
                    };
                }]);


                /**
	             * decorate $q with success and error functions
	             */
	            $provide.decorator('$q', ['$delegate',function ($delegate) {
	                var defer = $delegate.defer;
	                $delegate.defer = function () {
	                  var deferred = defer();
	                  deferred.promise.success = function (fn) {
	                    deferred.promise.then(function (value) {
	                      fn(value);
	                    });
	                    return deferred.promise;
	                  };
	                  deferred.promise.error = function (fn) {
	                    deferred.promise.then(null, function (value) {
	                      fn(value);
	                    });
	                    return deferred.promise;
	                  };
	                  return deferred;
	                };
	                return $delegate;
	            }]);

	            /////////////////////////
                // app & global states //
                /////////////////////////
                console.log("VIEW",VIEW);
                // Use $urlRouterProvider to configure any redirects (when) and invalid urls (otherwise).
                if(VIEW == "client"){
                    $urlRouterProvider
                        // The `when` method says if the url is ever the 1st param, then redirect to the 2nd param
                        // Here we are just setting up some convenience urls.
                        .when('', '/account/')
                        .when('/account', '/account/')
                        // regex for catching all "org" paths
                        .when(/\/org\//i, '/account/')
                        // If the url is ever invalid, e.g. '/asdf', then redirect to '/' aka the home state
                        .otherwise('/404');
                }else{
                    $urlRouterProvider
                        .when('', '/org/')
                        .when('/org', '/org/')
                        .when('/account', '/org/')
                        // regex for catching all "account" paths
                        .when(/\/account\//i, '/org/')
                        .otherwise('/404');
                }

                $stateProvider
                    .state('app', {
                        // With abstract set to true, that means this state can not be explicitly activated.
                        // It can only be implicitly activated by activating one if it's children.
                        abstract: true,
                        //url: '/org/{orgId}',
                        controller: 'appCtrl',
                        templateUrl: STATIC_PREFIX+'masterPage'+VIEW+'.html',
                        resolve: {
                            load: angularAMD.load([
                                STATIC_PREFIX + 'app/appCtrl.js',
                                STATIC_PREFIX + 'app/uploadCtrl.js',
                                STATIC_PREFIX + 'common/utils/generalFilters.js',
                                STATIC_PREFIX + 'common/directives/generalDirectives.js',
                                STATIC_PREFIX + 'common/services/generalServ.js'
                            ]),
                            resolveOrgId: ['$rootScope', 'GLOBAL_VARS' ,'$state', '$stateParams', function($rootScope, GLOBAL_VARS, $state, $stateParams){
                                setTimeout(function(){
                                    //console.log("$stateParams",$rootScope.$stateParams);
                                    GLOBAL_VARS.orgId = $rootScope.orgId = $rootScope.$stateParams.orgId;
                                    GLOBAL_VARS.accountId = $rootScope.accountId = $rootScope.$stateParams.accountId;
                                }, 50)
                            }]
                        },
                        ncyBreadcrumb: {
                            skip: true
                        }
                    })
                    .state('app.notFound', {
                        url: '/404',
                        templateUrl: STATIC_PREFIX+'404.html',
                        ncyBreadcrumb: {
                            skip: true
                        }
                    })
                    .state('app.adminView', {
                        url: '/admin',
                        controller: 'adminCtrl',
                        templateUrl: STATIC_PREFIX + APP_FOLDER_NORMAL + 'admin/views/admin.html',
                        resolve: {
                            load: angularAMD.load([
                                    STATIC_PREFIX + APP_FOLDER_NORMAL + 'admin/controllers/adminCtrl.js'
                            ])
                        },
                        ncyBreadcrumb: {
                            label: 'admin'
                        }
                    })
                    .state('app.adminViewDebug', {
                        url: '/admin-debug',
                        controller: 'adminCtrl',
                        templateUrl: STATIC_PREFIX + APP_FOLDER_NORMAL + 'admin/views/admin.html',
                        resolve: {
                            load: angularAMD.load([
                                STATIC_PREFIX + APP_FOLDER_NORMAL + 'admin/services/adminServ.js',
                                STATIC_PREFIX + APP_FOLDER_NORMAL + 'admin/controllers/adminCtrl.js'
                            ])
                        },
                        ncyBreadcrumb: {
                            skip: true
                        }
                    })
                    .state('app.help', {
                        url: '/help',
                        controller: 'helpCtrl',
                        templateUrl: STATIC_PREFIX + APP_FOLDER_NORMAL + 'help/views/help.html',
                        resolve: {
                            load: angularAMD.load([
                                STATIC_PREFIX + APP_FOLDER_NORMAL + 'help/controllers/helpCtrl.js'
                            ])
                        },
                        ncyBreadcrumb: {
                            skip: true
                        }
                    })
                if(VIEW == "client") {
                    $stateProvider
                        .state('app.client', {
                            url: '/account/',
                            controller: ['$rootScope', 'GLOBAL_VARS' , '$state', function ($rootScope, GLOBAL_VARS, $state) {
                                //console.log("accountId>>>> --- ", $rootScope.accountId, $rootScope.$stateParams.accountId, GLOBAL_VARS.accountId);

                                // FOR URI: (By Chaim, Feb 6th 2015)
                                // Handle dashboard as the landing page:
                                // Change the following value to your "default" account ID
                                // Remove after demo
                                // $rootScope.$stateParams.accountId = 1;
                                if (!$rootScope.$stateParams.accountId) {
                                    $state.go('app.adminView');
                                } else {
                                    $state.go('app.clientDashboard', {accountId: $rootScope.$stateParams.accountId});
                                }
                            }],
                            ncyBreadcrumb: {
                                label: 'Help'
                            }
                        })
                        /*.state('app.dashboard', {
                            url: '/account/{accountId}',
                            controller: 'propertiesCtrl',
                            templateUrl: STATIC_PREFIX + APP_FOLDER + 'properties/views/properties.html',
                            resolve: {
                                load: angularAMD.load([
                                    STATIC_PREFIX + 'common/services/generalServ.js',
                                    STATIC_PREFIX + 'common/services/uploadServ.js',
                                    STATIC_PREFIX + APP_FOLDER + 'properties/controllers/propertiesCtrl.js',
                                    STATIC_PREFIX + APP_FOLDER + 'properties/controllers/createNewPropertyCtrl.js'
                                ])
                            },
                            ncyBreadcrumb: {
                                label: 'Properties'
                            }
                        })*/
                        .state('app.clientTenantsView.rentRoll.pdf', {
                            url: '/tenants/{tenantId}/documents/{docId}/annotations/{annotationId}',
                            header:{label:"Rent Roll"},
                            resolve: {
                                load: angularAMD.load([
                                    STATIC_PREFIX + 'common/utils/pdf/pdf.worker.js',
                                    STATIC_PREFIX + 'common/utils/pdf/annotations_layer_builder.js',
                                    STATIC_PREFIX + 'common/utils/pdf/text_layer_builder.js',
                                    STATIC_PREFIX + 'common/utils/pdf/ui_utils.js',
                                    STATIC_PREFIX + 'common/utils/pdf/angular-pdf-viewer.js',
                                    STATIC_PREFIX + 'common/utils/pdf/pdf.js',
                                    STATIC_PREFIX + APP_FOLDER + 'pdf/controllers/pdfViewerCtrl.js',
                                    STATIC_PREFIX + APP_FOLDER + 'pdf/controllers/pdfPartialCtrl.js'
                                ])
                            }
                        })
                        .state('app.clientTenantsView.rentRoll.doc', {
                            url: '/tenants/{tenantId}/documents/{docId}',
                            header:{label:"Rent Roll"},
                            views: {
                                'bottomPanel@app.clientTenantsView.rentRoll': {
                                    controller: 'docPartialCtrl',
                                    templateUrl: STATIC_PREFIX + APP_FOLDER + 'doc/views/doc-partial.html'
                                }
                            },
                            resolve: {
                                load: angularAMD.load([
                                    STATIC_PREFIX + APP_FOLDER + 'doc/services/docServ.js',
                                    STATIC_PREFIX + APP_FOLDER + 'doc/controllers/docPartialCtrl.js'
                                ])
                            }
                        })
                        .state('app.clientTenantsView.groundTruth', {
                            url: '/ground-truth',
                            header:{label:"Ground Truth"},
                            views: {
                                'propertyView@app.clientTenantsView': {
                                    controller: 'truthCtrl',
                                    templateUrl: STATIC_PREFIX + APP_FOLDER + 'truth/views/truth.html'
                                }
                            },
                            resolve: {
                                load: angularAMD.load([
                                    STATIC_PREFIX + 'common/directives/update-data-on-field.js',
                                    STATIC_PREFIX + 'common/directives/angular-dropdowns.js',
                                    STATIC_PREFIX + 'common/directives/ngBindHtmlUnsafe.js',
                                    STATIC_PREFIX + APP_FOLDER + 'truth/services/truthServ.js',
                                    STATIC_PREFIX + APP_FOLDER + 'truth/controllers/truthCtrl.js'
                                ])
                            },
                            ncyBreadcrumb: {
                                parent: 'app.clientTenantsView',
                                label: 'Ground Truth'
                            }
                        })
                        .state('app.clientTenantsView.groundTruth.tenants', {
                            url: '/tenants/{tenantId}',
                            header:{label:"Ground Truth"}
                        })
                        .state('app.clientTenantsView.groundTruth.tenants.documents', {
                            url: '/documents/{docId}',
                            header:{label:"Ground Truth"}
                        })
                        .state('app.clientTenantsView.groundTruth.tenants.documents.clauses', {
                            url: '/clauses/{clauseId}',
                            header:{label:"Ground Truth"}
                        })
                        .state('app.clientDashboard', {
                            url: '/account/{accountId}/projects/{projectId}/dashboard',
                            views: {
                                "": {
                                    controller: 'dashboardCtrl',
                                    templateUrl: STATIC_PREFIX + APP_FOLDER + 'dashboard/views/dashboard.html'
                                },
                                'rightPanel@app.clientDashboard': {
                                    controller: 'recentsCtrl',
                                    templateUrl: STATIC_PREFIX + APP_FOLDER + 'dashboard/views/recents.html'
                                }
                            },
                            resolve: {
                                load: angularAMD.load([

                                    STATIC_PREFIX + 'common/directives/resizer.js',
                                    STATIC_PREFIX + APP_FOLDER + 'dashboard/controllers/dashboardCtrl.js',
                                    STATIC_PREFIX + APP_FOLDER + 'dashboard/controllers/recentsCtrl.js'
                                ])
                            },
                            ncyBreadcrumb: {
                                parent: 'app.clientTenantsView',
                                label: 'dashboard'
                            }
                        })
                        .state('app.clientReportView', {
                            url: '/account/{accountId}/projects/{projectId}/reports',
                            views: {
                                "": {
                                    controller: 'reportCtrl',
                                    templateUrl: STATIC_PREFIX + APP_FOLDER + 'report/views/report.html'
                                }
                            },
                            resolve: {
                                load: angularAMD.load([

                                    STATIC_PREFIX + APP_FOLDER + 'report/services/reportServ.js',
                                    STATIC_PREFIX + APP_FOLDER + 'report/controllers/reportCtrl.js'
                                ])
                            },
                            ncyBreadcrumb: {
                                parent: 'app.clientTenantsView',
                                label: 'report'
                            }
                        })
                        .state('app.clientDocumentView', {
                            url: '/account/{accountId}/projects/{projectId}/tenants/{tenantId}/documents/{docId}/document',
                            views: {
                                "": {
                                    controller: 'docPageCtrl',
                                    templateUrl: STATIC_PREFIX + APP_FOLDER + 'doc/views/doc-page.html'
                                },
                                'rightPanel@app.clientDocumentView': {
                                    controller: 'docCtrl',
                                    templateUrl: STATIC_PREFIX + APP_FOLDER + 'doc/views/doc.html'
                                },
                                'leftPanel@app.clientDocumentView': {
                                    controller: 'tocCtrl',
                                    templateUrl: STATIC_PREFIX + APP_FOLDER + 'doc/views/toc.html'
                                }
                            },
                            resolve: {
                                load: angularAMD.load([
                                    STATIC_PREFIX + 'common/directives/resizer.js',
                                    STATIC_PREFIX + 'common/directives/legend-tree.js',
                                    STATIC_PREFIX + 'common/directives/doc-label.js',
                                    STATIC_PREFIX + APP_FOLDER + 'doc/services/tocServ.js',
                                    STATIC_PREFIX + APP_FOLDER + 'doc/controllers/docPageCtrl.js',
                                    STATIC_PREFIX + APP_FOLDER + 'doc/controllers/tocCtrl.js',
                                    STATIC_PREFIX + APP_FOLDER + 'doc/services/docServ.js',
                                    STATIC_PREFIX + APP_FOLDER + 'doc/controllers/docCtrl.js'
                                ])
                            },
                            ncyBreadcrumb: {
                                parent: 'app.clientAbstract',
                                label: '{{docTitle}}'
                            }
                        })
                        .state('app.clientDocumentView.section', {
                            url: '/section/{sectionId}',
                            ncyBreadcrumb: {
                                parent: 'app.clientAbstract',
                                label: '{{docTitle}}'
                            }
                        })
                        .state('app.clientDocumentView.phrase', {
                            url: '/phrase/{phrase}',
                            ncyBreadcrumb: {
                                parent: 'app.clientAbstract',
                                label: '{{docTitle}}'
                            }
                        })
                        .state('app.clientPdfView', {
                            url: '/account/{accountId}/projects/{projectId}/tenants/{tenantId}/documents/{docId}/pdf',
                            views: {
                                "": {
                                    controller: 'pdfCtrl',
                                    templateUrl: STATIC_PREFIX + APP_FOLDER + 'pdf/views/pdf.html'
                                },
                                'leftPanel@app.clientPdfView': {
                                    controller: 'tocCtrl',
                                    templateUrl: STATIC_PREFIX + APP_FOLDER + 'doc/views/toc.html'
                                }
                            },
                            resolve: {
                                load: angularAMD.load([
                                    STATIC_PREFIX + 'common/directives/resizer.js',
                                    STATIC_PREFIX + 'common/directives/legend-tree.js',
                                    STATIC_PREFIX + 'common/utils/pdf/pdf.worker.js',
                                    STATIC_PREFIX + 'common/utils/pdf/annotations_layer_builder.js',
                                    STATIC_PREFIX + 'common/utils/pdf/text_layer_builder.js',
                                    STATIC_PREFIX + 'common/utils/pdf/ui_utils.js',
                                    STATIC_PREFIX + 'common/utils/pdf/angular-pdf-viewer.js',
                                    STATIC_PREFIX + 'common/utils/pdf/pdf.js',
                                    STATIC_PREFIX + APP_FOLDER + 'doc/services/tocServ.js',
                                    STATIC_PREFIX + APP_FOLDER + 'doc/controllers/tocCtrl.js',
                                    STATIC_PREFIX + APP_FOLDER + 'pdf/services/pdfServ.js',
                                    STATIC_PREFIX + APP_FOLDER + 'pdf/controllers/pdfViewerCtrl.js',
                                    STATIC_PREFIX + APP_FOLDER + 'pdf/controllers/pdfCtrl.js'
                                ])
                            },
                            ncyBreadcrumb: {
                                parent: 'app.clientAbstract',
                                label: '{{docTitle}}'
                            }
                        })
                        .state('app.clientPdfView.annotation', {
                            url: '/annotations/{annotationId}',
                            ncyBreadcrumb: {
                                parent: 'app.clientAbstract',
                                label: '{{docTitle}}'
                            }
                        })
                        .state('app.clientPdfView.annotation.preview', {
                            url: '/preview/{pageNumber}',
                            ncyBreadcrumb: {
                                parent: 'app.clientAbstract',
                                label: '{{docTitle}}'
                            }
                        })
                        .state('app.clientPdfView.toc', {
                            url: '/tocItem/{tocItemId}',
                            ncyBreadcrumb: {
                                parent: 'app.clientAbstract',
                                label: '{{docTitle}}'
                            }
                        })
                        .state('app.clientSplitPdfView', {
                            url: '/account/{accountId}/projects/{projectId}/tenants/{tenantId}/documents/{docId}/split',
                            views: {
                                "": {
                                    controller: 'pdfPartialCtrl',
                                    templateUrl: STATIC_PREFIX + APP_FOLDER + 'pdf/views/pdf-split.html'
                                },
                                'leftPanel@app.clientSplitPdfView': {
                                    controller: 'splitCtrl',
                                    templateUrl: STATIC_PREFIX + APP_FOLDER + 'pdf/views/split.html'
                                }
                            },
                            resolve: {
                                load: angularAMD.load([
                                    STATIC_PREFIX + 'common/directives/resizer.js',
                                    STATIC_PREFIX + 'common/utils/pdf/pdf.worker.js',
                                    STATIC_PREFIX + 'common/utils/pdf/annotations_layer_builder.js',
                                    STATIC_PREFIX + 'common/utils/pdf/text_layer_builder.js',
                                    STATIC_PREFIX + 'common/utils/pdf/ui_utils.js',
                                    STATIC_PREFIX + 'common/utils/pdf/angular-pdf-viewer.js',
                                    STATIC_PREFIX + 'common/utils/pdf/pdf.js',
                                    STATIC_PREFIX + APP_FOLDER + 'pdf/services/splitServ.js',
                                    STATIC_PREFIX + APP_FOLDER + 'pdf/services/pdfServ.js',
                                    STATIC_PREFIX + APP_FOLDER + 'pdf/controllers/splitCtrl.js',
                                    STATIC_PREFIX + APP_FOLDER + 'pdf/controllers/pdfViewerCtrl.js',
                                    STATIC_PREFIX + APP_FOLDER + 'pdf/controllers/pdfPartialCtrl.js'
                                ])
                            },
                            ncyBreadcrumb: {
                                parent: 'app.clientAbstract',
                                label: '{{docTitle}}'
                            }
                        })
                        .state('app.clientSplitPdfView.split', {
                            url: '/{splitPage}',
                            ncyBreadcrumb: {
                                parent: 'app.clientAbstract',
                                label: '{{docTitle}}'
                            }
                        })
                        .state('app.clientAbstract', {
                            url: '/account/{accountId}/projects/{projectId}/tenants/{tenantId}',
                            views: {
                                "": {
                                    controller: 'abstractCtrl',
                                    templateUrl: STATIC_PREFIX + APP_FOLDER + 'abstract/views/abstract.html'
                                },
                                'rightPanel@app.clientAbstract': {
                                    controller: 'multiViewCtrl',
                                    templateUrl: STATIC_PREFIX + APP_FOLDER + 'abstract/views/multiView.html'
                                },
                                'pdfPreview@app.clientAbstract': {
                                    controller: 'pdfPartialCtrl',
                                    templateUrl: STATIC_PREFIX + APP_FOLDER + 'pdf/views/pdf-partial.html'
                                },
                                'docPreview@app.clientAbstract': {
                                    controller: 'docPartialCtrl',
                                    templateUrl: STATIC_PREFIX + APP_FOLDER + 'doc/views/doc-partial.html'
                                }

                            },
                            resolve: {
                                load: angularAMD.load([
                                    STATIC_PREFIX + 'common/directives/resizer.js',
                                    STATIC_PREFIX + 'common/directives/ng-single-click.js',
                                    STATIC_PREFIX + APP_FOLDER + 'doc/services/docServ.js',
                                    STATIC_PREFIX + APP_FOLDER + 'doc/controllers/docPartialCtrl.js',
                                    STATIC_PREFIX + APP_FOLDER + 'abstract/services/abstractServ.js',
                                    STATIC_PREFIX + APP_FOLDER + 'abstract/services/multiViewServ.js',
                                    STATIC_PREFIX + APP_FOLDER + 'abstract/controllers/abstractCtrl.js',
                                    STATIC_PREFIX + APP_FOLDER + 'abstract/controllers/multiViewCtrl.js',
                                    STATIC_PREFIX + 'common/utils/pdf/pdf.worker.js',
                                    STATIC_PREFIX + 'common/utils/pdf/annotations_layer_builder.js',
                                    STATIC_PREFIX + 'common/utils/pdf/text_layer_builder.js',
                                    STATIC_PREFIX + 'common/utils/pdf/ui_utils.js',
                                    STATIC_PREFIX + 'common/utils/pdf/angular-pdf-viewer.js',
                                    STATIC_PREFIX + 'common/utils/pdf/pdf.js',
                                    STATIC_PREFIX + APP_FOLDER + 'pdf/services/pdfServ.js',
                                    STATIC_PREFIX + APP_FOLDER + 'pdf/controllers/pdfViewerCtrl.js',
                                    STATIC_PREFIX + APP_FOLDER + 'pdf/controllers/pdfPartialCtrl.js'

                                ])
                            },
                            ncyBreadcrumb: {
                                parent: 'app.property',
                                label: '{{tenantName}}'
                            }
                        })
                        .state('app.clientAbstract.multiView', {
                            url: '/search/{q}',
                            ncyBreadcrumb: {
                                skip: true
                            }/*,
                            resolve: {
                                multiViewLabel: ['$rootScope', function($rootScope) {
                                    var multiViewLabel = $rootScope.multiViewLabel;
                                    $rootScope.multiViewLabel = null;
                                    return multiViewLabel;
                                }]
                            }*/
                        })
                        .state('app.clientAbstract.multiView.pdf', {
                            url: '/pdfs/{docId}/annotations/{annotationId}',
                            ncyBreadcrumb: {
                                parent: 'app.clientAbstract',
                                label: '{{docTitle}}'
                            }
                        })
                        .state('app.clientAbstract.multiView.pdf.preview', {
                            url: '/preview/{pageNumber}',
                            ncyBreadcrumb: {
                                parent: 'app.clientAbstract',
                                label: '{{docTitle}}'
                            }
                        })
                        .state('app.clientAbstract.multiView.doc', {
                            url: '/documents/{docId}/phrase/{phrase}',
                            ncyBreadcrumb: {
                                parent: 'app.clientAbstract',
                                label: '{{docTitle}}'
                            }
                        })
                        .state('app.clientAbstract.doc', {
                            url: '/documents/{docId}/phrase/{phrase}',
                            ncyBreadcrumb: {
                                parent: 'app.clientAbstract',
                                label: '{{docTitle}}'
                            }
                        })
                        .state('app.clientSingleAbstract', {
                                url: '/account/{accountId}/projects/{projectId}/tenants/{tenantId}/documents/{docId}/abstract',
                                views: {
                                    "": {
                                        controller: 'abstractSingleCtrl',
                                        templateUrl: STATIC_PREFIX + APP_FOLDER + 'abstract/views/abstractSingle.html'
                                    },
                                    'rightPanel@app.clientSingleAbstract': {
                                        controller: 'docCtrl',
                                        templateUrl: STATIC_PREFIX + APP_FOLDER + 'doc/views/doc.html'
                                    }
                                },
                            resolve: {
                                load: angularAMD.load([
                                    STATIC_PREFIX + 'common/directives/resizer.js',
                                    STATIC_PREFIX + 'common/directives/ng-single-click.js',
                                    STATIC_PREFIX + 'common/directives/doc-token-selection.js',
                                    STATIC_PREFIX + 'common/directives/ng-number-only.js',
                                    STATIC_PREFIX + APP_FOLDER + 'doc/services/docServ.js',
                                    STATIC_PREFIX + APP_FOLDER + 'doc/controllers/docCtrl.js',
                                    STATIC_PREFIX + APP_FOLDER + 'abstract/controllers/abstractSingleCtrl.js'
                                ])
                            },
                            ncyBreadcrumb: {
                                parent: 'app.clientAbstract',
                                label: '{{docTitle}}'
                            }
                        })
                        .state('app.clientSingleAbstract.editTemplate', {
                                url: '/template',
                                views: {
                                    'rightPanel@app.clientSingleAbstract.editTemplate': {
                                        controller: 'editTemplateCtrl',
                                        templateUrl: STATIC_PREFIX + APP_FOLDER + 'editTemplate/views/editTemplate.html'
                                    }
                                },
                            resolve: {
                                load: angularAMD.load([
                                    STATIC_PREFIX + 'common/directives/resizer.js',
                                    STATIC_PREFIX + 'common/directives/ng-single-click.js',
                                    STATIC_PREFIX + 'common/directives/ng-number-only.js'
                                ])
                            },
                            ncyBreadcrumb: {
                                parent: 'app.clientAbstract',
                                label: '{{docTitle}}'
                            }
                        })
                        .state('app.clientSingleAbstract.editTemplatePhrase', {
                            url: '/phrase',
                            ncyBreadcrumb: {
                                parent: 'app.clientSingleAbstract',
                                label: '{{docTitle}}'
                            }
                        })
                        .state('app.clientSingleAbstract.editTemplateSection', {
                            url: '/section',
                            ncyBreadcrumb: {
                                parent: 'app.clientSingleAbstract',
                                label: '{{docTitle}}'
                            }
                        })
                        .state('app.clientSingleAbstract.section', {
                            url: '/section/{sectionId}',
                            ncyBreadcrumb: {
                                parent: 'app.clientAbstract',
                                label: '{{docTitle}}'
                            }
                        })
                        .state('app.clientSingleAbstract.phrase', {
                            url: '/phrase/{phrase}' ,
                            ncyBreadcrumb: {
                                parent: 'app.clientAbstract',
                                label: '{{docTitle}}'
                            }
                        })
                }else{
                    $stateProvider
                        .state('app.org', {
                            url: '/org/',
                            controller: ['$rootScope', 'GLOBAL_VARS' , '$state', function ($rootScope, GLOBAL_VARS, $state) {
                                     if (!$rootScope.$stateParams.orgId) {
                                    $state.go('app.adminView');
                                } else {
                                    $state.go('app.treeView', {orgId: $rootScope.$stateParams.orgId});
                                }
                                //}
                            }]
                        })
                        .state('app.treeView', {
                            url: '/org/{orgId}/folder',
                            controller: 'treeCtrl',
                            templateUrl: STATIC_PREFIX + APP_FOLDER + 'tree/views/tree.html',
                            resolve: {
                                load: angularAMD.load([
                                        STATIC_PREFIX + APP_FOLDER + 'tree/controllers/templatesAssignCtrl.js',
                                        STATIC_PREFIX + APP_FOLDER + 'partials/controllers/linesheetSelectionCtrl.js',
                                        STATIC_PREFIX + 'common/directives/angular-tree-control.js',
                                        STATIC_PREFIX + 'common/directives/ng-context-menu.js',
                                        STATIC_PREFIX + 'common/directives/ng-enter.js',
                                        STATIC_PREFIX + 'common/directives/ng-drop.js',
                                        STATIC_PREFIX + APP_FOLDER + 'tree/services/treeServ.js',
                                        STATIC_PREFIX + APP_FOLDER + 'tree/controllers/treeCtrl.js',
                                        STATIC_PREFIX + 'common/directives/focusMe.js',
                                        STATIC_PREFIX + APP_FOLDER + 'partials/controllers/searchModalCtrl.js',
                                        STATIC_PREFIX + APP_FOLDER + 'partials/services/advancedSearchServ.js',
                                        STATIC_PREFIX + 'common/directives/datePicker.js'
                                ])
                            }
                        })
                        .state('app.docView', {
                            url: '/org/{orgId}/doc/{doc}',
                            views: {
                                "": {
                                    controller: 'docCtrl',
                                    templateUrl: STATIC_PREFIX + APP_FOLDER + 'doc/views/doc.html'
                                },
                                'rightPanel@app.docView': {
                                    controller: 'rightPanelCtrl',
                                    templateUrl: STATIC_PREFIX + APP_FOLDER + 'partials/views/rightPanel-wide.html'
                                }
                            },
                            resolve: {
                                load: angularAMD.load([

                                        STATIC_PREFIX + 'common/directives/legend-tree.js',
                                        STATIC_PREFIX + 'common/directives/ng-context-menu.js',
                                        STATIC_PREFIX + APP_FOLDER + 'doc/services/docServ.js',
                                        STATIC_PREFIX + APP_FOLDER + 'doc/controllers/classAssignCtrl.js',
                                        STATIC_PREFIX + APP_FOLDER + 'doc/controllers/docCtrl.js',
                                        STATIC_PREFIX + APP_FOLDER + 'partials/controllers/rightPanelCtrl.js'
                                ])
                            }
                        })
                        .state('app.docView.section', {
                            url: '/section/{sectionId}',
                            views: {
                                "": {
                                    templateUrl: STATIC_PREFIX + APP_FOLDER + 'doc/views/doc.html'
                                }
                            }
                        })
                        .state('app.docView.phrase', {
                            url: '/phrase/{phrase}',
                            views: {
                                "": {
                                    templateUrl: STATIC_PREFIX + APP_FOLDER + 'doc/views/doc.html'
                                }
                            }
                        })
                        .state('app.rentRollView', {
                            url: '/org/{orgId}/rentRoll/folder/{folderId}',
                            views: {
                                "": {
                                    controller: 'rentRollCtrl',
                                    templateUrl: STATIC_PREFIX + APP_FOLDER + 'rentRoll/views/rentRoll.html'
                                },
                                'rightPanel@app.rentRollView': {
                                    controller: 'docCtrl',
                                    templateUrl: STATIC_PREFIX + APP_FOLDER + 'doc/views/doc-partial.html'
                                }
                            },
                            resolve: {
                                load: angularAMD.load([

                                        STATIC_PREFIX + 'common/directives/ng-context-menu.js',
                                        STATIC_PREFIX + 'common/directives/ng-enter.js',
                                        STATIC_PREFIX + 'common/directives/ng-focus.js',
                                        STATIC_PREFIX + 'common/directives/ng-blur.js',
                                        STATIC_PREFIX + 'common/directives/ng-right-click.js',
                                        STATIC_PREFIX + APP_FOLDER + 'doc/controllers/classAssignCtrl.js',
                                        STATIC_PREFIX + 'common/directives/resizer.js',
                                        STATIC_PREFIX + APP_FOLDER + 'doc/services/docServ.js',
                                        STATIC_PREFIX + APP_FOLDER + 'rentRoll/controllers/rentRollCtrl.js',
                                        STATIC_PREFIX + APP_FOLDER + 'doc/controllers/docCtrl.js',
                                        STATIC_PREFIX + APP_FOLDER + 'rentRoll/services/rentRollServ.js',
                                        STATIC_PREFIX + 'common/directives/datePicker.js'
                                ])
                            }
                        })
                        .state('app.rentRollView.doc', {
                            url: '/doc/{doc}',
                            views: {
                                "": {
                                    templateUrl: STATIC_PREFIX + APP_FOLDER + 'rentRoll/views/rentRoll.html'
                                }
                            }
                        })
                        .state('app.rentRollView.phrase', {
                            url: '/doc/{doc}/phrase/{phrase}',
                            views: {
                                "": {
                                    templateUrl: STATIC_PREFIX + APP_FOLDER + 'rentRoll/views/rentRoll.html'
                                }
                            }
                        })


                        .state('app.argusView', {
                            url: '/org/{orgId}/argus/folder/{folderId}',
                            views: {
                                "": {
                                    controller: 'argusCtrl',
                                    templateUrl: STATIC_PREFIX + APP_FOLDER + 'argus/views/argus.html'
                                },
                                'rightPanel@app.argusView': {
                                    controller: 'docCtrl',
                                    templateUrl: STATIC_PREFIX + APP_FOLDER + 'doc/views/doc-partial.html'
                                }
                            },
                            resolve: {
                                load: angularAMD.load([

                                        STATIC_PREFIX + 'common/directives/ng-context-menu.js',
                                        STATIC_PREFIX + 'common/directives/ng-enter.js',
                                        STATIC_PREFIX + 'common/directives/ng-focus.js',
                                        STATIC_PREFIX + 'common/directives/ng-blur.js',
                                        STATIC_PREFIX + 'common/directives/ng-right-click.js',
                                        STATIC_PREFIX + APP_FOLDER + 'doc/controllers/classAssignCtrl.js',
                                        STATIC_PREFIX + 'common/directives/resizer.js',
                                        STATIC_PREFIX + APP_FOLDER + 'doc/services/docServ.js',
                                        STATIC_PREFIX + APP_FOLDER + 'argus/controllers/argusCtrl.js',
                                        STATIC_PREFIX + APP_FOLDER + 'doc/controllers/docCtrl.js',
                                        STATIC_PREFIX + APP_FOLDER + 'argus/services/argusServ.js',
                                        STATIC_PREFIX + 'common/directives/datePicker.js'
                                ])
                            }
                        })
                        .state('app.argusView.doc', {
                            url: '/doc/{doc}',
                            views: {
                                "": {
                                    templateUrl: STATIC_PREFIX + APP_FOLDER + 'argus/views/argus.html'
                                }
                            }
                        })
                        .state('app.argusView.phrase', {
                            url: '/doc/{doc}/phrase/{phrase}',
                            views: {
                                "": {
                                    templateUrl: STATIC_PREFIX + APP_FOLDER + 'argus/views/argus.html'
                                }
                            }
                        })


                        .state('app.termsheetView', {
                            url: '/org/{orgId}/termsheet/doc/{doc}?forceView',
                            views: {
                                "": {
                                    controller: 'termsheetCtrl',
                                    templateUrl: STATIC_PREFIX + APP_FOLDER + 'termsheet/views/termsheet.html'
                                },
                                'rightPanel@app.termsheetView': {
                                    controller: 'docCtrl',
                                    templateUrl: STATIC_PREFIX + APP_FOLDER + 'doc/views/doc-partial.html'
                                }
                            },
                            resolve: {
                                load: angularAMD.load([

                                        STATIC_PREFIX + 'common/directives/ng-context-menu.js',
                                        STATIC_PREFIX + 'common/directives/ng-enter.js',
                                        STATIC_PREFIX + 'common/directives/ng-focus.js',
                                        STATIC_PREFIX + 'common/directives/doc-token-selection.js',
                                        STATIC_PREFIX + APP_FOLDER + 'doc/controllers/classAssignCtrl.js',
                                        STATIC_PREFIX + 'common/directives/resizer.js',
                                        STATIC_PREFIX + APP_FOLDER + 'doc/services/docServ.js',
                                        STATIC_PREFIX + APP_FOLDER + 'termsheet/controllers/termsheetCtrl.js',
                                        STATIC_PREFIX + APP_FOLDER + 'doc/controllers/docCtrl.js',
                                        STATIC_PREFIX + APP_FOLDER + 'termsheet/services/termsheetServ.js'
                                ])
                            }
                        })
                        .state('app.termsheetView.phrase', {
                            url: '/phrase/{phrase}',
                            views: {
                                "": {
                                    templateUrl: STATIC_PREFIX + APP_FOLDER + 'termsheet/views/termsheet.html'
                                }
                            }
                        })
                        .state('app.linesheetView', {
                            url: '/org/{orgId}/linesheet/{nodeType}/{nodeId}',
                            views: {
                                "": {
                                    controller: 'linesheetCtrl',
                                    templateUrl: STATIC_PREFIX + APP_FOLDER + 'termsheet/views/linesheet.html'
                                },
                                'rightPanel@app.linesheetView': {
                                    controller: 'docCtrl',
                                    templateUrl: STATIC_PREFIX + APP_FOLDER + 'doc/views/doc-partial.html'
                                }
                            },
                            resolve: {
                                load: angularAMD.load([

                                        STATIC_PREFIX + 'common/directives/ng-context-menu.js',
                                        STATIC_PREFIX + APP_FOLDER + 'doc/controllers/classAssignCtrl.js',
                                        STATIC_PREFIX + 'common/directives/resizer.js',
                                        STATIC_PREFIX + APP_FOLDER + 'doc/services/docServ.js',
                                        STATIC_PREFIX + APP_FOLDER + 'termsheet/controllers/linesheetCtrl.js',
                                        STATIC_PREFIX + APP_FOLDER + 'doc/controllers/docCtrl.js'
                                ])
                            }
                        })
                        .state('app.linesheetView.phrase', {
                            url: '/doc/{doc}/phrase/{phrase}',
                            views: {
                                "": {
                                    templateUrl: STATIC_PREFIX + APP_FOLDER + 'termsheet/views/linesheet.html'
                                }
                            }
                        })
                        .state('app.linesheetView.docPhrase', {
                            url: '/phrase/{phrase}',
                            views: {
                                "": {
                                    templateUrl: STATIC_PREFIX + APP_FOLDER + 'termsheet/views/linesheet.html'
                                }
                            }
                        })
                        .state('app.linesheetView.section', {
                            url: '/doc/{doc}/section/{sectionId}',
                            views: {
                                "": {
                                    templateUrl: STATIC_PREFIX + APP_FOLDER + 'termsheet/views/linesheet.html'
                                }
                            }
                        })
                        .state('app.linesheetView.docSection', {
                            url: '/section/{sectionId}',
                            views: {
                                "": {
                                    templateUrl: STATIC_PREFIX + APP_FOLDER + 'termsheet/views/linesheet.html'
                                }
                            }
                        })
                        .state('app.reportView', {
                            url: '/org/{orgId}/report',
                            views: {
                                "": {
                                    controller: 'reportCtrl',
                                    templateUrl: STATIC_PREFIX + APP_FOLDER + 'report/views/report.html'
                                }
                            },
                            resolve: {
                                load: angularAMD.load([

                                        STATIC_PREFIX + APP_FOLDER + 'report/services/reportServ.js',
                                        STATIC_PREFIX + APP_FOLDER + 'report/controllers/reportCtrl.js',
                                        STATIC_PREFIX + 'common/directives/calendar.js',
                                        STATIC_PREFIX + 'common/directives/indexer.js'
                                ])
                            }
                        })
                        .state('app.calendarView', {
                            url: '/org/:orgId/calendar/{date}',
                            views: {
                                "": {
                                    controller: 'calendarCtrl',
                                    templateUrl: STATIC_PREFIX + APP_FOLDER + 'calendar/views/calendar.html'
                                }
                            },
                            resolve: {
                                load: angularAMD.load([
                                        STATIC_PREFIX + APP_FOLDER + 'calendar/services/calendarServ.js',
                                        STATIC_PREFIX + APP_FOLDER + 'calendar/controllers/calendarCtrl.js',
                                        STATIC_PREFIX + 'common/directives/calendar.js'
                                ])
                            }
                        })
                        .state('app.rulesetView', {
                            url: '/org/:orgId/ruleset',
                            views: {
                                "": {
                                    controller: 'calendarCtrl',
                                    templateUrl: STATIC_PREFIX + APP_FOLDER + 'ruleset/views/ruleset.html'
                                }
                            },
                            resolve: {
                                load: angularAMD.load([
                                ])
                            }
                        })

                        .state('app.searchView', {
                            url: '/org/{orgId}/search/{nodeType}/{nodeId}?page',
                            views: {
                                "": {
                                    controller: 'searchCtrl',
                                    templateUrl: STATIC_PREFIX + APP_FOLDER + 'search/views/search.html'
                                },
                                'rightPanel@app.searchView': {
                                    controller: 'docCtrl',
                                    templateUrl: STATIC_PREFIX + APP_FOLDER + 'doc/views/doc-partial.html'
                                }
                            },
                            resolve: {
                                load: angularAMD.load([
                                        STATIC_PREFIX + 'common/utils/hilitor.js',
                                        STATIC_PREFIX + 'common/directives/focusMe.js',
                                        STATIC_PREFIX + 'common/directives/ng-enter.js',
                                        STATIC_PREFIX + APP_FOLDER + 'search/controllers/searchCtrl.js',
                                        STATIC_PREFIX + 'common/directives/ngBindHtmlUnsafe.js',
                                        STATIC_PREFIX + 'common/directives/ng-context-menu.js',
                                        STATIC_PREFIX + APP_FOLDER + 'doc/controllers/classAssignCtrl.js',
                                        STATIC_PREFIX + 'common/directives/doc-token-selection.js',
                                        STATIC_PREFIX + 'common/directives/doc-label-selection.js',
                                        STATIC_PREFIX + 'common/directives/resizer.js',
                                        STATIC_PREFIX + APP_FOLDER + 'doc/services/docServ.js',
                                        STATIC_PREFIX + APP_FOLDER + 'doc/controllers/docCtrl.js'
                                ])
                            }
                        })
                        .state('app.searchView.section', {
                            url: '/doc/{doc}/section/{sectionId}',
                            views: {
                                "": {
                                    templateUrl: STATIC_PREFIX + APP_FOLDER + 'search/views/search.html'
                                }
                            }
                        })
                        .state('app.searchView.phrase', {
                            url: '/doc/{doc}/phrase/{phrase}',
                            views: {
                                "": {
                                    templateUrl: STATIC_PREFIX + APP_FOLDER + 'search/views/search.html'
                                }
                            }
                        })
                        .state('app.searchView.docSection', { // in case the search was on a document
                            url: '/section/{sectionId}',
                            views: {
                                "": {
                                    templateUrl: STATIC_PREFIX + APP_FOLDER + 'search/views/search.html'
                                }
                            }
                        })
                        .state('app.searchView.docPhrase', { // in case the search was on a document
                            url: '/phrase/{phrase}',
                            views: {
                                "": {
                                    templateUrl: STATIC_PREFIX + APP_FOLDER + 'search/views/search.html'
                                }
                            }
                        })
                        .state('app.predictView', {
                            url: '/org/{orgId}/predict/{nodeType}/{nodeId}',
                            views: {
                                "": {
                                    controller: 'predictCtrl',
                                    templateUrl: STATIC_PREFIX + APP_FOLDER + 'trainPredict/views/trainPredict.html'
                                },
                                'rightPanel@app.predictView': {
                                    controller: 'docCtrl',
                                    templateUrl: STATIC_PREFIX + APP_FOLDER + 'doc/views/doc-partial.html'
                                }
                            },
                            resolve: {
                                load: angularAMD.load([
                                        STATIC_PREFIX + APP_FOLDER + 'trainPredict/services/trainPredictServ.js',
                                        STATIC_PREFIX + APP_FOLDER + 'trainPredict/controllers/predictCtrl.js',

                                        STATIC_PREFIX + 'common/directives/ngBindHtmlUnsafe.js',
                                        STATIC_PREFIX + 'common/directives/ng-context-menu.js',
                                        STATIC_PREFIX + APP_FOLDER + 'doc/controllers/classAssignCtrl.js',
                                        STATIC_PREFIX + 'common/directives/resizer.js',
                                        STATIC_PREFIX + APP_FOLDER + 'doc/services/docServ.js',
                                        STATIC_PREFIX + APP_FOLDER + 'doc/controllers/docCtrl.js'
                                ])
                            }
                        })
                        .state('app.predictView.section', {
                            url: '/doc/{doc}/section/{sectionId}',
                            views: {
                                "": {
                                    templateUrl: STATIC_PREFIX + APP_FOLDER + 'search/views/search.html'
                                }
                            }
                        })
                        .state('app.predictView.docSection', { // in case the search was on a document
                            url: '/section/{sectionId}',
                            views: {
                                "": {
                                    templateUrl: STATIC_PREFIX + APP_FOLDER + 'trainPredict/views/trainPredict.html'
                                }
                            }
                        })
                        .state('app.trainView', {
                            url: '/org/{orgId}/train/{nodeType}/{nodeId}',
                            views: {
                                "": {
                                    controller: 'trainCtrl',
                                    templateUrl: STATIC_PREFIX + APP_FOLDER + 'trainPredict/views/trainPredict.html'
                                },
                                'rightPanel@app.trainView': {
                                    controller: 'docCtrl',
                                    templateUrl: STATIC_PREFIX + APP_FOLDER + 'doc/views/doc-partial.html'
                                }
                            },
                            resolve: {
                                load: angularAMD.load([
                                        STATIC_PREFIX + APP_FOLDER + 'trainPredict/services/trainPredictServ.js',
                                        STATIC_PREFIX + APP_FOLDER + 'trainPredict/controllers/trainCtrl.js',

                                        STATIC_PREFIX + 'common/directives/ngBindHtmlUnsafe.js',
                                        STATIC_PREFIX + 'common/directives/ng-context-menu.js',
                                        STATIC_PREFIX + APP_FOLDER + 'doc/controllers/classAssignCtrl.js',
                                        STATIC_PREFIX + 'common/directives/resizer.js',
                                        STATIC_PREFIX + 'common/directives/scroll-sticky-header.js',
                                        STATIC_PREFIX + APP_FOLDER + 'doc/services/docServ.js',
                                        STATIC_PREFIX + APP_FOLDER + 'doc/controllers/docCtrl.js'
                                ])
                            }
                        })
                        .state('app.trainView.section', {
                            url: '/doc/{doc}/section/{sectionId}',
                            views: {
                                "": {
                                    templateUrl: STATIC_PREFIX + APP_FOLDER + 'search/views/search.html'
                                }
                            }
                        })
                        .state('app.trainView.docSection', { // in case the search was on a document
                            url: '/section/{sectionId}',
                            views: {
                                "": {
                                    templateUrl: STATIC_PREFIX + APP_FOLDER + 'trainPredict/views/trainPredict.html'
                                }
                            }
                        })
                }
            }])

        .run([ '$rootScope', '$state', '$stateParams', '$templateCache', '$http', '$cookies',
            function ($rootScope, $state, $stateParams, $templateCache, $http, $cookies) {
                $rootScope.$state = $state;
                $rootScope.$stateParams = $stateParams;

                // set the CSRF token here
                $http.defaults.headers.post['X-CSRFToken'] = $cookies['csrftoken'];

                $templateCache.put('/dialogs/process.html',
                    '<div class="modal">'+
                        '<div class="modal-dialog">'+
                        '<div class="modal-content">'+
                        '<div class="modal-header">'+
                        '<a class="icon-close Flex__rev pointer" style="margin-top:10px;" ng-click="$modalCancel()"></a>'+
                        '<h4 class="modal-title">Processing...</h4></div>'+
                        '<div class="modal-body">'+
                        '<div>{{msgText}}</div></div>' +
                        '<div class="modal-footer">' +
                        '<button type="button" class="Btn Btn__action" ng-click="$modalCancel()">OK</button>' +
                        '</div></div></div></div>'
                );
                $templateCache.put('ngDropdowns/templates/dropdownSelect.html', [
                    '<div class="wrap-dd-select">',
                    '<span class="selected">{{dropdownModel[labelField]}}</span>',
                    '<ul class="dropdown">',
                    '<li ng-repeat="item in dropdownSelect"',
                    ' class="dropdown-item"',
                    ' dropdown-model="dropdownModel"',
                    ' dropdown-select-item="item"',
                    ' dropdown-item-label="labelField">',
                    '</li>',
                    '</ul>',
                    '</div>'
                ].join(''));

                $templateCache.put('ngDropdowns/templates/dropdownSelectItem.html', [
                    '<li ng-class="{divider: (dropdownSelectItem.divider && !dropdownSelectItem[dropdownItemLabel]), \'divider-label\': (dropdownSelectItem.divider && dropdownSelectItem[dropdownItemLabel]), \'is-selected\': (dropdownModel[dropdownItemLabel] == dropdownSelectItem[dropdownItemLabel])}">',
                    '<a href="" class="dropdown-item"',
                    ' ng-if="!dropdownSelectItem.divider"',
                    ' ng-href="{{dropdownSelectItem.href}}"',
                    ' ng-click="selectItem()">',
                    '{{dropdownSelectItem[dropdownItemLabel]}}',
                    '</a>',
                    '<span ng-if="dropdownSelectItem.divider">',
                    '{{dropdownSelectItem[dropdownItemLabel]}}',
                    '</span>',
                    '</li>'
                ].join(''));

                $templateCache.put('ngDropdowns/templates/dropdownMenu.html', [
                    '<ul class="dropdown">',
                    '<li ng-repeat="item in dropdownMenu"',
                    ' class="dropdown-item"',
                    ' dropdown-item-label="labelField"',
                    ' dropdown-menu-item="item">',
                    '</li>',
                    '</ul>'
                ].join(''));

                $templateCache.put('ngDropdowns/templates/dropdownMenuItem.html', [
                    '<li ng-class="{divider: dropdownMenuItem.divider, \'divider-label\': dropdownMenuItem.divider && dropdownMenuItem[dropdownItemLabel]}">',
                    '<a href="" class="dropdown-item"',
                    ' ng-if="!dropdownMenuItem.divider"',
                    ' ng-href="{{dropdownMenuItem.href}}"',
                    ' ng-click="selectItem()">',
                    '{{dropdownMenuItem[dropdownItemLabel]}}',
                    '</a>',
                    '<span ng-if="dropdownMenuItem.divider">',
                    '{{dropdownMenuItem[dropdownItemLabel]}}',
                    '</span>',
                    '</li>'
                ].join(''));


            }
        ]);



    return app;

});