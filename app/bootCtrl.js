define(function (require) {
    'use strict';

    var app = require('appModule');

    app.controller('bootCtrl',
        [            '$scope', '$state', '$stateParams', 'GLOBAL_VARS', '$dialog', 'httpRequestTracker', 'i18nNotifications', '$rootScope', '$cookies',
            function ($scope,   $state,   $stateParams,   GLOBAL_VARS,   $dialog,   httpRequestTracker,   i18nNotifications, $rootScope, $cookies) {

                $scope.notifications = i18nNotifications;

                $scope.removeNotification = function (notification) {
                    i18nNotifications.remove(notification);
                };
                $scope.removeAllNotifications = function () {
                    i18nNotifications.removeAll();
                };


            }]
    );
});