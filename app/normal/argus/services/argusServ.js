define(function (require) {
    'use strict';

    var app = require('appModule');

    app.register.service('argusServ', ['$http', '$timeout','$q', 'DEALSUMM_CONFIG', 'GLOBAL_VARS', '$cookies', '$rootScope',
        function ($http, $timeout, $q, DEALSUMM_CONFIG, GLOBAL_VARS, $cookies, $rootScope) {

            var returnObject = {}, promise;
            var basePath = DEALSUMM_CONFIG.REST_PATH;// + GLOBAL_VARS.orgId;

            returnObject.getRentRoll = function (folder_id) {
                var data = {};
                data['folder_id'] = folder_id;
                var url = basePath + $rootScope.$stateParams.orgId + '/argus/';
                console.log("getRentRoll",data, url);
                return $http.post(url, data);
            };
            returnObject.editRRCell = function (cell, new_val) {
                var data = cell;

                console.log("new_val",new_val);
                data['val'] = new_val;
                var url = basePath + $rootScope.$stateParams.orgId + '/update_rentroll_cell/';
                console.log("editRRCell",data, url);
                return $http.post(url, data);
            };
            returnObject.updatePeriod = function (start_year, no_of_years) {
                var data = {};
                data['start_year'] = start_year;
                data['no_of_years'] = no_of_years;
                var url = basePath + $rootScope.$stateParams.orgId + '/update_xls_period/';
                console.log("updatePeriod",data, url);
                return $http.post(url, data);
            };
            returnObject.downloadRentRoll = function (info) {
                var data = {};
                data['info'] = info;
                var url = basePath + $rootScope.$stateParams.orgId + '/download_rentRoll/';
                console.log("downloadRentRoll",data, url);
                return $http.post(url, data);
            };

            return returnObject;

        }]);


});



