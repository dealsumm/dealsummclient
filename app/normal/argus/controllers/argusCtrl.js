define(function (require) {
    'use strict';
    var app = require('appModule');

    app.register.controller('argusCtrl',
                ['$scope', 'docServ', 'argusServ', '$state', '$stateParams', 'GLOBAL_VARS', 'filterFilter', '$cookies', '$dialog', '$rootScope', '$timeout',
        function ($scope,   docServ,   argusServ,   $state,   $stateParams,   GLOBAL_VARS,   filterFilter,   $cookies,   $dialog,   $rootScope,   $timeout) {

            $scope.docUrl = "";

            if(!$stateParams.folderId){
		        console.log("FID",folderId);
                $state.go('app.argusView', {folderId:694}); // MAGIC folder_id, to be connected later
                return;
            }
            var stateParams = $rootScope.$stateParams;
            //$scope.hideDocColorsOnInit = true;
            var folderId = $stateParams.folderId;
            var hide_cols;

            var temp_isDataReal = false;
            argusServ.getRentRoll(folderId).then(function (resp) {
                console.log("getRentRoll folder:",folderId, resp);
                temp_isDataReal = true;
                //var item = resp.data;
                $scope.tableData = resp.data;
                $scope.info = $scope.tableData.info;
                $scope.start_year = $scope.start_year_copy = $scope.info.first_xls_year;
                $scope.end_year = $scope.end_year_copy = $scope.info.first_xls_year + $scope.info.no_years;
                hide_cols = $scope.tableData.info.hide_cols;
                $scope.initAfterLoaded();
            })
            $scope.rightClickedOnRRCell = function(cell, doc_id){
                console.log("cell",cell);
                if(cell.ts_line_id && cell.ts_cell_id){
                    localStorage.setItem("rentRollCellExplore", JSON.stringify({line_id:cell.ts_line_id, cell_id:cell.ts_cell_id}));
                    $state.go('app.termsheetView.phrase', {doc:doc_id, phrase:cell.phrase});
                }
            }
            $scope.currentEditedCell = null;
            $scope.clickedCell = null;
            $scope.clickedOnRRCell = function(cell, evt, row){
                //console.log("row",row);
                var docId = row.info.doc_id;

                $scope.clickedCell = cell;
                if($scope.docId == docId){
                    // no need to load new document
                    $scope.markPhraseOnDoc(cell, row);
                }else{
                    $scope.currentDocName = row.info.doc_name;
                    $scope.docId = docId;
                    $scope.docScope.token_spans = null;  // reset the selected SPANs array
                    $scope.docScope.docUrl = ""; // to empty the doc container
                    docServ.getDocDetails($scope.docId, folderId, $scope.docScope);
                }
                if(!$scope.RRcontentLocked){

                    //console.log("evt",evt, evt.target);
                    // edit content mode > also use functionality
                    var pos = $(evt.target).position();

                    //console.log("edit-cell", pos.left, pos.top, cell.id);

                    $scope.currentEditedCell = cell;
                    //console.log("edit-cell", $("#edit-cell").outerWidth());
                    $scope.cellNewValue = cell.val;

                    $("#edit-cell").css({"left":pos.left,"top":pos.top+105}).show(); // 105 is padding top of wrapping element
                    $("#edit-cell-name").focus();
                }
            }
            $scope.resetYears = function(elt){
                if($scope.start_year.length<4 || isNaN($scope.start_year)){
                    $scope.start_year = $("#start_year").data("original");
                }
                if($scope.end_year.length<4 || isNaN($scope.end_year)){
                    $scope.end_year = $("#end_year").data("original");
                }
                if($scope.start_year>$scope.end_year){
                    $scope[elt] = $("#"+elt).data("original");
                }
            }
            $scope.waitBeforeUpdate = false;
            $scope.updateYears = function(elt){
                if($scope.waitBeforeUpdate){
                    return;
                }
                var valid = true;
                if($scope.start_year.length<4 || isNaN($scope.start_year)){
                    $scope.start_year = $("#start_year").data("original");
                    valid = false;
                }
                if(($scope.end_year.length<4 || isNaN($scope.end_year)) && valid){
                    $scope.end_year = $("#end_year").data("original");
                    valid = false;
                }
                if(($scope.start_year>$scope.end_year) && valid){
                    $scope[elt] = $("#"+elt).data("original");
                    valid = false;
                }
                if(valid){
                    $scope.waitBeforeUpdate = true;
                    $("#"+elt).data("original", $scope[elt]);
                    var no_of_years = (1*$scope.end_year) - (1*$scope.start_year);
                    $timeout(function(){
                        $("#"+elt).blur();
                        $scope.waitBeforeUpdate = false;
                    }, 100)
                    argusServ.updatePeriod($scope.start_year, no_of_years).then(function(resp){
                        console.log("updatePeriod",resp);
                    })
                }
            }
            /*$scope.commencementDate = new Date();
            $scope.dateCellId = null;
            $(".quickdate > a").on("click", function(){

            })
            $scope.handleDatePicker = function(evt, cell){
                if(cell.type=="date"){
                    $scope.dateCellId = cell.id;
                    $scope.commencementDate = new Date(cell.val);
                    //console.log("evt",evt.target);
                    var pos = $(evt.target).position();
                    $("#rentroll-date-picker").css({"left":pos.left+24,"top":pos.top+107}).show(); // 105 is padding top of wrapping element
                }
            }
            $scope.hideDatePicker = function(cell){
                if(cell.type=="date"){
                    $timeout(function(){
                        if(!$scope.overDatePicker && cell.id==$scope.dateCellId){
                            $("#rentroll-date-picker").hide();
                            $scope.dateCellId = null;
                        }
                    },0)
                }
            }*/
            $scope.downloadXls = function(){
                argusServ.downloadRentRoll($scope.info).then(function(resp){
                    console.log("downloadRentRoll",resp);
                })
            }
            $scope.cancelEditCellName = function(){
                $scope.currentEditedCell = null;
                $("#edit-cell").hide();
            }
            $scope.saveEditCellName = function(){
                var cell = $scope.currentEditedCell;
                argusServ.editRRCell(cell, $scope.cellNewValue).then(function(resp){
                    $scope.currentEditedCell.val = $scope.cellNewValue;
                    $scope.currentEditedCell = null;
                    // todo - run on results array to place data on right cells
                })
                $("#edit-cell").hide();
            }
            $scope.docLoadedFunc = null;
            $scope.initAfterLoaded = function(){
                $scope.docScope = angular.element($("#doc_container")).scope();
                $scope.docScope.hideDocNavigationButtons = true;
                //console.log("$scope.docScope.docLoaded",$scope.docScope.docLoaded);
                if(!$scope.docLoadedFunc){
                    // extend the docLoad function
                    // do this only one time
                    $scope.docLoadedFunc = $scope.docScope.docLoaded;
                    $scope.docScope.docLoaded = function(){
                        $scope.docLoadedFunc.apply(this, arguments);
                        $scope.markPhraseOnDoc();
                    }
                }
                $scope.loadOnFirstResult();
            }
            $scope.loadOnFirstResult = function(){

                //console.log("stateParams.doc", stateParams,stateParams.doc);
                if($scope.tableData && $scope.tableData.rows.length>0){
                    //var firstDoc = $scope.tableData.rows[0].info.doc_id;
                    var firstDoc = stateParams.doc?stateParams.doc:$scope.tableData.info.first_doc_id;
                    $scope.currentDocName = $scope.tableData.info.first_doc_name;
                    console.log("firstDoc",firstDoc, $scope.currentDocName);
                    if(firstDoc){
                        $scope.docId = firstDoc;
                        $scope.docScope.doc = $scope.docId;
                        $scope.docScope.docUrl = ""; // to empty the doc container
                        //console.log("$scope.tableData[0].head.doc",$scope.tableData[0].head.doc);
                        //console.log("docScope",docScope);
                        if(stateParams.phrase){
                            $state.go('app.argusView.phrase', {doc:$scope.docId, phrase:stateParams.phrase});
                        }else{
                            $state.go('app.argusView.doc', {doc:$scope.docId});
                        }

                        docServ.getDocDetails($scope.docId, $scope.nodeId, $scope.docScope);
                    }
                }else{
                    console.log("no parsing - no data found in result");
                }
            }
            $scope.markPhraseOnDoc = function(){
                //var folderId = $stateParams.phrase;
                var phrase = $scope.clickedCell?$scope.clickedCell.phrase:stateParams.phrase?stateParams.phrase:null;

                if(phrase && phrase.indexOf("-1")!=0){
                    console.log("clickedCell", phrase, phrase.indexOf("-1"));
                    $state.go('app.argusView.phrase', {phrase:phrase, doc:$scope.docId});
                }else{
                    // todo here...

                }
            }
            $scope.isHidable = function(idx){
                return $.inArray( idx, hide_cols )>-1;
            }
	    // UZ 072614 download URL
            $scope.xls_path = STATIC_PREFIX+'/downloads/test.csv'
	    // end UZ 072614 download URL

            // temp s/b removed after real data is working:
            // simulate time of loading doc...
            /*$timeout(function(){
                if(!temp_isDataReal) {
                    $scope.info = $scope.tableData.info;
                    hide_cols = $scope.tableData.info.hide_cols;
                    $scope.initAfterLoaded();
                }
            }, 1000)*/
        }
    ]);
});
