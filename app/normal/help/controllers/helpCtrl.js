/**
 * Created by Itay Amar on 01/07/2015.
 */
define(function (require) {
    'use strict';
    var app = require('appModule');

    app.register.controller('helpCtrl',['$scope','DEALSUMM_CONFIG',
        function ($scope,DEALSUMM_CONFIG) {
            $scope.IMAGES_PREFIX = DEALSUMM_CONFIG.BASE_PATH_IMAGES;
        }
    ]);
});