define(function (require) {
    'use strict';

    var app = require('appModule');

    app.register.service('trainPredictServ',
                ['$http', '$timeout','$q', 'DEALSUMM_CONFIG', 'GLOBAL_VARS', '$cookies', '$rootScope', 'localStorageService',
        function ($http,     $timeout,  $q,   DEALSUMM_CONFIG,   GLOBAL_VARS,   $cookies,   $rootScope, localStorageService) {

            var returnObject = {}, promise;
            var basePath = DEALSUMM_CONFIG.REST_PATH;// + GLOBAL_VARS.orgId;

            returnObject.runTrainPredict = function (node_id, rel, type, forceRefresh) {
                var data = {};
                data['id'] = node_id;
                data['rel'] = rel;
                data['reCalc'] = forceRefresh;
                data[type] = 'true'; // pass on the command

                var url = basePath + $rootScope.$stateParams.orgId + '/' + rel + '/' + node_id + '/';
                promise = $http({ method: 'POST', url: url, data: angular.toJson(data), bypassErrorsInterceptor:400}).then(function (response) {
                    return response;
                });


                //promise = $http.post(url, data);

                return promise;
            };

            return returnObject;

        }]);


});



