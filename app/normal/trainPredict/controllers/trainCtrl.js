define(function (require) {
    'use strict';
    var app = require('appModule');

    app.register.controller('trainCtrl',
                ['$scope', '$state', 'trainPredictServ', 'docServ', '$stateParams', 'GLOBAL_VARS', 'filterFilter', '$fileUploader', '$cookies', '$dialog', '$rootScope', '$timeout', 'localStorageService',
        function ($scope,   $state,  trainPredictServ,   docServ, $stateParams,   GLOBAL_VARS,   filterFilter,   $fileUploader,   $cookies,   $dialog, $rootScope, $timeout, localStorageService) {
            var stateParams = $rootScope.$stateParams;

            $scope.docLoadedFunc = null;
            $scope.init = function() {
                $scope.hideDocColorsOnInit = false;
                $scope.sectionSpecialPulseAnimation = true;
                $scope.nodeId = stateParams.nodeId;
                $scope.nodeType = stateParams.nodeType;
                $scope.docId = stateParams.doc;
                $scope.section = stateParams.sectionId;
                $scope.relevantView = "app.trainView.section";
                if ($scope.nodeType == "doc") {
                    $scope.relevantView = "app.trainView.docSection";
                }
                $scope.viewName = "train";
                $scope.viewLogicName = "train";
                localStorageService.set("trainCtrl", {nodeId:$scope.nodeId, nodeType:$scope.nodeType});
                $scope.runTrainPredict(false);
            }
            $scope.runTrainPredict = function(forceRefresh){
                trainPredictServ.runTrainPredict($scope.nodeId, $scope.nodeType, 'train', forceRefresh).then(function (resp) {
                    console.log("trainJSON", resp, " reCalc:", forceRefresh);
                    $scope.initAfterLoaded(resp.data);
                })
            }
            $scope.reloadFromServer = function(){
                $scope.tableData = null;
                $scope.runTrainPredict(true);
            }
            $scope.clickedOnLine = function(section, docId){
                $scope.section = section;
                if($scope.docId == docId){
                    // no need to load new document
                    $scope.markSectionOnDoc()
                }else{
                    $scope.docId = docId;
                    //console.log("$scope.docUrl...");
                    $scope.docScope.docUrl = "";
                    $scope.docScope.getDocDetails($scope.docId);
                }
                //$state.go($scope.relevantView, {doc:$scope.docId, sectionId:$scope.section});
                //console.log("section",$scope.section, doc);
            }
            $scope.markSectionOnDoc = function(){
                $state.go($scope.relevantView, {doc:$scope.docId, sectionId:$scope.section});
            }
            $scope.setFocusOnFirstResult = function(){
                //console.log("$scope.setFocusOnFirstResult");
                if($scope.tableData && $scope.tableData.length>0){
                    var firstDoc = $scope.tableData[0];
                    $scope.docId = stateParams.doc || firstDoc.doc;
                    //console.log("firstDoc.doc",stateParams.doc,firstDoc.doc);
                    if(firstDoc.items) {
                        $scope.section = stateParams.sectionId || firstDoc.items[0].pid;
                        $scope.docScope.doc = $scope.docId;
                        //console.log("$scope.tableData[0].head.doc",$scope.tableData[0].head.doc);
                        //console.log("docScope",docScope);
                        $scope.docScope.getDocDetails($scope.docId);
                    }
                }
            };

            $scope.initAfterLoaded = function(data){
                $scope.selectedClause = 'all';
                $scope.tableData = data.list;
                $scope.objName = data.obj_name;
                $scope.objType = data.obj_type;
                $scope.docClasses = $scope.analyseClassesColors($scope.tableData);
                $scope.selectedClassBackgroundColor = $scope.setClassesBackgroundColor($scope.docClasses, $scope.backgroundColorsArray, '.trainPredict-table-wrap');
                $scope.docScope = angular.element($("#doc_container")).scope();
                $scope.docScope.showDocName = true;
                console.log("$scope.tableData",$scope.tableData);
                $scope.clausesFound = 0;
                var clausesNamesFound = [{value:'all', label:'All'}];
                var tempClausesNamesFound = [];
                angular.forEach($scope.tableData, function(doc){
                    angular.forEach(doc.items, function(docData){
                        $scope.clausesFound++;
                        if(tempClausesNamesFound.indexOf(docData.class_name)==-1){
                            tempClausesNamesFound.push(docData.class_name);
                            clausesNamesFound.push({value:docData.class_name, label:docData.class_name});
                        }
                    })
                })
                $scope.clausesNamesFound = clausesNamesFound;//.join(", ");

                //console.log("$scope.docScope.docLoaded",$scope.docScope.docLoaded);
                if(!$scope.docLoadedFunc){
                    // extend the docLoad function
                    // do this only one time
                    $scope.docLoadedFunc = $scope.docScope.docLoaded;
                    $scope.docScope.docLoaded = function(){
                        $scope.docLoadedFunc.apply(this, arguments);
                        $scope.markSectionOnDoc();
                    }
                }
                $scope.setFocusOnFirstResult();
            };

            $scope.init();
            $scope.$on('$stateChangeSuccess',
                function(event, toState, toParams, fromState, fromParams){
                    //console.log("toParams",toParams, toState);
                    console.log("app.fromState",fromState, toState);
                    if(fromState.name.indexOf("app.trainView")>-1 && toState.name=="app.trainView"){
                        console.log("*************************");
                        $scope.init();
                    }
                }
            );
        }
    ]);
});
