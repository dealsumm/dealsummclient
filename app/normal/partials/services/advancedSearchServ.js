define(function (require) {
    'use strict';

    var app = require('appModule');

    app.register.service('advancedSearchServ', ['$http', '$timeout','$q', 'DEALSUMM_CONFIG', 'GLOBAL_VARS', '$cookies', '$rootScope',
        function ($http, $timeout, $q, DEALSUMM_CONFIG, GLOBAL_VARS, $cookies, $rootScope) {

            var returnObject = {}, promise;
            var basePath = DEALSUMM_CONFIG.REST_PATH;// + GLOBAL_VARS.orgId;

            returnObject.getAdvancedSearchData = function (nodeId, nodeType) {
                var data = {};
                data['nodetype'] = nodeType;
                data['nodeid'] = nodeId;
                data['get_advanced_search_params'] = 'true'; // pass the command
                var url = basePath + $rootScope.$stateParams.orgId + '/' + nodeType + '/' + nodeId + '/';
                //console.log("data",data, url);
                return $http.post(url, data);
            };
            return returnObject;

    }]);


});



