define(function (require) {
    'use strict';
    var app = require('appModule');

    app.register.controller('searchModalCtrl',
                    ['$scope', '$state', '$stateParams', 'generalServ', 'advancedSearchServ', 'GLOBAL_VARS', '$filter', '$cookies', '$dialog', '$rootScope', '$timeout', 'localStorageService',
            function ($scope,   $state,   $stateParams,  generalServ,   advancedSearchServ,   GLOBAL_VARS,   $filter,   $cookies,   $dialog,   $rootScope,   $timeout, localStorageService) {

                $scope.nodeId = $scope.params.node_id;
                $scope.nodeType = $scope.params.nodeType;
                $scope.nodeLabel = $scope.params.node_label;

                $scope.today = new Date();
                $scope.query = {};
                $scope.query.searchPhrase = null;
                $scope.query.searchItem = null;
                $scope.newConstraints = [];

                $scope.addLine = function(){
                    $scope.newConstraints.push({});
                    console.log("$scope.newConstraints",$scope.newConstraints);
                }
                /*advancedSearchServ.getAdvancedSearchData($scope.nodeId, $scope.nodeType).then(function(advanced) {
                    $scope.advancedSearchData = advanced.data;
                    console.log("advancedSearchData", $scope.advancedSearchData);
                })*/
                $scope.showSearchHistory = false;
                $scope.showNamedSearchHistory = false;
                $scope.collapseHistory = function(){
                    $scope.showSearchHistory = false;
                    $scope.showNamedSearchHistory = false;
                }
                $scope.toggleSearchHistory = function(force){
                    $scope.showSearchHistory = angular.isDefined(force)?force:!$scope.showSearchHistory;
                    if($scope.showSearchHistory && !$scope.search_history){
                        $scope.getSearchHistory();
                    }
                }
                $scope.getSearchHistory = function(){
                    generalServ.http('search', 'getSearchHistory',[]).then(function(resp) {
                        $scope.search_history = resp.data;
                        //console.log("search_history", $scope.search_history);
                    });
                }
                $scope.toggleNamedSearchHistory = function(force){
                    console.log("toggleNamedSearchHistory");
                    $scope.showNamedSearchHistory = angular.isDefined(force)?force:!$scope.showNamedSearchHistory;
                    if($scope.showNamedSearchHistory && !$scope.named_search_history){
                        $scope.getNamedSearchHistory();
                    }
                }
                $scope.getNamedSearchHistory = function(){
                    console.log("getQueryTemplates");
                    generalServ.http('search', 'getQueryTemplates',[]).then(function(resp) {
                        $scope.named_search_history = resp.data;
                        //console.log("named_search_history ", $scope.named_search_history);
                    });
                }
                $scope.clickedHistoryItem = function(item){
                    console.log("item",item);
                    $scope.query.searchItem = item;
                    $scope.query.searchPhrase = item.query_text;
                    $scope.collapseHistory();
                    $timeout(function(){
                        $('#searchPhrase')[0].focus();
                    }, 100);
                }
                $scope.isEmpty = function (obj) {
                    return angular.equals({},obj);
                };
                $scope.clickedSearch = function(){
                    var dataToSend = {};
                    if(!$scope.query.searchPhrase){
                       return;
                    }
                    if($scope.query.searchItem){
                        if($scope.query.searchItem.query_text != $scope.query.searchPhrase){
                            $scope.query.searchItem = null;
                        }
                    }
                    dataToSend["item"] = $scope.query.searchItem;
                    dataToSend["phrase"] = $scope.query.searchPhrase;

                    if($scope.isEmpty(dataToSend)){
                        console.log("Must fill values!");
                        return;
                    }
                    console.log("dataToSend",dataToSend);

                    localStorageService.set("searchData", dataToSend);
                    $state.go('app.searchView', {orgId: $stateParams.orgId, nodeType:$scope.nodeType, nodeId:$scope.nodeId});
                    $scope.$modalCancel();
                }
                $scope.$modalOk = function(){
                    $scope.clickedSearch();
                }

            }
        ]);
});
