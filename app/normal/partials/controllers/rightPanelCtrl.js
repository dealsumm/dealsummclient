define(function (require) {
    'use strict';
    var app = require('appModule');

    app.register.controller('rightPanelCtrl',
                ['$scope', '$state', 'docServ', '$stateParams', 'GLOBAL_VARS', 'filterFilter', '$fileUploader', '$cookies', '$dialog', '$rootScope', '$timeout',
        function ($scope,   $state,  docServ, $stateParams,   GLOBAL_VARS,   filterFilter,   $fileUploader,   $cookies,   $dialog, $rootScope, $timeout) {

            var doc = $scope.doc = $stateParams.doc;
            // get the TOC
            docServ.getTocTree(doc).then(function(docTocJson) {
                //$scope.docDetails = docDetails.data;
                console.log("docTocJson",docTocJson);
                $scope.toc_tree = docTocJson.data;
                //$scope.docUrl = STATIC_PREFIX + $scope.docDetails.doc_url;
            })

            // get the short TS
            docServ.getTsTree(doc).then(function(tsJSON) {
                //$scope.docDetails = docDetails.data;
                console.log("tsJSON",tsJSON);
                $scope.termsheet_tree = tsJSON.data;
                //$scope.docUrl = STATIC_PREFIX + $scope.docDetails.doc_url;
            })

            // isopenAmendments
            // isopenNotes
            // isopenToC
            // isopenTermSheet
            // isopenDefinitions
            $scope.isopenToC = true;

            // for accordion
            $scope.oneAtATime = true;

            $scope.showHideLabels = function(e){
                //e.preventDefault();
                $scope.hideUnlabled=!$scope.hideUnlabled;
                if($scope.hideUnlabled){
                    $(".legend-tree-holder li").not(".has-label").hide();
                }else{
                    $(".legend-tree-holder li").show();
                }
            }

        }
    ]);
});
