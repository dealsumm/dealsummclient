define(function (require) {
    'use strict';
    var app = require('appModule');

    app.register.controller('linesheetSelectionCtrl',
                ['$scope', 'treeServ', '$state', 'GLOBAL_VARS', 'filterFilter', '$cookies', '$dialog', '$timeout',
        function ($scope,   treeServ,   $state, GLOBAL_VARS, filterFilter, $cookies, $dialog, $timeout) {
            //console.log("getTSTForTreeItem", $scope);
            //console.log("params", $scope.params, $scope.params.rel)
            var node_id = $scope.params.node_id;
            var rel = $scope.params.rel;
            $scope.selectedTst = null;
            treeServ.getLinesheetSections(node_id, rel).then(function(response) {
                $scope.list = response.data;
                //console.log("LinesheetSections",response);
            })
            $scope.tst_selected = function(item){
                //console.log("selectedTst",tst_id);
                $scope.selectedTst = item;
            }
            $scope.tst_select_n_go = function(item){
                //console.log("item",item, $scope.$modalCancel);
                $scope.selectedTst = item;
                var node = {id:node_id, rel:rel, label:""};
                $scope.linesheetChoosen($scope.selectedTst, node);
                $timeout(function(){
                    $scope.$modalCancel();
                }, 300)

            }
        }
    ]);
});
