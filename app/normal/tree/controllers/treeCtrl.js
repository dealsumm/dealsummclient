define(function (require) {
    'use strict';
    var app = require('appModule');

    app.register.controller('treeCtrl',
                ['$scope', 'treeServ', 'generalServ', '$state', 'GLOBAL_VARS', '$filter', '$fileUploader', '$cookies', '$dialog', '$timeout', '$stateParams', '$rootScope', 'localStorageService',
        function ($scope,   treeServ,   generalServ,   $state,   GLOBAL_VARS,   $filter,   $fileUploader,   $cookies,   $dialog,   $timeout,  $stateParams, $rootScope, localStorageService) {
            treeServ.getTreeInit().then(function(tree) {
                //console.log("data",tree.data);
                //console.log("data",tree.data.tree);
                //GLOBAL_VARS.org_id = $scope.fix_id(tree.data.tree[0].id);
                /*var arr = [];
                var jsn = JSON.stringify(tree.data.tree[0].children[0]);
                var jsn2, jsn3;
                for(var j=0;j<30;j++){
                    jsn2 = jsn.replace("F_1", "F_1" + j).replace("Chaim's", "Chaim-" + j);//.replace("false", "true");
                    jsn3 = JSON.parse(jsn2)
                    arr.push(jsn3)
                }
                tree.data.tree[0].children = arr;
                console.log("arr",arr);*/
                $scope.treedata = tree.data;
                mapTreeItems();
                //console.log("data",JSON.stringify(tree.data.tree[0].children[0]));
                console.log("$scope.treedata",$scope.treedata);
            });
            $scope.docsMap = {};
            function mapTreeItems(){
                var tree = $scope.treedata.tree;
                // run on root(s)
                for(var i=0;i<tree.length;i++){
                    // run on properties
                    angular.forEach(tree[i].children, function(property){
                        if(property.rel=="folder") {
                            analyzeTreeProperty(property);
                        }
                    })
                }
                console.log("docsMap",$scope.docsMap);
            }
            function analyzeTreeProperty(propertyObj){
                var parentFolder;
                if(propertyObj.children.length>0){
                    angular.forEach(propertyObj.children, function(folder){
                        if(folder.rel=="folder") {
                            parentFolder = folder.id;
                            angular.forEach(folder.children, function(doc){
                                if(doc.rel=="doc"){
                                    $scope.docsMap[doc.id] = parentFolder;
                                }
                            })
                        }

                    })
                }
            }
            $scope.showTermsheet = function(){
                console.log("showTermsheet");
            }
            $scope.treeOptions = {
                nodeChildren: "children",
                dirSelectable: false,
                injectClasses: {
                    iExpanded: "icon-arrow-down",
                    iCollapsed: "icon-arrow-right"
                }
            }
            $scope.clickOnDocument = function(node){
                $state.transitionTo('app.docView', {
                    orgId: $stateParams.orgId,
                    doc: $scope.fix_id(node.id)
                });
                /*$state.transitionTo('app.docView');*/
                //console.log("node",node, $scope.fix_id(node.id));
            }
            $scope.loadNode = function(node){
                 //console.log("HHHHH", node);
                 console.log("loadNode", node);
                 treeServ.getTreeNode(node.id).then(function(tree) {
                    //console.log("tree beaf",tree.data, node);
                    node.children = tree.data;
                    mapTreeItems();
                    //console.log("Beaf",node.children);
                 });
            };

            /* ************************ */
            /* RCM for tree view         */
            /* ************************ */
            $scope.defineRightMenuItems = [
                {"label": "Create Folder", "callback":"createFolderDialog", "vars":{scope:$scope}, display:"rel=='folder' || rel=='root'"},
                {"label": "Delete", "callback":"confirmDeleteItem", "vars":{scope:$scope}, display:"rel!='root'"},
                {"label": "Rename", "callback":"renameItemDialog", "vars":{scope:$scope}, display:"rel!='root'"},
                {"label": "Index", "callback":"indexTreeItem", "vars":{scope:$scope}, display:true},
                {"label": "Search", "callback":"searchTreeItem", "vars":{scope:$scope}, display:true},
                {"label": "Re-Process", "callback":"reprocessTreeItem", "vars":{scope:$scope}, display:true},
                {"label": "Re-Process TreeNLP", "callback":"reprocessTreeNlpOfTreeItem", "vars":{scope:$scope}, display:true},
                {"label": "Re-Process Inference", "callback":"reprocessInference", "vars":{scope:$scope}, display:true},
                {"label": "Sanitize", "callback":"sanitizeTreeItem", "vars":{scope:$scope}, display:true},
                {"label": "Count-Words", "callback":"countWordsTreeItem", "vars":{scope:$scope}, display:true},
                {"label": "Export", "callback":"exportTreeItem", "vars":{scope:$scope}, display:true},
                {"label": "Import", "callback":"importTreeItem", "vars":{scope:$scope}, display:true},
                {"label": "Entities", "callback":"entities", "scope":$scope, display:true}, // needs to be re-implemented
                {"label": "ShowPreamble", "callback":"showPreamble", "vars":{scope:$scope}, display:true},
                {"label": "Train", "callback":"trainTreeItem", "vars":{scope:$scope}, display:true, "rightIcon": "icon-newtab"},
                {"label": "Predict", "callback":"predictTreeItem", "vars":{scope:$scope}, display:true, "rightIcon": "icon-newtab"},
                {"label": "Connect Predict", "callback":"connectPredictItem", "vars":{scope:$scope}, display:true},
                {"label": "Assign Template", "callback":"itemTemplate", "vars":{scope:$scope}, display:true},
                {"label": "Train Commands", "callback":"trainCommands", "vars":{scope:$scope}, display:true},
                {"label": "Predict Commands", "callback":"predictCommands", "vars":{scope:$scope}, display:true},
                {"label": "Linesheet", "callback":"linesheetSectionsModal", "vars":{scope:$scope}, display:true, "rightIcon": "icon-newtab"},
                {"label": "RentRoll", "callback":"rentroll", "vars":{scope:$scope}, display:"rel=='folder'", "rightIcon": "icon-newtab"},
                {"label": "Argus", "callback":"argus", "vars":{scope:$scope}, display:"rel=='folder'", "rightIcon": "icon-newtab"},
                {"label": "ExportAssets", "callback":"exportAssets", "vars":{scope:$scope}, display:"rel=='root'"},
                {"label": "ImportAssets", "callback":"importAssets", "vars":{scope:$scope}, display:"rel=='root'"}
            ]
            //GLOBAL_VARS.rcmAction = "";

            $scope.initRightMenu = function(eltNode){
                $scope.elt_node = eltNode;
                //console.log("$parent",$scope);
                // build the RCM on each right click, depending on terms of display items
                var rel = eltNode.rel;
                var rightMenuItems = [];
                var items = $scope.defineRightMenuItems;
                angular.forEach(items, function(item){
                    if(item.display){
                        if(eval(item.display)){
                            rightMenuItems.push(item);
                        }
                    }else{
                        rightMenuItems.push(item);
                    }
                })
                $scope.rightMenuItems = rightMenuItems;
            }

            $scope.clickItem = function(callBack, vars){
                console.log("clickItem > callBack:", callBack, vars);
                $scope[callBack](vars);
            }
            $scope.newName = ""; // in dialog
            $scope.nameDialogTitle = "";
            $scope.saveDialogCallback = "";
            $scope.deleteDialogCallback = "";
            $scope.confirmDialogTitle = "";
            $scope.rcmAction = "";
            $scope.getRcmAction = function(){
                return $scope.rcmAction;
            };
            $scope.setRcmAction = function(acn){
                $scope.rcmAction = acn;
            };
            $scope.dialogCancel = function(){
                $scope.rcmAction = "";
            }
            $scope.dialogSave = function(cb){
                $scope[cb]();
            }
            /* ************************ */
            /* RCM Create Folder        */
            /* ************************ */
            $scope.createFolderDialog = function(vars){
                //console.log("createFolderDialog",vars);
                $scope.nameDialogTitle = "Create New Folder";
                $scope.saveDialogCallback = "createFolderSave";
                $scope.newName = "Untitled";
                $scope.rcmAction = "createFolderDialog";
            }
            $scope.createFolderSave = function(){
                //console.log("createFolderSave", $scope.newName, $scope.elt_node);
                var node = $scope.elt_node;
                var parent_id = $scope.fix_id(node.id);
                var folder_name = $scope.newName;
                $scope.rcmAction = "";
                //node['expanded'] = true;
                treeServ.saveNewFolder(parent_id, folder_name, node.rel).then(function(newFolder) {
                    console.log("newFolder>>",newFolder.data, node, node.expanded);
                    $scope.safeApply(function () {
                        if (!node['children']) {
                            node['children'] = [];
                        }
                        node['children'].push(newFolder.data);
                        node['expanded'] = "true";
                    })
                });
            }

            /* ************************ */
            /* RCM Delete               */
            /* ************************ */
            $scope.confirmDeleteItem = function(vars){
                console.log("confirm_delete",vars, $scope.elt_node);
                var node = $scope.elt_node;
                $scope.deleteDialogCallback = "deleteTreeItem";
                $scope.confirmDialogTitle = "Delete '" + node.rel + " " + node.label + "'?";
                $scope.rcmAction = "confirmDeleteItem";
            }

            //return an array of objects according to key, value, or key and value matching
            //https://gist.github.com/iwek/3924925
            function getObjects(obj, key, val) {
                var objects = [];
                for (var i in obj) {
                    if (!obj.hasOwnProperty(i)) continue;
                    if (typeof obj[i] == 'object') {
                        objects = objects.concat(getObjects(obj[i], key, val));
                    } else
                    //if key matches and value matches or if key matches and value is not passed (eliminating the case where key matches but passed value does not)
                    if (i == key && obj[i] == val || i == key && val == '') { //
                        objects.push(obj);
                    } else if (obj[i] == val && key == ''){
                        //only add if the object is not already in the array
                        if (objects.lastIndexOf(obj) == -1){
                            objects.push(obj);
                        }
                    }
                }
                return objects;
            }

            //return an array of values that match on a certain key
            function getValues(obj, key) {
                var objects = [];
                for (var i in obj) {
                    if (!obj.hasOwnProperty(i)) continue;
                    if (typeof obj[i] == 'object') {
                        objects = objects.concat(getValues(obj[i], key));
                    } else if (i == key) {
                        objects.push(obj[i]);
                    }
                }
                return objects;
            }

            //return an array of keys that match on a certain value
            function getKeys(obj, val) {
                var objects = [];
                for (var i in obj) {
                    if (!obj.hasOwnProperty(i)) continue;
                    if (typeof obj[i] == 'object') {
                        objects = objects.concat(getKeys(obj[i], val));
                    } else if (obj[i] == val) {
                        objects.push(i);
                    }
                }
                return objects;
            }
            $scope.deleteTreeItem = function(){
                //console.log("delete is confirmed");
                $scope.rcmAction = "";
                var node = $scope.elt_node;
                var rel = node.rel;
                var node_id = $scope.fix_id(node.id);

                treeServ.callActionOnTreeItem(node_id, rel, "delete").then(function(response) {
                    if(response.data && response.data.folder_id){
                        //console.log("response.data.folder_id",response.data.folder_id);
                        //console.log("node",node, $scope.treedata.tree);
                        var objParent = getObjects($scope.treedata.tree[0],'id',response.data.folder_id)[0];
                        if(objParent && objParent.children){
                            var idx = $(objParent.children).index(node);
                            if(!isNaN(idx)){
                                $scope.safeApply(function () {
                                    objParent.children.splice(idx,1);
                                })
                            }
                        }
                    }
                });
            }
            /* ************************ */
            /* watch drag&drop          */
            /* ************************ */
            $rootScope.uploadParentId = null;
            $scope.$watch(function() {
                return $rootScope.uploadFilesCounter;
            }, function(val) {
                //console.log("uploadFilesCounter",val, $rootScope.uploadFilesCounter, $rootScope.uploadParentId);
                if(val==0 && $rootScope.uploadParentId){
                    console.log("upload is done: parent is", $rootScope.uploadParentId);
                    var parentId = $rootScope.uploadParentId;
                    if(parentId){
                        var objParent = getObjects($scope.treedata.tree[0],'id', parentId)[0];
                        if(objParent){
                            $scope.safeApply(function () {
                                objParent['expanded'] = "true";
                            })
                        }
                    }
                    $rootScope.uploadParentId = null;
                }
            });
            /* ************************ */
            /* RCM Rename               */
            /* ************************ */
            $scope.renameItemDialog = function(vars){
                console.log("renameItemDialog",vars, $scope.elt_node);
                var node = $scope.elt_node;
                $scope.nameDialogTitle = "Rename " + node.rel;
                $scope.saveDialogCallback = "renameItemSave";
                $scope.newName = node.label;
                $scope.rcmAction = "renameItemDialog";
            }
            $scope.renameItemSave = function(){
                //console.log("renameSave", $scope.elt_node);
                $scope.rcmAction = "";
                var node = $scope.elt_node;
                var rel = node.rel;
                var node_id = $scope.fix_id(node.id);
                var new_name = $scope.newName;
                treeServ.renameTreeItem(node_id, new_name, rel).then(function(confirm) {
                    console.log("rename succeed");
                    $scope.safeApply(function () {
                        node.label = new_name;
                    })
                });
            }
            /* ************************ */
            /* RCM index               */
            /* ************************ */
            $scope.indexTreeItem = function(vars){
                var node = $scope.elt_node;
                var rel = node.rel;
                console.log("rel",rel);
                var node_id = $scope.fix_id(node.id);
                treeServ.callActionOnTreeItem(node_id, rel, "index").then(function(confirm) {
                    console.log("indexTreeItem", confirm);
                });
            }
            /* ************************ */
            /* RCM re-process           */
            /* ************************ */
            $scope.reprocessTreeItem = function(vars){
                var node = $scope.elt_node;
                var rel = node.rel;
                console.log("rel",rel, node);
                var node_id = $scope.fix_id(node.id);
                $scope.showProcessModal("Reprocessing:", node.label);
                if(rel=="doc"){
                    var folder_id = $scope.fix_id($scope.docsMap[node.id]);
                    var async = false;
                    generalServ.http('directory', 'reprocessDoc',[folder_id, node_id, async]).then(function(resp) {
                        console.log("reprocessTreeItem", resp);
                        $scope.closeModal("Done!");
                    })
                }else{
                    treeServ.callActionOnTreeItem(node_id, rel, "reprocess").then(function(confirm) {
                        console.log("reprocessTreeItem", confirm);
                        $scope.closeModal("Done!");
                    });
                }
            }

            /* ************************ */
            /* Tree NLP re-process      */
            /* ************************ */
            $scope.reprocessTreeNlpOfTreeItem = function(vars){
                var node = $scope.elt_node;
                var rel = node.rel;
                console.log("rel",rel);
                var node_id = $scope.fix_id(node.id);
                $scope.showProcessModal("reprocess_tree_nlp:", node.label);
                treeServ.callActionOnTreeItem(node_id, rel, "reprocess_tree_nlp").then(function(confirm) {
                    console.log("reprocessTreeNlpOfTreeItem", confirm);
                    $scope.closeModal("Done!");
                });
            }

            /* ************************ */
            /* Train & Predict Commands */
            /* ************************ */
            $scope.trainCommands = function(vars){
                var node = $scope.elt_node;
                var rel = node.rel;
                console.log("rel",rel);
                var node_id = $scope.fix_id(node.id);
                $scope.showProcessModal("train_commands:", node.label);
                treeServ.callActionOnTreeItem(node_id, rel, "train_commands").then(function(confirm) {
                    console.log("trainCommands", confirm);
                    $scope.closeModal("Done!");
                });
            }

            $scope.predictCommands = function(vars){
                var node = $scope.elt_node;
                var rel = node.rel;
                console.log("rel",rel);
                var node_id = $scope.fix_id(node.id);
                $scope.showProcessModal("predict_commands:", node.label);
                treeServ.callActionOnTreeItem(node_id, rel, "predict_commands").then(function(confirm) {
                    console.log("predictCommands", confirm);
                    $scope.closeModal("Done!");
                });
            }

            $scope.reprocessInference = function(vars){
                var node = $scope.elt_node;
                var rel = node.rel;
                console.log("rel",rel);
                var node_id = $scope.fix_id(node.id);
                $scope.showProcessModal("reprocess_inference:", node.label);
                treeServ.callActionOnTreeItem(node_id, rel, "reprocess_inference").then(function(confirm) {
                    $scope.closeModal("Done!");
                });
            }

            $scope.connectPredictItem = function(vars){
                var node = $scope.elt_node;
                var rel = node.rel;
                console.log("rel",rel);
                var node_id = $scope.fix_id(node.id);
                $scope.showProcessModal("connectPredictItem:", node.label);
                treeServ.callActionOnTreeItem(node_id, rel, "connect_predict").then(function(confirm) {
                    $scope.closeModal("Done!");
                });
            }

            $scope.exportLabels = function(vars){
                var node = $scope.elt_node;
                var rel = node.rel;
                console.log("rel",rel);
                var node_id = $scope.fix_id(node.id);
                $scope.showProcessModal("Export Labels:", node.label);
                treeServ.callActionOnTreeItem(node_id, rel, "export_labels").then(function(confirm) {
                    $scope.closeModal("Done!");
                });
            }

            /* ************************ */
            /* RCM sanitize             */
            /* ************************ */
            $scope.sanitizeTreeItem = function(vars){
                var node = $scope.elt_node;
                var rel = node.rel;
                console.log("rel",rel);
                var node_id = $scope.fix_id(node.id);
                $scope.showProcessModal("Sanitizing:", node.label);
                treeServ.callActionOnTreeItem(node_id, rel, "sanitize").then(function(confirm) {
                    console.log("sanitizeTreeItem", confirm);
                    $scope.closeModal("Done!");
                });
            }

            /* ************************ */
            /* RCM ExportAssets           */
            /* ************************ */
            $scope.exportAssets = function(vars){
                var node = $scope.elt_node;
                var rel = node.rel;
                var node_id = $scope.fix_id(node.id);
                $scope.showProcessModal("Exporting Assets:", node.label);
                treeServ.callActionOnTreeItem(node_id, rel, "exportAssets").then(function(confirm) {
                    console.log("exportAssets", confirm);
                    $scope.closeModal("Done!");
                });
            }

            /* ************************ */
            /* RCM ImportAssets           */
            /* ************************ */
            $scope.importAssets = function(vars){
                var node = $scope.elt_node;
                var rel = node.rel;
                var node_id = $scope.fix_id(node.id);
                $scope.showProcessModal("Importing Assets:", node.label);
                treeServ.callActionOnTreeItem(node_id, rel, "importAssets").then(function(confirm) {
                    console.log("importAssets", confirm);
                    $scope.closeModal("Done!");
                });
            }

            /* ************************ */
            /* RCM count-words          */
            /* ************************ */
            $scope.countWordsTreeItem = function(vars){
                var node = $scope.elt_node;
                var rel = node.rel;
                var node_id = $scope.fix_id(node.id);
                $scope.showProcessModal("Counting Words for:", node.label);
                treeServ.callActionOnTreeItem(node_id, rel, "count_words").then(function(confirm) {
                    console.log("countWordsTreeItem", confirm);
                    $scope.closeModal("Done!");
                });
            }
            /* ************************ */
            /* RCM export               */
            /* ************************ */
            $scope.exportTreeItem = function(vars){
                var node = $scope.elt_node;
                var rel = node.rel;
                var node_id = $scope.fix_id(node.id);
                $scope.showProcessModal("Exporting:", node.label);
                treeServ.callActionOnTreeItem(node_id, rel, "MyExport").then(function(confirm) {
                    console.log("exportTreeItem", confirm);
                    $scope.closeModal("Done!");
                });
            }
            /* ************************ */
            /* RCM import               */
            /* ************************ */
            $scope.importTreeItem = function(vars){
                var node = $scope.elt_node;
                var rel = node.rel;
                var node_id = $scope.fix_id(node.id);
                $scope.showProcessModal("Importing:", node.label);
                treeServ.callActionOnTreeItem(node_id, rel, "MyImport").then(function(confirm) {
                    console.log("importTreeItem", confirm);
                    $scope.closeModal("Done!");
                });
            }
            /* ************************ */
            /* RCM show Preamble        */
            /* ************************ */
            $scope.showPreamble = function(vars){
                var node = $scope.elt_node;
                var rel = node.rel;
                var node_id = $scope.fix_id(node.id);
                $scope.showProcessModal("Preamble for:", node.label);
                treeServ.callActionOnTreeItem(node_id, rel, "ShowPreamble").then(function(confirm) {
                    console.log("showPreamble", confirm);
                    $scope.closeModal("Done!");
                });
            }
            /* ************************ */
            /* RCM trainTreeItem        */
            /* ************************ */
            $scope.trainTreeItem = function(vars){
                var node = $scope.elt_node;
                var nodeType = node.rel;
                var nodeId = $scope.fix_id(node.id);

               // localStorageService.set("forceServerRefresh", true);

                $state.go('app.trainView', {orgId: $stateParams.orgId, nodeType:nodeType, nodeId:nodeId});
            }
            /* ************************ */
            /* RCM rentroll        */
            /* ************************ */
            $scope.rentroll = function(){
                var node = $scope.elt_node;
                var nodeId = $scope.fix_id(node.id);

                console.log("nodeId",nodeId);
                $state.go('app.rentRollView', {orgId: $stateParams.orgId, folderId:nodeId});
            }
            /* ************************ */
            /* RCM argus        */
            /* ************************ */
            $scope.argus = function(){
                var node = $scope.elt_node;
                var nodeId = $scope.fix_id(node.id);

                console.log("nodeId",nodeId);
                $state.go('app.argusView', {orgId: $stateParams.orgId, folderId:nodeId});
            }
            /* ************************ */
            /* RCM predictTreeItem      */
            /* ************************ */
            $scope.predictTreeItem = function(vars){
                var node = $scope.elt_node;
                var nodeType = node.rel;
                var nodeId = $scope.fix_id(node.id);

                //localStorageService.set("forceServerRefresh", true);

                $state.go('app.predictView', {orgId: $stateParams.orgId, nodeType:nodeType, nodeId:nodeId});
                /*
                $scope.showProcessModal("Predict for:<br/>" + node.label);
                treeServ.callActionOnTreeItem(node_id, rel, "predict").then(function(confirm) {
                    console.log("predictTreeItem", confirm);
                    $scope.closeModal("Done!");
                });*/
            }
            /* ************************ */
            /* RCM searchTreeItem      */
            /* ************************ */

            $scope.searchTreeItem = function(vars){
                var node = $scope.elt_node;
                var nodeType = node.rel;
                var nodeId = $scope.fix_id(node.id);

                $dialog.dialog(STATIC_PREFIX+APP_FOLDER+"partials/views/search-modal.html", {
                    backdropClick: false,
                    title: "Search Item",
                    ok: {label: 'Search'},
                    params: {node_id: nodeId, nodeType: nodeType, node_label: node.label },
                    controller: "searchModalCtrl"
                });
            }
            /* ************************ */
            /* RCM itemTemplate         */
            /* ************************ */
            $scope.itemTemplate = function(vars){
                console.log("$scope",$scope);
                var node = $scope.elt_node;
                console.log("node",node);
                $dialog.dialog(STATIC_PREFIX+APP_FOLDER+'tree/views/templatesAssign.html', {
                    backdropClick: false,
                    title: "Templates Assign",
                    controller: "templatesAssignCtrl",
                    scope: $scope,
                    params: {node_id: $scope.fix_id(node.id), rel: node.rel, node_label: node.label }
                });
            }
            /* ****************************** */
            /* RCM linesheet  (LONG TS)       */
            /* ****************************** */
            $scope.linesheetSectionsModal = function(vars){
                var node = $scope.elt_node;
                console.log("node",node);
                $dialog.dialog(STATIC_PREFIX+APP_FOLDER+'partials/views/linesheetSection.html', {
                    backdropClick: false,
                    title: "TST selection",
                    controller: "linesheetSelectionCtrl",
                    ok:{label:"select",
                        fn:function(){
                            //console.log("selectedTst", $scope.selectedTst)
                            if($scope.selectedTst){
                                $scope.linesheetChoosen($scope.selectedTst, node);
                                $scope.$modalCancel;
                            }
                        }
                    },
                    scope: $scope,
                    params: {node_id: $scope.fix_id(node.id), rel: node.rel, node_label: node.label }
                });
            }
            $scope.linesheetChoosen = function(selectedTst, node){
                //var linesheetTstData = {rel:node.rel, label:node.label, tst_id:selectedTst.tst_id, tst_name:selectedTst.name}
                //localStorage.setItem("linesheetTstData", JSON.stringify(linesheetTstData));
                $state.go('app.linesheetView.docSection', {orgId: $stateParams.orgId, nodeType:node.rel, nodeId:$scope.fix_id(node.id), sectionId:selectedTst.tst_id});
            }
            /* ****************************** */
            /* RCM modal functions & vars     */
            /* ****************************** */
            $scope.closeModal = function(title){
                if(modalScope){
                    modalScope.$title = title;
                    $timeout(function(){
                        modalCloseFn();
                    }, 1000);
                }

            }
            var modalCloseFn;
            var modalScope;
            $scope.showProcessModal = function(msg, label){
                msg = "<div style='width:400px;overflow-x:hidden;'>" + msg + "<br/><label class='process-label' title='"+label+"'>" + label + "</label><div class='small-loading'></div>" + "</div>";
                $dialog.dialog(null, {
                    template: msg,
                    backdropClick: false,
                    title: "Working...",
                    negative: false,
                    footerTemplate: false,
                    controller: ['$scope', '$state', function ($scope, $state ) {
                        // pass cancel function and $scope to controller
                        // in order to be able to close the modal and change it's title from outside
                        modalCloseFn = $scope.$modalCancel;
                        modalScope = $scope;
                    }]
                });
            };
            $rootScope.hideUploadModal = function(){
                //console.log("hideUploadModal");
                $scope.closeModal("Done");
                $scope.currentFile = null;
                $scope.$apply();
            }
            $rootScope.showUploadModal = function(currentFile){
                $scope.currentFile = currentFile;
                $scope.filesList = $rootScope.uploadFilesList;

                console.log("$scope.filesList",$scope.filesList);
                if($rootScope.uploadFilesCounter==0){
                    $dialog.dialog(STATIC_PREFIX+APP_FOLDER+'tree/views/upload-dialog.html', {
                        //template: msg,
                        backdropClick: false,
                        title: "Uploading...",
                        negative: false,
                        //headerTemplate: false,
                        footerTemplate: false,
                        scope: $scope,
                        controller: ['$scope', '$state', function ($scope, $state ) {
                            // pass cancel function and $scope to controller
                            // in order to be able to close the modal and change it's title from outside
                            modalCloseFn = $scope.$modalCancel;
                            modalScope = $scope;
                        }]
                    });
                }
                $scope.$apply();
            }
        }
    ]);
});
