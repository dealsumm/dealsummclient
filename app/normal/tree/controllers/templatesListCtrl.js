define(function (require) {
    'use strict';
    var app = require('appModule');

    app.register.controller('templatesAssignCtrl',
                ['$scope', 'treeServ', '$state', 'GLOBAL_VARS', 'filterFilter', '$fileUploader', '$cookies', '$dialog', '$timeout',
        function ($scope,   treeServ,   $state, GLOBAL_VARS, filterFilter, $fileUploader, $cookies, $dialog, $timeout) {
            //console.log("getTSTForTreeItem", $scope);
            console.log("params", $scope.params)
            var node_id = $scope.params.node_id;
            $scope.rel = $scope.params.rel;
            treeServ.TSTAssignment(node_id, $scope.rel).then(function(confirm) {
                console.log("TSTAssignment", confirm.data);
                var result = confirm.data;
                $scope.tst_list = result['tst_list'];
                //$scope.itemName = result['my_obj_name'];
                //$scope.tstName = result['real_tst_name'];
            })


        }
    ]);


});
