define(function (require) {
    'use strict';

    var app = require('appModule');

    app.register.service('treeServ',
                ['$http',  'generalServ','$timeout','$q', 'DEALSUMM_CONFIG', 'GLOBAL_VARS', '$cookies', '$rootScope',
        function ($http,    generalServ,  $timeout,  $q,   DEALSUMM_CONFIG,   GLOBAL_VARS,   $cookies,   $rootScope) {

            var returnObject = {}, promise;
            //console.log("GLOBAL_VARS.orgId",GLOBAL_VARS.orgId, $rootScope.orgId, $rootScope.$stateParams);
            var basePath = DEALSUMM_CONFIG.REST_PATH;// + $rootScope.$stateParams.orgId;

            returnObject.getTreeInit = function () {
                var url = basePath + $rootScope.$stateParams.orgId + '/tree/?func=get_doc_and_org';
                //console.log("URL:", url);
                promise = $http.get(url).then(function (response) {
                    return response;
                });
                // Return the promise to the controller
                return promise;
            };
            returnObject.getTreeNode = function (parent_id) {
                // $http returns a promise, which has a then function, which also returns a promise
                var url = basePath + $rootScope.$stateParams.orgId + '/tree/?func=get_node&id=' + parent_id;
                promise = $http.get(url).then(function (response) {
                    return response;
                });
                // Return the promise to the controller
                return promise;
            };
            returnObject.saveNewFolder = function (id, name, rel) {
                var data = {};
                data['id'] = id;
                data['name'] = name;
                data['createFolder'] = 'true'; // pass the command
                data['function'] = 'createFolder'; // trying to pass the command (UZ)
                var url = basePath + $rootScope.$stateParams.orgId + '/' + rel + '/' + data['id'] + '/';
                //console.log("data",data, url);
                return $http.post(url, data);
            };
            returnObject.renameTreeItem = function (node_id, name, rel) {
                var data = {};
                data['id'] = node_id;
                data['name'] = name;
                data['rel'] = rel;
                data['rename'] = 'true'; // pass the command
                var url = basePath + $rootScope.$stateParams.orgId + '/' + rel + '/' + data['id'] + '/';
                console.log("data",data, url);
                return $http.post(url, data);
            };
            returnObject.callActionOnTreeItem = function (node_id, rel, action) {
                var data = {};
                data['id'] = node_id;
                data['rel'] = rel;
                data[action] = 'true'; // pass the command
                var url = basePath + $rootScope.$stateParams.orgId + '/' + rel + '/' + data['id'] + '/';
                console.log("data for callActionOnTreeItem",data, url);
                return $http.post(url, data);
            };
            returnObject.TSTAssignment = function (node_id, rel) { // the new call for TST assignment modal
                var data = {};
                data['obj_id'] = node_id;
                data['obj_type'] = rel;
                var url = basePath + $rootScope.$stateParams.orgId + '/TSTAssignment/';
                console.log("data",data, url);
                return $http.post(url, data);
            };
            returnObject.TSTUnAssign = function (tst_id, det_obj_id, rel) { // remove assigned TST
                var data = {};
                data['tst_id'] = tst_id;
                data['delete_assign_tst'] = true;
                data['obj_type'] = rel;
                var url = basePath + $rootScope.$stateParams.orgId + '/' + rel + '/' + det_obj_id + '/';
                console.log("data",data, url);
                return $http.post(url, data);
            };
            returnObject.assignNewTST = function (tst_id, node_id, rel) { // assign a new TST to item
                var data = {};
                data['tst_id'] = tst_id;
                data['assign_new_tst'] = true;
                console.log("tst_id", tst_id, node_id);

                var url = basePath + $rootScope.$stateParams.orgId + '/' + rel + '/' + node_id + '/';
                console.log("data",data, url);
                return $http.post(url, data);
            };
            returnObject.getLinesheetSections = function (node_id, rel) { // get list of sections for linesheet
                var data = {};
                data['node_id'] = node_id;
                data['rel'] = rel;
                data['linesheet_sections'] = true;
                console.log("linesheet_sections", node_id);

                var url = basePath + $rootScope.$stateParams.orgId + '/' + rel + '/' + node_id + '/';
                console.log("data",data, url);
                return $http.post(url, data);
            };

            // from drag & drop
            $rootScope.uploadFilesCounter = 0;
            returnObject.uploadDone = function(){
                $rootScope.uploadFilesCounter = 0;
                $rootScope.hideUploadModal();
            }
            returnObject.uploadProgressUpdate = function(currentItem){
                //$rootScope.uploadFilesCounter++;
                //$rootScope.showUploadModal(currentFile)
                $rootScope.showUploadModal(currentItem);
                $rootScope.uploadFilesCounter++;
                //console.log("currentItem",currentItem);
            }
            returnObject.uploadFolder = function (itemEntryName, parentFolderId, parentType, deferred) {

                returnObject.uploadProgressUpdate(itemEntryName);
                var url = basePath + $rootScope.$stateParams.orgId + '/addfolder/';
                var data = {};
                data['name'] = itemEntryName;
                data['parent_folder_id'] = parentFolderId;
                data['parent_type'] = parentType;

                console.log("uploadFolder-data",data);
                return $http.post(url, data);
            };
            returnObject.uploadFile = function (file, folderId, parentType, idx, deferred) {

                returnObject.uploadProgressUpdate(file.name);

                //var url = basePath + $rootScope.$stateParams.orgId + '/adddoc/';
                var url = basePath + $rootScope.$stateParams.orgId + '/folders/' + folderId + '/documents/?async=false';
                var xhr = new XMLHttpRequest();
                xhr.open('POST', url, true);

                var formData = new FormData();
                formData.append('file', file);
                formData.append('folder_id', folderId);
                formData.append('parent_type', parentType);
                var csrftoken = $cookies['csrftoken'];
                xhr.setRequestHeader("X-CSRFToken", csrftoken);

                xhr.onreadystatechange=function() {
                    if (xhr.readyState==4 && xhr.status==200) {
                        var obj = JSON.parse(xhr.responseText);
                        console.log("uploadFile", obj);
                        deferred.resolve(obj.doc_id);
                        //return obj.doc_id;
                    }
                }
                xhr.send(formData);
            };

            return returnObject;

    }]);


});


