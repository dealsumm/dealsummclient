define(function (require) {
    'use strict';
    var app = require('appModule');

    app.register.controller('adminCtrl',
                ['$scope', '$state', 'generalServ', '$stateParams', 'GLOBAL_VARS', 'filterFilter', '$cookies', '$dialog', '$rootScope', '$timeout', 'localStorageService',
        function ($scope,   $state,  generalServ,  $stateParams,   GLOBAL_VARS,   filterFilter,   $cookies,   $dialog, $rootScope, $timeout, localStorageService) {

            generalServ.http('admin', 'getOrganizationsList',[]).then(function(resp) {
                $scope.organizations = resp.data;
            });
            generalServ.http('admin', 'sysConfig',[]).then(function(resp) {
                console.log("sysConfig", resp.data);
                var items = resp.data;
                angular.forEach(items, function(item){
                    if(item.value=="True"){
                        item.value = 'true';
                    }
                    item.displayName = removeUnderScores(item.param_name);
                })
                $scope.configItems = items;
            });
            function removeUnderScores(text){
                return text.replace(/_/g, ' ');
            }

            $scope.debug =  angular.isDefined($scope.debugMode)?($scope.debugMode==='true'||$scope.debugMode===true?'on':'off'):'off';
            $scope.debugModeChanged = function(val){
                $scope.updateDebugMode(val=='on'?true:false);
            }

            $scope.clientView = localStorageService.get("viewMode")=="client"?'client':'normal';
            console.log("$scope.clientView",$scope.clientView);
            $scope.clientViewRadio = angular.copy($scope.clientView);

            $scope.switchConfigItem = function(item){
                console.log("item",item, item.value);
                var dataToSend = {};
                dataToSend['param_name'] = item.param_name;
                dataToSend['value'] = item.value;
                generalServ.http('admin', 'updateSysConfigItem',[item.id], dataToSend).then(function(resp) {
                    console.log("updateSysConfigItem > OK",resp.data);
                });
            }
            //
        }
    ]);
});
