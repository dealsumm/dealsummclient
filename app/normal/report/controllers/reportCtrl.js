define(function (require) {
    'use strict';
    var app = require('appModule');

    app.register.controller('reportCtrl',
                ['$scope', 'reportServ', '$state', '$stateParams', 'GLOBAL_VARS', 'filterFilter', '$fileUploader', '$cookies', '$dialog', '$rootScope', '$timeout',
        function ($scope,   reportServ,   $state,   $stateParams,   GLOBAL_VARS,   filterFilter,   $fileUploader,   $cookies,   $dialog, $rootScope, $timeout) {

            // optional charts:
            // "ColumnChart", "PieChart", "AreaChart", "LineChart", "Table", "BarChart"

            // URI - call the other report JSON here
            /*reportServ.getReportData().then(function(reportJSON) {
                console.log("reportJSON", reportJSON.data);
                //$scope.piechart = reportJSON.data;
            })*/
            $scope.hideUnusedLetters = false;
            $scope.contacts = [
                {firstName: 'Jacques', lastName: 'Chirac'},
                {firstName: 'Alain', lastName: 'Madelin'},
                {firstName: 'Robert', lastName: 'Hue'},
                {firstName: 'Jean-Pierre', lastName: 'Chevènement'},
                {firstName: 'René', lastName: 'Coty'},
                {firstName: 'Vincent', lastName: 'Auriol'},
                {firstName: 'Georges', lastName: 'Pompidou'},
                {firstName: 'Paul', lastName: 'Doumer'},
                {firstName: 'Gaston', lastName: 'Doumergue'},
                {firstName: 'Raymond', lastName: 'Poincaré'},
                {firstName: 'Félix', lastName: 'Faure'},
                {firstName: 'Jean-Pierre', lastName: 'Raffarin'},
                {firstName: 'Pierre', lastName: 'Bérégovoy'},
                {firstName: 'Louis-Napoléon', lastName: 'Bonaparte'},
                {firstName: 'Charles', lastName: 'De Gaulle'},
                {firstName: 'Vercingetorix', lastName: ''},
                {firstName: 'Pierre', lastName: 'Mauroy'},
                {firstName: 'Michel', lastName: 'Rocard'},
                {firstName: 'Edouard', lastName: 'Balladur'},
                {firstName: 'Dominique', lastName: 'De Villepin'},
                {firstName: 'Jean-Marc', lastName: 'Ayrault'},
                {firstName: 'Lionel', lastName: 'Jospin'},
                {firstName: 'Alain', lastName: 'Juppé'},
                {firstName: 'Edith', lastName: 'Cresson'},
                {firstName: 'Laurent', lastName: 'Fabius'},
                {firstName: 'Raymond', lastName: 'Barre'},
                {firstName: 'André', lastName: 'Malraux'},
                {firstName: 'Michel', lastName: 'Debré'},
                {firstName: 'Gaston', lastName: 'Deferre'},
                {firstName: 'Jacques', lastName: 'Delors'},
                {firstName: 'Jack', lastName: 'Lang'},
                {firstName: 'Simone', lastName: 'Veil'},
                {firstName: 'Jacques', lastName: 'Toubon'},
                {firstName: 'Nicolas', lastName: 'Sarkozy'},
                {firstName: 'Francois', lastName: 'Fillon'},
                {firstName: 'Michèle', lastName: 'Alliot-Marie'},
                {firstName: 'Jean-Louis', lastName: 'Borloo'},
                {firstName: 'Xavier', lastName: 'Bertrand'},
                {firstName: 'Michel', lastName: 'Sapin'},
                {firstName: 'Laurent', lastName: 'Wauquiez'},
                {firstName: 'Gérard', lastName: 'Larcher'},
                {firstName: 'Laurent', lastName: 'Hénart'},
                {firstName: 'Nicole', lastName: 'Ameline'},
                {firstName: 'Elisabeth', lastName: 'Guigou'},
                {firstName: 'Martine', lastName: 'Aubry'},
                {firstName: 'Christian', lastName: 'Poncelet'},
                {firstName: 'Yvon', lastName: 'Morandat'},
                {firstName: 'Stéphane', lastName: 'Le Foll'},
                {firstName: 'Christine', lastName: 'Lagarde'},
                {firstName: 'Dominique', lastName: 'Bussereau'},
                {firstName: 'Michel', lastName: 'Cointat'},
                {firstName: 'François', lastName: 'Guillaume'},
                {firstName: 'Jean', lastName: 'Glavany'},
                {firstName: 'Raymond', lastName: 'Marcellin'},
                {firstName: 'Louis', lastName: 'Mermaz'},
                {firstName: 'Jean-Pierre', lastName: 'Soisson'},
                {firstName: 'Louis', lastName: 'Le Pensec'},
                {firstName: 'Thierry', lastName: 'Breton'},
                {firstName: 'Hervé', lastName: 'Gaymard'},
                {firstName: 'Christian', lastName: 'Sautter'},
                {firstName: 'Dominique', lastName: 'Strauss-Kahn'},
                {firstName: 'Francis', lastName: 'Mer'},
                {firstName: 'Jean', lastName: 'Arthuis'},
                {firstName: 'René', lastName: 'Monory'},
                {firstName: 'Valéry', lastName: 'Giscard d\'Estaing'},
                {firstName: 'François-Xavier', lastName: 'Ortoli'},
                {firstName: 'Maurice', lastName: 'Couve de Murville'},
                {firstName: 'Antoine', lastName: 'Pinay'},
                {firstName: 'Edgar', lastName: 'Faure'},
                {firstName: 'Pierre', lastName: 'Pflimlin'},
                {firstName: 'Robert', lastName: 'Lacoste'},
                {firstName: 'Paul', lastName: 'Ramadier'},
                {firstName: 'Robert', lastName: 'Buron'},
                {firstName: 'Maurice', lastName: 'Bourgès-Maunoury'},
                {firstName: 'Maurice', lastName: 'Petsche'},
                {firstName: 'René', lastName: 'Pleven'},
                {firstName: 'Lucien', lastName: 'Lamoureux'},
                {firstName: 'Manuel', lastName: 'Valls'},
                {firstName: 'Claude', lastName: 'Guéant'},
                {firstName: 'Brice', lastName: 'Hortefeux'},
                {firstName: 'François', lastName: 'Baroin'},
                {firstName: 'Daniel', lastName: 'Vaillant'},
                {firstName: 'Jean-Louis', lastName: 'Debré'},
                {firstName: 'Charles', lastName: 'Pasqua'},
                {firstName: 'Paul', lastName: 'Quilès'},
                {firstName: 'Philippe', lastName: 'Marchand'},
                {firstName: 'Pierre', lastName: 'Joxe'},
                {firstName: 'Michel', lastName: 'Poniatowski'},
                {firstName: 'Léon', lastName: 'Martinaud-Deplat'},
                {firstName: 'Henri', lastName: 'Queuille'},
            ];

            $scope.piechart = {
                "type": "PieChart",
                "cssStyle": "height:435px; width:100%;",
                "data": {
                    "cols": [
                        {
                            "id": "month",
                            "label": "Month",
                            "type": "string",
                            "p": {}
                        },
                        {
                            "id": "laptop-id",
                            "label": "Laptop",
                            "type": "number",
                            "p": {}
                        },
                        {
                            "id": "desktop-id",
                            "label": "Desktop",
                            "type": "number",
                            "p": {}
                        },
                        {
                            "id": "server-id",
                            "label": "Server",
                            "type": "number",
                            "p": {}
                        },
                        {
                            "id": "cost-id",
                            "label": "Shipping",
                            "type": "number"
                        }
                    ],
                    "rows": [
                        {
                            "c": [
                                {
                                    "v": "January"
                                },
                                {
                                    "v": 19,
                                    "f": "42 items"
                                },
                                {
                                    "v": 12,
                                    "f": "Ony 12 items"
                                },
                                {
                                    "v": 7,
                                    "f": "7 servers"
                                },
                                {
                                    "v": 4
                                }
                            ]
                        },
                        {
                            "c": [
                                {
                                    "v": "February"
                                },
                                {
                                    "v": 13
                                },
                                {
                                    "v": 1,
                                    "f": "1 unit (Out of stock this month)"
                                },
                                {
                                    "v": 12
                                },
                                {
                                    "v": 2
                                }
                            ]
                        },
                        {
                            "c": [
                                {
                                    "v": "March"
                                },
                                {
                                    "v": 24
                                },
                                {
                                    "v": 0
                                },
                                {
                                    "v": 11
                                },
                                {
                                    "v": 6
                                }
                            ]
                        }
                    ]
                },
                "options": {
                    "title": "Sales per month",
                    "isStacked": "true",
                    "fill": 20,
                    "displayExactValues": true,
                    "vAxis": {
                        "title": "Sales unit",
                        "gridlines": {
                            "count": 6
                        }
                    },
                    "hAxis": {
                        "title": "Date"
                    },
                    "is3D": true
                },
                "formatters": {},
                "displayed": true
            }


            $scope.columnchart = {
                "type": "ColumnChart",
                "cssStyle": "height:435px; width:100%;",
                "displayed": true,
                "data": {
                    "cols": [
                        {
                            "id": "month",
                            "label": "Month",
                            "type": "string",
                            "p": {}
                        },
                        {
                            "id": "laptop-id",
                            "label": "Laptop",
                            "type": "number",
                            "p": {}
                        },
                        {
                            "id": "desktop-id",
                            "label": "Desktop",
                            "type": "number",
                            "p": {}
                        },
                        {
                            "id": "server-id",
                            "label": "Server",
                            "type": "number",
                            "p": {}
                        },
                        {
                            "id": "cost-id",
                            "label": "Shipping",
                            "type": "number"
                        }
                    ],
                    "rows": [
                        {
                            "c": [
                                {
                                    "v": "January"
                                },
                                {
                                    "v": 19,
                                    "f": "42 items"
                                },
                                {
                                    "v": 12,
                                    "f": "Ony 12 items"
                                },
                                {
                                    "v": 7,
                                    "f": "7 servers"
                                },
                                {
                                    "v": 4
                                }
                            ]
                        },
                        {
                            "c": [
                                {
                                    "v": "February"
                                },
                                {
                                    "v": 13
                                },
                                {
                                    "v": 1,
                                    "f": "1 unit (Out of stock this month)"
                                },
                                {
                                    "v": 12
                                },
                                {
                                    "v": 2
                                }
                            ]
                        },
                        {
                            "c": [
                                {
                                    "v": "March"
                                },
                                {
                                    "v": 24
                                },
                                {
                                    "v": 5
                                },
                                {
                                    "v": 11
                                },
                                {
                                    "v": 6
                                }
                            ]
                        }
                    ]
                },
                "options": {
                    "title": "Sales per month",
                    "isStacked": "true",
                    "fill": 20,
                    "displayExactValues": true,
                    "vAxis": {
                        "title": "Sales unit",
                        "gridlines": {
                            "count": 10
                        }
                    },
                    "hAxis": {
                        "title": "Date"
                    }
                },
                "formatters": {}
            }

            $scope.calendarContent = {
                "2014":
                {
                    "4":
                    {
                        "7":
                            [
                                "Google it: <a href=\"http://google.com\" target=\"_blank\">Open Tab</a>"
                            ]
                    }
                }
            };
            /*
             tested component
             - https://github.com/lord2800/angular-calendar
             - https://github.com/jhiemer/angularjs-calendar
             - https://github.com/waganse/angular-ui-calendar
             + https://github.com/mofas/angular-bootstrap-calendar

             */
        }
    ]);
});
