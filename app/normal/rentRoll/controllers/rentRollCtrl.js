define(function (require) {
    'use strict';
    var app = require('appModule');

    app.register.controller('rentRollCtrl',
        ['$scope', 'docServ', 'rentRollServ', '$state', '$stateParams', 'GLOBAL_VARS', 'filterFilter', '$cookies', '$dialog', '$rootScope', '$timeout',
            function ($scope, docServ, rentRollServ, $state, $stateParams, GLOBAL_VARS, filterFilter, $cookies, $dialog, $rootScope, $timeout) {

                var getRentRollLastPosition = function () {
                    return JSON.parse(localStorage.getItem("rentRollLastPosition"));
                }
                var updateRentrollPosition = function (data) {
                    //console.log("SET/data - rentRollLastPosition",data);
                    if ($scope.currentDocName) {
                        data.currentDocName = $scope.currentDocName;
                    }
                    localStorage.setItem("rentRollLastPosition", JSON.stringify(data));
                }


                $scope.docUrl = "";
                $scope.currentDocName = null;
                var stateParams = $rootScope.$stateParams;
                var rentRollLastPosition = getRentRollLastPosition();
                console.log("rentRollLastPosition", rentRollLastPosition);
                $scope.noFolderId = false;
                if (!stateParams.folderId) {
                    console.log("stateParams", stateParams);
                    if (rentRollLastPosition) {
                        var params = rentRollLastPosition.params;
                        if (params.phrase) {
                            $state.go('app.rentRollView.phrase', {folderId: params.folderId, doc: params.doc, phrase: params.phrase});
                        } else if (params.doc) {
                            $state.go('app.rentRollView.doc', {folderId: params.folderId, doc: params.doc});
                        } else {
                            $state.go('app.rentRollView', {folderId: params.folderId});
                        }

                    } else {
                        $scope.noFolderId = true;
                    }
                    return;
                }
                if (rentRollLastPosition && rentRollLastPosition.currentDocName) {
                    $scope.currentDocName = rentRollLastPosition.currentDocName;
                }

                //$scope.hideDocColorsOnInit = true;
                var folderId = $stateParams.folderId;
                var hide_cols;

                var temp_isDataReal = false;
                rentRollServ.getRentRoll(folderId).then(function (resp) {
                    console.log("getRentRoll folder:", folderId, resp);
                    temp_isDataReal = true;
                    //var item = resp.data;
                    $scope.tableData = resp.data;
                    $scope.info = $scope.tableData.info;
                    $scope.start_year = $scope.start_year_copy = $scope.info.first_xls_year;
                    $scope.end_year = $scope.end_year_copy = $scope.info.first_xls_year + $scope.info.no_years;
                    $scope.gla = $scope.info.gla;
                    $scope.xls_path = '/' + resp.data.xls_path
                    hide_cols = $scope.tableData.info.hide_cols;
                    $scope.initAfterLoaded();
                })
                $scope.rightClickedOnRRCell = function (cell, doc_id) {
                    //console.log("cell",cell);
                    if (cell.ts_line_id && cell.ts_cell_id) {
                        localStorage.setItem("rentRollCellExplore", JSON.stringify({line_id: cell.ts_line_id, cell_id: cell.ts_cell_id}));
                        $state.go('app.termsheetView.phrase', {doc: doc_id, phrase: cell.phrase});
                    }
                }
                $scope.currentEditedCell = null;
                $scope.clickedCell = null;

                //$scope.rowsCounter = 0;
                $scope.showRow = {};

                $scope.tempRowVal = "";
                $scope.trimVal = function (val) {
                    if (val) {
                        $scope.tempRowVal = (val).replace(/\W/g, '').toLowerCase();
                    }
                    //console.log("$scope.tempRowVal",$scope.tempRowVal);
                    return $scope.tempRowVal;
                }
                $scope.clickedOnRRCell = function (cell, evt, row, rowsCounter, isFirst) {
                    //console.log("cell.val",cell.val,"|", row.info.doc_id);
                    if (cell.col == 0 && isFirst) {
                        $scope.showRow[rowsCounter] = !$scope.showRow[rowsCounter];
                        //console.log("expand...", $scope.showRow[rowsCounter]);
                        return;
                    }
                    console.log("cell doc id=", cell.doc);
                    if (!cell.val || cell.doc == -1) {
                        return;
                    }

                    //console.log("row",row);

                    var docId = cell.doc;//row.info.doc_id;
                    $scope.nodeId = stateParams.nodeId;
                    //console.log("$scope.docId",$scope.docId, docId);
                    $scope.clickedCell = cell;
                    if ($scope.docId == docId) {
                        // no need to load new document
                        $scope.markPhraseOnDoc(cell, row);
                    } else {
                        $scope.currentDocName = cell.dn||row.info.doc_name;
                        $scope.docId = docId;
                        $scope.docScope.token_spans = null;  // reset the selected SPANs array
                        $scope.docScope.docUrl = ""; // to empty the doc container
                        docServ.getDocDetails($scope.docId, $scope.nodeId, $scope.docScope);
                    }
                    if (!$scope.RRcontentLocked) {

                        //console.log("evt",evt, evt.target);
                        // edit content mode > also use functionality
                        var pos = $(evt.target).position();

                        //console.log("edit-cell", pos.left, pos.top, cell.id);

                        $scope.currentEditedCell = cell;
                        //console.log("edit-cell", $("#edit-cell").outerWidth());
                        $scope.cellNewValue = cell.val;

                        $("#edit-cell").css({"left": pos.left, "top": pos.top + 105}).show(); // 105 is padding top of wrapping element
                        $("#edit-cell-name").focus();
                    }
                }
                $scope.resetYears = function (elt) {
                    if ($scope.start_year.length < 4 || isNaN($scope.start_year)) {
                        $scope.start_year = $("#start_year").data("original");
                    }
                    if ($scope.end_year.length < 4 || isNaN($scope.end_year)) {
                        $scope.end_year = $("#end_year").data("original");
                    }
                    if ($scope.start_year > $scope.end_year) {
                        $scope[elt] = $("#" + elt).data("original");
                    }
                }
                $scope.waitBeforeUpdate = false;

                $scope.updateGla = function () {
                    rentRollServ.updateGla($scope.gla, $scope.info.folder).then(function (resp) {
                        console.log("updateGla", resp);
                    })
                }

                $scope.updateYears = function (elt) {
                    if ($scope.waitBeforeUpdate) {
                        return;
                    }
                    var valid = true;
                    if ($scope.start_year.length < 4 || isNaN($scope.start_year)) {
                        $scope.start_year = $("#start_year").data("original");
                        valid = false;
                    }
                    if (($scope.end_year.length < 4 || isNaN($scope.end_year)) && valid) {
                        $scope.end_year = $("#end_year").data("original");
                        valid = false;
                    }
                    if (($scope.start_year > $scope.end_year) && valid) {
                        $scope[elt] = $("#" + elt).data("original");
                        valid = false;
                    }
                    if (valid) {
                        $scope.waitBeforeUpdate = true;
                        $("#" + elt).data("original", $scope[elt]);
                        var no_of_years = (1 * $scope.end_year) - (1 * $scope.start_year);
                        $timeout(function () {
                            $("#" + elt).blur();
                            $scope.waitBeforeUpdate = false;
                        }, 100)
                        rentRollServ.updatePeriod($scope.start_year, no_of_years, $scope.info.folder).then(function (resp) {
                            console.log("updatePeriod", resp);
                        })
                    }
                }
                /*$scope.commencementDate = new Date();
                 $scope.dateCellId = null;
                 $(".quickdate > a").on("click", function(){

                 })
                 $scope.handleDatePicker = function(evt, cell){
                 if(cell.type=="date"){
                 $scope.dateCellId = cell.id;
                 $scope.commencementDate = new Date(cell.val);
                 //console.log("evt",evt.target);
                 var pos = $(evt.target).position();
                 $("#rentroll-date-picker").css({"left":pos.left+24,"top":pos.top+107}).show(); // 105 is padding top of wrapping element
                 }
                 }
                 $scope.hideDatePicker = function(cell){
                 if(cell.type=="date"){
                 $timeout(function(){
                 if(!$scope.overDatePicker && cell.id==$scope.dateCellId){
                 $("#rentroll-date-picker").hide();
                 $scope.dateCellId = null;
                 }
                 },0)
                 }
                 }*/
                $scope.downloadXls = function () {
                    rentRollServ.downloadRentRoll($scope.info).then(function (resp) {
                        console.log("downloadRentRoll", resp);
                    })
                }
                $scope.cancelEditCellName = function () {
                    $scope.currentEditedCell = null;
                    $("#edit-cell").hide();
                }
                $scope.saveEditCellName = function () {
                    var cell = $scope.currentEditedCell;
                    rentRollServ.editRRCell(cell, $scope.cellNewValue).then(function (resp) {
                        $scope.currentEditedCell.val = $scope.cellNewValue;
                        $scope.currentEditedCell = null;
                        // todo - run on results array to place data on right cells
                    })
                    $("#edit-cell").hide();
                }
                $scope.docLoadedFunc = null;
                $scope.initAfterLoaded = function () {
                    $scope.docScope = angular.element($("#doc_container")).scope();
                    $scope.docScope.hideDocNavigationButtons = true;
                    //console.log("$scope.docScope.docLoaded",$scope.docScope.docLoaded);
                    if (!$scope.docLoadedFunc) {
                        // extend the docLoad function
                        // do this only one time
                        $scope.docLoadedFunc = $scope.docScope.docLoaded;
                        $scope.docScope.docLoaded = function () {
                            $scope.docLoadedFunc.apply(this, arguments);
                            $scope.markPhraseOnDoc();
                        }
                    }
                    $scope.loadOnFirstResult();
                }
                $scope.loadOnFirstResult = function () {

                    //console.log("stateParams.doc", stateParams,stateParams.doc);
                    if ($scope.tableData && $scope.tableData.rows.length > 0) {
                        //var firstDoc = $scope.tableData.rows[0].info.doc_id;
                        var firstDoc = stateParams.doc ? stateParams.doc : $scope.tableData.info.first_doc_id;
                        if (!$scope.currentDocName) {
                            $scope.currentDocName = $scope.tableData.info.first_doc_name;
                        }


                        //console.log("firstDoc",firstDoc, "stateParams.doc", stateParams.doc, $scope.currentDocName);
                        if (firstDoc) {
                            $scope.docId = firstDoc;
                            $scope.docScope.doc = $scope.docId;
                            $scope.docScope.docUrl = ""; // to empty the doc container
                            //console.log("$scope.tableData[0].head.doc",$scope.tableData[0].head.doc);
                            //console.log("docScope",docScope);
                            if (stateParams.phrase) {
                                updateRentrollPosition({params: {folderId: stateParams.folderId, doc: $scope.docId, phrase: stateParams.phrase}});
                                $state.go('app.rentRollView.phrase', {doc: $scope.docId, phrase: stateParams.phrase});
                            } else {
                                updateRentrollPosition({params: {folderId: stateParams.folderId, doc: $scope.docId}});
                                $state.go('app.rentRollView.doc', {doc: $scope.docId});
                            }

                            docServ.getDocDetails($scope.docId, $scope.nodeId, $scope.docScope);
                        }
                    } else {
                        console.log("no parsing - no data found in result");
                    }
                }
                $scope.markPhraseOnDoc = function () {
                    //var folderId = $stateParams.phrase;
                    var phrase = $scope.clickedCell ? $scope.clickedCell.phrase : stateParams.phrase ? stateParams.phrase : null;

                    if (phrase && phrase.indexOf("-1") != 0) {
                        //console.log("clickedCell", phrase, phrase.indexOf("-1"));
                        updateRentrollPosition({params: {folderId: stateParams.folderId, phrase: phrase, doc: $scope.docId}});
                        $state.go('app.rentRollView.phrase', {phrase: phrase, doc: $scope.docId});
                    } else {
                        updateRentrollPosition({params: {folderId: stateParams.folderId, doc: $scope.docId}});
                        $state.go('app.rentRollView.doc', {doc: $scope.docId});
                    }

                }

                $scope.isHidable = function (idx) {
                    return $.inArray(idx, hide_cols) > -1;
                }
                // UZ 072614 download URL
//                $scope.xls_path = STATIC_PREFIX + '../downloads/test.csv'
                // end UZ 072614 download URL

                // temp s/b removed after real data is working:
                // simulate time of loading doc...
                /*$timeout(function(){
                 if(!temp_isDataReal) {
                 $scope.info = $scope.tableData.info;
                 hide_cols = $scope.tableData.info.hide_cols;
                 $scope.initAfterLoaded();
                 }
                 }, 1000)*/
            }
        ]);
});
