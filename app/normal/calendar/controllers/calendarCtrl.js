define(function (require) {
    'use strict';
    var app = require('appModule');

    app.register.controller('calendarCtrl',
        function ($scope,   calendarServ,   $state,   $stateParams,   GLOBAL_VARS,   filterFilter,   $fileUploader,   $cookies,   $dialog, $rootScope, $timeout) {

            if($stateParams.date){
                var theDate = new Date($stateParams.date);
                console.log("theDate",theDate.getDate());
                if(theDate){
                    $scope.calendarDate = theDate.getDate();
                    $scope.calendarMonth = theDate.getMonthFormatted();
                    $scope.calendarYear = theDate.getFullYear();
                }
            }


            $scope.calendarContent = {
                "2014":
                {
                    "4":
                    {
                        "7":
                            [
                                "Google it: <a href=\"http://google.com\" target=\"_blank\">Open Tab</a>"
                            ]
                    }
                }
            };
            /*
             tested component
             - https://github.com/lord2800/angular-calendar
             - https://github.com/jhiemer/angularjs-calendar
             - https://github.com/waganse/angular-ui-calendar
             + https://github.com/mofas/angular-bootstrap-calendar

             */
        },
        ['$scope', 'calendarServ', '$state', '$stateParams', 'GLOBAL_VARS', 'filterFilter', '$fileUploader', '$cookies', '$dialog', '$rootScope', '$timeout'
    ]);
});
