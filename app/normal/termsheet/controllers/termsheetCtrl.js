define(function (require) { 
    'use strict';
    var app = require('appModule');
    // https://github.com/nitishkumarsingh13/Angularjs-Directive-Accept-Number-Only
    app.register.directive('nksOnlyNumber',[ function(){
        return {
            restrict: 'EA',
            require: 'ngModel',
            link: function (scope, element, attrs, ngModel) {
                scope.$watch(attrs.ngModel, function(newValue, oldValue) {
                    var spiltArray = String(newValue).split("");

                    if(attrs.allowNegative == "false") {
                        if(spiltArray[0] == '-') {
                            newValue = newValue.replace("-", "");
                            ngModel.$setViewValue(newValue);
                            ngModel.$render();
                        }
                    }

                    if(attrs.allowDecimal == "false") {
                        newValue = parseInt(newValue);
                        ngModel.$setViewValue(newValue);
                        ngModel.$render();
                    }

                    if(attrs.allowDecimal != "false") {
                        if(attrs.decimalUpto) {
                            var n = String(newValue).split(".");
                            if(n[1]) {
                                var n2 = n[1].slice(0, attrs.decimalUpto);
                                newValue = [n[0], n2].join(".");
                                ngModel.$setViewValue(newValue);
                                ngModel.$render();
                            }
                        }
                    }


                    if (spiltArray.length === 0) return;
                    if (spiltArray.length === 1 && (spiltArray[0] == '-' || spiltArray[0] === '.' )) return;
                    if (spiltArray.length === 2 && newValue === '-.') return;

                    /*Check it is number or not.*/
                    if (isNaN(newValue)) {
                        ngModel.$setViewValue(oldValue);
                        ngModel.$render();
                    }
                });
            }
        };
    }])
    app.register.controller('termsheetCtrl',
                ['$scope', 'docServ', 'termsheetServ', '$state', '$stateParams', 'GLOBAL_VARS', 'filterFilter', '$fileUploader', '$cookies', '$dialog', '$rootScope', '$timeout',
        function ($scope,   docServ,   termsheetServ,   $state,   $stateParams,   GLOBAL_VARS,   filterFilter,   $fileUploader,   $cookies,   $dialog, $rootScope, $timeout) {

            $scope.docUrl = "";
            var stateParams = $rootScope.$stateParams;
            if(!stateParams.doc){
                $state.go('app.termsheetView', {doc:1});
                return;
            }
            //$scope.hideDocColorsOnInit = true;
            var doc = stateParams.doc;

            $scope.toggleChart = false;
            $scope.expandedSection = null;
            $scope.highlightedCell = "";
            $scope.currentEditedCell = "";
            if(localStorage.getItem("rentRollCellExplore") && localStorage.getItem("rentRollCellExplore")!="null"){
                var rentRollCellData = JSON.parse(localStorage.getItem("rentRollCellExplore"));
                console.log("rentRollCellData",rentRollCellData);
                localStorage.setItem("rentRollCellExplore", null);
                $scope.expandedSection = rentRollCellData.line_id;
                $scope.highlightedCell = rentRollCellData.cell_id;
            }

            if(localStorage.getItem("tsChartsOptions") && localStorage.getItem("tsChartsOptions")!="null"){
                $scope.tsChartsOptions = JSON.parse(localStorage.getItem("tsChartsOptions"));
                //console.log("tsChartsOptions",tsChartsOptions);
                if($scope.tsChartsOptions[stateParams.orgId]){
                    if($scope.tsChartsOptions[stateParams.orgId][stateParams.doc]){
                        $scope.chartOptions = $scope.tsChartsOptions[stateParams.orgId][stateParams.doc];
                    }
                }
            }
            $scope.getTsTable = function(){
                // get the central termsheet // CHAIM: THIS NEEDS TO RUN IN A LOOP!!
                docServ.getTsTable(doc).then(function(tsTableJSON) {
                    console.log("tsTableJSON", tsTableJSON.data);
                    $scope.tableData = tsTableJSON.data.list;

                    // Chaim 2015-01-31
                    // Chaim/Uri: un-comment the following to simulate more than one charts in response
                    /*
                     var temp = angular.copy(tsTableJSON.data.charts_zone[0]);
                     temp.section_info.label = "temp chart";
                     temp.section_info.sectionID = 17;
                     tsTableJSON.data.charts_zone.push(temp);
                     */

                    $scope.chartsZones = tsTableJSON.data.charts_zone;
                    $scope.prevDoc = tsTableJSON.data.info.prev_doc;
                    $scope.nextDoc = tsTableJSON.data.info.next_doc;
                    $scope.tst_id = tsTableJSON.data.info.tst_id;
                    $scope.tst_name = tsTableJSON.data.info.tst_name;
                    //$scope.initSchemaLock();
                })
            };
            $scope.getTsTable();

            $scope.dragStart = function(e, ui) {
                // http://jsfiddle.net/dj_straycat/Q5FWt/3/
                ui.item.data('start', ui.item.index());
            }
            $scope.dragStartCell = function(e, ui){
                $scope.dragStart(e, ui);
                $('.cell-sortable-placeholder').width($(ui.item).width());
            }
            $scope.dragEndAndUpdateSectionsNewOrder = function(e, ui) {
                var start = ui.item.data('start'),
                    end = ui.item.index();

                $scope.tableData.splice(end, 0, $scope.tableData.splice(start, 1)[0]);
                $scope.$apply();
                var sectionsNewOrder = [];
                angular.forEach($scope.tableData, function(section){
                    sectionsNewOrder.push(section.sectionID);
                })

                console.log("sectionsNewOrder>>",sectionsNewOrder);

                termsheetServ.reorderSections(doc, sectionsNewOrder, $scope.tst_id).then(function(resp){
                    // console.log("resp",resp);
                })
            }
            $scope.dragEndAndUpdateCellsNewOrder = function(e, ui){
                console.log("dragEndAndUpdateCellsNewOrder");
                var start = ui.item.data('start'),
                    end = ui.item.index();

                var sectionIndex = ui.item.data('index');
                var section_id = ui.item.data('section_id');

                $scope.tableData[sectionIndex].headers.splice(end, 0, $scope.tableData[sectionIndex].headers.splice(start, 1)[0]);
                $scope.$apply();

                var cellsNewOrder = [];
                angular.forEach($scope.tableData[sectionIndex].headers, function(cell){
                    cellsNewOrder.push(cell.attribute);
                })
                console.log("cellsNewOrder",cellsNewOrder);

                termsheetServ.reorderCells(doc, section_id, cellsNewOrder, $scope.tst_id).then(function(resp){
                    console.log("reorderCells",resp);
                    var respData = resp.data;
                    var thisSectionId = null;
                    $scope.tableData.filter(function(item, idx){
                        if(item.sectionID == respData.sectionID){
                            thisSectionId = idx;
                        }
                    })
                    /*console.log("thisSection",thisSection, thisSectionId);
                    console.log("tableData", $scope.tableData[thisSectionId]);
                    console.log("respData.data", respData);*/
                    $scope.tableData[thisSectionId] = respData;
                    //$scope.$apply();
                    $scope.safeApply(function () {
                        $scope.tableData[thisSectionId] = respData;
                        $( ".row.header" ).sortable( "destroy" );
                        $timeout(function(){
                            $scope.initRowHeaderSortable();
                        }, 200)
                    })
                })
            }
            var schemaLockInitialized = false;
            $scope.initSchemaLock = function(){
                schemaLockInitialized = true;

                $("#sections-sortable").sortable({
                    handle: ".move-section",
                    axis: "y",
                    placeholder: "section-sortable-placeholder",
                    forceHelperSize: true,
                    forcePlaceholderSize: true,
                    start: $scope.dragStart,
                    update: $scope.dragEndAndUpdateSectionsNewOrder
                }).disableSelection();
                $("#sections-sortable").sortable( "option", "disabled", true );
                $scope.initRowHeaderSortable();
                $(".row.header").sortable( "option", "disabled", true );
            }
            $scope.initRowHeaderSortable = function(){
                $(".row.header").sortable({
                    handle: ".move-cell",
                    axis: "x",
                    placeholder: "cell-sortable-placeholder",
                    forceHelperSize: true,
                    forcePlaceholderSize: true,
                    start: $scope.dragStartCell,
                    update: $scope.dragEndAndUpdateCellsNewOrder
                }).disableSelection();
            }
            $scope.handleSchemaLock = function(state){
                if(!schemaLockInitialized){
                    $scope.initSchemaLock();
                }
                console.log("state",state);
                if(state) {
                    $("#sections-sortable").sortable( "option", "disabled", true );
                    $(".row.header").sortable( "option", "disabled", true );
                    if($scope.sectionNameInEditMode){
                        $scope.sectionNameInEditMode.editMode = false;
                        $scope.sectionNameInEditMode = null;
                    }
                }else{
                    $("#sections-sortable").sortable( "option", "disabled", false );
                    $(".row.header").sortable( "option", "disabled", false );
                }
            }
            $scope.sectionNameInEditMode = null;
            $scope.cellNameInEditMode = null;

            /* ***************************** */
            /*  SECTIONS                     */
            /* ***************************** */
            $scope.clickEditSectionName = function(elt){
                if($scope.sectionNameInEditMode){
                    $scope.sectionNameInEditMode.editMode = false;
                };
                elt.editMode = true;
                $scope.sectionNameInEditMode = elt;
            }
            $scope.cancelEditSectionName = function(elt){
                var eltSection = elt.section;
                $scope.safeApply(function () {
                    eltSection.label = eltSection.original;
                    elt.editMode = false;
                })
            }
            $scope.saveEditSectionName = function(elt){
                var eltSection = elt.section;
                eltSection.original = eltSection.label;
                elt.editMode = false;
                termsheetServ.renameSection(doc, eltSection.sectionID, eltSection.label, $scope.tst_id).then(function(resp){
                    //console.log("resp",resp);
                })
            }
            $scope.clickDeleteSection = function(elt){
                console.log("clickEditSectionName - idx", elt.section, elt);
                $scope.sectionToDelete = elt.section;
                $scope.showConfirmModal("Delete section '"+$scope.sectionToDelete.label+"'",
                        "Are you sure you want to delete field '"+$scope.sectionToDelete.label+"'?",
                    $scope.removeSection
                )
            }
            $scope.removeSection = function(){
                console.log("sectionToDelete", $scope.sectionToDelete.sectionID, $scope.tst_id);
                termsheetServ.removeSection(doc, $scope.sectionToDelete.sectionID, $scope.tst_id).then(function(resp){
                    // todo - server interaction - delete section
                    // in case the answer is positive - remove it from dom
                    console.log("resp",resp);
                    var respData = resp.data;
                    var thisSectionId = null;
                    var thisSection = $scope.tableData.filter(function(item, idx){
                        if(item.sectionID == respData.removed_tst_field){
                            thisSectionId = idx;
                        }
                    })
                    console.log("thisSection",thisSection);
                    delete $scope.tableData[thisSectionId];
                    //$scope.tableData[thisSection];
                    //delete thisSection;
                })
            }
            /* ***************************** */
            /*  HEADER CELLS                 */
            /* ***************************** */
            $scope.cancelEditHeaderCellName = function(elt){
                var eltHeader = elt.header;
                $scope.safeApply(function () {
                    eltHeader.label = eltHeader.original;
                    elt.editModeCell = false;
                })
            }
            $scope.saveEditHeaderCellName = function(elt, sectionId){
                var eltHeader = elt.header;
                var renamed_cell = eltHeader.original;
                eltHeader.original = eltHeader.label;
                elt.editModeCell = false;
                console.log("saveEditheaderName",elt);
                console.log("$scope",$scope);
                termsheetServ.renameCell(doc, eltHeader.label, $scope.tst_id, sectionId, renamed_cell).then(function(resp){
                    //console.log("resp",resp);
                })
            }
            $scope.editedChartHeader = {};
            $scope.clickEditChartHeaderCellName = function(chart, header){
                if($scope.editedChartHeader['obj']){
                    $scope.editedChartHeader['obj'].label = $scope.editedChartHeader['obj'].original;
                }
                $scope.editedChartHeader['id'] = chart.chart_id+'_'+header.column_no;
                $scope.editedChartHeader['obj'] = header;
            }
            $scope.cancelEditChartHeaderCellName = function(header){
                $scope.editedChartHeader = {};
                header.label = header.original;
            }
            $scope.saveEditChartHeaderCellName = function(chart, header){
                termsheetServ.renameHeader(chart.chart_id, header.column_no, header.label).then(function(resp){
                    header.original = header.label;
                })
            }
            $scope.clickEditHeaderCellName = function(elt){
                if($scope.cellNameInEditMode){
                    $scope.cellNameInEditMode.editModeCell = false;
                };
                elt.editModeCell = true;
                $scope.cellNameInEditMode = elt;
            }
            $scope.clickDeleteHeaderCell = function(elt, section_id){
                console.log("clickDeleteCell - idx", elt.header.attribute, section_id);
                $scope.cellToDelete = elt.header.attribute;
                $scope.relatedSection = section_id;
                $scope.showConfirmModal("Delete cell '"+$scope.cellToDelete+"'",
                        "Are you sure you want to delete cell '"+$scope.cellToDelete+"'?",
                    $scope.removeHeaderCell
                )
            }
            $scope.removeHeaderCell = function(){
                console.log("cellToDelete", $scope.cellToDelete);
                termsheetServ.removeHeaderCell(doc, $scope.relatedSection, $scope.cellToDelete, $scope.tst_id).then(function(resp){
                    // todo - server interaction - delete cell
                    // in case the answer is positive - remove it from dom
                    console.log("resp",resp);
                })
            }
            $scope.clickApproveSection = function(section){
                console.log("approvedSections",section.approved);
                termsheetServ.approveLine(doc, section.approved, $scope.tst_id, section.sectionID).then(function(resp){
                    console.log("approveLine",resp);
                })
            }
            $scope.clickSubmitChanges = function(){
                console.log("submitChanges",$scope.info.approve);
                termsheetServ.approveDocument(doc, $scope.info.approve, $scope.tst_id).then(function(resp){
                    console.log("approveDocument",resp);
                })
            }
            $scope.toggleIgnore = function(row, chart){
                var chart_id = chart.chart_id;
                //console.log("row, chartId",row, chart_id, row.row_id, row.ignore);
                termsheetServ.toggleIgnore(chart_id, row.row_id, !row.ignore).then(function(resp){
                    if(!isNaN(resp.data.row_no)){
                        chart.rows.splice(resp.data.row_no, 1);
                    }else{
                        row.ignore = !row.ignore;
                    }
                })
            };
            $scope.addingNewRow = false;
            $scope.clickedInsertRow = function(chart, idx){
                if($scope.tsContentLocked){
                    return;
                }
                $scope.addingNewRow = true;

                termsheetServ.addNewRow(chart.chart_id, idx).then(function(resp){
                    var newRow = resp.data;
                    //newRow.ignore = null;
                    //newRow.property_id = newRow.prop_id;
                    console.log("newRow",newRow);
                    $scope.addingNewRow = false;
                    chart.rows.splice(idx, 0, newRow);
                });
            }
            $scope.clickedInsertColumn = function(chart){
                if($scope.tsContentLocked || $scope.addingNewColumn){
                    return;
                }
                $scope.cancelAddColumn = function(){
                    $scope.addingNewColumn = false;
                }
                $scope.addingNewColumn = true;
                $dialog.dialog(null, {
                    template: "<br/><label>New column name: &nbsp;</label><input type='text' ng-model='newColumnName' maxlength='30'/>",
                    backdropClick: false,
                    title: "Add new column",
                    ok:{label:"Add column"},
                    negative:{label:"Cancel"},
                    params:{submitNewColumn:$scope.submitNewColumn, chart:chart, cancelAddColumn:$scope.cancelAddColumn},
                    controller: ['$scope', '$document',
                        function( scope, $document) {
                            scope.newColumnName = "";
                            scope.saveModalCancel = scope.$modalCancel;
                            scope.$modalOk = function(){
                                if(scope.newColumnName && scope.newColumnName.length>0){
                                    scope.params.submitNewColumn(scope.params.chart, scope.newColumnName);
                                    scope.$modalCancel();
                                }
                            }
                            scope.$modalCancel = function(){
                                scope.params.cancelAddColumn();
                                scope.saveModalCancel();
                            }
                        }]
                });
            };
            $scope.submitNewColumn = function(chart, columnName){
                console.log("columnName",columnName, chart.chart_id);
                termsheetServ.addNewColumn(chart.chart_id, columnName).then(function(resp){
                    // due to server technical issues, in order to see the full chart need to reload it from server
                    $scope.getTsTable();
                });
            }
            $scope.toggleIgnoreColumn = function(chart, header){
                //console.log("headerData",header,  chart);
                var ignored = !header.ignore;
                termsheetServ.toggleIgnoreColumn(chart.chart_id, header.column_no, ignored).then(function(resp){
                    //console.log("resp",resp);
                    header.ignore = ignored;
                })
            }
            /* ***************************** */
            /*  CELLS                        */
            /* ***************************** */
            $scope.currentEditedCellId = null;
            //$rootScope.selectionPhrase = {};
            $scope.selectionPhrase = $rootScope.selectionPhrase;
            $scope.cellNewValue = null;
            $scope.pasteFromSelection = function(){
                $scope.cellNewValue.label = $scope.selectionPhrase.label;
                $scope.cellNewValue.from_token = $scope.selectionPhrase.phrase.from_token;
                $scope.cellNewValue.to_token = $scope.selectionPhrase.phrase.to_token;
                $scope.cellNewValue.doc_id = $scope.selectionPhrase.doc_id;
            }
            $scope.clickedOnTSCell = function(cell, sectionId, row_id, evt, chart_id, ignored, isChart){
                if(ignored){
                    return;
                }
                if(cell.to_token!=-1 && cell.to_token!=null && cell.from_token!=-1 && cell.from_token!=null){
                    $scope.markPhraseOnDoc(cell);
                }
                if(!$scope.tsContentLocked) {
                    $scope.advanced = false;

                    // edit content mode > also use functionality
                    var pos = $(evt.target).closest(".cell").position();
                    // handle first cell padding
                    pos.left += ($(evt.target).closest(".cell").is(":first-child")) ? 28 : 0;

                    //console.log("edit-cell", pos.left, pos.top, cell.id);
                    //console.log("cell", cell);
                    $scope.currentEditedRowId = row_id;
                    $scope.currentEditedCell = cell;
                    $scope.currentEditedCellSectionId = sectionId;
                    $scope.currentEditedCellChartId = chart_id;
                    //console.log("edit-cell", $("#edit-cell").outerWidth());
                    $scope.cellNewValue = angular.copy(cell);//.label;

                    $("#edit-cell").css({"left": pos.left, "top": pos.top + 105}).show(); // 105 is padding top of wrapping element
                    $("#edit-cell-name").focus();
                }
            };
            $scope.cancelEditCellName = function(){
                $scope.cellNewValue = null;
                $scope.currentEditedRowId = null;
                $scope.currentEditedCellId = null;
                $scope.currentEditedCellSectionId = null;
                $("#edit-cell").hide();
            }
            $scope.saveEditCellName = function(){
                var cell = $scope.currentEditedCell;
                termsheetServ.editCellNewApi($scope.cellNewValue, cell.property_id).then(function(resp){
                    console.log("editCellNewApi > resp",resp);
                    $scope.currentEditedCell.label = $scope.cellNewValue.label;
                    $scope.currentEditedCell.from_token = $scope.cellNewValue.from_token;
                    $scope.currentEditedCell.to_token = $scope.cellNewValue.to_token;
                    $scope.currentEditedRowId = null;
                    $scope.cellNewValue = null;
                    $scope.currentEditedCellSectionId = null;
                }, function(){
                    $scope.cellNewValue = null;
                })
                $("#edit-cell").hide();
            };
            $scope.saveEditScore = function(chartData){
                //console.log("chartData",chartData.chart_id,chartData.score);
                termsheetServ.updateScore(chartData.chart_id, chartData.score).then(function(){
                    console.log("Score updated")
                    chartData.originalScore = chartData.score;
                },function(){
                    console.log("Score FAILED", chartData.originalScore);
                    chartData.score = chartData.originalScore;
                })
            }
            $scope.toggleTstActionsFunc = function(){
                if(!$scope.tsSchemaLocked){
                    $scope.toggleTstActions=!$scope.toggleTstActions;
                }
            };
            $scope.tstAction = function(newName, acn){
                console.log("tstAction", newName, acn);
                termsheetServ.tstAction(newName, acn, $scope.tst_id).then(function(resp){
                    console.log("tstAction resp",resp);
                })
            };

            $scope.addRow = function(elt){
                var section_id = elt.section.sectionID;
                var rows = elt.section.rows;
                termsheetServ.addRow(doc, section_id, $scope.tst_id).then(function (resp) {
                    console.log("addRow", resp);
                    var item = resp.data;
                    rows.unshift(item);
                })
            }
            $scope.addRowChart = function(elt, pos, row_id, section_id) {
                var chart_id = elt.chart.chart_id;
                //console.log("section_id",section_id, elt.chart);
                //var section = elt.section;
                termsheetServ.addRowChart(doc, chart_id, section_id, $scope.tst_id, pos, row_id).then(function (resp) {
                    //console.log("addRowChart", resp, elt);
                    //console.log("$scope.tableData", $scope.tableData);
                    var item = $scope.tableData.filter(function (chart, index) {
                        return chart.chart_id == chart_id;
                    })[0];
                    var _index = $scope.tableData.indexOf(item);
                    $scope.tableData[_index] = resp.data;

                    console.log(angular.element($("#section_" + chart_id)).scope())
                    //angular.element($("#section_"+section_id)).scope()
                    //section_{{section.sectionID}}

                })
            }
            $scope.makePrimary = function(elt, row_id, event){
                console.log('In makePrimary');
                var section_id = elt.section.sectionID;
                var rows = elt.section.rows;

                var item = rows.filter(function(row, index){
                    return row.row_id == row_id;
                })[0];

                var _index = rows.indexOf(item);
                rows.splice(_index, 1);
                rows.unshift(item);

                var itemToHighlight = event.target.parentElement.parentElement;
                $(itemToHighlight).pulse({backgroundColor: 'rgba(245,245,245,0.5)', color: '#1a9cf9'}, {duration: 400, pulses: 5});//, returnObject.removePulse(item, itemStyle, 2000));

                termsheetServ.makePrimary(doc, section_id, row_id, $scope.tst_id).then(function(resp){
                    // todo - server interaction - make primary
                    console.log("resp",resp);
                })
            }
            var initSectionsTopPosition = null;
            $scope.toggleSection = function(stat, sectionId){
                if(!initSectionsTopPosition){
                    initSectionsTopPosition = $("#sections-sortable").position().top;
                }
                if(stat){
                    $('.scrollable-container').animate({ scrollTop: $('.scrollable-container').scrollTop() + $("#"+sectionId).position().top - initSectionsTopPosition }, '2000');
                }
            } ;
            $scope.toggleChart = {};
            $scope.toggleChartStat = function(chartId, zoneSectionId){
                $scope.toggleChart[zoneSectionId]=!$scope.toggleChart[zoneSectionId];
                if(!initSectionsTopPosition){
                    initSectionsTopPosition = $("#sections-sortable").position().top;
                }
                if($scope.toggleChart[zoneSectionId]){
                    $('.scrollable-container').animate({ scrollTop: $('.scrollable-container').scrollTop() + $("#"+chartId).position().top - initSectionsTopPosition }, '2000');
                }
            }

            $scope.markPhraseOnDoc = function(cellData){
                console.log("markPhraseOnDoc",cellData);
                var from_token = cellData.from_token;
                var to_token = cellData.to_token;
                console.log("phrase>", to_token);
                if(from_token.indexOf("-1")!=0 && to_token.indexOf("-1")!=0){
                    $state.go('app.termsheetView.phrase', {phrase:from_token+"-"+to_token});
                }
            }
            $scope.openTstModal = function(acn){
                var okText, ttl, newNameStr, subHeaderText;
                if(acn=="rename"){
                    okText = "Rename";
                    ttl = "Rename TST " + $scope.tst_name;
                    newNameStr = $scope.tst_name;
                    subHeaderText = "Please type the new name:";
                }else{
                    okText = "Save as";
                    ttl = "Save TST " + $scope.tst_name + " as";
                    newNameStr = $scope.tst_name+"-copy";
                    subHeaderText = "Please type the new name to save:";
                }
                var originalTstName = $scope.tst_name;
                var msg = subHeaderText + "<br/><input type='text' ng-model='newName' class='Form__field'/>";
                var submitTstAction = function(newName){
                    console.log("submitTstAction", newName, acn)
                    $scope.tstAction(newName, acn);
                }
                $dialog.dialog(null, {
                    template: msg,
                    backdropClick: false,
                    title: ttl,
                    ok:{label:okText},
                    controller: ['$scope', '$document',
                        function( scope, $document) {
                            scope.newName = newNameStr;
                            scope.$modalOk = function(){
                                if(scope.newName && scope.newName.length>0 && scope.newName!=originalTstName){
                                    submitTstAction(scope.newName);
                                    scope.$modalCancel();
                                }
                            }
                        }]
                });
            }
            $scope.showConfirmModal = function(ttl, msg, fn){
                $dialog.dialog(null, {
                    template: msg,
                    backdropClick: false,
                    title: ttl,
                    okCss:"Btn__danger",
                    ok:{label:"delete",fn:fn}
                });
            };

            /* ************************ */
            /* RCM for termsheet view   */
            /* ************************ */

            $scope.defineRightMenuItems = [
                {"label": "Cell data", "callback":"RCM_ts_show_data", rightIcon:"icon-arrow-right", showExtra:"cell-data"},
                {"label": "Make option", "callback":"RCM_ts_handle_options"}
                /*{"separator": true},
                 {"label": "Clear all selections", "callback":"RCMclearAllSelections"}*/
            ]
            $scope.rcmAction = "";
            $scope.getRcmAction = function(){
                return $scope.rcmAction;
            };
            $scope.setRcmAction = function(acn){
                $scope.rcmAction = acn;
            };
            $scope.dialogCancel = function(){
                $scope.rcmAction = "";
            };
            $scope.initRightMenu = function(eltNode, evt){
                //console.log("$scope.currentParagraphId",$scope.currentParagraphId, $scope.currentParagraph.attr("id"));
                var rightMenuItems = [];
                var items = $scope.defineRightMenuItems;
                angular.forEach(items, function(item){
                    item.vars = evt;
                    if(item.showExtra){
                        if(item.showExtra=="cell-data"){
                            item.extra = $scope.RCM_ts_get_data(evt)
                        }
                    }
                    if((item.showExtra && item.extra) || !item.showExtra) {
                        if (item.display) {
                            if (eval(item.display)) {
                                rightMenuItems.push(item);
                            }
                        } else {
                            if (item.disableFunc) {
                                if (typeof(item.disableFunc) == "boolean") {
                                    item.disable = item.disableFunc;
                                } else {
                                    item.disable = $scope[item.disableFunc]();
                                }
                            }
                            rightMenuItems.push(item);
                        }
                    }
                })

                //console.log("rightMenuItems",rightMenuItems);
                $scope.rightMenuItems = rightMenuItems;
            }
            $scope.clickItem = function(callBack, vars){
                //console.log("clickItem > callBack:", callBack, vars);
                $scope[callBack](vars);
            }

            $scope.RCM_ts_get_data = function(vars){
                if(!$(vars.target).attr('rcm-data')){
                    return false;
                }

                var RCM_data = JSON.parse(JSON.stringify(eval("(" + $(vars.target).attr('rcm-data') + ")")));
                //console.log("RCM_data",RCM_data);
                return RCM_data;
            }
            $scope.RCM_ts_handle_options = function(vars){

                if(!$(vars.target).attr('opt-data')){
                    return false;
                }
                var options_data = JSON.parse(JSON.stringify(eval("(" + $(vars.target).attr('opt-data') + ")")));
                var chart_id = options_data.chart_id;
                var row_id = options_data.row_id;
                console.log("chart_id",chart_id, "row_id", row_id);

                termsheetServ.makeRowOption(row_id, 1)

                var obj = {};
                if($scope.tsChartsOptions) {
                    obj = $scope.tsChartsOptions;
                }
                if(!angular.isDefined(obj[stateParams.orgId])){
                    obj[stateParams.orgId] = {};
                }
                if(!angular.isDefined(obj[stateParams.orgId][stateParams.doc])){
                    obj[stateParams.orgId][stateParams.doc] = {};
                }
                if(!angular.isDefined(obj[stateParams.orgId][stateParams.doc][chart_id])){
                    obj[stateParams.orgId][stateParams.doc][chart_id] = {};
                }
                if(angular.isDefined(obj[stateParams.orgId][stateParams.doc][chart_id][row_id])){
                    obj[stateParams.orgId][stateParams.doc][chart_id][row_id] = !obj[stateParams.orgId][stateParams.doc][chart_id][row_id];
                }else{
                    obj[stateParams.orgId][stateParams.doc][chart_id][row_id] = true;
                }
                localStorage.setItem("tsChartsOptions", JSON.stringify(obj));

                /*var obj = {};
                 obj[stateParams.orgId] = {};
                 obj[stateParams.orgId][stateParams.doc] = {};
                 obj[stateParams.orgId][stateParams.doc][46] = {};
                 obj[stateParams.orgId][stateParams.doc][46][47] = true;
                 localStorage.setItem("tsChartsOptions", JSON.stringify(obj));*/

            }
        }
    ]);
});
