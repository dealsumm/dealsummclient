define(function (require) {
    'use strict';
    var app = require('appModule');

    app.register.controller('linesheetCtrl',
                ['$scope', 'docServ', '$state', '$stateParams', 'GLOBAL_VARS', 'filterFilter', '$cookies', '$dialog', '$rootScope', '$timeout',
        function ($scope,   docServ,   $state,   $stateParams,   GLOBAL_VARS,   filterFilter,   $cookies,   $dialog, $rootScope, $timeout) {

            $scope.docUrl = "";

            //$scope.hideDocColorsOnInit = true;
            $scope.isCached = false;
            var stateParams = $rootScope.$stateParams;
            $scope.nodeId = stateParams.nodeId;
            $scope.nodeType = stateParams.nodeType;
            $scope.lineId = stateParams.sectionId;

            console.log("nodeType",$scope.nodeType, $scope.nodeId, $scope.lineId);

            // temp
            //$scope.item_title = "type: Lease / line: Document / scope: Folder-Phoenix Arizona Properties";
            //$scope.item_id = $scope.nodeId;
            docServ.getTsLong($scope.nodeId, $scope.nodeType, $scope.lineId).then(function(tsLongJSON) {
                console.log("tsLongJSON", tsLongJSON.data);
                $scope.initAfterLoaded(tsLongJSON.data);
                //$scope.linesheetTstData = JSON.parse(localStorage.getItem("linesheetTstData"));

                //console.log("linesheetTstData",$scope.linesheetTstData);
                //, JSON.stringify(node)
            })
            $scope.clickedOnCell = function(cell){
                $scope.selectedPhrase = cell.phrase;

                if($scope.docId == cell.doc){
                    // no need to load new document
                    $scope.markSectionOnDoc()
                }else{
                    $scope.docId = cell.doc;
                    $scope.docScope.token_spans = null;  // reset the selected SPANs array
                    docServ.getDocDetails($scope.docId, $scope.nodeId, $scope.docScope);
                }
            }
            $scope.setFocusOnFirstResult = function(){
                if($scope.tableData.length>0){
                    var firstDoc = null;//$scope.tableData[0].rows[0][0];
                    angular.forEach($scope.tableData, function(section, idx){
                        if(!firstDoc && section.doc_id){
                            firstDoc = section.doc_id;
                        }
                    })

                    if(firstDoc){
                        $scope.docId = firstDoc;
                        $scope.section = 1;
                        $scope.docScope.doc = $scope.docId;
                        $scope.docScope.docUrl = ""; // to empty the doc container
                        //console.log("$scope.tableData[0].head.doc",$scope.tableData[0].head.doc);
                        //console.log("docScope",docScope);
                        docServ.getDocDetails($scope.docId, $scope.nodeId, $scope.docScope);
                    }
                }else{
                    console.log("no parsing - no data found in result");
                }
            }
            $scope.loadDocument = function(section){
                console.log("section",$scope.docScope);
                if($scope.docId != section.doc_id){
                    $scope.docId = section.doc_id;
                    $scope.docScope.doc = $scope.docId;
                    $scope.docScope.docUrl = ""; // to empty the doc container
                    docServ.getDocDetails($scope.docId, $scope.nodeId, $scope.docScope);
                }
            }
            $scope.markSectionOnDoc = function(){
                if($scope.selectedPhrase){
                    if($scope.nodeType=="doc") {
                        $scope.relevantView = "app.linesheetView.docPhrase";
                    }else {
                        $scope.relevantView = "app.linesheetView.phrase";
                    }
                    $state.go($scope.relevantView, {doc:$scope.docId, phrase:$scope.selectedPhrase});
                }else{
                    if($scope.nodeType=="doc") {
                        $scope.relevantView = "app.linesheetView.docSection";
                    } else {
                        $scope.relevantView = "app.linesheetView.section";
                    }
                    $state.go($scope.relevantView, {doc:$scope.docId, sectionId:$scope.section});
                }
            }
            $scope.docLoadedFunc = null;
            $scope.initAfterLoaded = function(data){
                $scope.tableData = data;
                $scope.docScope = angular.element($("#doc_container")).scope();
                //console.log("$scope.docScope.docLoaded",$scope.docScope.docLoaded);
                if(!$scope.docLoadedFunc){
                    // extend the docLoad function
                    // do this only one time
                    $scope.docLoadedFunc = $scope.docScope.docLoaded;
                    $scope.docScope.docLoaded = function(){
                        $scope.docLoadedFunc.apply(this, arguments);
                        $scope.markSectionOnDoc();
                    }
                }
                $scope.setFocusOnFirstResult();
            }

            $scope.linesheetSectionsModal = function(vars){
                var node = {id:$scope.nodeId, rel:$scope.nodeType, label:""};
                //console.log("node",node);
                $dialog.dialog(STATIC_PREFIX+APP_FOLDER+'partials/views/linesheetSection.html', {
                    backdropClick: false,
                    title: "TST selection",
                    controller: "linesheetSelectionCtrl",
                    ok:{label:"select",
                        fn:function(){
                            //console.log("selectedTst", $scope.selectedTst)
                            if($scope.selectedTst){
                                $scope.linesheetChoosen($scope.selectedTst, node);
                                $scope.$modalCancel;
                            }
                        }
                    },
                    scope: $scope,
                    params: {node_id: $scope.fix_id(node.id), rel: node.rel, node_label: node.label }
                });
            }
            $scope.linesheetChoosen = function(selectedTst, node){
                //var linesheetTstData = {rel:node.rel, label:node.label, tst_id:selectedTst.tst_id, tst_name:selectedTst.name}
                //localStorage.setItem("linesheetTstData", JSON.stringify(linesheetTstData));
                $state.go('app.linesheetView.docSection', {orgId: $stateParams.orgId, nodeType:node.rel, nodeId:$scope.fix_id(node.id), sectionId:selectedTst.tst_id});
            }
           /* $scope.markSectionOnDoc = function(cellData){
                console.log("cellData",cellData);

                $scope.phrase = cellData.phrase;
                // first load the document and then mark the phrase on the document
                $scope.getDocDetails(cellData.doc);


            }*/
        }
    ]);
});
