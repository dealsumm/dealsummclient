define(function (require) {
    'use strict';

    var app = require('appModule');

    app.register.service('termsheetServ', ['$http', '$timeout','$q', 'DEALSUMM_CONFIG', 'GLOBAL_VARS', '$cookies', '$rootScope',
        function ($http, $timeout, $q, DEALSUMM_CONFIG, GLOBAL_VARS, $cookies, $rootScope) {

            var returnObject = {}, promise;
            var basePath = DEALSUMM_CONFIG.REST_PATH;// + GLOBAL_VARS.orgId;
            var newBasePath = DEALSUMM_CONFIG.DSPROD_REST_PATH;

            // SCHEMA related
            returnObject.reorderSections = function (doc_id, sections_order, tst_id) {
                var data = {};
                data['doc_id'] = doc_id;
                data['fields_new_order'] = sections_order;
                data['tst_id'] = tst_id;
                var url = basePath + $rootScope.$stateParams.orgId + '/reorderTstFields/';
                console.log("sections_order",data, url);
                return $http.post(url, data);
            };

            returnObject.removeSection = function (doc_id, field_id, tst_id) {
                var data = {};
                data['doc_id'] = doc_id;
                data['field_id'] = field_id;
                data['tst_id'] = tst_id;
                var url = basePath + $rootScope.$stateParams.orgId + '/removeTstField/';
                console.log("removeSection",data, url);
                return $http.post(url, data);
            };

            returnObject.renameSection = function (doc_id, field_id, new_name, tst_id) {
                var data = {};
                data['doc_id'] = doc_id;
                data['field_id'] = field_id;
                data['new_name'] = new_name;
                data['tst_id'] = tst_id;
                var url = basePath + $rootScope.$stateParams.orgId + '/renameTstField/';
                console.log("renameTstField",data, url);
                return $http.post(url, data);
            };
            returnObject.tstAction = function (new_name, acn, tst_id) {
                var data = {};
                data['new_name'] = new_name;
                data['acn'] = acn;
                data['tst_id'] = tst_id;
                var url = basePath + $rootScope.$stateParams.orgId + '/tstAction/';
                console.log("tstAction",data, url);
                return $http.post(url, data);
            };

            returnObject.renameCell = function (doc_id, new_name, tst_id, field_id, cell) {
                var data = {};
                data['doc_id'] = doc_id;
                data['new_name'] = new_name;
                data['tst_id'] = tst_id;
                data['field_id'] = field_id;
                data['renamed_cell'] = cell;
                // todo - fix this service name
                var url = basePath + $rootScope.$stateParams.orgId + '/renameCell/';
                console.log("renameCell",data, url);
                return $http.post(url, data);
            };
            returnObject.editCellNewApi = function(cell, cell_id) {
                // todo - replace constants on the following url:
                var url = newBasePath + $rootScope.$stateParams.orgId + '/projects/1/tenants/1/annotation_periods/1/properties/'  + cell_id + '/annotations/';
                var dataToSend = {
                    new_value:cell.label,
                    from_token:cell.from_token,
                    to_token:cell.to_token
                };
                promise = $http({ method: 'POST', url: url, data: angular.toJson(dataToSend)}).success(function (data, status) {
                    return data;
                });
                return promise;
            }
            /*returnObject.editCell = function (doc_id, cell_id, new_value, tst_id, field_id, main, row_id, chart_id) {
                var data = {};
                data['doc_id'] = doc_id;
                data['cell_id'] = cell_id;
                data['row_id'] = row_id;
                data['new_value'] = new_value;
                data['tst_id'] = tst_id;
                data['field_id'] = field_id;
                data['chart_id'] = chart_id;
                data['main'] = main;
                var url = basePath + $rootScope.$stateParams.orgId + '/editCell/';
                console.log("editCell",data, url);
                return $http.post(url, data);
            };*/
            returnObject.approveLine = function (doc_id, value, tst_id, field_id) {
                var data = {};
                data['doc_id'] = doc_id;
                data['value'] = value; // true/false
                data['tst_id'] = tst_id;
                data['field_id'] = field_id;
                var url = basePath + $rootScope.$stateParams.orgId + '/approveLine/';
                console.log("approveLine",data, url);
                return $http.post(url, data);
            };
            returnObject.approveDocument = function (doc_id, value, tst_id) {
                var data = {};
                data['doc_id'] = doc_id;
                data['value'] = value; // true/false
                data['tst_id'] = tst_id;
                var url = basePath + $rootScope.$stateParams.orgId + '/approveDocument/';
                console.log("approveDocument",data, url);
                return $http.post(url, data);
            };
            returnObject.removeHeaderCell = function (doc_id, field_id, cell, tst_id) {
                var data = {};
                data['doc_id'] = doc_id;
                data['field_id'] = field_id;
                data['removed_cell'] = cell;
                data['tst_id'] = tst_id;
                var url = basePath + $rootScope.$stateParams.orgId + '/removeCell/';
                console.log("removeCell",data, url);
                return $http.post(url, data);
            };

            returnObject.reorderCells = function (doc_id, field_id, cells_order, tst_id) {
                var data = {};
                data['doc_id'] = doc_id;
                data['field_id'] = field_id;
                data['cells_new_order'] = cells_order;
                data['tst_id'] = tst_id;
                var url = basePath + $rootScope.$stateParams.orgId + '/reorderCells/';
                console.log("cells_new_order",data, url);
                return $http.post(url, data);
            };


            // CONTENT related
            returnObject.makePrimary = function (doc_id, section_id, primary_line, tst_id) {
                var data = {};
                data['doc_id'] = doc_id;
                data['section_id'] =  section_id;
                data['primary_line'] =  primary_line;
                data['tst_id'] =  tst_id;

                var url = basePath + $rootScope.$stateParams.orgId + '/makePrimary/';
                console.log("makePrimary",data, url);
                return $http.post(url, data);
            };
            returnObject.addRow = function (doc_id, section_id, tst_id) {
                var data = {};
                data['doc_id'] = doc_id;
                data['section_id'] =  section_id;
                data['tst_id'] =  tst_id;

                var url = basePath + $rootScope.$stateParams.orgId + '/insertNewOption/';
                console.log("addRow",data, url);
                return $http.post(url, data);
            };
            returnObject.addRowChart = function (doc_id, chart_id, section_id, tst_id, pos, row_id) {
                var data = {};
                data['doc_id'] = doc_id;
                data['chart_id'] =  chart_id;
                data['section_id'] =  section_id;
                data['tst_id'] =  tst_id;
                data['pos'] =  pos;    // String: 'before' / 'after'
                data['row_id'] =  row_id;    // position is related to this row_id

                var url = basePath + $rootScope.$stateParams.orgId + '/insertNewOptionChart/';
                console.log("addRowChart",data, url);
                return $http.post(url, data);
            };
            returnObject.makeRowOption = function (row_id, value) {
                var data = {};
                data['row_id'] =  row_id;
                data['setAsOption'] = value;

                var url = basePath + $rootScope.$stateParams.orgId + '/setChartRowAsOption/';

                return $http.post(url, data);
            };
            returnObject.updateScore = function (chart_id, score) {
                var data = {};
                data['score'] = score;
                var url = basePath + $rootScope.$stateParams.orgId + '/charts/' + chart_id + '/';
                console.log("updateScore",data, url);
                return $http.post(url, data);
            };
            returnObject.toggleIgnore = function (chart_id, row_id, ignore) {
                var data = {};
                data['ignore'] = ignore;
                var url = basePath + $rootScope.$stateParams.orgId + '/charts/' + chart_id + '/periods/' + row_id + '/';
                //console.log("toggleIgnore",data, url);
                promise = $http({ method: 'PATCH', url: url, data: angular.toJson(data)});

                return promise;
            };
            returnObject.toggleIgnoreColumn = function (chart_id, column_no, ignore) {
                var data = {};
                data['ignore'] = ignore;
                var url = basePath + $rootScope.$stateParams.orgId + '/charts/' + chart_id + '/columns/' + column_no + '/';
                //console.log("toggleIgnore",data, url);
                promise = $http({ method: 'PATCH', url: url, data: angular.toJson(data)});

                return promise;
            };
            returnObject.renameHeader = function (chart_id, column_no, new_name) {
                var data = {};
                data['name'] = new_name;
                var url = basePath + $rootScope.$stateParams.orgId + '/charts/' + chart_id + '/columns/' + column_no + '/';
                promise = $http({ method: 'PATCH', url: url, data: angular.toJson(data)});

                return promise;
            };
            returnObject.addNewRow = function (chart_id, row_no) {
                var data = {};
                data['row_no'] = row_no;
                //data['cells'] = cells;
                var url = basePath + $rootScope.$stateParams.orgId + '/charts/' + chart_id + '/periods/';
                //console.log("addNewRow",data, url);
                promise = $http({ method: 'POST', url: url, data: angular.toJson(data)});

                return promise;
            };
            returnObject.addNewColumn = function (chart_id, column_name) {
                var data = {};

                data['column_name'] = column_name;
                //data['cells'] = cells;
                var url = basePath + $rootScope.$stateParams.orgId + '/charts/' + chart_id + '/columns/';
                //console.log("addNewRow",data, url);
                promise = $http({ method: 'POST', url: url, data: angular.toJson(data)});

                return promise;
            };

            return returnObject;

    }]);


});



