define(function (require) {
    'use strict';
    var app = require('appModule');

    app.register.controller('searchCtrl',
                ['$scope', '$state', 'generalServ', '$stateParams', 'GLOBAL_VARS', '$filter', '$fileUploader', '$cookies', '$dialog', '$rootScope', 'docServ', 'localStorageService', '$timeout', '$location',
        function ($scope,   $state, generalServ,  $stateParams,   GLOBAL_VARS,   $filter,   $fileUploader,   $cookies,   $dialog, $rootScope, docServ, localStorageService, $timeout, $location) {


            //$scope.hideDocColorsOnInit = true;
            var stateParams = $rootScope.$stateParams;
            $scope.isCached = false;
            $scope.nodeId = stateParams.nodeId;
            $scope.nodeType = stateParams.nodeType;
            $scope.docId = stateParams.doc;
            $scope.sectionId = stateParams.sectionId;
            $scope.labelingMode = localStorageService.get("labelingMode")=="false"?false:true;
            localStorageService.set("searchCtrl", {nodeId:$scope.nodeId, nodeType:$scope.nodeType});
            $scope.classCategories = null;
            $scope.savedQueryTemplates = null;
            $scope.search_history_id = null;
            function initSearch(phraseObj){
                var searchData = typeof phraseObj=='object'?phraseObj:localStorageService.get("searchData");
                console.log("searchData",searchData);
                //console.log("forceRefresh",forceRefresh);
                var search_page = null;
                var dataToSend = {};
                dataToSend['id'] = $scope.nodeId;
                dataToSend['rel'] = $scope.nodeType;
                dataToSend['search'] = 'true'; // pass the command
                angular.extend(dataToSend, searchData);
                if($location.search().page){
                    search_page = "?page=" + $location.search().page;
                }
                $scope.tableData = null;
                generalServ.http('search', 'runSearch',[$scope.nodeType, $scope.nodeId, search_page], dataToSend).then(function(resp) {
                    console.log("searchJSON", resp.data);
                    if(angular.isDefined(resp.data['search_history_id'])){
                        $scope.search_history_id = resp.data['search_history_id'];
                        $scope.getSearchHistoryItem();
                    }
                    $scope.initAfterLoaded(resp.data);
                });
                if(!$scope.classCategories){
                    generalServ.http('category', 'getClassCategories',[]).then(function(resp) {
                        console.log("getClassCategories", resp.data);
                        $scope.classCategories = resp.data;
                    });
                }
                if(!$scope.savedQueryTemplates){
                    generalServ.http('search', 'getQueryTemplates',[]).then(function(resp) {
                        console.log("getQueryTemplates", resp.data);
                        $scope.savedQueryTemplates = resp.data;
                    });
                }
            }
            initSearch();
            $scope.moreResults = function(page_number){
                if(angular.isDefined(page_number) && !isNaN(page_number) && page_number>0){
                    $state.current.reloadOnSearch = false;
                    $location.search('page', page_number);
                    initSearch();
                    $timeout(function () {
                        $state.current.reloadOnSearch = undefined;
                    });
                }
            }
            $scope.showTemplates = {};
            $scope.showTemplates.picker = false;
            $scope.selectedTemplate = {};
            $scope.newQueryTemplateName = "";
            $scope.saveQueryTemplate = function(){
                var dataToSend = {};
                dataToSend['display_name'] = $scope.newQueryTemplateName;
                dataToSend['search_history_id'] = $scope.search_history_id;
                generalServ.http('search', 'setNewQueryTemplateName', [], dataToSend).then(function(resp){
                    $scope.newQueryTemplateName = "";
                    console.log("setNewQueryTemplateName",resp.data);
                });
            }
            /*$scope.updateQueryTemplateName = function(cat){
                var dataToSend = {};
                dataToSend['name'] = cat.tempName;
                generalServ.http('search', 'updateQueryTemplateName', [cat.id], dataToSend).then(function(resp){
                    console.log("updateQueryTemplateName",resp.data);
                    cat.display_name = resp.data.display_name;
                    cat.tempName = resp.data.name;
                })
            }*/
            /*$scope.setQueryTemplate = function(cat){
                $scope.showTemplates.picker = false;
                $scope.selectedTemplate = {};
                $scope.selectedTemplate.name = cat.display_name || cat.name;
                $scope.selectedTemplate.id = cat.id;
            }*/
            $scope.getSearchHistoryItem = function(){
                generalServ.http('search', 'getSearchHistoryItem',[$scope.search_history_id]).then(function(resp) {
                    $scope.search_history = resp.data;
                });
            };
            $scope.newSearchPhrase = null;
            $scope.applyHistoryToSearch = function(){
                $scope.newSearchPhrase = $scope.search_history?$scope.search_history['query_text']:"";
                $("#searchPhrase").focus();
            }
            $scope.runSearchAgain = function(){
                if($scope.newSearchPhrase && $scope.newSearchPhrase.length>0){
                    if($location.search().page){
                        $state.current.reloadOnSearch = false;
                        $location.search('page', '1');
                        $timeout(function () {
                            $state.current.reloadOnSearch = undefined;
                        });
                    }
                    var newSearchPhrase = $scope.newSearchPhrase;
                    initSearch({phrase: newSearchPhrase});
                    localStorageService.set("searchData", {phrase: newSearchPhrase});
                    $scope.newSearchPhrase = null;
                    $("#searchPhrase").blur();
                }
            }
            $scope.clickedLabeling = function() {
                $scope.labelingMode = !$scope.labelingMode;
                localStorageService.set("labelingMode", $scope.labelingMode);
                if(!$scope.labelingMode){
                    $scope.selectAll(false);
                }
            }
            $scope.clickedOnLine = function(node, docId, type, idx){
                //.log("node, docId, type, idx",node, docId, type, idx);
                $scope.selectedPhraseIndex = null;
                $scope.selectedPhrase = null;
                var saveSectionId = $scope.sectionId;
                $scope.sectionId = null;
                if(type=="phrase"){
                    $scope.selectedPhraseIndex = idx;
                    $scope.selectedPhrase = node;
                }else{
                    $scope.sectionId = node;
                }

                if($scope.docId == docId){
                    if($scope.sectionId == saveSectionId){
                        docServ.docScroll("p#" + $scope.sectionId, $scope.docScope, "#doc_container", false);
                        //var item = $("p#" + $scope.sectionId);
                        //docServ.highlightSection(item);
                    }else{
                        // no need to load new document
                        $scope.markSectionOnDoc();
                    }
                }else{
                    $scope.docId = docId;
                    $scope.docScope.token_spans = null; // reset the selected SPANs array
                    docServ.getDocDetails($scope.docId, $scope.nodeId, $scope.docScope);
                }
                var dataToSend = {};
                dataToSend['last_document'] =  $scope.docId;
                dataToSend['last_pid'] = $scope.sectionId;
                generalServ.http('search', 'setSearchHistoryItem',[$scope.search_history_id], dataToSend).then(function(resp) {
                    console.log("setSearchHistoryItem", resp);
                });
                //
                //console.log("section",$scope.section, doc);
            }
            $scope.markSectionOnDoc = function(){
                if($scope.selectedPhrase){
                    if($scope.nodeType=="doc") {
                        $scope.relevantView = "app.searchView.docPhrase";
                    }else {
                        $scope.relevantView = "app.searchView.phrase";
                    }
                    $state.go($scope.relevantView, {doc:$scope.docId, phrase:$scope.selectedPhrase});
                }else if($scope.sectionId){
                    if($scope.nodeType=="doc") {
                        $scope.relevantView = "app.searchView.docSection";
                    } else {
                        $scope.relevantView = "app.searchView.section";
                    }
                    //console.log("$scope.sectionId",$scope.sectionId);
                    $state.go($scope.relevantView, {doc:$scope.docId, sectionId:$scope.sectionId});
                }
            }
            $scope.setFocusOnFirstResult = function(docId, sectionId){
                if($scope.tableData.length>0){
                    for(var i=0;i<$scope.tableData.length;i++){
                        var doc = $scope.tableData[i];
                        if(doc.head && docId?doc.head.doc == docId:doc.head.doc){
                            $scope.docId = doc.head.doc;
                            for(var j=0;j<doc.list.length;j++){
                                if(sectionId ? doc.list[j].pid==sectionId : doc.list[j].pid){
                                    $scope.sectionId = doc.list[j].pid;
                                    break;
                                }
                            }
                            break;
                        }
                    }
                    $scope.docScope.doc = $scope.docId;
                    docServ.getDocDetails($scope.docId, $scope.nodeId, $scope.docScope);
                }
            }
            $scope.showCategories = {};
            $scope.showCategories.range = false;
            $scope.selectedCategory = null;
            $scope.newCategory = null;

            $scope.allSelected = false;
            $scope.selectAll = function(acn){
                angular.forEach($scope.checkedItems, function(doc, doc_id){
                    doc.doc = !acn;
                    $scope.clickedDocCheck(doc_id);
                    doc.doc = acn;
                })
            }
            $scope.setNewCategory = function(newName){
                var dataToSend = {};
                dataToSend['name'] = newName;
                generalServ.http('category', 'newCategory', [], dataToSend).then(function(resp){
                    $scope.showCategories.range = false;
                    $scope.selectedCategory = {};
                    $scope.selectedCategory.name = resp.data.display_name || resp.data.name;
                    $scope.selectedCategory.id = resp.data.id;
                    $scope.classCategories.push(resp.data);
                    console.log("newCategory",resp.data);
                });
            }

            $scope.updateCategoryName = function(cat){
                var dataToSend = {};
                dataToSend['name'] = cat.tempName;
                generalServ.http('category', 'updateCategoryName', [cat.id], dataToSend).then(function(resp){
                    console.log("updateCategoryName",resp.data);
                    cat.display_name = resp.data.display_name;
                    cat.tempName = resp.data.name;
                })
            }
            $scope.setCategory = function(cat){
                $scope.showCategories.range = false;
                $scope.selectedCategory = {};
                $scope.selectedCategory.name = cat.display_name || cat.name;
                $scope.selectedCategory.id = cat.id;
            };
            $scope.assignCategoryToParagraph = function(){
                //console.log("$scope.selectedLabels.merged", $scope.selectedLabels.merged, participatingLabelId);
                var docs = {}, item;
                for(var i=0;i<$scope.SelectedParagraphs.length;i++){
                    item = $scope.SelectedParagraphs[i];
                    if(!docs[item.doc_id]){
                        docs[item.doc_id] = [];
                    }
                    docs[item.doc_id].push(item.pid);
                }
                var arrayToSend = [], dataToSend;
                angular.forEach(docs, function(docsArray, doc_id){
                    dataToSend = {};
                    dataToSend['pids'] = docsArray;
                    dataToSend['document'] = doc_id;
                    arrayToSend.push(dataToSend);
                })
                $scope.selectAll(false);
                console.log("arrayToSend",arrayToSend);
                //console.log("docs > should be submitted to server:",arrayToSend);
                generalServ.http('category', 'setCategoryToParagraphs', [$scope.selectedCategory.id], arrayToSend).then(function(resp){
                    console.log("search setCategoryToParagraphs", resp.data);
                    $scope.selectedCategory = {};
                    onCategoryAssignSuccess(resp.data)
                })
            };
            function onCategoryAssignSuccess(data){
                var prepareData = [];
                for(var i=0;i<data.length;i++){
                    if(data[i].doc==$scope.docId){
                        prepareData.push(data[i]);
                    }
                }
                console.log("prepareData for doc:",$scope.docId,prepareData);
                $scope.docScope.updateFromSearchResults(prepareData);
            }

            $scope.SelectedDocs = [];
            $scope.SelectedParagraphs = [];
            $scope.showCategoryInfo = function(){
                //console.log("$scope.checkedItems",$scope.checkedItems);
                $scope.SelectedDocs = [];
                $scope.SelectedParagraphs = [];
                angular.forEach($scope.checkedItems, function(doc, doc_id){
                    //console.log("doc",doc, doc_id);
                    if(doc.doc){
                        $scope.SelectedDocs.push(doc_id);
                    }
                    angular.forEach(doc.list, function(item){
                        if(item.value){
                            $scope.SelectedParagraphs.push({doc_id:doc_id, pid: item.pid});
                        }
                    })
                })

                if($scope.SelectedDocs.length == $scope.stats_obj.no_of_result_files && $scope.SelectedParagraphs.length == $scope.stats_obj.no_of_result_docs){
                    $scope.allSelected = true;
                }else{
                    $scope.allSelected = false;
                }
                //console.log("SelectedDocs",$scope.SelectedDocs, $scope.stats_obj.no_of_result_docs, "SelectedParagraphs", $scope.SelectedParagraphs, $scope.stats_obj.no_of_result_files);
            }
            $scope.clickedDocCheck = function(doc_id){
                //console.log("$scope.checkedItems",$scope.checkedItems);
                // click event fired BEFORE the value has changed
                var doc_stat = !$scope.checkedItems[doc_id].doc;
                var list = $scope.checkedItems[doc_id].list;
                angular.forEach(list, function(item){
                    item.value = doc_stat;
                })
                $timeout(function(){
                    $scope.showCategoryInfo();
                }, 100);

            }
            $scope.changedParaCheck = function(doc_id){
                var list = $scope.checkedItems[doc_id].list;
                //console.log("list",list);
                var hasFalseValues = 0;
                angular.forEach(list, function(item){
                    if(item.value==false){
                        hasFalseValues++;
                    }
                })
                //console.log("hasFalseValues",hasFalseValues);
                if(hasFalseValues==0){
                    $scope.checkedItems[doc_id].doc = true;
                    $scope.checkedItems[doc_id].blink = 7; // must be odd number
                    blinkSelectedDoc(doc_id);
                }else{
                    $scope.checkedItems[doc_id].doc = false;
                    $scope.checkedItems[doc_id].clicked = false;
                }
                $scope.showCategoryInfo();
            }
            var blinkSelectedDoc = function(doc_id){
                 if($scope.checkedItems[doc_id].blink>0){
                     $scope.checkedItems[doc_id].blink--;
                     $timeout(function(){
                         $scope.checkedItems[doc_id].clicked = $scope.checkedItems[doc_id].blink%2==0;
                         blinkSelectedDoc(doc_id);
                     }, 120);
                 }else{
                     $scope.checkedItems[doc_id].clicked = false;
                 }
            }
            $scope.docLoadedFunc = null;
            $scope.initCheckedItems = function(docList){
                $scope.checkedItems = {};
                var doc_id;
                angular.forEach(docList, function(doc){
                    doc_id = doc.head.doc;
                    if(!$scope.checkedItems[doc_id]){
                        $scope.checkedItems[doc_id] = {};
                    }
                    $scope.checkedItems[doc_id].doc = false;
                    $scope.checkedItems[doc_id].clicked = false;
                    if(!$scope.checkedItems[doc_id].list){
                        $scope.checkedItems[doc_id].list = {};
                    }
                    for(var l=0;l<doc.list.length;l++){
                        $scope.checkedItems[doc_id].list[doc.list[l].pid] = {pid: doc.list[l].pid, value: false};
                    }
                })
                console.log("$scope.checkedItems",$scope.checkedItems);
            }
            $scope.initAfterLoaded = function(data){
                $scope.initCheckedItems(data.docs);
                $scope.tableData = data.docs;
                $scope.obj_type = data.info.obj_type;
                $scope.obj_name = data.info.obj_name;
                $scope.obj_id = data.info.obj_id;
                $scope.phrase = data.info.phrase;
                $scope.stats_obj = data.info.stats_obj;
                $scope.nextResultPage = data.info.nextResultPage;
                $scope.prevResultPage = data.info.prevResultPage;
                $scope.currentResultPage = data.info.currentResultPage;

                $scope.docScope = angular.element($("#doc_container")).scope();
                if($scope.tableData.length==0 || ($scope.tableData[0].list && $scope.tableData[0].list.length==0)){
                    $scope.docScope.error400 = "No result found";
                }

                //console.log("$scope.docScope.docLoaded",$scope.docScope.docLoaded);
                if(!$scope.docLoadedFunc){
                    // extend the docLoad function
                    // do this only one time
                    $scope.docLoadedFunc = $scope.docScope.docLoaded;
                    $scope.docScope.docLoaded = function(){
                        $scope.docLoadedFunc.apply(this, arguments);
                        $scope.markSectionOnDoc();
                    }
                }

                $scope.setFocusOnFirstResult($scope.docId, $scope.sectionId);

            }

        }
    ]);
});
