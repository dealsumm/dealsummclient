define(function (require) {
    'use strict';
    var app = require('appModule');

    app.register.controller('classAssignCtrl',
                ['$scope', 'docServ', '$state', 'GLOBAL_VARS', 'filterFilter', '$fileUploader', '$cookies', '$dialog', '$timeout',
        function ($scope,   docServ,   $state, GLOBAL_VARS, filterFilter, $fileUploader, $cookies, $dialog, $timeout) {
            $scope.classType = 'presetClass';
            //console.log("params", $scope.params, $scope.params.rel)
            console.log("docClasses",$scope.docClasses);
            $scope.assignClassSubmit = function(){
                var dataToSend = {};
                var firstAndLast = $scope.getFirstAndLast($scope.selectedLabels.paragraphs);
                dataToSend.f_pid = firstAndLast[0];
                dataToSend.l_pid = firstAndLast[1];

                //dataToSend.did = $scope.doc;
                //dataToSend.paragraphs = $scope.params.arr;
                //dataToSend.objId = $scope.params.objId;
                dataToSend.class_id = $scope.classType=='presetClass'?$scope.chosenClass:-1;
                dataToSend.new_class_name = $scope.classType=='newClass'?$scope.ownClass:-1;
                //console.log("assignClass>dataToSend:", dataToSend);
                //$scope.clearAllSelections();
                $scope.assignClass($scope.doc, dataToSend);
                $scope.$modalCancel();
            }
        }
    ]);
});
