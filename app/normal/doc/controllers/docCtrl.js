define(function (require) {
    'use strict';
    var app = require('appModule');

    app.register.controller('docCtrl',
        ['$scope', 'docServ', 'generalServ', '$state', '$stateParams', 'GLOBAL_VARS', '$filter', '$fileUploader', '$cookies', '$dialog', '$rootScope', '$timeout', '$window', 'localStorageService',
            function ($scope,   docServ, generalServ,   $state,   $stateParams,   GLOBAL_VARS,   $filter,   $fileUploader,   $cookies,   $dialog, $rootScope, $timeout, $window, localStorageService) {

                //console.log("docCtrl", $state, $stateParams);
                $scope.docUrl = "";
                var stateParams = $rootScope.$stateParams;

                //$scope.hideLabels = angular.isDefined($scope.hideDocColorsOnInit)?$scope.hideDocColorsOnInit:true;
                $scope.labelingMode = localStorageService.get("labelingMode")=="false"?false:true;
                //$scope.showDocName = true;
                $scope.hideLabels = localStorageService.get("hideLabels")=="false"?false:true;
                //console.log("hideLabels",$scope.hideLabels);

                $scope.clickedLabelEye = function(){
                    localStorageService.set("hideLabels", $scope.hideLabels);
                    //console.log("clickedLabelEye > hideLabels",$scope.hideLabels);
                }
                $scope.getTopmostParagraph = function(array, goal){
                    var ret = {}, n = array.length;
                    for (var i=0; i < n; ++i) {
                        //console.log(i, "array[i]",array[i].top, ">", goal);
                        if (array[i].top > goal) {
                            ret.top = array[i].top;
                            ret.id = array[i].id;
                            break;
                        }
                    }
                    ret.height = $("#doc_holder").height();
                    return ret;
                };
                var saveDocScrollPosition = function(){
                    //console.log("scrollTop1", $("#doc_container").scrollTop());
                    $scope.allParagraphs = mapAllParagraphs();
                    var topmostParagraph = $scope.getTopmostParagraph($scope.allParagraphs, $("#doc_container").scrollTop());
                    $scope.topmostParagraphId = topmostParagraph.id;
                    $scope.topmostGap = topmostParagraph.top - $("#doc_container").scrollTop();
                    $scope.topmostDocHeight = topmostParagraph.height;
                };
                var applySavedDocScrollPosition = function(){
                    if($scope.topmostParagraphId){
                        // multiple the gap between longer vs. shorter documents (not sure it helps)
                        var multiple = 0;
                        var multiple1 = Math.round($scope.topmostDocHeight / $("#doc_holder").height() * 100) / 100;
                        var multiple2 = Math.round($("#doc_holder").height() / $scope.topmostDocHeight * 100) / 100;
                        if($scope.topmostDocHeight < $("#doc_holder").height()){
                            multiple = Math.max(multiple1, multiple2);
                        }else{
                            multiple = Math.min(multiple1, multiple2);
                        }

                        //console.log(">>",multiple, multiple1, multiple2);

                        var newTop = $("p#" + $scope.topmostParagraphId).position().top - ($scope.topmostGap * multiple);
                        //console.log("newTop",$("p#" + $scope.topmostParagraphId).position().top , $scope.topmostParagraphId);
                        $("#doc_container").scrollTop(newTop);
                        //$.scrollTo("#doc_container", newTop);
                    }

                }
                $scope.clickedLabeling = function(force){
                    $scope.labelingMode = angular.isDefined(force)?force:!$scope.labelingMode;
                    localStorageService.set("labelingMode", $scope.labelingMode);
                    //console.log("paragraphsTopsArray", $scope.paragraphsTopsArray);
                    saveDocScrollPosition();
                    if($scope.labelingMode){
                        $scope.hideLabels = false;
                        //$scope.clickedLabelEye();
                        $timeout(
                            function(){
                                //console.log("scrollTop2", $("#doc_container").scrollTop());
                                $("#label_control").height($("#doc_holder").height());
                                labelPositioning($scope.objects);
                                $scope.allParagraphs = mapAllParagraphs();
                                applySavedDocScrollPosition();
                                $scope.initLabelMarkers();
                            }, 50)
                    }else{
                        $scope.clearLabelMarkers();
                        $timeout(
                            function(){
                                applySavedDocScrollPosition();
                            }, 250)
                    }
                }
                $scope.docIsLoaded = false;
                $scope.doc = stateParams.doc;
                $scope.nodeId = stateParams.nodeId;
                //console.log("doc",doc);
                $scope.getDocDetails = function(doc){
                    docServ.getDocDetails(doc, $scope.nodeId, $scope);
                }
                if($scope.doc){
                    $scope.getDocDetails($scope.doc);
                }

                $scope.docLoaded = function(){
                    $scope.generatedLabels = initLabels($scope.docLabels);
                    labelPositioning($scope.generatedLabels);
                    //console.log("generatedLabels",$scope.generatedLabels);
                    //console.log("docDetails",$scope.docDetails);
                    if($scope.labelingMode){
                        $scope.clickedLabeling(true);
                    }
                    //setSelectedParagraphs();
                };
                $scope.goToNextDoc = function(state){
                    console.log("$scope.nextDoc",$scope.nextDoc);
                    $state.go(state, {nodeId: $scope.nextDoc});
                }
                $scope.goToPrevDoc = function(state){
                    console.log("$scope.prevDoc",$scope.prevDoc);
                    $state.go(state, {nodeId: $scope.prevDoc});
                }

                $scope.parentScope = $scope;
                $scope.$on('$stateChangeStart',
                    function(event, toState, toParams, fromState, fromParams){
                        //console.log("start state change",toState.name);
                        if($scope.labelingMode){
                            if( (toState.name == "app.searchView.section" ||
                                toState.name == "app.searchView.docSection")
                                && (toParams.doc != fromParams.doc)){

                                $scope.clearLabelMarkers();
                            }
                        }
                    }
                )
                $scope.$on('$stateChangeSuccess',
                    function(event, toState, toParams, fromState, fromParams){
                        if($scope.token_spans && $scope.token_spans.length>0){
                            $scope.token_spans.removeClass("highlight");
                        }
                        $scope.highlight = null;
                        $scope.highlightPhraseDone = null;
                        $scope.doc = stateParams.doc;
                        $scope.nodeId = stateParams.nodeId;
                        if(toState.name == "app.docView.section"){
                            docServ.docScroll("p#" + toParams.sectionId, $scope, 'body');
                        }else if(toState.name == "app.docView.phrase"){
                            docServ.highlightPhrase(toParams.phrase, $scope, 'body');
                        }else if(toState.name == "app.termsheetView.phrase" ||
                            toState.name == "app.linesheetView.phrase" ||
                            toState.name == "app.rentRollView.phrase" ||
                            toState.name == "app.linesheetView.docPhrase" ||
                            toState.name == "app.searchView.docPhrase" ||
                            toState.name == "app.searchView.phrase"){
                            // for split page - TERMSHEET / LINESHEET / RENTROLL
                            // for split page - SEARCH PHRASE
                            docServ.highlightPhrase(toParams.phrase, $scope, "#doc_container");
                        }else if(toState.name == "app.trainView.section" ||
                            toState.name == "app.trainView.docSection" ||
                            toState.name == "app.predictView.section" ||
                            toState.name == "app.predictView.docSection"){
                            $scope.sectionId = toParams.sectionId;
                            if(toParams.sectionId){
                                docServ.docScroll("p#" + toParams.sectionId, $scope, "#doc_container", true);
                            }else{
                                console.log("no section id");
                            }
                        }else if( toState.name == "app.searchView.section" ||
                            toState.name == "app.searchView.docSection"){
                            $scope.sectionId = toParams.sectionId;
                            if(toParams.sectionId){
                                console.log("$scope.labelingMode",$scope.labelingMode);
                                docServ.docScroll("p#" + toParams.sectionId, $scope, "#doc_container");
                            }else{
                                console.log("no section id");
                            }
                        }
                    }
                )
                $scope.searchDoc = {};
                $scope.searchDoc.myHilitor = null;
                $scope.searchDoc.active = false;
                $scope.searchDoc.phrase = "";
                $scope.searchDoc.matches = 0;
                $scope.searchDoc.matchesIndex = -1;
                $scope.clearSearchInDocument = function(){
                    if($scope.searchDoc.myHilitor){
                        $scope.searchDoc.myHilitor.remove();
                    }
                    $scope.searchDoc.active = false;
                    $scope.searchDoc.phrase = "";
                    $scope.searchDoc.matches = 0;
                    $scope.searchDoc.matchesIndex = -1;
                    $("#searchDocPhrase").blur();
                }
                $scope.clickSearchInDocument = function(){
                    $scope.searchDoc.active = true;
                    $("#searchDocPhrase").focus();
                }
                $scope.searchInDocument = function(){
                    //console.log("$scope.searchDocPhrase",$scope.searchDoc.phrase, ":", $scope.searchDoc.active);
                    if($scope.searchDoc.active){
                        if($scope.searchDoc.phrase.length>2){
                            //console.log("$scope.searchDocPhrase",$scope.searchDoc.phrase);
                            $scope.searchDoc.myHilitor = new Hilitor2("doc_holder");
                            $scope.searchDoc.myHilitor.setMatchType("open");
                            $scope.searchDoc.myHilitor.apply($scope.searchDoc.phrase);
                            $timeout(function(){
                                $scope.searchDoc.matchesIndex = -1;
                                $scope.searchDoc.matches = $scope.getDocSearchMatches();
                                $scope.doSearchNav(1);
                            }, 100);
                        }else{
                            if($scope.searchDoc.myHilitor){
                                $scope.searchDoc.myHilitor.remove();
                            }
                            $scope.searchDoc.matchesIndex = -1;
                        }

                    }
                }
                $scope.doSearchNav = function(offset) {
                    $scope.searchDoc.matchesIndex += offset;
                    $scope.searchDoc.matchesIndex = $scope.searchDoc.matchesIndex % $scope.searchDoc.matches.length;
                    var matchElt = $($scope.searchDoc.matches[$scope.searchDoc.matchesIndex]);
                    var matchPosTop = matchElt.position().top;
                    matchPosTop += matchElt.closest("p").position().top;
                    matchPosTop -= 50;
                    $("#doc_container").scrollTo(matchPosTop, 400);
                    for (var i = 0; i < $scope.searchDoc.matches.length; i++) {
                        $scope.searchDoc.matches[i].style.outline = ($scope.searchDoc.matchesIndex == i) ? "1px solid red" : "";
                    }
                }
                $scope.getDocSearchMatches = function(){
                     return $("#doc_holder")[0].getElementsByTagName("EM");
                }
                //matchArr = container.getElementsByTagName("EM");
                $scope.startSelectionPid = null;
                $scope.paragraphs = {};// for pre-selected paragraphs
                $scope.newParagraphs = {};// for NEW selected paragraphs
                $scope.allParagraphs = [];
                $scope.paragraphsTopsArray = [];
                var mapAllParagraphs = function(){
                    $scope.paragraphsTopsArray = [];
                    var temp, elt, id, allParagraphs = [], saveElt, saveEltHeight, calcGap;
                    $("#doc_container p").each(function(){
                        id = $(this).attr('id');
                        if(id){
                            temp = $(this).position();
                            elt = {};
                            elt.left = temp.left;
                            elt.top = temp.top;
                            elt.width = $(this).outerWidth()*1;
                            elt.height = $(this).outerHeight()*1;
                            elt.label_ids = [];
                            elt.category_ids = [];
                            elt.marker_pos = 0;
                            elt.id = parseInt(id);
                            allParagraphs[id] = elt;
                            if(saveElt){
                                calcGap = Math.floor(elt.top - (saveElt.top + saveElt.height));
                                //console.log("elt.top",calcGap);
                                if(calcGap>0){
                                    saveEltHeight = allParagraphs[saveElt.id]['height'];
                                    allParagraphs[saveElt.id]['height'] = saveEltHeight + Math.floor(calcGap/2);
                                    allParagraphs[id]['top'] = elt.top - (calcGap/2);
                                    allParagraphs[id]['height'] = elt.height + (calcGap/2);
                                }
                            }
                            saveElt = elt;
                            //$scope.paragraphsTopsArray.push(elt.top);
                        }
                        //console.log("this",$(this).attr('id'));
                    })
                    allParagraphs = injectCategoriesToParagraphs(allParagraphs);
                    return allParagraphs;
                };
                var injectCategoriesToParagraphs = function(allParagraphs){
                    //console.log("$scope.generatedLabels",$scope.generatedLabels);
                    angular.forEach($scope.generatedLabels, function(obj){
                        //console.log("obj.paragraphs",obj,obj.paragraphs);
                        angular.forEach(obj.paragraphs, function(para_id) {
                            //console.log("para", para, allParagraphs[para]);
                            if(allParagraphs[para_id].id == para_id){
                                allParagraphs[para_id].label_ids.push(obj.label_id);
                                allParagraphs[para_id].category_ids.push(obj.category_id);
                            }
                        })
                    })
                    return allParagraphs;
                }
                var initLabels = function(labelsList){
                    var retObj = {}, category_id, label_id, pid, firstP, lastP, firstPosition, lastPosition;
                    angular.forEach(labelsList, function(label){
                        label_id = label.id;
                        category_id = label.category;
                        if(!retObj[label_id]){
                            retObj[label_id] = {label_id:label_id, category_id:category_id, paragraphs:[], marker_pos:0};
                        }
                        firstP = label.pid;
                        lastP = label.epid;
                        //console.log("firstP",firstP, "lastP", lastP);
                        for(var i=firstP;i<=lastP;i++){
                            retObj[label_id].paragraphs.push(i);
                            $scope.paragraphs[i] = ""+i;
                        }
                    });
                    return retObj;
                };
                var labelPositioning = function(obj){
                    angular.forEach(obj, function(label){
                        labelParagraphsPositioning(label);
                    })
                }
                var labelParagraphsPositioning = function(label){
                    var firstPosition, lastPosition;
                    if(label && label.paragraphs){
                        label.paragraphs.sort(function(a, b){return a-b});
                        // positioning
                        if(label.paragraphs.length>0){
                            firstPosition = $("p#"+label.paragraphs[0]).position();
                            lastPosition = $("p#"+label.paragraphs[label.paragraphs.length-1]).position();
                            if(firstPosition && lastPosition){
                                label.left = firstPosition.left;
                                label.width = $("p#"+label.paragraphs[0]).outerWidth();
                                label.top = firstPosition.top;
                                label.height = lastPosition.top - firstPosition.top + $("p#"+label.paragraphs[label.paragraphs.length-1]).outerHeight();
                            }
                        }
                    }
                };
                $scope.showCategories = {};
                $scope.showCategories.range = false;
                $scope.showCategories.selected = false;
                $scope.selectedLabelId = null;
                $scope.selectedCategory = null;
                $scope.newCategory = null;
                $scope.clickedAssignLabel = function(isUpdate){
                    if(isUpdate){
                        $scope.assignCategoryToParagraph();
                    }else{
                        $scope.showCategories.selected = true;
                    }
                }
                $scope.setNewCategory = function(newName){
                    var dataToSend = {};
                    dataToSend['name'] = newName;
                    //dataToSend['display_name'] = newName;
                    //dataToSend['description'] = null;
                    generalServ.http('category', 'newCategory', [], dataToSend).then(function(resp){
                        $scope.showCategories.range = false;
                        $scope.selectedCategory = {};
                        $scope.selectedCategory.name = resp.data.display_name || resp.data.name;
                        $scope.selectedCategory.id = resp.data.id;
                        $scope.classCategories.push(resp.data);
                        console.log("newCategory",resp.data);
                    });
                }
                $scope.updateCategoryName = function(cat){
                    var dataToSend = {};
                    dataToSend['name'] = cat.tempName;
                    generalServ.http('category', 'updateCategoryName', [cat.id], dataToSend).then(function(resp){
                        console.log("resp",resp);
                        cat.display_name = resp.data.display_name;
                        cat.tempName = resp.data.name;
                    })
                }
                $scope.setCategory = function(cat){
                    if(typeof cat=="string" && !isNaN(cat*1)){
                        cat = $filter('filter')($scope.classCategories, {id:cat})[0];
                    }
                    $scope.showCategories.range = false;
                    $scope.selectedCategory = {};
                    $scope.selectedCategory.name = cat.display_name || cat.name;
                    $scope.selectedCategory.id = cat.id;
                };
                $scope.removeLabel = function(){
                    var participatingLabelId = $scope.selectedLabelId;
                    var label_ids, category_ids;
                    generalServ.http('category', 'removeLabelFromParagraphs', [$scope.nodeId, $scope.doc, participatingLabelId]).then(function(resp){
                        angular.forEach($scope.generatedLabels[participatingLabelId]['paragraphs'], function(para_id){
                            label_ids = $scope.allParagraphs[para_id].label_ids;
                            category_ids = $scope.allParagraphs[para_id].category_ids;
                            if(label_ids.length==1){
                                $scope.allParagraphs[para_id].label_ids = [];
                            }else{
                                for(var i=0;i<label_ids.length;i++){
                                    if(label_ids[i]==participatingLabelId){
                                        $scope.allParagraphs[para_id].label_ids.splice(i,1);
                                        break;
                                    }
                                }
                            }
                            if(category_ids.length==1){
                                $scope.allParagraphs[para_id].category_ids = [];
                            }else{
                                for(var i=0;i<category_ids.length;i++){
                                    if(category_ids[i]==$scope.selectedCategory.id){
                                        $scope.allParagraphs[para_id].category_ids.splice(i,1);
                                        break;
                                    }
                                }
                            }
                        })
                        delete $scope.generatedLabels[participatingLabelId];
                        $scope.dismissLabelMarker();
                    })
                }
                $scope.assignCategoryToParagraph = function(){
                    var participatingLabelId = $scope.selectedLabelId;
                    //console.log("$scope.selectedLabels.merged", $scope.selectedLabels.merged, participatingLabelId);
                    var calledFunction = 'setCategoryToParagraphs';
                    var params = [$scope.selectedCategory.id];
                    if(participatingLabelId){
                        calledFunction = 'updateCategoryToParagraphs';
                        params.push(participatingLabelId);
                    }
                    var arrayToSend = [];
                    var dataToSend = {};
                    dataToSend['pids'] = $scope.selectedLabels.merged;
                    dataToSend['document'] = $scope.doc;
                    arrayToSend.push(dataToSend);
                    $scope.selectedCategory = null;
                    $scope.showCategories.selected = false;
                    $scope.dismissLabelMarker();
                    generalServ.http('category', calledFunction, params, arrayToSend).then(function(resp){
                        if(resp.status == 400){
                            $scope.error400 = "Same category conflict";
                        }else{
                            console.log("resp", calledFunction, ">",resp.data);
                            angular.extend($scope.generatedLabels, initLabels([resp.data]));
                            $scope.allParagraphs = mapAllParagraphs();
                        }
                    })
                };
                $scope.updateFromSearchResults = function(newData){
                    console.log("newData",newData);
                    angular.extend($scope.generatedLabels, initLabels(newData));
                    if($scope.labelingMode){
                        /*labelPositioning($scope.objects);
                        $scope.allParagraphs = mapAllParagraphs();
                        applySavedDocScrollPosition();
                        $scope.initLabelMarkers();*/
                        $scope.allParagraphs = mapAllParagraphs();
                        //$scope.initLabelMarkers();
                    }else{
                        $scope.clickedLabeling(true);
                    }

                }
                $scope.getFirstAndLast = function(arr){
                    return [arr[0], arr[arr.length-1]]
                };
                $scope.hasNoObjects = function(){
                    if(!$scope.generatedLabels){
                        return true;
                    }else{
                        return $scope.isEmpty($scope.generatedLabels)
                    }
                }
            }
        ]);
});

