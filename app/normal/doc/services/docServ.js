define(function (require) {
    'use strict';

    var app = require('appModule');

    app.register.service('docServ', ['$http',  'generalServ', '$timeout','$q', 'DEALSUMM_CONFIG', 'GLOBAL_VARS', '$cookies', '$rootScope',
        function ($http,  generalServ,$timeout, $q, DEALSUMM_CONFIG, GLOBAL_VARS, $cookies, $rootScope) {

            var returnObject = {}, promise;
            var basePath = DEALSUMM_CONFIG.REST_PATH;// + GLOBAL_VARS.orgId;

            returnObject.getDocDetails = function (doc_id, folder_id, scope) {
                var docDetails = $q.defer();
                var docLabels = $q.defer();
                var classCategories = $q.defer();
                var docPromise = {};
                docPromise['docDetails'] = docDetails.promise;
                docPromise['docLabels'] = docLabels.promise;
                docPromise['classCategories'] = classCategories.promise;
                generalServ.http('document', 'getDocDetails',[doc_id]).then(function(resp) {
                    docDetails.resolve(resp);
                });
                if(folder_id){
                    generalServ.http('document', 'getDocLabels',[folder_id, doc_id]).then(function(resp) {
                        docLabels.resolve(resp);
                    });
                }else{
                    docLabels.resolve();
                }

                generalServ.http('category', 'getClassCategories',[]).then(function(resp) {
                    classCategories.resolve(resp);
                });
                $q.all(docPromise).then(function(docPromise){
                    console.log("docPromise",docPromise);
                    scope.docDetails = scope.$parent.docDetails = docPromise.docDetails.data;
                    scope.item_title = scope.$parent.item_title = scope.docDetails.doc_title;
                    scope.item_id = scope.$parent.item_id = scope.docDetails.doc_id;
                    scope.docUrl = STATIC_PREFIX + scope.docDetails.doc_url;
                    scope.docLabels = docPromise.docLabels?docPromise.docLabels.data:null;
                    scope.docClasses = docPromise.classCategories.data;
                    scope.classCategories = docPromise.classCategories.data;
                    console.log("scope.classCategories",scope.classCategories);
                    scope.nextDoc = scope.docDetails.next_doc!=-1?scope.docDetails.next_doc:null;
                    scope.prevDoc = scope.docDetails.prev_doc!=-1?scope.docDetails.prev_doc:null;
                    console.log("docDetails",scope.docDetails);
                    //$('#doc_container').append("<div id='object_marker' class='object-marker'></div>")
                })
            };


            // get the TOC by calling get_toc_json from the server
            returnObject.getTocTree = function (doc_id) {
                var url = basePath + $rootScope.$stateParams.orgId + '/docs/?func=get_toc_json&doc_id=' + doc_id;
                var promise = $http.get(url).then(function (response) {
                    return response;
                });
                return promise;
            };

            // get the short TS by calling get_short_TS from the server -- should be lazy
            returnObject.getTsTree = function (doc_id) {
                var url = basePath + $rootScope.$stateParams.orgId + '/docs/?func=get_short_ts_json&doc_id=' + doc_id;
                var promise = $http.get(url).then(function (response) {
                    return response;
                });
                return promise;
            };

            // get the table TS from the server
            returnObject.getTsTable = function (doc_id) {
                var url = basePath + $rootScope.$stateParams.orgId + '/docs/?func=get_ts_table_json&doc_id=' + doc_id;
                var promise = $http.get(url).then(function (response) {
                    return response;
                });
                return promise;
            };


            // get the LONG TS from the server
            returnObject.getTsLong = function (nodeId, nodeType, lineId) {
                console.log("LONG nodeId: ", nodeId, nodeType, lineId);
                var url = basePath + $rootScope.$stateParams.orgId + '/docs/?func=get_ts_long_json&nodeType='+nodeType+'&nodeId=' + nodeId + '&lineId=' + lineId;
                console.log("getTsLong: ", url);
                var promise = $http.get(url).then(function (response) {
                    return response;
                });
                return promise;
            };

            /*returnObject.newCategory = function (name) {
                return generalServ.http('category', 'newCategory',[]);
            }*/
            /*returnObject.assignClass = function (id, data) {
                data['function'] = 'assignClass';
                //var url = basePath + $rootScope.$stateParams.orgId + '/doc/' + id + '/';
                var url = basePath + $rootScope.$stateParams.orgId + '/docs/?func=assignClass&doc_id=' + id;
                //console.log("data",data, url);
                console.log("in assignClass POST: data",data, url);
                var promise = $http.post(url, data).then(function (response) {
                    return response;
                });
                return promise;
            };
            returnObject.unAssignClass = function (id, data) {
                //data['removed_tst_id'] = 'unAssignClass';
                data['function'] = "unAssignClass";
                //var url = basePath + $rootScope.$stateParams.orgId + '/doc/' + id + '/';
                var url = basePath + $rootScope.$stateParams.orgId + '/docs/?func=unAssignClass&doc_id=' + id;
                //console.log("data",data, url);
                console.log("in unAssignClass POST: data",data, url);
                var promise = $http.post(url, data).then(function (response) {
                    return response;
                });
                return promise;
            };*/


            returnObject.timesToTryItem = 15; //after that the code will stop looking for the element on DOM
            returnObject.docScroll = function(selector, scope, parentElt, needsOffset) {
                var item = $(selector);
                if(!item || !item.position()){
                    returnObject.timesToTryItem--;
                    if(returnObject.timesToTryItem>0){
                        $timeout(function(){
                            console.log("no item found: " + selector);
                            returnObject.docScroll(selector, scope, parentElt);
                        },100)
                    }
                    return;
                }
                returnObject.timesToTryItem = 15;// reset the variable to count again
                var parent = $(parentElt);
                $(".border-after-highlight").each(function(){
                    $(this).removeClass("border-after-highlight");
                })
                var offset = item.position().top - 50;
                //console.log("item",item, offset);
                if(item.is("span")){
                    var p = item.closest("p");
                    offset += p.position().top
                }
                //console.log("offset", parent,offset, parent.scrollTop());
                if (offset != 0) {
                    if($(parent).is($('body'))){
                        offset = offset - 200;
                    }else{
                        // removed after adding copy paste (position relative)
                        if(needsOffset){
                            offset = parent.scrollTop() + offset - 50;
                        }
                    }

                    //console.log("parent",parent, offset);
                    //parent.scrollTop(offset);
                    parent.animate({ scrollTop: offset }, 400);
                }
                //console.log("scope.highlight",scope.highlight, "offset", offset, parent.scrollTop());

                if(!scope.highlight) { // in case we are highlighting a phrase
                    returnObject.highlightSection(item, scope);
                }
            }
            returnObject.highlightSection = function(item, scope){
                var itemStyle = $(item).attr("style");
                if (scope && scope.sectionSpecialPulseAnimation) {
                    // currently works in train / predict
                    $(item).pulse({backgroundColor: 'transparent', borderColor:'gray'}, {duration: 400, pulses: 5}, returnObject.removePulse(item, itemStyle, 2000));
                } else {
                    $(item).pulse({backgroundColor: 'red', borderColor:'transparent'}, {duration: 400, pulses: 5}, returnObject.removePulse(item, itemStyle, 2000));
                }
            }
            returnObject.removePulse = function(item, itemStyle, destroyTime){
                // remove all style of pulse animation
                $timeout(function(){
                    $(item).removeAttr("style");
                    //console.log("itemStyle",itemStyle);
                    if(itemStyle && itemStyle.length>0){
                        // in case there was a style -> replace it now
                        $(item).attr("style", itemStyle);
                    }
                    $(item).addClass("border-after-highlight")
                }, destroyTime+500)

            }
            returnObject.timesToTryTokensSpan = 33; //after that the code will stop looking for the element on DOM
            returnObject.highlightPhrase = function(phrase, scope, parentElt){
                // make sure we an arrasy of all spans of the document
                if(!scope.token_spans || scope.token_spans.length==0){
                    scope.token_spans = $("#doc_container span.token");
                    returnObject.timesToTryTokensSpan--;
                    if(returnObject.timesToTryTokensSpan>0){
                        $timeout(function(){
                            console.log("no token span found...(" + returnObject.timesToTryTokensSpan + ")");
                            returnObject.highlightPhrase(phrase, scope, parentElt)
                        },120)
                    }
                    return;
                }
                returnObject.timesToTryTokensSpan = 33;// reset the variable to count again
                var startIndex = phrase.split("-")[0];
                var endIndex = phrase.split("-")[1];
                scope.highlight = scope.token_spans.slice(startIndex, endIndex);
                console.log("startIndex", startIndex, endIndex, scope.highlight);
                //console.log("highlight", scope.highlight);
                scope.token_spans.removeClass("highlight");
                scope.highlight.addClass("highlight");
                //console.log("doc_container end highlightPhrase");
                returnObject.docScroll('#doc_container span:eq(' + startIndex + ')', scope, parentElt);
            }
            return returnObject;

        }]);
});




