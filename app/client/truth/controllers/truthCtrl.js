define(function (require) {
    'use strict';
    var app = require('appModule');

    app.register.controller('truthCtrl',
                ['$scope', 'truthServ', 'generalServ', '$state', 'GLOBAL_VARS', '$filter', '$cookies', '$dialog', '$timeout', '$stateParams', '$rootScope', '$window', 'localStorageService',
        function ($scope,   truthServ,   generalServ,  $state,   GLOBAL_VARS,   $filter,   $cookies,   $dialog,   $timeout,  $stateParams, $rootScope, $window, localStorageService) {

            var stateParams = $rootScope.$stateParams;
            $scope.$state = $state;
            $scope.projectId = stateParams.projectId;
            $scope.tenantId = null;
            $scope.docId = null;
            $scope.clauseId = $rootScope.clauseId = null;
            $scope.docClauses = null;
            $scope.stage = 1;
            $scope.currentContentSize = 8;
            $scope.minPasteWorkingTime = 700;
            $scope.showReference = false;
            $scope.ref = {};
            $rootScope.tenantName = null;
            $rootScope.docName = null;
            //console.log("$scope.projectId - ",$scope.projectId);

            $scope.init = function(){
                generalServ.http('general', 'getProjectInfo',[$scope.projectId]).then(function(resp) {
                    $scope.projectName = resp.data.name;
                    $scope.projectId = resp.data.id;
                })
                truthServ.getProjectsTenants($scope.projectId).then(function (resp) {
                    //console.log("resp",resp.data);
                    $scope.tenants = resp.data;
                    if($scope.tenantId){
                        $scope.tenantName = $rootScope.tenantName = $filter("filter")($scope.tenants, {id:$scope.tenantId})[0].name || null;
                    }
                })
                truthServ.getFieldsList(true).then(function (resp) {
                    console.log("fields",resp.data);
                    $scope.fields = resp.data;
                    $scope.commandSelectOptions = angular.copy($scope.fields['commands']);
                    $scope.modalitySelectOptions = angular.copy($scope.fields['modalities']);
                    $scope.periodSelectOptions = angular.copy($scope.fields['period_properties']);
                    $scope.ref.ref_propertiesOptions['commands'] = $scope.fields['commands'];
                    $scope.ref.ref_propertiesOptions['modalities'] = $scope.fields['modalities'];
                    $scope.ref.ref_propertiesOptions['period_properties'] = $scope.fields['period_properties'];
                })
            };
            $scope.initStage2 = function(){
                truthServ.getTenantsDocumentsAndClauses($scope.projectId, $scope.tenantId).then(function (resp) {
                    $scope.documents = resp.data;
                    console.log("$scope.documents",$scope.documents);
                    if($scope.docId){
                        $scope.docName = $rootScope.docName = $filter("filter")($scope.documents, {id:$scope.docId})[0].name || null;
                    }
                })
            };
            $scope.initStage3 = function(){
                truthServ.getClauseCommands($scope.projectId, $scope.tenantId, $scope.docId, $scope.clauseId).then(function (resp) {
                    //console.log("getDocumentsCommands resp",resp.data);
                    $scope.commands = $scope.populateCommands(resp.data);
                });
                $scope.getClause();
            };
            $scope.initStage3Document = function(){
                console.log("$scope.docId",$scope.docId);
                if($scope.docId){
                    truthServ.getClausesForDocument($scope.projectId, $scope.tenantId, $scope.docId).then(function (resp) {
                        console.log("getClausesForDocument resp",resp.data);
                        $scope.docClauses = resp.data;
                    });
                }
            }
            $scope.getClause = function(){
                $scope.serverProgress['working'] = true;
                truthServ.getClause($scope.projectId, $scope.tenantId, $scope.docId, $scope.clauseId).then(function (resp) {
                    console.log("getClause resp",resp.data);

                    $scope.clauseText = resp.data.text;
                    $scope.clause_start_pid = resp.data.start_pid;
                    $scope.clause_end_pid = resp.data.end_pid;
                    if($scope.clause_start_pid==-1 || $scope.clause_end_pid==-1 || $scope.clauseText==null){
                        $scope.clauseHasInitialData = false;
                    }else{
                        $scope.clauseHasInitialData = true;
                    }
                    $scope.stopServerProgress();
                });
            }
            $scope.populateCommands = function(data){
                console.log("getDocumentsCommands resp",data);
                //var data = angular.copy(data);
                angular.forEach(data, function(command){
                    command.command = {value: command.command?command.command:"None"};
                    command.modality = {value: command.modality?command.modality:"None"};
                    angular.forEach(command.periods, function(period){
                        period = $scope.populatePeriods(period);
                    })
                })
                return data;
            };
            $scope.populatePeriods = function(period){
                //console.log("period resp",period);
                angular.forEach(period.period_properties, function(property){
                    property.property_name = {value: property.property_name};
                    /*angular.forEach(property.annotations, function(annotation){
                     annotation.orig = {value: annotation.orig};
                     })*/
                })
                return period;
            };
            $scope.showDocumentInOldView = function(docId){
                $scope.no_copied_data_message = null;
                var url = "#/org/" + stateParams.accountId + "/doc/" + docId + "?forceView=copy-data";
                $window.open(url);
            };
            $scope.getCopiedClauseData = function(){
                $scope.no_copied_data_message = null;
                $scope.copiedData = localStorageService.get("copyClauseData");
                // make sure this is the same document as the one data was copied from
                if($scope.copiedData[$scope.docId]){
                    $scope.initializingClauseData = new Date().getTime();
                    $scope.updateClauseInitialData($scope.copiedData[$scope.docId]);
                }else{
                    $scope.no_copied_data_message = "Copied data document id does not match current document id, please try again.";
                    console.log("Copied data document does not match current document");
                }
            }
            $scope.init();

            $scope.$on('$stateChangeSuccess',
                function(event, toState, toParams, fromState, fromParams){
                    console.log("toState.name",toParams);
                    if(toParams.tenantId && toParams.tenantId != $scope.tenantId){
                        $scope.tenantId = toParams.tenantId;
                        $scope.docId = toParams.docId;
                        $scope.stage = 2;
                        $scope.docClauses = null;
                        $scope.initStage2();
                    }
                    if(toParams.clauseId && toParams.clauseId != $scope.clauseId && toParams.docId){
                        $scope.tenantId = toParams.tenantId;
                        $scope.docId = toParams.docId;
                        $scope.clauseId = $rootScope.clauseId = toParams.clauseId;
                        $scope.stage = 3;
                        $scope.docClauses = null;
                        $scope.initStage3();
                    }
                    if(!toParams.clauseId && toParams.docId){
                        $scope.stage = 3;
                        $scope.docId = toParams.docId;
                        $scope.clauseId = $rootScope.clauseId = null;
                        $scope.clauseText = null;
                        $scope.initStage3Document();
                    }
                    if(!toParams.docId){
                        $scope.stage = 2;
                        $scope.docId = null;
                        $scope.clauseId = $rootScope.clauseId = null;
                        $scope.clauseText = null;
                        $scope.commands = null;
                        $scope.docClauses = null;
                    }
                    if(!toParams.tenantId){
                        $scope.stage = 1;
                        $scope.tenantId = null;
                        $scope.docId = null;
                        $scope.clauseId = $rootScope.clauseId = null;
                        $scope.clauseText = null;
                        $scope.commands = null;
                        $scope.documents = null;
                        $scope.docClauses = null;
                    }
                    if($scope.tenantId && $scope.tenants){
                        $scope.tenantName = $rootScope.tenantName = $filter("filter")($scope.tenants, {id:$scope.tenantId})[0].name || null;
                    }
                    if($scope.docId && $scope.documents){
                        $scope.docName = $rootScope.docName = $filter("filter")($scope.documents, {id:$scope.docId})[0].name || null;
                    }
                    console.log("$scope.stage",$scope.stage);
                }
            )

            // update / delete functions
            $scope.serverProgress = {working:false};
            $scope.commandOrModalityOnChange = function(selected, command, attr){
                if($scope.serverProgress['working']){
                    console.log("server in progress");
                    return;
                }
                var data = {};
                data[attr] = selected.value;
                command[attr]._status = "updating";
                truthServ.updateCommandOrModality($scope.projectId, $scope.tenantId, $scope.docId, $scope.clauseId, command.id, data).then(function (resp) {
                    //console.log("updateCommandOrModality resp",resp.data);
                    command[attr]._status = "updated";
                }, function(){command[attr]._status = "error";});
            };
            $scope.periodsOnChange = function(selected, property, periodId, commandId, attr){
                if($scope.serverProgress['working']){
                    console.log("server in progress");
                    return;
                }
                property._status = "updating";
                var propertyId = property.id;
                var data = {};
                data[attr] = selected.value;
                truthServ.updatePeriodProperties($scope.projectId, $scope.tenantId, $scope.docId, $scope.clauseId, commandId, periodId, propertyId, data).then(function (resp) {
                    //console.log("updatePeriodProperties resp", resp.data);
                    property._status = "updated";
                }, function(){property._status = "error";});
            };
            $scope.updateClauseInitialData = function(paragraph_data){
                if($scope.serverProgress['working']){
                    console.log("server in progress");
                    return;
                }
                var data = {};
                data['start_pid'] = paragraph_data.start_pid;
                data['end_pid'] = paragraph_data.end_pid;
                $scope.serverProgress['working'] = true;
                truthServ.updateClauseInitialData($scope.projectId, $scope.tenantId, $scope.docId, $scope.clauseId, data).then(function (resp) {
                    //console.log("updateAnnotation resp", resp.data);
                    delete $scope.copiedData[$scope.docId];
                    localStorageService.set("copyClauseData", JSON.stringify($scope.copiedData));
                    var now = new Date().getTime();
                    var startTime = $scope.initializingClauseData + $scope.minPasteWorkingTime;
                    var remainingTime = startTime>now?startTime-now:10;
                    console.log("remainingTime",remainingTime);
                    $timeout(function(){
                        $scope.initializingClauseData = null;
                        $scope.getClause();
                    }, remainingTime);
                    //console.log("now",now, $scope.initializingClauseData, startTime>now);

                }, function(){$scope.initializingClauseData=null;$scope.stopServerProgress();});
            };
            $scope.annotationUpdate = function(obj, propertyId, periodId, commandId, attr){
                if($scope.serverProgress['working']){
                    console.log("server in progress");
                    return;
                }
                var data = {};
                data[attr] = obj[attr];
                obj._status = "updating";
                truthServ.updateAnnotation($scope.projectId, $scope.tenantId, $scope.docId, $scope.clauseId, commandId, periodId, propertyId, data).then(function (resp) {
                    //console.log("updateAnnotation resp", resp.data);
                    obj._status = "updated";
                }, function(){obj._status = "error";});
            };

            $scope.deletePeriodProperty = function(propertyId, periodId, commandId, property, period_properties){
                if($scope.serverProgress['working']){
                    console.log("server in progress");
                    return;
                }
                var idx = period_properties.indexOf(property);
                $scope.serverProgress['working'] = true;
                truthServ.deletePeriodProperty($scope.projectId, $scope.tenantId, $scope.docId, $scope.clauseId, commandId, periodId, propertyId).then(function () {
                    //console.log("deletePeriodProperty resp");
                    period_properties.splice(idx, 1);
                    $scope.stopServerProgress();
                }, function(){$scope.stopServerProgress();});
            };
            $scope.deleteClause = function(){
                if($scope.serverProgress['working']){
                    console.log("server in progress");
                    return;
                }
                var doc = $filter("filter")($scope.documents, {id:$scope.docId})[0];
                var clause = $filter("filter")(doc.clauses, {id:$scope.clauseId})[0];
                var idx = doc.clauses.indexOf(clause);
                $scope.serverProgress['working'] = true;
                truthServ.deleteClause($scope.projectId, $scope.tenantId, $scope.docId, $scope.clauseId).then(function () {
                    doc.clauses.splice(idx, 1);
                    if(doc.clauses.length>0){
                        $state.go('app.clientTenantsView.groundTruth.tenants.documents.clauses', {clauseId:doc.clauses[0].id});
                    } else{
                        $state.go('app.clientTenantsView.groundTruth.tenants.documents');
                    }
                    $scope.stopServerProgress();

                }, function(){$scope.stopServerProgress();});
            };
            $scope.deleteCommand = function(command, commands){
                if($scope.serverProgress['working']){
                    console.log("server in progress");
                    return;
                }
                var idx = commands.indexOf(command);
                var commandId = command.id;
                $scope.serverProgress['working'] = true;
                truthServ.deleteCommand($scope.projectId, $scope.tenantId, $scope.docId, $scope.clauseId, commandId).then(function () {
                    //console.log("deletePeriodProperty resp");
                    commands.splice(idx, 1);
                    $scope.stopServerProgress();
                }, function(){$scope.stopServerProgress();});
            };
            $scope.deletePeriod = function(propertyId, periodId, commandId, period, periods){
                if($scope.serverProgress['working']){
                    console.log("server in progress");
                    return;
                }
                var idx = periods.indexOf(period);
                $scope.serverProgress['working'] = true;
                truthServ.deletePeriod($scope.projectId, $scope.tenantId, $scope.docId, $scope.clauseId, commandId, periodId, propertyId).then(function () {
                    //console.log("deletePeriodProperty resp");
                    periods.splice(idx, 1);
                    $scope.stopServerProgress();
                }, function(){$scope.stopServerProgress();});
            };
            $scope.addClause = function(){
                if($scope.serverProgress['working']){
                    console.log("server in progress");
                    return;
                }
                var doc = $filter("filter")($scope.documents, {id:$scope.docId})[0];
                $scope.serverProgress['working'] = true;
                truthServ.addClause($scope.projectId, $scope.tenantId, $scope.docId).then(function (resp) {
                    console.log("addClause resp.data",resp.data);
                    doc.clauses.push(resp.data);
                    $scope.stopServerProgress();
                    $state.go('app.clientTenantsView.groundTruth.tenants.documents.clauses', {clauseId:resp.data.id});
                }, function(){$scope.stopServerProgress();});
            };
            $scope.addCommand = function(){
                if($scope.serverProgress['working']){
                    console.log("server in progress");
                    return;
                }
                $scope.serverProgress['working'] = true;
                truthServ.addCommand($scope.projectId, $scope.tenantId, $scope.docId, $scope.clauseId).then(function (resp) {
                    console.log("addCommand resp.data",resp.data);
                    $scope.commands.push($scope.populateCommands([resp.data])[0]);
                    //$scope.commands.push(resp.data);
                    $scope.stopServerProgress();
                }, function(){$scope.stopServerProgress();});
            };
            $scope.addPeriod = function(commandId, periods){
                if($scope.serverProgress['working']){
                    console.log("server in progress");
                    return;
                }
                $scope.serverProgress['working'] = true;
                truthServ.addPeriod($scope.projectId, $scope.tenantId, $scope.docId, $scope.clauseId, commandId).then(function (resp) {
                    console.log("resp.data",resp);
                    var period = $scope.populatePeriods(resp.data);
                    console.log("deletePeriodProperty resp", period);
                    periods.push(period);
                    $scope.stopServerProgress();
                }, function(){$scope.stopServerProgress();});
            };
            $scope.addProperty = function(periodId, commandId, attr, period_properties){
                if($scope.serverProgress['working']){
                    console.log("server in progress");
                    return;
                }
                var data = {};
                data[attr] = "None";
                $scope.serverProgress['working'] = true;
                truthServ.addProperty($scope.projectId, $scope.tenantId, $scope.docId, $scope.clauseId, commandId, periodId, data).then(function (resp) {
                    console.log("addProperty resp", resp.data);
                    var property =  resp.data;
                    property.property_name = {value: property.property_name};
                    period_properties.push(property);
                    $scope.stopServerProgress();
                }, function(){$scope.stopServerProgress();});
            };
            $scope.stopServerProgress = function(){
                $timeout(function(){
                    $scope.serverProgress['working'] = false;
                },200);
            }

            /*
            REFERENCE FUNCTIONS
             */
            $rootScope.toggleReference = function(){
                $scope.showReference =!$scope.showReference;
            }
            $scope.ref.ref_selectedCommand = null;
            $scope.ref.ref_selectedProperty = null;
            $scope.ref.ref_propertiesOptions = {};
            $scope.ref.ref_commandsOptions = [{label:"Commands", value:"commands"}, {label:"modalities", value:"modalities"}, {label:"Period properties", value:"period_properties"}];
            $scope.ref_selectCommand = function(commandValue){
                $scope.ref.ref_selectedCommand = commandValue;
                $scope.ref.ref_properties = $scope.ref.ref_propertiesOptions[commandValue];
                console.log("$scope.properties",$scope.ref.ref_propertiesOptions, commandValue);
            }
            $scope.ref_selectProperty = function(propertyValue){
                $scope.ref.ref_selectedProperty = propertyValue;
                truthServ.getPropertyClauses($scope.ref.ref_selectedCommand, propertyValue).then(function (resp) {
                    $scope.ref.ref_clauses = resp.data;
                    console.log("getPropertyClauses",resp.data);
                });
            }
        }
    ]);
});
