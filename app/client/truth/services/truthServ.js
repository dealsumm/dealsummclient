define(function (require) {
    'use strict';

    var app = require('appModule');

    app.register.service('truthServ',
                ['$http', '$timeout','$q', 'DEALSUMM_CONFIG', 'GLOBAL_VARS', '$cookies', '$rootScope',
        function ($http,     $timeout,  $q,   DEALSUMM_CONFIG,   GLOBAL_VARS,   $cookies,   $rootScope) {

            var returnObject = {}, promise, url;
            var basePath = DEALSUMM_CONFIG.DSPROD_REST_PATH;
            var basePathGT = DEALSUMM_CONFIG.GT_REST_PATH;
            var oldBasePath = DEALSUMM_CONFIG.REST_PATH;

            returnObject.getProjectsTenants = function (projectId) {
                url = basePath + $rootScope.$stateParams.accountId + '/projects/' + projectId + "/folders/";
                promise = $http.get(url).success(function (data, status) {
                    return data;
                });
                return promise;
            };
            returnObject.getTenantsDocumentsAndClauses = function (projectId, tenantId) {
                url = basePathGT + $rootScope.$stateParams.accountId + '/projects/' + projectId + "/tenants/" + tenantId + "/documents/";
                promise = $http.get(url).success(function (data, status) {
                    return data;
                });
                return promise;
            };
            returnObject.getClausesForDocument = function (projectId, tenantId, docId) {
                url = basePathGT + $rootScope.$stateParams.accountId + '/projects/' + projectId + "/tenants/" + tenantId + "/documents/" + docId + "/clauses/";
                promise = $http.get(url).success(function (data, status) {
                    return data;
                });              //projects/<origin_project_id>/tenants/<tenant_id>/documents/<document_id>/clauses
                return promise;
            };
            returnObject.getClauseCommands = function (projectId, tenantId, docId, clauseId) {
                url = basePathGT + $rootScope.$stateParams.accountId + '/projects/' + projectId + "/tenants/" + tenantId + "/documents/" + docId + "/clauses/" + clauseId + "/commands/";
                promise = $http.get(url).success(function (data, status) {
                    return data;
                });
                return promise;
            };
            returnObject.getClause = function (projectId, tenantId, docId, clauseId) {
                url = basePathGT + $rootScope.$stateParams.accountId + '/projects/' + projectId + "/tenants/" + tenantId + "/documents/" + docId + "/clauses/" + clauseId + "/";
                promise = $http.get(url).success(function (data, status) {
                    return data;
                });
                return promise;
            };
            returnObject.getFieldsList = function (force) {
                url = basePathGT + $rootScope.$stateParams.accountId + '/fieldslist/' + (force?"?force_reload=true":"");
                promise = $http.get(url).success(function (data, status) {
                    return data;
                });
                return promise;
            };
            returnObject.getPropertyClauses = function (commandValue, propertyValue) {
                url = basePathGT + $rootScope.$stateParams.accountId + '/fieldslist/' + commandValue + "/" + propertyValue + "/clauses/";
                promise = $http.get(url).success(function (data, status) {
                    return data;
                });
                return promise;
            };
            returnObject.updateClauseInitialData = function (projectId, tenantId, docId, clauseId, data) {
                url = basePathGT + $rootScope.$stateParams.accountId + '/projects/' + projectId + "/tenants/" + tenantId + "/documents/" + docId + "/clauses/" + clauseId + "/";
                promise = $http({ method: 'PATCH', url: url, data: angular.toJson(data)});
                return promise;
            };
            returnObject.updateCommandOrModality = function (projectId, tenantId, docId, clauseId, commandId, data) {
                url = basePathGT + $rootScope.$stateParams.accountId + '/projects/' + projectId + "/tenants/" + tenantId + "/documents/" + docId + "/clauses/" + clauseId + "/commands/" + commandId + "/";
                promise = $http({ method: 'PATCH', url: url, data: angular.toJson(data)});
                return promise;
            };
            returnObject.updatePeriodProperties = function (projectId, tenantId, docId, clauseId, commandId, periodId, propertyId, data) {
                url = basePathGT + $rootScope.$stateParams.accountId + '/projects/' + projectId + "/tenants/" + tenantId + "/documents/" + docId + "/clauses/" + clauseId + "/commands/" + commandId + "/periods/" + periodId + "/properties/" + propertyId + "/";
                promise = $http({ method: 'PATCH', url: url, data: angular.toJson(data)});
                return promise;
            };
            returnObject.updateAnnotation = function (projectId, tenantId, docId, clauseId, commandId, periodId, propertyId, data) {
                url = basePathGT + $rootScope.$stateParams.accountId + '/projects/' + projectId + "/tenants/" + tenantId + "/documents/" + docId + "/clauses/" + clauseId + "/commands/" + commandId + "/periods/" + periodId + "/properties/" + propertyId + "/annotation/";
                promise = $http({ method: 'PATCH', url: url, data: angular.toJson(data)});
                return promise;
            };
            returnObject.deletePeriodProperty = function (projectId, tenantId, docId, clauseId, commandId, periodId, propertyId) {
                url = basePathGT + $rootScope.$stateParams.accountId + '/projects/' + projectId + "/tenants/" + tenantId + "/documents/" + docId + "/clauses/" + clauseId + "/commands/" + commandId + "/periods/" + periodId + "/properties/" + propertyId + "/";
                promise = $http({ method: 'DELETE', url: url});
                return promise;
            };
            returnObject.deleteClause = function (projectId, tenantId, docId, clauseId) {
                url = basePathGT + $rootScope.$stateParams.accountId + '/projects/' + projectId + "/tenants/" + tenantId + "/documents/" + docId + "/clauses/" + clauseId + "/";
                promise = $http({ method: 'DELETE', url: url});
                return promise;
            };
            returnObject.deleteCommand = function (projectId, tenantId, docId, clauseId, commandId) {
                url = basePathGT + $rootScope.$stateParams.accountId + '/projects/' + projectId + "/tenants/" + tenantId + "/documents/" + docId + "/clauses/" + clauseId + "/commands/" + commandId + "/";
                promise = $http({ method: 'DELETE', url: url});
                return promise;
            };
            returnObject.deletePeriod = function (projectId, tenantId, docId, clauseId, commandId, periodId) {
                url = basePathGT + $rootScope.$stateParams.accountId + '/projects/' + projectId + "/tenants/" + tenantId + "/documents/" + docId + "/clauses/" + clauseId + "/commands/" + commandId + "/periods/" + periodId + "/";
                promise = $http({ method: 'DELETE', url: url});
                return promise;
            };
            returnObject.addClause = function (projectId, tenantId, docId) {
                url = basePathGT + $rootScope.$stateParams.accountId + '/projects/' + projectId + "/tenants/" + tenantId + "/documents/" + docId + "/clauses/";
                promise = $http({ method: 'PUT', url: url});
                return promise;
            };
            returnObject.addCommand = function (projectId, tenantId, docId, clauseId) {
                url = basePathGT + $rootScope.$stateParams.accountId + '/projects/' + projectId + "/tenants/" + tenantId + "/documents/" + docId + "/clauses/" + clauseId + "/commands/";
                promise = $http({ method: 'PUT', url: url});
                return promise;
            };
            returnObject.addPeriod = function (projectId, tenantId, docId, clauseId, commandId) {
                url = basePathGT + $rootScope.$stateParams.accountId + '/projects/' + projectId + "/tenants/" + tenantId + "/documents/" + docId + "/clauses/" + clauseId + "/commands/" + commandId + "/periods/";
                promise = $http({ method: 'PUT', url: url});
                return promise;
            };
            returnObject.addProperty = function (projectId, tenantId, docId, clauseId, commandId, periodId, data) {
                url = basePathGT + $rootScope.$stateParams.accountId + '/projects/' + projectId + "/tenants/" + tenantId + "/documents/" + docId + "/clauses/" + clauseId + "/commands/" + commandId + "/periods/" + periodId + "/properties/";
                promise = $http({ method: 'PUT', url: url, data: angular.toJson(data)});
                return promise;
            };

            return returnObject;
        }
    ]);
});

