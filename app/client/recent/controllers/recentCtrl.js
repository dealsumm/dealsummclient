define(function (require) {
    'use strict';
    var app = require('appModule');

    app.register.controller('recentCtrl',
        function ($scope,   generalServ,  $state,   GLOBAL_VARS,   $filter,   $cookies,   $dialog,   $timeout,  $stateParams, $rootScope) {

            var stateParams = $rootScope.$stateParams;
            console.log("recentCtrl");

            $scope.recent = {};
            $scope.recent.activity = {
                title: "recent activity",
                type: "activity",
                items:[
                    {
                        htmlText:"Approve <a href='#'>Rack Room Shoes</a>",
                        id:4244
                    },
                    {
                        htmlText:"Edit <a href='#'>Rack Room Expiration</a>: 2015/03/10",
                        id:8233
                    }
                ]
            };
            $scope.recent.messages = {
                title: "recent messages",
                type: "messages",
                items:[
                    {
                        htmlText:"Approve <a href='#'>Rack Room Shoes</a>",
                        id:4244
                    },
                    {
                        htmlText:"Edit <a href='#'>Rack Room Expiration</a>: 2015/03/10",
                        id:8233
                    }
                ]
            };
            generalServ.http('general', 'getUserInfo',[$scope.userDetails.id]).then(function(resp) {
                console.log("user Details", resp);
                $scope.setRecentDocuments([resp.data.last_doc]);
            });
            $scope.setRecentDocuments = function(lastDocArr){
                var itemsArr = [];
                angular.forEach(lastDocArr, function(docData){
                    itemsArr.push({htmlText:docData.name, id:docData.id});
                });
                $scope.recent.documents = {
                    title: "recent documents",
                    type: "documents",
                    items: itemsArr
                };
            };

        },
        ['$scope', 'generalServ', '$state', 'GLOBAL_VARS', '$filter', '$cookies', '$dialog', '$timeout', '$stateParams', '$rootScope'
        ]);
});
