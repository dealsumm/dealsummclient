define(function (require) {
    'use strict';
    var app = require('appModule');

    app.register.controller('createNewPropertyCtrl',
        ['$scope', 'generalServ', '$state', 'GLOBAL_VARS', '$filter', '$cookies', '$dialog', '$timeout', '$stateParams', '$rootScope',
            function ($scope,   generalServ,   $state,   GLOBAL_VARS,   $filter,   $cookies,   $dialog,   $timeout,  $stateParams, $rootScope) {

                console.log("createNewPropertyCtrl");
                $scope.date = new Date();
                $scope.proj = {};
                //$scope.projectName = '';

                $scope.createNewProperty = function(){
                    var propertyName = $scope.proj.projectName;
                    //var projectAddress = $scope.proj.projectAddress;
                    var dataToSend = {};
                    dataToSend['organization'] = $rootScope.$stateParams.accountId;
                    dataToSend['name'] = propertyName;
                    generalServ.http('properties', 'createProperty',[], dataToSend).then(function(resp) {
                        console.log('Created ' + propertyName, resp);
                        $scope.params.appendPropertyFn(resp.data);
                        $scope.$modalCancel();
                    })
                }
             }
        ]);
});
