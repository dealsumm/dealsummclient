define(function (require) {
    'use strict';
    var app = require('appModule');
    app.register.controller('propertiesCtrl',
                ['$scope', 'generalServ', 'uploadServ', '$state', 'GLOBAL_VARS', '$filter', '$cookies', '$dialog', '$timeout', '$stateParams', '$rootScope', 'localStorageService', 'Upload',
        function ($scope,   generalServ, uploadServ,  $state,   GLOBAL_VARS,   $filter,   $cookies,   $dialog,   $timeout,  $stateParams, $rootScope, localStorageService, Upload) {
            $(document).bind('dragover drop', function (e) {
                e.preventDefault();
            });
            var item;
            var stateParams = $rootScope.$stateParams;
            $scope.accountId = stateParams.accountId;
            $scope.sort = 'reverse';
            $scope.sortAvailable = true;
            $scope.sortOptions = [
                {label:"Upload Date", option: "upload_date", selected: false},
                {label:"Property Name", option: "name", selected: false}
            ];

            $scope.showPropertiesInList = false;
            $scope.clickedToggleGrid = function(){
                $scope.showPropertiesInList =!$scope.showPropertiesInList;
                $scope.$broadcast("toggleGridList", $scope.showPropertiesInList);
            }
            var last_sort = localStorageService.get("lastSort");
            if(last_sort){
                item = $filter("filter")($scope.sortOptions, {option:last_sort.option})[0];
                $scope.sort = last_sort.sort;
            }else{
                item = $scope.sortOptions[0];
            }
            item.selected = true;
            //console.log("$scope.sortOptions", $filter("filter")($scope.sortOptions, {selected:true}));

            $scope.changeSort = function(item){
                if(item.selected){
                    $scope.sort = $scope.sort=='reverse'?'':'reverse';
                    $scope.$broadcast("toggleGridSort", $scope.sort);
                }else{
                    var old = $filter("filter")($scope.sortOptions, {selected:true})[0];
                    old.selected = false;
                    item.selected = true;
                    $scope.sort = '';
                    initSort();
                }

                localStorageService.set("lastSort", {option:item.option,sort:$scope.sort});
                $scope.sortAvailable = false;
                $timeout(function(){
                    $scope.sortAvailable = true;
                },200);
            }
            $scope.filterProperties = "";
            $scope.clearFilter = function(){
                $scope.filterProperties = "";
            };
            $scope.filterChanged = function(){
                $scope.$broadcast("updateGridFilter", $scope.filterProperties);
            }
            function initSort(){
                $scope.sort_list_by = $filter("filter")($scope.sortOptions, {selected:true})[0];
                $scope.sort_list_option = $scope.sort_list_by['option'];
                $scope.sort_list_name = $scope.sort_list_by['label'];
                $scope.$broadcast("toggleGridSort", $scope.sort);
            }



            generalServ.http('properties', 'getProperties',[]).then(function(resp) {
                $scope.projectsList = resp.data;

                console.log("getProperties");
                $scope.$broadcast("has_properties");
                $timeout(function(){
                    initSort();
                }, 50);
                console.log("properties list ",resp.data);
            });
            $scope.appendPropertyFn = function(projData){
                $scope.projectsList.push(projData);
                $scope.addExtraTab(projData);
                //$state.go('app.property', {'projectId': projData.id});
            };
            $scope.showNewProjectModal = function(){
                $dialog.dialog(STATIC_PREFIX+APP_FOLDER+"properties/views/createNewProperty.html", {
                    backdropClick: false,
                    backdropCancel: false,
                    title: "Create new project",
                    footerTemplate: false,
                    ok:{label:"Create", fn:"scope.createNewProperty"},
                    params: {appendPropertyFn: $scope.appendPropertyFn },
                    controller: "createNewPropertyCtrl"
                });
            }
            $scope.addExtraTab = function(property){
                console.log("property",property);
                var tabIdx = -1;
                for(var i=0;i<$scope.extraTabsHolder.length;i++){
                    if(angular.equals($scope.extraTabsHolder[i].id, property.id)){
                        tabIdx = i;
                    }
                }
                if(tabIdx==-1){
                    if($scope.extraTabsHolder.length >= GLOBAL_VARS.maxExtraTabs) {
                        $scope.extraTabsHolder.shift();
                    }
                    $scope.extraTabsHolder.push(property);
                    $scope.extraTabsHolderData[$scope.accountId] = $scope.extraTabsHolder;
                    localStorageService.set("extraTabsHolderAccounts", $scope.extraTabsHolderData);

                }
                $state.go('app.property', {'accountId': $scope.accountId, 'projectId': property.id});
            }
            $rootScope.dragOverPropertyId = null;
            $scope.files = {};
            $scope.$watch('files', function() {
                if($scope.projectsList && $rootScope.dragOverPropertyId){
                    var dragOverId = $rootScope.dragOverPropertyId;
                    var project = $filter('filter')($scope.projectsList, {id:dragOverId})[0];
                    //console.log("dragOverPropertyId",dragOverId, project);
                    if(project){
                        $scope.initUpload($scope.files, project);
                    }
                }
            });
        }
    ]);
});
