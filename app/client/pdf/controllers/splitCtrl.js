define(function (require) {
    'use strict';
    var app = require('appModule');

    app.register.controller('splitCtrl',
        ['$scope', 'splitServ', 'generalServ', '$state', 'GLOBAL_VARS', '$filter', '$cookies', '$dialog', '$timeout', '$stateParams', '$rootScope', '$q',
        function ($scope,   splitServ, generalServ,  $state,   GLOBAL_VARS,   $filter,   $cookies,   $dialog,   $timeout,  $stateParams, $rootScope, $q) {

            var stateParams = $rootScope.$stateParams;

            $scope.projectId = stateParams.projectId;
            $scope.tenantId = stateParams.tenantId;
            $scope.docId = stateParams.docId;
            $scope.splitPage = stateParams.splitPage;

            $scope.httpModule = 'split';
            $scope.updatingApplySplit = false;
            $scope.init = function(){
                generalServ.http('split', 'getDocDetails',[$scope.docId]).then(function(resp) {
                    console.log("resp http",resp.data);
                    $scope.docDetails = resp.data;
                })
                generalServ.http('split', 'getSplits',[$scope.projectId, $scope.tenantId, $scope.docId]).then(function(splits) {
                    //splitServ.getSplits($scope.projectId, $scope.tenantId, $scope.docId).then(function(splits) {
                    console.log("splits",splits.data);
                    var splitData = splits.data;
                    angular.forEach(splitData, function(item, idx){
                        if(!item.from_page || item.from_page<0){
                            item.from_page = 0;
                        }
                        if(!item.to_page || item.to_page<0){
                            item.to_page = 0;
                        }
                        item.approved = item.approved == 0?false:true;
                    })
                    $scope.splitsData = splitData;
                    //$scope.splitsDataCopy = angular.copy(splitData);
                })
            };
            $scope.dragStart = function(e, ui) {
                ui.item.data('start', ui.item.index());
            }
            $scope.dragEnd = function(e, ui) {
                var start = ui.item.data('start'),
                    end = ui.item.index();

                $scope.splitsData.splice(end, 0, $scope.splitsData.splice(start, 1)[0]);
                //console.log("dragEnd",$scope.splitsData);
                $scope.$apply();
            }

            //http://jsfiddle.net/dj_straycat/Q5FWt/3/
            $('#sortable').sortable({
                handle: "p",
                start: $scope.dragStart,
                update: $scope.dragEnd
            });
            $("#sortable li").disableSelection();

            $scope.addNewSplit = function(){
                $scope.splitsData.push({from_page:0, to_page:0, title:"", pid:null, approved: false});
            }
            $scope.removeSplit = function(idx){
                $scope.splitsData.splice(idx, 1);
            }
            $scope.canApply = function(){
                if(!$scope.splitsData){
                    return false;
                }
                var noTitlesRecords = $filter('filter')($scope.splitsData, function(item){return item.title.length==0 || item.from_page<=0 || item.to_page<=0}).length;
                return $scope.splitsData.length>1 && noTitlesRecords==0 && !$scope.updatingApplySplit;;
            };
            $scope.applySplitConfirm = function(){
                $dialog.dialog(null, {
                    backdropClick: false,
                    title: "Apply Split",
                    template: "<div>New files will be created, continue?<br/><br/><br/></div>",
                    //ok: {label: 'Confirm', fn:$scope.applySplit},
                    ok: {label: 'Confirm'},
                    negative: {label: 'Cancel'},
                    params: {applySplit: $scope.applySplit },
                    controller: ['$scope', function ($scope) {
                        $scope.$modalOk = function(){
                            $scope.params.applySplit();
                            $scope.$modalCancel();
                        }
                    }]
                });
            }
            $scope.applySplit = function(){
                $scope.updatingApplySplit = true;
                var arrayToSend = [],dataToSend = {};
                angular.forEach($scope.splitsData, function(item){
                    dataToSend = {};
                    dataToSend['name'] = item.title;
                    dataToSend['from_page'] = item.from_page;
                    dataToSend['to_page'] = item.to_page;
                    arrayToSend.push(dataToSend)
                })

                generalServ.http('split', 'applySplit',[$scope.projectId, $scope.tenantId, $scope.docId], arrayToSend).then(function(resp) {
                    console.log("applySplit resp > status", resp.status);
                    if(resp.status == 400){
                        $scope.err_msg = resp.data.error;
                        $scope.updatingApplySplit = false;
                    }else{
                        $scope.err_msg = null;
                        $state.go('app.property');
                    }

                })
            };

            $scope.clickOnSplitItem = function(splitPage){
                if(!isNaN(splitPage) && splitPage>0){
                    $scope.splitPage = splitPage;
                    $state.go('app.clientSplitPdfView.split', {splitPage:splitPage});
                }
            };
            $scope.init();

        }
    ]);
});
