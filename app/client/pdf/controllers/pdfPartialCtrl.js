define(function (require) {
    'use strict';
    var app = require('appModule');

    app.register.controller('pdfPartialCtrl',
        function ($scope,   pdfServ, generalServ,   $state,   GLOBAL_VARS,   $filter,   $cookies,   $dialog,   $timeout,  $stateParams, $rootScope, $q, $controller) {
            var stateParams = $rootScope.$stateParams;

            $scope.projectId = stateParams.projectId;
            $scope.tenantId = stateParams.tenantId;
            $scope.docId = stateParams.docId;
            $scope.annotationId = stateParams.annotationId;
            $scope.splitPage = stateParams.splitPage;
            $scope.isOnePagePreview = false;
            $scope.pageNumber = stateParams.pageNumber;

            $scope.backToList = function(){
                $state.go('app.property');
            };

            var pdfViewerCtrl = $controller('pdfViewerCtrl',{$scope : $scope });

            $scope.init = function(){
                console.log("$scope.init pdfPartialCtrl");
                generalServ.http('general', 'getFolderInfo',[$scope.projectId, $scope.tenantId]).then(function(resp) {
                    $scope.tenantName = resp.data.name;
                })
                generalServ.http('general', 'getProjectInfo',[$scope.projectId]).then(function(resp) {
                    $scope.projectName = resp.data.name;
                })

                if($scope.docId){
                    $scope.getPdfDetails();
                    if($scope.annotationId){
                        $scope.getPdfAnnotationsData();
                    }
                }

            };
            $scope.getPdfDetails = function(){
                generalServ.http('pdf', 'getPdfDetails',[$scope.projectId, $scope.tenantId, $scope.docId]).then(function(resp){
                //pdfServ.getPdfDetails($scope.projectId, $scope.tenantId, $scope.docId).then(function(resp){
                    $scope.docTitle = resp.data.name;
                    $scope.single_totalPages = resp.data.no_of_pages;
                    if(stateParams.pageNumber){
                        $scope.pageNumber = stateParams.pageNumber;
                        $scope.isOnePagePreview = true;
                    }
                    console.log("getPdfDetails", resp.data);
                    $scope.getPdfUrlAndLoad($scope.isOnePagePreview);
                })
            }
            $scope.loadFullPdf = function(){
                $scope.isOnePagePreview = false;
                $scope.getPdfUrlAndLoad(false);
                $state.go('app.clientPdfView.annotation', {docId:$scope.docId, annotationId:$scope.annotationId});
            };

            $scope.getPdfAnnotationsData = function(){
                pdfServ.getAnnotationDetails($scope.projectId, $scope.tenantId, $scope.docId, $scope.annotationId).then(
                    function (annotation) {
                        //console.log("annotation", "x1",annotation.data.x1, "x2",annotation.data.x2, "y1",annotation.data.y1, "y2",annotation.data.y2);
                        console.log("annotations> x1:",annotation.data.x1," x2:", annotation.data.x2, " y1:", annotation.data.y1, " y2:", annotation.data.y2, " page:", annotation.data.page_no);
                        //var ann = {page_no:2, x1:384.515, y1:368.028, x2:425.292, y2:378.768};
                        if(annotation.data.page_no>0){
                            $scope.setPdfHighlightData(annotation.data);
                            if($scope.pdfTotalPages){
                                $scope.highlightPhrase();
                            }
                        }
                    },
                    function (err) {
                        console.log("annotation err", err);
                    }
                );
            };


            $scope.$on('$stateChangeSuccess',
                function(event, toState, toParams, fromState, fromParams){
                    console.log("toParams",toParams, fromParams);
                    if(toParams.splitPage){
                        if($scope.pdfTotalPages){
                            $scope.pdfCurrentPage = toParams.splitPage;
                            $scope.onPDFPageChanged();
                        }else{
                            $scope.pdfWantedPage = toParams.splitPage;
                        }
                    }else{
                        $scope.removeHighlight();
                        if(toParams.annotationId){
                            $scope.annotationId = toParams.annotationId;
                            if(toParams.docId){
                                $scope.docId = toParams.docId;
                                $scope.getPdfAnnotationsData();
                                if(!fromParams.docId || fromParams.docId != toParams.docId){
                                    $scope.getPdfDetails();
                                }
                            }
                        }
                    }
                }
            );

            $scope.init();
        },
        ['$scope', 'pdfServ', 'generalServ', '$state', 'GLOBAL_VARS', '$filter', '$cookies', '$dialog', '$timeout', '$stateParams', '$rootScope', '$q', '$controller'
    ]);
});
