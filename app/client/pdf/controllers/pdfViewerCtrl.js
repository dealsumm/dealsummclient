define(function (require) {
    // https://github.com/mozilla/pdf.js/blob/master/src/display/api.js
    // https://github.com/mozilla/pdf.js/blob/master/src/display/annotation_helper.js
    // https://github.com/mozilla/pdf.js
    // https://mozilla.github.io/pdf.js/examples/
    'use strict';
    var app = require('appModule');

    app.register.controller('pdfViewerCtrl',
        function ($scope,  pdfServ, generalServ,   $state,   GLOBAL_VARS,   $filter,   $cookies,   $dialog,   $timeout,  $stateParams, $rootScope, $q, $sce, $compile) {
            $scope.isLoading = false;
            $scope.downloadProgress = 0;

            $scope.pdfZoomLevels = [];
            $scope.pdfViewerAPI = {};
            $scope.pdfScale = 1;
            $scope.pdfURL = "";
            $scope.pdfFile = null;
            $scope.pdfTotalPages = 0;
            $scope.pdfCurrentPage = 0;
            $scope.pdfSearchTerm = "";
            $scope.pdfSearchResultID = 0;
            $scope.pdfSearchNumOccurences = 0;
            $scope.pdfWantedPage = null;
            $scope.pdfHighlight = null;

            // configurable parameters
            var startYFromBottom = true;        // true: meaning Y coordinates are measured from bottom
            //var fixLeftCoordinates = 0;         // looks like X coordinates have a glitch, this param should handle this glitch
            var highlightVerticalPadding = 2;   // highlight vertical padding, in case of zero, highlight size should be exactly as the highlighted phrase
            var highlightHorizontalPadding = 3; // highlight horizontal padding, in case of zero, highlight size should be exactly as the highlighted phrase
            //var defaultBgColor = "#ff5f5f";   // >> now comes from CSS  // highlight color

            var pdfViewerElement = $("#pdf-viewer");
            var appendStyle = function(){
                var pdfParent = pdfViewerElement.parent();
                var style = angular.element('<style ng-bind="pdfPositionFix"></style>');
                $compile(style)($scope);
                pdfParent.append(style);
            };
            appendStyle();

            $scope.getPdfUrlAndLoad = function(isOnePagePreview){
                var pdfURL = pdfServ.getPdfUrl($scope.projectId, $scope.tenantId, $scope.docId);
                if(isOnePagePreview){
                    pdfURL = pdfURL + $scope.pageNumber + "/";
                }
                console.log("pdfURL", pdfURL);
                $scope.loadPDF(pdfURL);
            };

            $scope.onPDFProgress = function (operation, state, value, total, message) {
                console.log("onPDFProgress(" + operation + ", " + state + ", " + value + ", " + total + ")");
                if(operation === "render" && value === 1) {
                    if(state === "success") {
                        if($scope.pdfZoomLevels.length === 0) {
                            // Read all the PDF zoom levels in order to populate the combobox...
                            var lastScale = 0.1;
                            do {
                                var curScale = $scope.pdfViewerAPI.getNextZoomInScale(lastScale);
                                if(curScale.value === lastScale) {
                                    break;
                                }

                                $scope.pdfZoomLevels.push(curScale);

                                lastScale = curScale.value;
                            } while(true);
                        }

                        $scope.pdfCurrentPage = 1;
                        $scope.pdfTotalPages = $scope.pdfViewerAPI.getNumPages();
                        $scope.pdfScale = $scope.pdfViewerAPI.getZoomLevel();
                        $scope.scaleSelection();
                        $scope.isLoading = false;
                        $scope.downloadProgress = 100;
                        //console.log("$scope.pdfScale",$scope.pdfScale);
                        if($scope.pdfWantedPage){
                            $scope.pdfCurrentPage = $scope.pdfWantedPage;
                            $scope.onPDFPageChanged();
                        }
                        if($scope.pdfHighlight){
                            $scope.highlightPhrase();
                        }
                    } else {
                        $scope.isLoading = false;
                    }
                } else if(operation === "download" && state === "loading") {
                    $scope.downloadProgress = (value / total) * 100.0;
                } else {
                    if(state === "failed") {
                        console.log("Something went really bad!\n" + message);
                    }
                }
            };

            $scope.onPDFZoomLevelChanged = function () {
                $scope.pdfViewerAPI.zoomTo($scope.pdfScale);
                $scope.afterZooming();
            };

            $scope.onPDFPageChanged = function () {
                $scope.pdfViewerAPI.goToPage($scope.pdfCurrentPage);
            };
            $scope.nextPage = function() {
                var curr = $scope.pdfCurrentPage;
                var next = curr+1;
                if(next < $scope.pdfTotalPages){
                    $scope.pdfCurrentPage = next;
                    $scope.onPDFPageChanged();
                }
            };
            $scope.prevPage = function() {
                var curr = $scope.pdfCurrentPage;
                var prev = curr-1;
                if(prev>0){
                    $scope.pdfCurrentPage = prev;
                    $scope.onPDFPageChanged();
                }
            };

            $scope.zoomIn = function () {
//			console.log("zoomIn()");
                var nextScale = $scope.pdfViewerAPI.getNextZoomInScale($scope.pdfScale);
                $scope.pdfViewerAPI.zoomTo(nextScale.value);
                $scope.pdfScale = nextScale.value;
                $scope.afterZooming();
            };

            $scope.zoomOut = function () {
//			console.log("zoomOut()");
                var nextScale = $scope.pdfViewerAPI.getNextZoomOutScale($scope.pdfScale);
                $scope.pdfViewerAPI.zoomTo(nextScale.value);
                $scope.pdfScale = nextScale.value;
                $scope.afterZooming();
            };
            $scope.afterZooming = function(){
                $scope.scaleSelection();
                $scope.highlightPhrase();
            };
            $scope.loadPDF = function (pdfURL) {
                if($scope.pdfURL === pdfURL) {
                    return;
                }
                $scope.isLoading = true;
                $scope.downloadProgress = 0;
                $scope.pdfZoomLevels = [];
                $scope.pdfSearchTerm = "";
                $scope.pdfFile = null;
                $scope.pdfURL = pdfURL;
            };

            $scope.findNext = function () {
                $scope.pdfViewerAPI.findNext();
            };

            $scope.findPrev = function () {
                $scope.pdfViewerAPI.findPrev();
            };
            $scope.scaleSelection = function(){
                // this will fix the position of text-layer in order to fit mouse selection (there is a glitch)
                var margeTopCalc =  $scope.pdfScale * -2;
                $scope.pdfPositionFix = ".pdf-viewer .text-layer{margin-top:"+margeTopCalc+"px;}";
            };

            $scope.onPDFPassword = function (reason) {
                return prompt("The selected PDF is password protected. PDF.js reason: " + reason, "");
            };

            $scope.trustSrc = function(src) {
                return $sce.trustAsResourceUrl(src);
            };
            $scope.getCanvasSize = function(page){
                var hasCanvas = $q.defer();
                //console.log("page",page);
                // make sure we break the loop
                var max_retries_counter = 20;
                function getCanvas(){
                    var canvas = $(page).find('canvas');
                    if(canvas.length>0){
                        hasCanvas.resolve({width:parseFloat(canvas.attr("width")), height:parseFloat(canvas.attr("height"))});
                    }else{
                        max_retries_counter--;
                        if(max_retries_counter>0){
                            $timeout(function(){
                                getCanvas();
                            }, 100)
                        }
                    }
                }
                getCanvas();
                return hasCanvas.promise;
            };
            $scope.setPdfHighlightData = function(data){
                var page_no = data.page_no;
                if($scope.isOnePagePreview){
                    $scope.single_pageNumber = page_no;
                    page_no = 1;
                }
                $scope.pdfHighlight = {
                    page: page_no,
                    left: parseFloat(data.x1) - highlightHorizontalPadding,
                    top: parseFloat(data.y2) + highlightVerticalPadding,
                    width: parseFloat((data.x2-data.x1) + (highlightHorizontalPadding*2)),
                    height: Math.abs((data.y2-data.y1)) + (highlightVerticalPadding*2)
                };
            };
            $scope.highlightPhrase = function(){
                //console.log("highlightPhrase",$scope.pdfHighlight);
                if($scope.pdfHighlight){
                    var scaleFactor = $scope.pdfScale;
                    var highlightData = $scope.pdfHighlight;
                    var page_no = highlightData.page;
                    if(page_no != $scope.pdfCurrentPage){
                        $scope.pdfCurrentPage = page_no;
                        $scope.onPDFPageChanged();
                    }
                    var page = $("#page_" + (page_no-1));
                    $scope.getCanvasSize(page).then(function(canvasSize) {
                        var highlightLayer = page.find(".highlight-layer");//angular.element(highlightLayer);
                        if(highlightLayer.length==0){
                            // prepend in order to make it first layer and under the text-layer
                            page.prepend("<div class='highlight-layer'></div>");
                            highlightLayer = page.find(".highlight-layer");
                        }
                        //console.log("highlightData",highlightData.top, page);
                        var top = Math.ceil((startYFromBottom? (canvasSize.height/scaleFactor)-highlightData.top : highlightData.top) * scaleFactor);
                        var left = Math.ceil(highlightData.left * scaleFactor);
                        var width = Math.ceil(highlightData.width * scaleFactor);
                        var height = Math.ceil(highlightData.height * scaleFactor);
                        highlightLayer.css("left", left + "px");
                        highlightLayer.css("top", top + "px");
                        highlightLayer.css("width", width + "px");
                        highlightLayer.css("height", height + "px");
                        //highlightLayer.css("background-color", defaultBgColor);
                        //console.log("canvasSize",canvasSize, "top", top, scaleFactor);
                    })
                }
            };
            $scope.removeHighlight = function(){
                $scope.pdfHighlight = null;
                $(".highlight-layer").remove();
            }
        },
        ['$scope', 'pdfServ', 'generalServ', '$state', 'GLOBAL_VARS', '$filter', '$cookies', '$dialog', '$timeout', '$stateParams', '$rootScope', '$q', '$sce', '$compile'
        ]);
});
