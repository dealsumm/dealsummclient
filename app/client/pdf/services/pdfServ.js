define(function (require) {
    'use strict';

    var app = require('appModule');

    app.register.service('pdfServ',
        ['$http', '$timeout','$q', 'DEALSUMM_CONFIG', 'GLOBAL_VARS', '$cookies', '$rootScope',
            function ($http,     $timeout,  $q,   DEALSUMM_CONFIG,   GLOBAL_VARS,   $cookies,   $rootScope) {

                var returnObject = {}, promise, url;
                var basePath = DEALSUMM_CONFIG.DSPROD_REST_PATH;
                var oldBasePath = DEALSUMM_CONFIG.REST_PATH;

                returnObject.getPdfUrl = function(projectId, tenantId, documentId) {
                    return oldBasePath + $rootScope.$stateParams.accountId + "/folders/" + tenantId +"/documents/" + documentId + "/orig/";
                };
                returnObject.getAnnotationDetails = function(projectId, tenantId, documentId, annotationId) {
                    url = basePath + $rootScope.$stateParams.accountId + '/projects/' + projectId + "/tenants/" + tenantId +"/documents/" + documentId + "/annotations/" + annotationId + "/";
                    promise = $http.get(url).success(function (data, status) {
                        return data;
                    });
                    return promise;
                };
                returnObject.getPdfTocItemDetails = function(projectId, tenantId, documentId, tocItemId) {
                    url = basePath + $rootScope.$stateParams.accountId + '/projects/' + projectId + "/tenants/" + tenantId +"/documents/" + documentId + "/toc_items/" + tocItemId + "/";
                    promise = $http.get(url).success(function (data, status) {
                        return data;
                    });
                    return promise;
                };


                return returnObject;
            }
        ]);
});

