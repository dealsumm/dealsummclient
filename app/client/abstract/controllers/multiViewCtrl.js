define(function (require) {
    'use strict';
    var app = require('appModule');

    app.register.controller('multiViewCtrl',
        function ($scope, multiViewServ, generalServ,  $state,   GLOBAL_VARS,   $filter,   $cookies,   $dialog,   $timeout,  $stateParams, $rootScope, $q) {

            $scope.stateParams = $rootScope.$stateParams;
            $scope.onePageThreshold = 5; // above this value, clicking a search result will ask for one page only
            var init = function (queryIdChanged){
                if($scope.docDrawer.drawerState>1){
                    if(!$scope.stateParams.annotationId && !$scope.stateParams.docId){
                        $scope.docDrawer.drawerState = 1;
                        $scope.docDrawer.enableActions = false;
                    }
                }
                multiViewServ.getSearchPhrase($scope.queryId).then(function(resp) {
                    //console.log("getSearchPhrase",resp.data);
                    $scope.queryLabel = resp.data['value'];
                });
                multiViewServ.getSearchResults($scope.queryId).then(function(resp) {
                    //console.log("getSearchResults",resp.data);
                    //console.log("additional",resp.data['additional']);
                    $scope.resultsDirectData = resp.data['direct'];
                    $scope.resultsAdditionalData = resp.data['additional'];
                    //console.log("direct",$scope.resultsDirectData, $scope.resultsDirectData.length);
                    // auto show document
                    if($scope.resultsDirectData.length==1 || $scope.docDrawer.drawerState>1){//} && $state.is('app.clientAbstract.multiView')){
                        $scope.redirectToDocument($scope.resultsDirectData[0]);
                    }/*else{
                        if(queryIdChanged){
                            $scope.docId = null;
                            $scope.annotationId = null;
                            $scope.docDrawer.drawerState = 0;
                            //$scope.docDrawer.enableActions = false;
                            $state.go('app.clientAbstract.multiView', {q: $scope.stateParams.q});
                        }
                    }*/
                });
            };
            $scope.$on('$stateChangeSuccess',
                function(event, toState, toParams, fromState, fromParams){
                    console.log("multiViewCtrl stateChange",toParams, toState);
                    var queryIdChanged = ($scope.queryId && $scope.queryId != $scope.stateParams.q);
                    console.log("queryIdChanged",queryIdChanged);
                    if($scope.stateParams.q && (($scope.queryId && $scope.queryId != $scope.stateParams.q) || !$scope.queryId)){
                        $scope.queryId = $scope.stateParams.q;//decodeURIComponent(stateParams.q);
                        $scope.docId = $scope.stateParams.docId;
                        $scope.annotationId = $scope.stateParams.annotationId;
                        init(queryIdChanged);
                    }
                }
            );



            $scope.redirectToDocument = function(item){
                if(!item){
                    return;
                }
                var docId = item.document.id;
                //var phrase = item.phrase.from + "-" + item.phrase.to;
                console.log("item", item.document.filetype, item);
                //$scope.docDrawer.showDrawer = true;
                $scope.docDrawer.drawerState = 2;
                $scope.docDrawer.enableActions = true;
                /*if($scope.docDrawer.collapseDrawer){
                    $scope.docDrawer.collapseDrawer = false;
                }*/
                if(item.document.filetype=="text/html"){
                    //console.log("item HTML",item);
                    var startToken = item.start_token*1;
                    var endToken = startToken + (item.no_of_tokens*1);
                    $state.go('app.clientAbstract.multiView.doc', {docId:docId, phrase: startToken + "-" + endToken});
                    //$state.go('app.clientDocumentView.phrase', {docId:docId, phrase: startToken + "-" + endToken});
                }else{
                    var annotationId = item.id;
                    //console.log("annotationId",annotationId);
                    //console.log("item.document.no_of_pages",item.document.no_of_pages);
                    //item.document.no_of_pages = 30;
                    /*if(item.document.no_of_pages && item.document.no_of_pages > $scope.onePageThreshold){
                        $state.go('app.clientPdfView.annotation.preview', {docId:docId, annotationId:annotationId, pageNumber:item.page_no});
                    }else{
                        $state.go('app.clientPdfView.annotation', {docId:docId, annotationId:annotationId});
                    }*/
                    //console.log("$scope.showDrawer",$scope,$scope.docDrawer.showDrawer);
                    if(item.document.no_of_pages && item.document.no_of_pages > $scope.onePageThreshold){
                        $state.go('app.clientAbstract.multiView.pdf.preview', {docId:docId, annotationId:annotationId, pageNumber:item.page_no});
                    }else{
                        $state.go('app.clientAbstract.multiView.pdf', {docId:docId, annotationId:annotationId});
                    }
                }
            }
        },
        ['$scope', 'multiViewServ', 'generalServ', '$state', 'GLOBAL_VARS', '$filter', '$cookies', '$dialog', '$timeout', '$stateParams', '$rootScope', '$q']
    );
});
