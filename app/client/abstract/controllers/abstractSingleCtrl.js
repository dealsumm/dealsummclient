define(function (require) {
    'use strict';
    var app = require('appModule');

    app.register.controller('abstractSingleCtrl',
        function ($scope, generalServ, $state, GLOBAL_VARS, $filter, $cookies, $dialog, $timeout, $stateParams, $rootScope) {


            console.log("abstractSingleCtrl");
            var stateParams = $rootScope.$stateParams;
            $scope.resizer_force_left_width = 0;

            $scope.expandedZone = [];
            $scope.expandedChart = [];
            $scope.expandedSection = [];
            $scope.editLocked = true;
            /*$scope.toggleChart = false;
            $scope.expandedSection = null;
            $scope.highlightedCell = "";
            $scope.currentEditedCell = "";
            $scope.abstractViewType = false;*/

            $scope.backToList = function () {
                $state.go('app.property');
            };
            $scope.enableTokenSelection = true;

            $scope.switchToAbstract = function(val){
                if(stateParams.phrase){
                    $state.go('app.clientDocumentView.phrase');
                }else if(stateParams.sectionId) {
                    $state.go('app.clientDocumentView.section');
                }else{
                    $state.go('app.clientDocumentView');
                }
            };


            $scope.doc_id = stateParams.docId;
            $scope.projectId = stateParams.projectId;
            $scope.tenantId = stateParams.tenantId;
            $scope.init = function () {
                generalServ.http('general', 'getFolderInfo',[$scope.projectId, $scope.tenantId]).then(function(resp) {
                    $scope.tenantName = resp.data.name;
                })
                generalServ.http('general', 'getProjectInfo',[$scope.projectId]).then(function(resp) {
                    $scope.projectName = resp.data.name;
                })
                $scope.getDocAbstract();
            }


            $scope.getDocAbstract = function(){
                generalServ.http('document', 'getDocAbstract',[$scope.doc_id]).then(function(resp) {
                    console.log("abstractTableJSON", resp.data);
                    $scope.listData = resp.data['list'];
                    $scope.chartsZones = resp.data['charts_zone'];
                    //$scope.prevDoc = resp.data.info.prev_doc;
                    //$scope.nextDoc = resp.data.info.next_doc;
                    $scope.tst_id = resp.data['info']['tst_id'];
                    $scope.tst_name = resp.data['info']['tst_name'];
                })
            };
            $scope.init();
            $scope.switchToDocumentView = function(){
                if(stateParams.phrase){
                    $state.go('app.clientDocumentView.phrase');
                }else if(stateParams.sectionId) {
                    $state.go('app.clientDocumentView.section');
                }else{
                    $state.go('app.clientDocumentView');
                }
            };
            $scope.switchToEditView = function() {
                console.log('edit template');
                if(stateParams.phrase){
                    $state.go('app.clientSingleAbstract.editTemplatePhrase');
                }else if(stateParams.sectionId) {
                    $state.go('app.clientSingleAbstract.editTemplateSection');
                }else{
                    $state.go('app.clientSingleAbstract.editTemplate');
                }
            };


            $scope.saveEditScore = function(chartData){
                //console.log("chartData",chartData.chart_id,chartData.score);
                var dataToSend = {};
                dataToSend['score'] = chartData.score;
                generalServ.http('abstract', 'updateScore',[chartData.chart_id], dataToSend).then(function(resp) {
                    if(resp.status=="400"){
                        console.log("Score FAILED", chartData.originalScore);
                        chartData.score = chartData.originalScore;
                    }else{
                        chartData.originalScore = chartData.score;
                    }
                })
            }
            $scope.clickedToggleIgnoreColumn = function(chart, header){
                var dataToSend = {};
                dataToSend['ignore'] = !header.ignore;
                generalServ.http('abstract', 'toggleIgnoreColumn',[chart.chart_id, header.column_no], dataToSend).then(function(resp) {
                    header.ignore = !header.ignore;
                })
            }
            $scope.clickedToggleIgnoreRow = function(chart, row){
                var dataToSend = {};
                dataToSend['ignore'] = !row.ignore;
                console.log("row ", row.row_id, row.ignore);
                generalServ.http('abstract', 'toggleIgnoreRow',[chart.chart_id, row.row_id], dataToSend).then(function(resp) {
                    if(!isNaN(resp.data.row_no)){
                        chart.rows.splice(resp.data.row_no, 1);
                    }else{
                        row.ignore = !row.ignore;
                    }
                })
            };
            $scope.clickedMakeCellPrimary = function(){
                var dataToSend = {};
                dataToSend['column_no'] = $scope.editedCell.column_no;
                var property_id = $scope.editedCell.new['property_id'];
                var chart_id = $scope.editedCell.chart['chart_id'];
                $scope.cancelEditValue();
                generalServ.http('abstract', 'makeCellPrimary',[chart_id, property_id], dataToSend).then(function(resp) {
                    // due to server technical issues, in order to see the full chart need to reload it from server
                    console.log("makeCellPrimary OK");
                    $scope.getDocAbstract();
                })
            }
            $scope.clickedAddNewRow = function(chart, row_no){
                $scope.addingNewRow = true;
                var dataToSend = {};
                dataToSend['row_no'] = row_no;
                generalServ.http('abstract', 'addNewRow',[chart.chart_id], dataToSend).then(function(resp) {
                    var newRow = resp.data;
                    console.log("newRow",newRow);
                    $scope.addingNewRow = false;
                    chart.rows.splice(row_no, 0, newRow);
                })
            }

            $scope.clickedCell = function(row, cell_ref, evt){
                var cell = row.cells[cell_ref];
                if(cell && cell.to_token!=-1 && cell.to_token!=null && cell.from_token!=-1 && cell.from_token!=null){
                    console.log("phrase>", cell.from_token+"-"+cell.to_token);
                    cell.clicked = true;
                    removeCellHighlight(cell);
                    $state.go('app.clientSingleAbstract.phrase', {phrase: cell.from_token+"-"+cell.to_token});
                }
            }
            function removeCellHighlight(cell){
                $timeout(function(){
                   delete cell.clicked;
                }, 2100);
            }
            $scope.editedCell = {};
            $scope.cancelEditValue = function(){
                $scope.editedCell = {};
                $("#edit-cell").hide();
            }
            $scope.saveEditValue = function(){
                if($scope.editedCell.type=='header'){
                    saveHeaderNewValue();
                }else if($scope.editedCell.type=='column'){
                    addNewColumn();
                }else if($scope.editedCell.type=='cell'){
                    saveCellNewValue();
                }
                $("#edit-cell").hide();
            };
            function addNewColumn(){
                var newColumnLabel = $scope.editedCell.new.label;
                var chart_id = $scope.editedCell.chart.chart_id;
                var dataToSend = {
                    column_name: newColumnLabel
                };
                generalServ.http('abstract', 'addNewColumn',[chart_id], dataToSend).then(function(resp) {
                    // due to server technical issues, in order to see the full chart need to reload it from server
                    console.log("addNewColumn - handle here");
                    $scope.getDocAbstract();
                })
            }
            function saveHeaderNewValue(){
                var newHeader = $scope.editedCell.new;
                var chart_id = $scope.editedCell.chart.chart_id;
                var dataToSend = {
                    name: newHeader.label
                };
                generalServ.http('abstract', 'saveHeaderValue',[chart_id, newHeader['column_no']], dataToSend).then(function(resp) {
                    $scope.editedCell.cell.label = newHeader.label;
                })
            }
            function saveCellNewValue(){
                var newCell = $scope.editedCell.new;
                console.log("cell",newCell);
                var dataToSend = {
                    new_value:newCell.label,
                    from_token:newCell.from_token,
                    to_token:newCell.to_token
                };
                generalServ.http('abstract', 'saveCellValue',[newCell.property_id], dataToSend).then(function(resp) {
                    $scope.editedCell.cell.label = newCell.label;
                    $scope.editedCell.cell.from_token = newCell.from_token;
                    $scope.editedCell.cell.to_token = newCell.to_token;
                    $scope.editedCell = {};
                })
            }
            $scope.clickedPasteFromSelection = function(){
                $scope.selectionPhrase = $rootScope.selectionPhrase;
                console.log("$rootScope.selectionPhrase",$rootScope.selectionPhrase);
                if($scope.selectionPhrase){
                    $scope.editedCell.new.label = $scope.selectionPhrase.label;
                    $scope.editedCell.new.from_token = $scope.selectionPhrase.phrase.from_token;
                    $scope.editedCell.new.to_token = $scope.selectionPhrase.phrase.to_token;
                    $scope.editedCell.new.doc_id = $scope.selectionPhrase.doc_id;
                }

            }
            $scope.clickedEditHeaderValue = function(chart, header, evt){
                if(header.ignore){
                    return;
                }
                $scope.editedCell.advanced = true;
                $scope.editedCell.message = null;
                $scope.editedCell.type = "header";
                $scope.editedCell.chart = chart;
                $scope.editedCell.cell = header;
                $scope.editedCell.column_no = null;
                $scope.editedCell.new = angular.copy(header);
                positionEditModal(evt, ".cell");
            }
            $scope.clickedEditCellValue = function(row, cell_ref, chart, evt){
                //console.log("row, cell_ref, chart",row, cell_ref, chart);
                var cell = row.cells[cell_ref];
                if(!cell || (row.ignore || cell.ignore)){
                    return;
                }
                var column_no = $filter("filter")(chart.headers, {ref: cell_ref})[0]['column_no'];
                $scope.editedCell.advanced = false;
                $scope.editedCell.message = null;
                $scope.editedCell.type = "cell";
                $scope.editedCell.chart = chart;
                $scope.editedCell.cell = cell;
                $scope.editedCell.column_no = column_no;
                $scope.editedCell.new = angular.copy(cell);
                positionEditModal(evt, ".cell");
            }
            $scope.clickedAddNewColumn = function(chart, evt){
                $scope.editedCell.advanced = false;
                $scope.editedCell.message = "New column name:";
                $scope.editedCell.type = "column";
                $scope.editedCell.chart = chart;
                $scope.editedCell.cell = null;
                $scope.editedCell.column_no = null;
                $scope.editedCell.new = {label:""};
                //console.log("evt",evt);
                positionEditModal(evt, ".left-spacer");
            }
            function positionEditModal(evt, closest){
                var pos = getRealPosition($(evt.target).closest(closest), "single-abstract-table");
                pos.top -= 3;
                //console.log("pos.left",pos.left, $("#edit-cell").outerWidth(), $("#edit-cell").width());
                if(pos.left + $("#edit-cell").outerWidth() > pos._width){
                    // in case the modal goes out of bounds > move it to max left
                    pos.left = pos._width - $("#edit-cell").outerWidth() -1*($scope.editedCell.type=="cell"?40:20);
                }
                $("#edit-cell").css({"left": pos.left, "top": pos.top}).show();
                $("#edit-cell-name").focus();
            }
            function getRealPosition(elt, requestedClass) {
                elt = elt[0];
                var xPosition = 0;
                var yPosition = 0;
                while(elt && !angular.element(elt).hasClass(requestedClass)) {
                    xPosition += (elt.offsetLeft - elt.scrollLeft + elt.clientLeft);
                    yPosition += (elt.offsetTop - elt.scrollTop + elt.clientTop);
                    elt = elt.offsetParent;
                }
                var _width = elt.offsetWidth;
                var _height = elt.offsetHeight;
                //console.log("pos >>",elt, xPosition, yPosition);
                return { left: xPosition, top: yPosition, _width:_width, _height:_height};
            }
        },
        ['$scope', 'abstractServ', 'generalServ', '$state', 'GLOBAL_VARS', '$filter', '$cookies', '$dialog', '$timeout', '$stateParams', '$rootScope'
        ]);
});
