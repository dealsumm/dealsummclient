define(function (require) {
    'use strict';
    var app = require('appModule');
    app.register.controller('abstractCtrl',
        function ($scope, abstractServ, generalServ, $state, GLOBAL_VARS, $filter, $cookies, $dialog, $timeout, $stateParams, $rootScope) {


            $scope.expandedDiv = [];
            $scope.expandedRow = [];
            $scope.expandedTable = [];
            $scope.approvedChecks = [];
            $scope.activeTab = 0;
            $scope.abstractViewType = false;
            $scope.maxRowsOnTable = 7;



            var stateParams = $scope.stateParams = $rootScope.$stateParams;
            $scope.$state = $state;
            // temp - handle PDF abstract page
            if ($state.includes('app.clientAbstractPdf')) {
                $scope.resizer_force_left_width = 40;
            }

            $scope.docId = stateParams.docId;
            $scope.projectId = stateParams.projectId;
            $scope.tenantId = stateParams.tenantId;

            $scope.docDrawer = {};
            //$scope.docDrawer.showDrawer = false;
            $scope.docDrawer.collapseDrawer = false;
            $scope.docDrawer.expandMaxDrawer = false;
            $scope.docDrawer.enableActions = true;
            $scope.docDrawer.drawerState = 0;
            $scope.clickedExpandBtn = function(){
                if($scope.docDrawer.drawerState==3){
                    $scope.docDrawer.drawerState=2;
                }else if($scope.docDrawer.drawerState==2){
                    $scope.docDrawer.drawerState=3;
                }else{
                    $scope.docDrawer.drawerState=3;
                }
            }
            $scope.clickedCollapseBtn = function(){
                if($scope.docDrawer.drawerState==3){
                    $scope.docDrawer.drawerState=1;
                }else if($scope.docDrawer.drawerState==1){
                    $scope.docDrawer.drawerState=2;
                }else{
                    $scope.docDrawer.drawerState=1;
                }
            };
            $scope.$watch('docDrawer.drawerState', function(val){
                $('#document-drawer').removeClass("expand-max-drawer").removeClass("collapsed-drawer");
                //console.log("docDrawer.drawerState",val);
                if(val==3){
                    $('#document-drawer').addClass("expand-max-drawer");
                }else if(val==1){
                    $('#document-drawer').addClass("collapsed-drawer");
                }
            });
            if($state.includes('app.clientAbstract.multiView.pdf') || $state.is('app.clientAbstract.multiView.doc') || $state.is('app.clientAbstract.doc') && stateParams.docId) {
                if(stateParams.annotationId || stateParams.phrase){
                    $scope.docDrawer.drawerState = 2;
                }
            }
            $scope.init = function () {
                abstractServ.getAbstractTable($scope.projectId, $scope.tenantId).then(function (abstractJSON) {
                    $scope.abstractData = abstractJSON.data;
                    var headers = $scope.abstractData.charts_zone.rent_roll_periods.headers;
                    //headers[0].format = "dateTime";
                    $scope.abstractChartsHeaders = headers;
                    $scope.abstractChartsRows = $scope.abstractData.charts_zone.rent_roll_periods.periods;
                    console.log("abstractJSON - ", abstractJSON);
                    //$scope.tst_id = abstractJSON.data.info.tst_id;
                })
                generalServ.http('general', 'getFolderInfo',[$scope.projectId, $scope.tenantId]).then(function(resp) {
                    $scope.tenantName = resp.data.name;
                    $scope.tenantSuite = resp.data.suite;
                })
                generalServ.http('general', 'getProjectInfo',[$scope.projectId]).then(function(resp) {
                    $scope.projectName = resp.data.name;
                })
            }
            // temp - handle PDF abstract page (if not...)
            if (!$state.is('app.clientAbstractPdf')) {
                $scope.init();
            }

            var tempDocType = ",Amendment,Master,Certificate,";
            var tempOptionalLetters = ['M', 'A', 'B', 'C'];
            $scope.tempGetLetter = function (cells, isTable) {
                var ret = "";
                if (!isTable) {
                    angular.forEach(cells, function (cell, idx) {
                        if (tempDocType.indexOf("," + cell.label + ",") > -1) {
                            ret = cell.label.substring(0, 1);
                        }
                    })
                }
                if (ret.length == 0) {
                    var rnd = Math.floor((Math.random() * 4));
                    ret = tempOptionalLetters[rnd];
                }
                return ret;
            };
            $scope.backToList = function () {
                $state.go('app.property');
            };
            $scope.unEditCell = function (evt) {
                $scope.editedCell.cell.isOnEditMode = false;
                $scope.editedCell = null;
                // remove class from parent cell
                $("#edit-cell-content").parent().removeClass("edit-mode-cell");
                evt.preventDefault();
                evt.stopPropagation();
            };
            $scope.saveEditedCell = function (evt) {
                var cell = $scope.editedCell.cell;
                console.log("can't save cell value...");
                // TODO - this is temp, change the cell value without saving
                $scope.editedCell.cell.value = $scope.editedCell.value;
                /*abstractServ.editCell($scope.docId, cell.id, $scope.editedCell.value, $scope.tst_id, $scope.currentEditedCellSectionId, cell.main, $scope.currentEditedRowId, $scope.currentEditedCellChartId).then(function(resp){
                 $scope.editedCell.cell.label = $scope.editedCell.value;
                 })*/
                // after saving...
                $scope.unEditCell(evt);
            }
            $scope.editCellValue = function (cell, evt) {
                if ($scope.editedCell) {
                    if ($scope.editedCell.cell.$$hashKey == cell.$$hashKey) {
                        return;
                    }
                    $scope.unEditCell(evt);
                }
                var domCell = evt.target;
                cell.isOnEditMode = true;
                $scope.editedCell = {value: cell.value, cell: cell};
                $(domCell).addClass("edit-mode-cell");
                $("#edit-cell-content").appendTo(domCell);
            };
            $scope.isCellSearch = false;
            $scope.clickedCellSearch = function () {
                //$scope.isCellSearch = true;
            };
            $scope.clickedOnCell = function (cell) {
                console.log("cell mark phrase", cell.isOnEditMode, $scope.isCellSearch);
                if (!cell || cell.isOnEditMode) {
                    return;
                }
                var phrase = cell.phrase;
                var cell_docId = cell.doc_id;
                //var source_doc_id = cell.source_doc_id;
                var prop_id = cell.property_id;

                console.log("cell>", cell);
                //doc_id = 105;
                //console.log("doc_id>", $scope.docId, doc_id);
                if(prop_id){
                    if(stateParams.docId && stateParams.docId == cell_docId){
                        if(stateParams.annotationId){
                            //console.log("stateParams.docId",stateParams.docId, cell_docId);
                            if(stateParams.pageNumber) {
                                $state.go('app.clientAbstract.multiView.pdf.preview', {q: prop_id});
                            }else{
                                $state.go('app.clientAbstract.multiView.pdf', {q: prop_id});
                            }
                        }else if(stateParams.phrase){
                            $state.go('app.clientAbstract.multiView.doc', {q: prop_id});
                        }
                    }else{
                        $state.go('app.clientAbstract.multiView', {q: prop_id});
                    }
                }else if(phrase){
                    $scope.docDrawer.drawerState = 2;
                    if(stateParams.q){
                        $state.go('app.clientAbstract.multiView.doc', {q:stateParams.q, docId: cell_docId, phrase:phrase});
                    }else{
                        $state.go('app.clientAbstract.doc', {docId: cell_docId, phrase:phrase});
                    }
                }
            };
            $scope.checkOptionsArr = ['not-reviewed', 'reviewed', 'questionable', 'missing'];
            $scope.toggleCheck = function (outer, inner) {
                var val = $scope.approvedChecks[outer][inner];
                val++;
                if (val > $scope.checkOptionsArr.length - 1) {
                    val = 0;
                }
                //console.log("val",val);
                $scope.approvedChecks[outer][inner] = val;
            };
            $scope.getRandomInt = function (min, max) {
                return Math.floor(Math.random() * (max - min + 1)) + min;
            }
            $scope.makePrimary = function (line, row_id, event) {
                var section_id = line.sectionID;
                var rows = line.rows;

                var item = rows.filter(function (row) {
                    return row.row_id == row_id;
                })[0];

                var _index = rows.indexOf(item);
                rows.splice(_index, 1);
                rows.unshift(item);
                //console.log("rows._index",rows[_index]);


                //var itemToHighlight = event.target.parentElement.parentElement;
                //$(itemToHighlight).pulse({backgroundColor: 'rgba(245,245,245,0.5)', color: '#1a9cf9'}, {duration: 400, pulses: 5});

                var eltContainer = $(event.target.parentElement.parentElement);
                var myTop = eltContainer.position().top;
                eltContainer.attr("style", "position:absolute;background-color:rgba(255,255,255,0.5); top:" + myTop + "px; width:100%");
                eltContainer.animate({
                    top: 0
                }, {
                    duration: 700,
                    specialEasing: {
                        top: "easeOutBack"
                    },
                    complete: function () {
                        eltContainer.removeAttr("style");
                        $(eltContainer).pulse({
                            backgroundColor: 'rgba(245,245,245,0.5)',
                            color: '#1a9cf9'
                        }, {duration: 400, pulses: 3});
                    }
                });
                //console.log("$(itemToHighlight)",$(event.target.parentElement.parentElement.parentElement).offset());
                abstractServ.makePrimary($scope.docId, section_id, row_id, $scope.tst_id).then(function (resp) {
                    console.log("makePrimary ok");
                })
            }
        },
        ['$scope', 'abstractServ', 'generalServ', '$state', 'GLOBAL_VARS', '$filter', '$cookies', '$dialog', '$timeout', '$stateParams', '$rootScope'
        ]);
});
