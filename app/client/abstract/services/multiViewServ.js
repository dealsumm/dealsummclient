define(function (require) {
    'use strict';

    var app = require('appModule');

    app.register.service('multiViewServ',
                ['$http', '$timeout','$q', 'DEALSUMM_CONFIG', 'GLOBAL_VARS', '$cookies', '$rootScope',
        function ($http,     $timeout,  $q,   DEALSUMM_CONFIG,   GLOBAL_VARS,   $cookies,   $rootScope) {

            var returnObject = {}, promise, url;
            var basePath = DEALSUMM_CONFIG.DSPROD_REST_PATH;
            var oldBasePath = DEALSUMM_CONFIG.REST_PATH;
            returnObject.getSearchPhrase = function (propID) {
                var url = basePath + $rootScope.$stateParams.accountId + '/properties/' + propID + "/";
                var promise = $http({ method: 'GET', url: url}).then(function (response) {
                    return response;
                });
                return promise;
            };
            returnObject.getSearchResults = function (propID) {
                var url = basePath + $rootScope.$stateParams.accountId + '/properties/' + propID + "/annotations/";
                var promise = $http({ method: 'GET', url: url}).then(function (response) {
                    return response;
                });
                return promise;
            };
            returnObject.getMultiViewAccordion = function() {
                var retJson = [
                    {label:"​Direct Results", name:"direct", query:"/?direct=true/"},
                    {label:"More Results", name:"secondary", query:"/"}
                ];
                var deferred = $q.defer();
                deferred.resolve(retJson);
                return deferred.promise;
            };

            return returnObject;
        }
    ]);
});

