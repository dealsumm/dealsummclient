define(function (require) {
    'use strict';

    var app = require('appModule');

    app.register.service('abstractServ',
                ['$http', '$timeout','$q', 'DEALSUMM_CONFIG', 'GLOBAL_VARS', '$cookies', '$rootScope',
        function ($http,     $timeout,  $q,   DEALSUMM_CONFIG,   GLOBAL_VARS,   $cookies,   $rootScope) {

            var returnObject = {}, promise, url;
            var basePath = DEALSUMM_CONFIG.DSPROD_REST_PATH;
            var oldBasePath = DEALSUMM_CONFIG.REST_PATH;

            // get the table TS from the server
            returnObject.getAbstractTable = function (projectId, tenant_id) {
                var url = basePath + $rootScope.$stateParams.accountId + '/projects/' + projectId + '/tenants/' + tenant_id + '/abstract/';
                var promise = $http.get(url).then(function (response) {
                    return response;
                });
                return promise;
            };
            returnObject.makePrimary = function (doc_id, section_id, primary_line, tst_id) {
                var data = {};
                data['doc_id'] = doc_id;
                data['section_id'] =  section_id;
                data['primary_line'] =  primary_line;
                data['tst_id'] =  tst_id;

                var url = oldBasePath + $rootScope.$stateParams.accountId + '/makePrimary/';
                //console.log("makePrimary",data, url);
                return $http.post(url, data);
            };
            returnObject.editCell = function (doc_id, cell_id, new_value, tst_id, field_id, main, row_id, chart_id) {
                var data = {};
                data['doc_id'] = doc_id;
                data['cell_id'] = cell_id;
                data['row_id'] = row_id;
                data['new_value'] = new_value;
                data['tst_id'] = tst_id;
                data['field_id'] = field_id;
                data['chart_id'] = chart_id;
                data['main'] = main;
                var url = oldBasePath + $rootScope.$stateParams.accountId + '/editCell/';
                console.log("editCell",data, url);
                return $http.post(url, data);
            };

            // get the table TS from the server
            // OLD API
            returnObject.getTsTable = function (doc_id) {
                var url = oldBasePath + $rootScope.$stateParams.accountId + '/docs/?func=get_ts_table_json&doc_id=' + doc_id;
                var promise = $http.get(url).then(function (response) {
                    return response;
                });
                return promise;
            };

            return returnObject;
        }
    ]);
});

