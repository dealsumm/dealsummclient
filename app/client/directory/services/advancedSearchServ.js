define(function (require) {
    'use strict';

    var app = require('appModule');

    app.register.service('advancedSearchServ', ['$http', '$timeout','$q', 'DEALSUMM_CONFIG', 'GLOBAL_VARS', '$cookies', '$rootScope',
        function ($http, $timeout, $q, DEALSUMM_CONFIG, GLOBAL_VARS, $cookies, $rootScope) {

            var returnObject = {}, promise;
            var basePath = DEALSUMM_CONFIG.DSPROD_REST_PATH;
            var oldBasePath = DEALSUMM_CONFIG.REST_PATH;

            returnObject.getAdvancedSearchData = function (nodeId, nodeType) {
                var data = {};
                data['nodetype'] = nodeType;
                data['nodeid'] = nodeId;
                data['get_advanced_search_params'] = 'true'; // pass the command
                var url = oldBasePath + $rootScope.$stateParams.accountId + '/' + nodeType + '/' + nodeId + '/';
                //console.log("data",data, url);
                return $http.post(url, data);
            };
            return returnObject;

    }]);


});



