define(function (require) {
    'use strict';

    var app = require('appModule');

    app.register.service('directoryServ',
        ['$http', '$timeout','$q', 'DEALSUMM_CONFIG', 'GLOBAL_VARS', '$cookies', '$rootScope',
            function ($http,     $timeout,  $q,   DEALSUMM_CONFIG,   GLOBAL_VARS,   $cookies,   $rootScope) {

                var returnObject = {}, promise, url;
                var basePath = DEALSUMM_CONFIG.DSPROD_REST_PATH;
                var oldBasePath = DEALSUMM_CONFIG.REST_PATH;

                returnObject.globalActionsForItemService = function (itemId, rel, action, async) {
                    var data = {};
                    data['id'] = itemId; // could be an ID of folder or document
                    data['rel'] = rel;
                    data[action] = 'true'; // pass the command
                    if(async){
                        data['async'] = 'true';
                    }
                    var url = oldBasePath + $rootScope.$stateParams.accountId + '/' + data['rel'] + '/' + data['id'] + '/';
                    console.log("data reprocess",data, url);
                    return $http.post(url, data);
                };
                returnObject.renameItem = function (itemId, rel, name) {
                    var data = {};
                    data['id'] = itemId;
                    data['name'] = name;
                    data['rel'] = rel;
                    data['rename'] = 'true'; // pass the command
                    var url = oldBasePath + $rootScope.$stateParams.accountId + '/' + data['rel'] + '/' + data['id'] + '/';
                    return $http.post(url, data);
                };
                returnObject.templateAssignment = function (node_id, rel) { // the new call for TST assignment modal
                    // http://10.0.0.15:8000/deals/orgs/1/TSTAssignment/ old
                    // http://10.0.0.15:8000/deals/orgs/1/TSTAssignment/ new
                    var data = {};
                    data['obj_id'] = ""+node_id;
                    data['obj_type'] = rel;
                    var url = oldBasePath + $rootScope.$stateParams.accountId + '/TSTAssignment/';
                    console.log("data",data, url);
                    return $http.post(url, data);
                };
                returnObject.templateUnAssign = function (tst_id, det_obj_id, rel) { // remove assigned TST
                    var data = {};
                    data['tst_id'] = tst_id;
                    data['delete_assign_tst'] = true;
                    data['obj_type'] = rel;
                    var url = oldBasePath + $rootScope.$stateParams.accountId + '/' + rel + '/' + det_obj_id + '/';
                    console.log("data",data, url);
                    return $http.post(url, data);
                };
                returnObject.assignNewTemplate = function (tst_id, node_id, rel) { // assign a new TST to item
                    var data = {};
                    data['tst_id'] = tst_id;
                    data['assign_new_tst'] = true;
                    console.log("tst_id", tst_id, node_id);

                    var url = oldBasePath + $rootScope.$stateParams.accountId + '/' + rel + '/' + node_id + '/';
                    console.log("data",data, url);
                    return $http.post(url, data);
                };
                return returnObject;
            }
        ]);
});

