

define(function (require) {
    'use strict';

    var app = require('appModule');

    app.register.service('directoryRcmServ',
        ['$http', 'directoryServ',  'generalServ','$timeout','$q', 'DEALSUMM_CONFIG', 'GLOBAL_VARS', '$cookies', '$rootScope', '$dialog', '$filter',
            function ($http, directoryServ,   generalServ,  $timeout,  $q,   DEALSUMM_CONFIG,   GLOBAL_VARS,   $cookies,   $rootScope, $dialog, $filter) {

                var returnObject = {}, promise, url;
                var basePath = DEALSUMM_CONFIG.DSPROD_REST_PATH;
                var oldBasePath = DEALSUMM_CONFIG.REST_PATH;

                /* ************************ */
                /* RCM for tree view         */
                /* ************************ */
                returnObject.setParentScope = function(scope){
                    returnObject.scope = scope;
                }
                returnObject.defineRightMenuItems = [
                    //{"label": "Create Folder", "callback":"createFolderDialog", "vars":{}, display:"parentId==null"},
                    {"label": "Delete", "callback":"deleteItem", "vars":{}, display:true},
                    {"label": "Rename", "callback":"renameItem", "vars":{}, display:true},
                    {"label": "Search", "callback":"searchTreeItem", "vars":{}, display:true},
                    {"label": "Re-Process", "callback":"reprocessItem", "vars":{title:'Re-process', func:'reprocess'}, display:true},
                    {"label": "Re-Process Inference", "callback": "globalActionsForItem", "vars": {title:'Re-process Inference', func:'reprocess_inference'}, display: true},
                    {"label": "Re-Process NLP", "callback": "globalActionsForItem", "vars": {title:'Re-process NLP', func:'reprocess_tree_nlp'}, display: true},
                    //{"label": "Re-Process Coordinates (doc)", "callback": "globalActionsForItem", "vars": {title:'Re-process coordinates', func:'reprocess_coordinates'}, display: true},
                    {"label": "Sanitize", "callback":"globalActionsForItem", "vars":{title:'Sanitizing', func:'sanitize'}, display:true},
                    {"label": "Count-Words", "callback":"globalActionsForItem", "vars":{title:'Count-Words', func:'count_words'}, display:true},
                    {"label": "Export", "callback":"globalActionsForItem", "vars":{title:'Export', func:'MyExport'}, display:true},
                    {"label": "Import", "callback":"globalActionsForItem", "vars":{title:'Import', func:'MyImport'}, display:true},
                    {"label": "Show Preamble", "callback":"globalActionsForItem", "vars":{title:'Show Preamble', func:'ShowPreamble'}, display:true},
                    {"label": "Assign Template", "callback":"itemTemplate", "vars":{}, display:true}
                ];

                returnObject.initRightMenu = function(evt){
                    console.log("evt",evt);
                    var elt = evt.target;
                    //$rootScope.rcmAction = '';
                    // build the RCM on each right click, depending on terms of display items
                    //var rel = $(elt).attr('rel');
                    var parentId = elt.getAttribute('item-parent-id')||null;
                    var itemId = elt.getAttribute('item-data');
                    //console.log("rel",itemId, parentId);
                    //console.log("elt",$(elt));
                    var rightMenuItems = [];
                    var items = returnObject.defineRightMenuItems;
                    angular.forEach(items, function(item){
                        item.vars.itemId = itemId;
                        item.vars.parentId = parentId;
                        if(item.display){
                            if(eval(item.display)){
                                rightMenuItems.push(item);
                            }
                        }else{
                            rightMenuItems.push(item);
                        }
                    })
                    //returnObject.rightMenuItems = rightMenuItems;
                    return rightMenuItems;
                }

                returnObject.clickItem = function(callBack, vars){
                    //$rootScope.rcmAction = callBack;
                    returnObject[callBack](vars);
                };
                var getRelevantItem = function(vars, obj){
                    //console.log("itemId",vars.itemId);
                    //console.log("parentId",vars.parentId);
                    var elt, findTenant, tenant;
                    if(vars.parentId){
                        // find tenant and child document
                        findTenant = $filter('getById')(obj, vars.parentId);
                        tenant = findTenant.obj;
                        if(tenant){
                            var findDoc = $filter('getById')(tenant.children, vars.itemId);
                            elt = findDoc.obj;
                        }
                    }else{
                        // find tenant
                        findTenant = $filter('getById')(obj, vars.itemId);
                        elt = findTenant.obj;
                    }
                    if(elt){
                        return elt;
                    }
                    return false;
                };
                var closeModal = function(title){
                    if(modalScope){
                        modalScope.$title = title||'';
                        $timeout(function(){
                            modalCloseFn();
                        }, 1000);
                    }

                }
                var modalCloseFn;
                var modalScope;
                returnObject.renameItem = function(vars){
                    //console.log("vars",vars, vars.parentId);
                    var rel = vars.parentId?"doc":"folder";
                    var new_elt = angular.copy(getRelevantItem(vars, returnObject.projectsFolders));
                    $dialog.dialog(STATIC_PREFIX+APP_FOLDER+"directory/views/rcm-modals/rename.html", {
                        backdropClick: false,
                        backdropCancel: false,
                        title: "Rename",
                        ok:{label:"Save", fn:"scope.renameItem"},
                        params: {elt: new_elt, inProgress:false },
                        controller: ['$scope',
                            function( $scope) {
                                /*$scope.disableModalButtons = true;
                                $scope.$digest();*/
                                $scope.renameItem = function(){
                                    $scope.params.inProgress = true;
                                    $scope.$setModalButtonsStatus(true);
                                    var new_name = $scope.params.elt.name;
                                    var itemId = $scope.params.elt.id;
                                    renameItemService(itemId, rel, new_name).then(function() {
                                        $scope.safeApply(function () {
                                            var elt = getRelevantItem(vars, returnObject.projectsFolders);
                                            elt.name = $scope.params.elt.name;
                                        })
                                        $scope.$modalCancel();
                                    });
                                }
                            }
                        ]
                    });
                };
                returnObject.itemTemplate = function(vars){
                    var rel = vars.parentId?"doc":"folder";
                    var item = angular.copy(getRelevantItem(vars, returnObject.projectsFolders));
                    //console.log("item",item);

                    $dialog.dialog(STATIC_PREFIX+APP_FOLDER+'directory/views/rcm-modals/templates-assign.html', {
                        backdropClick: false,
                        title: "Templates Assign",
                        controller: "templatesAssignCtrl",
                        scope: returnObject.scope,
                        params: {node_id: item.id, rel: rel, node_label: item.label }
                    });
                };
                returnObject.deleteItem = function(vars){
                    var rel = vars.parentId?"doc":"folder";
                    var item = angular.copy(getRelevantItem(vars, returnObject.projectsFolders));
                    console.log("deleteItem",item);
                    var item_id = item.id;
                    if(rel=="folder"){
                        removeFolderAndDoc(item_id, null).then(function (resp) {
                            returnObject.scope.removeTenantFromDom(item);
                        })
                    }else{
                        var folder_id = item.folder;
                        removeFolderAndDoc(returnObject.scope.projectId, folder_id, item_id).then(function (resp) {
                            returnObject.scope.removeDocumentFromDom(item, item_id);
                        })
                    }
                };
                returnObject.reprocessItem = function(vars){
                    //var rel = vars.parentId?"doc":"folder";
                    var elt = angular.copy(getRelevantItem(vars, returnObject.projectsFolders));
                    var rel = elt.folder?"doc":"folder";
                    var title = vars.title;
                    var tenant = elt;
                    if(elt.folder){ // case it's a document!!!!!
                        tenant = $filter('filter')(returnObject.projectsFolders, elt.folder)[0];
                        var async = true;
                        generalServ.http('directory', 'reprocessDoc',[tenant.id, elt.id, async]).then(function(resp) {
                            returnObject.scope.getFolderDocs(tenant);
                        })
                    }else{
                        directoryServ.globalActionsForItemService(elt.id, rel, vars.func, true).then(function(resp) {
                            //console.log(title + " reprocessItem DONE resp", resp);
                            returnObject.scope.getFolderDocs(tenant);
                        });
                    }
                    if(elt.folder) {
                        tenant = $filter('filter')(returnObject.projectsFolders, elt.folder)[0];
                    }
                    //console.log("elt",elt, tenant);
                };
                returnObject.globalActionsForItem = function(vars){
                    var rel = vars.parentId?"doc":"folder";
                    var elt = angular.copy(getRelevantItem(vars, returnObject.projectsFolders));
                    console.log("elt",elt);
                    var title = vars.title;
                    directoryServ.globalActionsForItemService(elt.id, rel, vars.func).then(function(resp) {
                        console.log(title + " DONE resp", resp);
                        closeModal(title + " done");
                    });
                    $dialog.dialog(STATIC_PREFIX+APP_FOLDER+"directory/views/rcm-modals/process.html", {
                        backdropClick: false,
                        backdropCancel: false,
                        title: title + " " + rel,
                        footerTemplate: false,
                        params: {inProgress:true },
                        controller: ['$scope',
                            function( $scope) {
                                // pass cancel function and $scope to controller
                                // in order to be able to close the modal and change it's title from outside
                                modalCloseFn = $scope.$modalCancel;
                                modalScope = $scope;
                            }
                        ]
                    });
                };
                returnObject.searchTreeItem = function(vars){
                    var node = angular.copy(getRelevantItem(vars, returnObject.projectsFolders));
                    var nodeType = vars.parentId?"doc":"folder";
                    var nodeId = node.id;

                    $dialog.dialog(STATIC_PREFIX+APP_FOLDER+"directory/views/rcm-modals/search-modal.html", {
                        backdropClick: false,
                        title: "Search Item",
                        ok: {label: 'Search'},
                        params: {node_id: nodeId, nodeType: nodeType, node_label: node.label },
                        controller: "searchModalCtrl"
                    });
                }
                // TODO
                // server old APIs - should be replace with new APIs
                // the following actions available from RCM and from action buttons on view
                // to prevent duplications we call the common action services on directoryServ
                var renameItemService = function (itemId, rel, name) {
                    return directoryServ.renameItem(itemId, rel, name);
                };
                var removeFolderAndDoc = function(projectId, tenantId, documentId) {
                    if(documentId!=null){
                        return generalServ.http('directory', 'removeDoc',[projectId, tenantId, documentId]);
                    }else{
                        return generalServ.http('directory', 'removeFolder',[projectId, tenantId]);
                    }
                };
                return returnObject;
            }
        ]);
});

