define(function (require) {
    'use strict';
    var app = require('appModule');

    app.register.controller('searchModalCtrl',
                    ['$scope', '$state', '$stateParams', 'advancedSearchServ', 'GLOBAL_VARS', '$filter', '$cookies', '$dialog', '$rootScope', '$timeout',
            function ($scope,   $state,   $stateParams,   advancedSearchServ,   GLOBAL_VARS,   $filter,   $cookies,   $dialog,   $rootScope,   $timeout) {

                $scope.nodeId = $scope.params.node_id;
                $scope.nodeType = $scope.params.nodeType;
                $scope.nodeLabel = $scope.params.node_label;

                $scope.today = new Date();
                $scope.newConstraints = [];

                $scope.addLine = function(){
                    $scope.newConstraints.push({});
                    console.log("newConstraints",$scope.newConstraints);
                }
                advancedSearchServ.getAdvancedSearchData($scope.nodeId, $scope.nodeType).then(function(advanced) {
                    $scope.advancedSearchData = advanced.data;
                    console.log("advancedSearchData", $scope.advancedSearchData);
                })

                function isEmpty(obj) {
                    return angular.equals({},obj);
                };
                $scope.clickedSearch = function(){
                    var scope = angular.element($('#searchPhrase')).scope();
                    var dataToSend = {};
                    //console.log("constraints",scope.constraints);

                    // after cleaning the empty constraints - make sure there are still some...
		            if(scope.newConstraints && scope.newConstraints.length>0){
                        var dateFormat = 'MM/dd/yyyy';
                        angular.forEach(scope.newConstraints, function(line){
                            if(line.line_type=="date"){
                                line.value = $filter('date')(line.value, dateFormat);
                                //console.log("line", line.value);
                            }
                        })
                        dataToSend["constraints"] = scope.newConstraints;
                    } 
                    if(scope.searchPhrase && scope.searchPhrase.length>0){
                        dataToSend["phrase"] = scope.searchPhrase;
                    } 
                    if(scope.searchPhraseTitle && scope.searchPhraseTitle.length>0){
                        dataToSend["phraseTitle"] = scope.searchPhraseTitle;
                    }
                    if(isEmpty(dataToSend)){
                        console.log("Must fill values!")
                        return;
                    }
                    console.log("dataToSend",dataToSend);

                    //localStorage.setItem("forceServerRefresh", true);
                    //localStorage.setItem("searchData", JSON.stringify(dataToSend));
                    $state.go('app.clientDocumentView', {nodeId:$scope.nodeId});
                    $scope.$modalCancel();
                }
                $scope.$modalOk = function(){
                    $scope.clickedSearch();
                }

            }
        ]);
});
