define(function (require) {
    'use strict';
    var app = require('appModule');

    app.register.controller('directoryCtrl',
        ['$scope', 'directoryServ', 'generalServ', 'uploadServ', 'directoryRcmServ', '$state', 'GLOBAL_VARS', '$filter', '$cookies', '$dialog', '$timeout', '$stateParams', '$rootScope', '$interval', '$q', 'DEALSUMM_CONFIG', 'Upload',
            function ($scope, directoryServ, generalServ, uploadServ, directoryRcmServ, $state, GLOBAL_VARS, $filter, $cookies, $dialog, $timeout, $stateParams, $rootScope, $interval, $q, DEALSUMM_CONFIG, Upload) {

                var stateParams = $rootScope.$stateParams;

                // prevent from dropping files on the document
                $(document).bind('dragover drop', function (e) {
                    e.preventDefault();
                });
                var checkDocsListsInterval = 2000;
                var checkFirstDocsListsInterval = 1000;
                $scope.statusChart = {
                    "0": "Pending",
                    "1": "Uploaded",
                    "2": "Processing",
                    "3": "Processed",
                    "4": "Failed"
                };

                //
                var DOC_STATUS_PENDING = 0;
                var DOC_STATUS_UPLOADED = 1;
                var DOC_STATUS_PROCESSING = 2;
                var DOC_STATUS_PROCESSED = 3;
                var DOC_STATUS_FAILED = 4;
                //
                $scope.updateTimeout = undefined;
                $scope.completness = 0;
                $scope.oldCompletness = 0;

                $scope.$on("$destroy", function () {
                    if (angular.isDefined($scope.updateTimeout)) {
                        $interval.cancel($scope.updateTimeout);
                    }
                    $scope.updateTimeout = undefined;
                });
                $scope.projectId = stateParams.projectId;


                $scope.init = function(){
                    var readyPromise = [];
                    var projectsFoldersPromise = $q.defer();
                    var projectDetailsPromise = $q.defer();
                    var userDetailsPromise = $q.defer();
                    readyPromise.push(projectsFoldersPromise.promise);
                    readyPromise.push(projectDetailsPromise.promise);
                    readyPromise.push(userDetailsPromise.promise);

                    generalServ.http('general', 'getUserInfo',[$scope.userDetails.id]).then(function(resp) {
                        $scope.lastFolder = resp.data.last_folder;
                        $scope.lastDoc = $scope.lastFolder ? resp.data.last_doc : null;
                        userDetailsPromise.resolve();
                    });
                    generalServ.http('general', 'getProjectInfo',[$scope.projectId]).then(function(resp) {
                        $scope.projectDetails = resp.data;
                        $scope.numOfStores = $scope.projectDetails.numOfStores;
                        $scope.numOfDocs = $scope.projectDetails.numOfDocs;
                        $scope.projectName = $scope.projectDetails.name;
                        $scope.$parent.projectName = $scope.projectName; // for breadcrumbs
                        $scope.projectId = $scope.projectDetails.id;
                        $scope.uploadDate = $scope.projectDetails.upload_date;
                        //console.log("project Details", resp);
                        console.log("$scope.projectName ",$scope.projectName );

                        projectDetailsPromise.resolve();
                    })
                    generalServ.http('directory', 'getProjectFolders',[$scope.projectId]).then(function(resp) {
                        $scope.projectsFolders = resp.data;
                        $scope.workingList = [];
                        console.log("project folders", resp.data);
                        projectsFoldersPromise.resolve();
                    })
                    directoryRcmServ.setParentScope($scope);

                    $q.all(readyPromise).then(function () {
                        temp_getAllDocuments();
                    });
                };
                $scope.init();
                var temp_getAllDocuments = function () {
                    angular.forEach($scope.projectsFolders, function (tenant) {
                        if (($scope.lastFolder && $scope.lastFolder == tenant.id) || !$scope.lastFolder) {
                            tenant.folderExpanded = true;
                        }
                        $scope.getFolderDocs(tenant);
                        //console.log("tenant", tenant);
                    })
                };

                $scope.resetInterval = function () {
                    //console.log("resetInterval", $scope.updateTimeout);
                    if (angular.isDefined($scope.updateTimeout)) {
                        $interval.cancel($scope.updateTimeout);
                    }
                    $scope.updateTimeout = undefined;
                };
                $scope.alignHeaderCellWidths = function(){
                    $("#for_scrolled_header > div").each(function(idx){
                        var hdr = $("#scrolled_header").children('div')[idx];
                        $(hdr).width($(this).width());
                        //$("#scrolled_header").children()$(this).width();
                        console.log("this",this, idx);
                    })
                };
                $scope.getFolderDocs = function (tenant) {
                    if (tenant.folderExpanded) {
                        //console.log("folderExpanded >> true");
                        var tenantId = tenant.id;
                        if($scope.lastFolder != tenantId){
                            $scope.lastDoc = null;
                            $scope.updateLastFolderAndDoc(tenantId);
                        }
                        generalServ.http('directory', 'getFolderDocs',[$scope.projectId, tenantId]).then(function(resp) {
                            //directoryServ.getFolderDocs($scope.projectId, tenantId).then(function (resp) {
                            //console.log(">> tenant docs", resp.data);
                            //console.log(">> tenant docs", resp.data.length, tenant.children, resp.data);
                            $scope.safeApply(function () {
                                if (!tenant.children || tenant.children.length == resp.data.length) {
                                    tenant.children = resp.data;
                                    //console.log("tenant",tenant.children);
                                }
                                //console.log("tenant.children",tenant.children.length, resp.data.length);
                                checkForUnProcessedDocs(resp.data, tenant);
                                //$scope.alignHeaderCellWidths();
                            })
                        })
                    }else{
                        //$scope.alignHeaderCellWidths();
                    }
                };
                $scope.new_tenant = "";
                $scope.showAddTenant = false;
                $scope.addingTenant = false;
                $scope.addingTenantError = null;
                $scope.setFocus = function(id, isActive){
                    if(isActive){
                        $scope.addingTenantError = null;
                        $('#'+id).focus();
                    }else{
                        $('#'+id).blur();
                    }
                };
                $scope.addTenant = function(){
                    $('#new_tenant').blur();
                    $scope.addingTenant = true;
                    var parent_type = "root";
                    var parent_id = $scope.projectId;
                    var folder_name = $scope.new_tenant;
                    uploadServ.uploadFolder(folder_name, parent_id, parent_type).then(function(resp) {
                        $scope.addingTenant = false;
                        if(resp.status == 400){
                            $scope.addingTenantError = resp.data.error;
                        }else {
                            $scope.showAddTenant = false;
                            console.log("newFolder created >>", resp.data);
                        }
                    });
                };
                // this function will calculate the percentage of uploaded files
                $scope.calculatedTenants = {};
                var updateFilesCompleteness = function () {
                    var totalDocsCount = 0;
                    var totalTenantsCount = 0;
                    var unProcessedDocs = 0;
                    angular.forEach($scope.projectsFolders, function (tenant, idx) {
                        totalTenantsCount++;
                        if (tenant.children && tenant.children.length > 0) {
                            angular.forEach(tenant.children, function (doc) {
                                totalDocsCount++;
                                if (parseInt(doc.status) != DOC_STATUS_PROCESSED && parseInt(doc.status) != DOC_STATUS_FAILED) {
                                    unProcessedDocs++;
                                }
                            })
                        }
                    });
                    if (!($scope.oldCompletness == 0 && $scope.completness == 0)) {
                        $scope.oldCompletness = $scope.completness;
                    }

                    //$scope.numOfStores = totalTenantsCount;
                    //$scope.numOfDocs = totalDocsCount;
                    //console.log("updateFilesCompleteness",totalDocsCount, unProcessedDocs);
                    $scope.completness = Math.round(100 - (unProcessedDocs / totalDocsCount * 100));
                };
                var checkForUnProcessedDocs = function (docList, tenant) {
                    updateFilesCompleteness();
                    if (docList.length > 0) {
                        var unProcessedDocs = 0;
                        //var folderId = tenant.id;
                        angular.forEach(docList, function (doc, idx) {
                            //console.log("doc.status", doc.status, parseInt(doc.status) == DOC_STATUS_PROCESSING);
                            if (parseInt(doc.status) == DOC_STATUS_UPLOADED || parseInt(doc.status) == DOC_STATUS_PROCESSING) {
                                unProcessedDocs++;
                            }
                        })
                        if (unProcessedDocs == 0) {
                            removeTenantFromWorkingList(tenant);
                        } else {
                            addTenantToWorkingList(tenant);
                        }
                    }
                };
                var addTenantToWorkingList = function (tenant) {
                    var checkInterval = checkDocsListsInterval;
                    if ($scope.workingList.indexOf(tenant.id) == -1) {
                        $scope.workingList.push(tenant.id);
                        // if we just pushed the first tenant - make first server call faster
                        if ($scope.workingList.length == 1) {
                            checkInterval = checkFirstDocsListsInterval;
                        }
                    }
                    $scope.updateWorkingList(checkInterval);
                };
                var removeTenantFromWorkingList = function (tenant) {
                    if ($scope.workingList.indexOf(tenant.id) > -1) {
                        $scope.workingList.splice($scope.workingList.indexOf(tenant.id), 1);
                    }
                    // after removing an tenant from list - need to call the next tenant immediately and not wait another round
                    $scope.updateWorkingList(100);
                };
                $scope.updateWorkingList = function (callServiceSpeed) {
                    $scope.resetInterval();
                    if ($scope.workingList.length > 0) {
                        console.log("workingList", $scope.workingList);
                        $scope.updateTimeout = $interval(function () {
                            var tenant = $filter('getById')($scope.projectsFolders, $scope.workingList[0]);
                            if(tenant && tenant.obj){
                                $scope.getFolderDocs(tenant.obj);
                            }
                            //console.log("tenant-", tenant, $scope.workingList[0]);

                        }, callServiceSpeed)
                    }
                };
                $scope.removeTenantFromDom = function (tenant) {
                    var tenant = $filter('getById')($scope.projectsFolders, tenant.id);
                    var tenant_idx = tenant.idx;
                    $scope.projectsFolders.splice(tenant_idx, 1);
                    removeTenantFromWorkingList(tenant);
                    $scope.selectedItems.documents = [];
                    $scope.selectedItems.folders = [];
                    updateFilesCompleteness();
                }
                $scope.removeDocumentFromDom = function (item) {
                    var tenant = $filter('getById')($scope.projectsFolders, item.folder);
                    var document = null;
                    if(tenant && tenant.obj){ // handle a case the tenant was deleted before it's documents
                        if (tenant.obj.children) {
                            document = $filter('getById')(tenant.obj.children, item.id);
                        }
                        if(document){
                            tenant.obj.children.splice(document.idx, 1);
                            $scope.selectedItems.documents = [];
                            $scope.selectedItems.folders = [];
                            updateFilesCompleteness();
                        }
                    }
                };
                $scope.removeBulkItems = function(items, projectId){
                    $scope.removePromise = [];
                    var itemPromise = $q.defer();
                    $scope.removePromise.push(itemPromise.promise);
                    var removedItem = items[0];

                    $scope.removeItem(removedItem, projectId, itemPromise);
                    $q.all($scope.removePromise).then(function(){
                        items.shift();
                        if(items.length>0){
                            $scope.removeBulkItems(items, projectId);
                        }
                    })
                };
                $scope.removeItem = function (item, projectId, itemPromise) {
                    console.log("removeFolderAndDoc",item);
                    var tenantId = item.folder || item.id;
                    var docId = item.folder ? item.id : null;

                    if(docId!=null){
                        generalServ.http('directory', 'removeDoc',[projectId, tenantId, docId]).then(function(resp) {
                            if($scope.removeDocumentFromDom){
                                $scope.removeDocumentFromDom(item);
                            }
                            itemPromise.resolve();
                        })
                    }else{
                        generalServ.http('directory', 'removeFolder',[tenantId]).then(function(resp) {
                            if($scope.removeTenantFromDom){
                                $scope.removeTenantFromDom(item);
                            }
                            itemPromise.resolve();
                        })
                    }
                };
                $scope.removeItems = function () {
                    // console.log("folders",$scope.selectedItems.folders);
                    // console.log("documents",$scope.selectedItems.documents);

                    var items = [];
                    if ($scope.selectedItems.folders.length >= 1) {
                        items = angular.extend($scope.selectedItems.folders, items);
                    }
                    if ($scope.selectedItems.documents.length >= 1) {
                        items = angular.extend($scope.selectedItems.documents, items);
                    }
                    if(items.length>0){
                        $scope.removeBulkItems(items, $scope.projectId);
                    }
                };
                $scope.reProcessItem = function () {
                    var item, rel, tenant;
                    // in case a folder is marked catch it first - before we catch the documents
                    var async = true;
                    if ($scope.selectedItems.folders.length > 0) {
                        item = $scope.selectedItems.folders[0];
                        tenant = item;
                        rel = "folder";
                        directoryServ.globalActionsForItemService(item.id, rel, 'reprocess', async).then(function(resp) {
                            //console.log(title + " reprocessItem DONE resp", resp);
                            $scope.getFolderDocs(tenant);
                        });
                    } else {
                        item = $scope.selectedItems.documents[0];
                        tenant = $filter('filter')($scope.projectsFolders, item.folder)[0];
                        generalServ.http('directory', 'reprocessDoc',[tenant.id, item.id, async]).then(function(resp) {
                            $scope.getFolderDocs(tenant);
                        })
                    }
                };
                $scope.checkDisableDocumentsActionButtons = function () {
                    return ($scope.selectedItems.folders.length == 0 && $scope.selectedItems.documents.length == 0);
                };
                var found;
                $scope.clickedOnTenantCheckbox = function (tenant, checked) {
                    //console.log("tenant", tenant, checked);
                    if (checked) {
                        $scope.selectedItems.folders.push(tenant);
                    }else{
                        found = $filter('getById')($scope.selectedItems.folders, tenant.id);
                        $scope.selectedItems.folders.splice(found.idx, 1);
                    }
                    if (tenant.children) {
                        angular.forEach(tenant.children, function (item) {
                            if (checked) {
                                $scope.selectedItems.documents.push(item);
                                //console.log("******",$("#doc_"+item.id) , $("#doc_"+item.id).find("input[type=checkbox]"));
                                $("#doc_" + item.id).find("input[type=checkbox]").attr("disabled", "disabled");
                            } else {
                                found = $filter('getById')($scope.selectedItems.documents, item.id);
                                if(found){
                                    $scope.selectedItems.documents.splice(found.idx, 1);
                                }
                                $("#doc_" + item.id).find("input[type=checkbox]").removeAttr("disabled");
                            }
                        })
                    }
                    //console.log("$scope.selectedItems",$scope.selectedItems);
                };
                $scope.updateLastFolderAndDoc = function (folder_id, doc_id) {
                    $scope.lastFolder = folder_id;
                    if(doc_id){
                        $scope.lastDoc = doc_id;
                    }
                    generalServ.updateLastFolderAndDoc(folder_id, doc_id, $scope.userDetails.id);
                };
                $scope.showAbstracts = function (tenant) {
                    $scope.updateLastFolderAndDoc(tenant.id, null);
                    $state.go('app.clientAbstract', {
                        'projectId': $scope.projectId,
                        'tenantId': tenant.id
                    });
                };
                $scope.showDocument = function (doc) {
                    if (parseInt(doc.status) == DOC_STATUS_PROCESSED) {
                        var docId = doc.id;
                        var tenant = doc.folder;
                        $scope.updateLastFolderAndDoc(tenant, docId);
                        console.log("doc", doc);
                        $state.go('app.clientDocumentView', {
                            'projectId': $scope.projectId,
                            'tenantId': tenant,
                            'docId': docId
                        });
                    }
                };
                $scope.showSplitDocument = function (doc) {
                    var docId = doc.id;
                    var tenant = doc.folder;
                    $scope.updateLastFolderAndDoc(tenant, docId);
                    $state.go('app.clientSplitPdfView', {
                        'projectId': $scope.projectId,
                        'tenantId': tenant,
                        'docId': docId
                    });
                };
                $scope.listMainCheckbox = function () {
                    if ($scope.mainCheckbox) {
                        $scope.selectedItems.folders = angular.copy($scope.projectsFolders);
                    } else {
                        $scope.selectedItems.folders = [];
                    }
                }

                $scope.initRightMenu = function (evt) {
                    directoryRcmServ.projectsFolders = $scope.projectsFolders;
                    $scope.rightMenuItems = directoryRcmServ.initRightMenu(evt);
                    $scope.clickItem = function(callBack, vars){
                        var docId = null;
                        var folderId = null;
                        if(vars.parentId){
                            docId = vars.itemId;
                            folderId = vars.parentId;
                        }else{
                            folderId = vars.itemId;
                        }
                        if($scope.lastFolder != folderId){
                            $scope.lastDoc = null;
                        }
                        $scope.updateLastFolderAndDoc(folderId, docId);
                        directoryRcmServ.clickItem(callBack, vars);
                    }
                }
                $scope.selectedItems = {
                    folders: [],
                    documents: []
                };


                $scope.closeModal = function (title) {
                    if (modalScope) {
                        modalScope.$title = title;
                        $timeout(function () {
                            modalCloseFn();
                        }, 1000);
                    }
                }
                var modalCloseFn;
                var modalScope;
                $scope.showProcessModal = function (msg, label) {
                    msg = "<div style='width:400px;overflow-x:hidden;'>" + msg + "<br/><label class='process-label' title='" + label + "'>" + label + "</label><div class='small-loading'></div>" + "</div>";
                    $dialog.dialog(null, {
                        template: msg,
                        backdropClick: false,
                        title: "Working...",
                        negative: false,
                        footerTemplate: false,
                        controller: ['$scope', '$state', function ($scope, $state) {
                            // pass cancel function and $scope to controller
                            // in order to be able to close the modal and change it's title from outside
                            modalCloseFn = $scope.$modalCancel;
                            modalScope = $scope;
                        }]
                    });
                };
                $scope.docWarnings = null;
                $scope.showingDocWarnings = null;
                $scope.handleWarningsModal = function(evt, folder_id, doc_id){
                    if((folder_id==null && doc_id==null) || $scope.showingDocWarnings == doc_id){
                        $scope.showingDocWarnings = null;
                        $scope.docWarnings = null;
                        $("#warnings_popup").css({"left": '-5000'});
                        return;
                    }
                    generalServ.http('document', 'getDocWarnings',[folder_id, doc_id]).then(function(resp) {
                        console.log("getDocWarnings",resp.data);
                        $scope.showingDocWarnings = doc_id;
                        $scope.docWarnings = resp.data;
                        var closest = $(evt.target).closest("div");
                        console.log("directory_container",$("#directory_container")[0].scrollTop);
                        var pos = closest.position();
                        var top = pos.top + $("#directory_container")[0].scrollTop + 7;
                        var closestWidth = closest.width() + 30;

                        $("#warnings_popup").css({"left": pos.left - 400 + closestWidth, "top": top, width: "400px"}).show();
                    })
                };
                $scope.uploadObj.doneUploadingDocument = function(folder_id, document_name){
                    console.log("document",document_name, "folder_id", folder_id);
                    var newObj = {};
                    newObj.name = document_name;
                    newObj.upload_date = new Date();
                    newObj.effective_date = "N/A";
                    newObj.what_new = "N/A";
                    newObj.status = DOC_STATUS_PENDING;
                    newObj.id = "doc_temp_" + (new Date().getTime()) + "_" + Math.floor((Math.random() * 10000) + 1);
                    var folder = $filter('getById')($scope.projectsFolders, folder_id);
                    //var folder = found.obj;
                    if (folder.obj) {
                        if (!folder.obj.children) {
                            folder.obj.children = [];
                        }
                        $scope.safeApply(function () {
                            folder.obj.children.push(newObj);
                            $timeout(function () {
                                $scope.getFolderDocs(folder.obj);
                            }, 500)
                        })
                    }
                }
                $scope.uploadObj.doneUploadingDirectory = function(folder){
                    folder.folderExpanded = true;
                    $scope.safeApply(function () {
                        console.log("folder",folder);
                        $scope.projectsFolders.push(folder);
                        //console.log("projectsFolders.push",tenant);
                        $timeout(function () {
                            //console.log("getFolderDocs");
                            $scope.getFolderDocs(folder);
                        }, 200)
                    })
                }
                $scope.rootScope = $rootScope;
                $rootScope.dragOverFolderId = null;
                $scope.isOverDirectory = $rootScope.dragOverFolderId;
                $scope.filesToFolder = {};
                $scope.$watch('filesToFolder', function() {
                    //console.log("$scope.projectsFolders",$scope.projectsFolders, $rootScope.dragOverFolderId);
                    if($scope.projectsFolders && $rootScope.dragOverFolderId){
                        var dragOverId = $rootScope.dragOverFolderId;
                        var folderObj = $filter('filter')($scope.projectsFolders, {id:dragOverId});
                        //console.log("folderObj",folderObj);
                        if(folderObj){
                            $scope.initUpload($scope.filesToFolder, $scope.projectDetails, folderObj[0]);
                        }
                    }
                });
                $scope.$watch('files', function() {
                    $scope.initUpload($scope.files, $scope.projectDetails);
                });
            }
        ]);
})
