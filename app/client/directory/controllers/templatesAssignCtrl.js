define(function (require) {
    'use strict';
    var app = require('appModule');

    app.register.controller('templatesAssignCtrl',
                ['$scope', 'directoryServ', '$state', 'GLOBAL_VARS', 'filterFilter', '$cookies', '$dialog', '$timeout',
        function ($scope,   directoryServ,   $state, GLOBAL_VARS, filterFilter, $cookies, $dialog, $timeout) {
            //console.log("getTSTForTreeItem", $scope);
            //console.log("params", $scope.params, $scope.params.rel)
            var node_id = $scope.params.node_id;
            var rel = $scope.params.rel;
            $scope.server_working = false;

            directoryServ.templateAssignment(node_id, rel).then(function(response) {
                console.log("TSTAssignment", response.data);
                var results = response.data;
                var current = results['curr_assignment'];
                $scope.node_label = $scope.params.node_label;
                $scope.displayRel = current['det_obj_type'];//current['det_obj_type']=="doc"?"document":current['det_obj_type']=="org"?"Organization":"Folder";
                $scope.displayName = current['det_obj_name'];
                $scope.inherit = current['inherit'];
                $scope.current_id = current['tst_id'];
                $scope.current_tst = current['tst_name'];
                $scope.det_obj_id = current['det_obj_id'];
                if(!$scope.current_id){
                    $scope.$title = "Please choose a default template at the top-organization level";
                }
                $scope.tst_list = results['tst_list'];
            })
            $scope.unAssignTST = function(){
                if(!$scope.server_working){
                    $scope.server_working = true;
                    directoryServ.templateUnAssign($scope.current_id, $scope.det_obj_id, rel).then(function(response) {
                        $scope.server_working = false;
                        console.log("removed", response);

                    })
                }

            }
            $scope.assignNewTST = function(new_tst_id){
                if(new_tst_id!==$scope.current_id && !$scope.server_working){
                    $scope.server_working = true;
                    directoryServ.assignNewTemplate(new_tst_id, node_id, rel).then(function(response) {
                        // console.log("assigned new", response.data);
                        // update the view
                        $scope.server_working = false;
                        $scope.current_id = response.data['assigned_tst_id'];
                        $scope.current_tst = response.data['assigned_tst_name'];
                    })
                }
            }
        }
    ]);


});
