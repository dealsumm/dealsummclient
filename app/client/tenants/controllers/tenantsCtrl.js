define(function (require) {
    'use strict';
    var app = require('appModule');

    app.register.controller('tenantsCtrl',
        ['$scope', 'generalServ', '$state', 'GLOBAL_VARS', '$filter', '$cookies', '$dialog', '$timeout', '$stateParams', '$rootScope', '$interval', '$q',
            function ($scope, generalServ, $state, GLOBAL_VARS, $filter, $cookies, $dialog, $timeout, $stateParams, $rootScope, $interval, $q) {

                console.log("tenantsCtrl");
                var stateParams = $rootScope.$stateParams;
                $rootScope.$state = $state;
                $scope.rootScope = $rootScope;
                $scope.projectId = stateParams.projectId;

                $scope.init = function () {
                    generalServ.http('general', 'getProjectInfo',[$scope.projectId]).then(function(resp) {
                        $scope.projectDetails = resp.data;
                        $scope.numOfStores = $scope.projectDetails.numOfStores;
                        $scope.numOfDocs = $scope.projectDetails.numOfDocs;
                        $scope.projectName = $scope.projectDetails.name;
                        $scope.projectId = $scope.projectDetails.id;
                        $scope.uploadDate = $scope.projectDetails.upload_date;
                        console.log("project Details", resp);
                    })
                };
                /*$scope.$on('$stateChangeSuccess',
                    function(event, toState, toParams, fromState, fromParams){
                        console.log("toState.name",toState, toParams);
                        if(toState && toState.header){
                            $scope.currentHeader = toState.header.label;
                        }
                    }
                );*/
                //$scope.rejectedFiles = null;
                $scope.isOverDirectory = $rootScope.dragOverFolderId;
                $scope.$watch('files', function() {
                    $scope.initUpload($scope.files, $scope.projectDetails);
                });
                /*$scope.$watch('rejectedFiles', function() {
                    $scope.initUpload($scope.rejectedFiles, $scope.projectDetails, null, true);
                });*/
                $scope.init();
                //$rootScope("$stateT")
                //console.log("$state",$state.current.header.label);
                //$scope.currentHeader = $state.current.header.label;
            }
        ]);


})
