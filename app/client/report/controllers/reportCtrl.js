define(function (require) {
    'use strict';
    var app = require('appModule');
    app.register.controller('reportCtrl',
        function ($scope, reportServ, generalServ,  $state,   GLOBAL_VARS,   $filter,   $cookies,   $dialog,   $timeout,  $stateParams, $rootScope, $q, $window, headersServ) {

            var stateParams = $rootScope.$stateParams;
            $scope.projectId = stateParams.projectId;
            $scope.tenantId = stateParams.tenantId;
            $scope.init = function(){
                generalServ.http('general', 'getProjectInfo',[$scope.projectId]).then(function(resp) {
                    $scope.projectName = resp.data.name;
                    console.log("project Details", resp.data);
                })
                headersServ.get('report').then(function(data){
                    $scope.reportTableHeaders = data['headers'];
                    $scope.sortBy = data['sortBy'];
                    //console.log("report >>",$scope.reportTableHeaders);
                });
                reportServ.getReport($scope.projectId).then(function (resp) {
                    console.log("reportList",resp.data);
                    $scope.reportList = resp.data;
                });
            }
            $scope.sortReportTable = function(header_cell){
                if(header_cell.sortable){
                    var ref = header_cell.ref;
                    if($scope.sortBy.ref==ref){
                        $scope.sortBy.sortAscent = !$scope.sortBy.sortAscent
                    }else{
                        $scope.sortBy.sortAscent = true;
                    }
                    $scope.sortBy.ref = ref;
                    //console.log("ref", $scope.sortBy,ref);
                }
            };
            $scope.setCellClass = function(val, cell){
                //console.log("val",val);
                var conditions = cell['conditionalClass'].split("|");
                //console.log("conditions",conditions);
                var condition;
                for(var i=0;i<conditions.length;i++){
                    condition = conditions[i].split(":");
                    switch (condition[1]){
                        case "equals":
                            if (val == condition[2]) {
                                return condition[0];
                            }
                            break;
                        case "less":
                            if (val < condition[2]) {
                                return condition[0];
                            }
                            break;
                    }
                }
            }
            $scope.initTable = function(){
                $timeout(function(){
                        var $demo1 = $('table.report-table');
                        $demo1.floatThead({
                            scrollContainer: function($table){
                                return $table.closest('.table-wrapper');
                            }
                        });
                    }, 300)

            }
            /*$scope.headerInitilized = false;
            $scope.initTableHeaders = function(){
                //console.log("initTableHeaders");
                $scope.headerInitilized = false;
                $timeout(function(){
                    var _w;
                    $('#report-header > div').each(function(idx) {
                        idx = idx + 1;
                        _w = $(this).outerWidth();
                        $('#report-header-visible > div:nth-child(' + (idx) + ')').outerWidth(_w + 'px')
                    });
                    $scope.headerInitilized = true;
                }, 1000);
            };*/
            /*angular.element($window).bind('resize', function() {
                $scope.initTableHeaders();
            });*/
            $scope.init();

        },
        ['$scope', 'reportServ', 'generalServ', '$state', 'GLOBAL_VARS', '$filter', '$cookies', '$dialog', '$timeout', '$stateParams', '$rootScope', '$q', '$window', 'headersServ'
        ]);
});
