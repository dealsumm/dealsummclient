define(function (require) {
    'use strict';

    var app = require('appModule');

    app.register.service('reportServ',
                ['$http', '$timeout','$q', 'DEALSUMM_CONFIG', 'GLOBAL_VARS', '$cookies', '$rootScope',
        function ($http,     $timeout,  $q,   DEALSUMM_CONFIG,   GLOBAL_VARS,   $cookies,   $rootScope) {

            var returnObject = {};
            var basePath = DEALSUMM_CONFIG.DSPROD_REST_PATH;
            var oldBasePath = DEALSUMM_CONFIG.REST_PATH;

            returnObject.getReport = function (project_id) {
                var url = oldBasePath + $rootScope.$stateParams.accountId + '/folders/' + project_id + '/importreport/';
                //console.log("makePrimary",data, url);
                return $http.get(url);
            };
            return returnObject;
        }
    ]);
});

