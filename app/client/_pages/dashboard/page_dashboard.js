define(function (require) {
    'use strict';
    var angularAMD = require('angularAMD');
    require('angular');
    require('angular-ui-router');

    angular.module('dealsumm.dashboard', ['ui.router'])
        .config(['$stateProvider'
            ,function ($stateProvider) {
                $stateProvider
                    .state('app.dashboard', {
                        url: '/account/{accountId}',
                        views: {
                            '': {
                                templateUrl: STATIC_PREFIX + APP_FOLDER + '_pages/dashboard/page_dashboard.html'
                            },
                            'leftPane@app.dashboard': {
                                controller: 'propertiesCtrl',
                                templateUrl: STATIC_PREFIX + APP_FOLDER + 'properties/views/properties.html'
                            },
                            'rightPane@app.dashboard': {
                                controller: 'recentCtrl',
                                templateUrl: STATIC_PREFIX + APP_FOLDER + 'recent/views/recent.html'
                            }
                        },
                        resolve: {
                            load: angularAMD.load(
                                getModulesDependencies(
                                    ['recent', 'properties']
                                )
                            )
                        },
                        ncyBreadcrumb: {
                            label: 'Properties'
                        }
                    })
            }])


});