define(function (require) {
    'use strict';
    var angularAMD = require('angularAMD');
    require('angular');
    require('angular-ui-router');

    angular.module('dealsumm.property', ['ui.router'])
        .config(['$stateProvider'
            ,function ($stateProvider) {
                $stateProvider
                    .state('app.property', {
                        url: '/account/{accountId}/projects/{projectId}',
                        views: {
                            '': {
                                templateUrl: STATIC_PREFIX + APP_FOLDER + '_pages/property/page_property.html'
                            },
                            'leftPane@app.property': {
                                controller: 'directoryCtrl',
                                templateUrl: STATIC_PREFIX + APP_FOLDER + 'directory/views/directory.html'
                            },
                            'rightPane@app.property': {
                                controller: 'rentRollCtrl',
                                templateUrl: STATIC_PREFIX + APP_FOLDER + 'rentRoll/views/rentRoll.html'
                            }
                        },
                        resolve: {
                            load: angularAMD.load(
                                getModulesDependencies(
                                    ['directory', 'rentRoll']
                                )
                            )
                        },
                        ncyBreadcrumb: {
                            parent: 'app.dashboard',
                            label: '{{projectName}}'
                        }
                    })
            }])
});