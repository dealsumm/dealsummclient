define(function (require) {
    'use strict';

    var app = require('appModule');

    app.register.service('rentRollServ',
                ['$http', '$timeout','$q', 'DEALSUMM_CONFIG', 'GLOBAL_VARS', '$cookies', '$rootScope',
        function ($http,     $timeout,  $q,   DEALSUMM_CONFIG,   GLOBAL_VARS,   $cookies,   $rootScope) {

            var returnObject = {}, promise, url;
            var basePath = DEALSUMM_CONFIG.DSPROD_REST_PATH;

            returnObject.getRentRoll = function (folder_id) {
                var url = basePath + $rootScope.$stateParams.accountId + '/projects/' + folder_id + '/tenants/';
                console.log("getRentRoll", url);
                return $http.get(url);
            };
            returnObject.addRentRollRow = function (projectId, data) {
                url = basePath + $rootScope.$stateParams.accountId + '/projects/' + projectId + "/tenants/";
                promise = $http({ method: 'PUT', url: url, data: angular.toJson(data)});
                return promise;
            };
            returnObject.updateRentRollCell = function (projectId, tenantId, data) {
                url = basePath + $rootScope.$stateParams.accountId + '/projects/' + projectId + "/tenants/" + tenantId + "/";
                promise = $http({ method: 'PATCH', url: url, data: angular.toJson(data)});
                return promise;
            };


            return returnObject;
        }
    ]);
});

