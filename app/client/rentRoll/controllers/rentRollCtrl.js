define(function (require) {
    'use strict';
    var app = require('appModule');

    app.register.controller('rentRollCtrl',
                ['$scope', 'generalServ', 'rentRollServ', 'uploadServ', '$state', 'GLOBAL_VARS', '$filter', '$window', '$dialog', '$timeout', '$stateParams', '$rootScope', '$q', 'headersServ',
        function ($scope,   generalServ, rentRollServ, uploadServ, $state,   GLOBAL_VARS,   $filter,   $window,   $dialog,   $timeout,  $stateParams, $rootScope, $q, headersServ) {

            var stateParams = $rootScope.$stateParams;
            $scope.projectId = stateParams.projectId;

            $scope.init = function(){
                generalServ.http('directory', 'getProjectFolders',[$scope.projectId]).then(function(resp) {
                    //generalServ.getProjectFolders($scope.projectId).then(function (resp) {
                    $scope.projectFolders = resp.data;
                    console.log("$scope.projectFolders ",$scope.projectFolders );
                });
                headersServ.get('rentroll').then(function(data){
                    $scope.rentRollHeaders = data['headers'];
                    $scope.sortBy = data['sortBy'];
                    console.log("rentroll >>",$scope.rentRollHeaders);
                });
                rentRollServ.getRentRoll($scope.projectId).then(function (resp) {
                    $scope.rentRollData = resp.data;
                    console.log("rentRollData", resp.data);
                });
            };
            $scope.rentRollNewRow = {};
            $scope.resetRentRollNewRow = function(form){
                angular.forEach($scope.rentRollNewRow, function(item, itemName){
                    //console.log("item", itemName,item);
                    $scope.rentRollNewRow[itemName] = '';
                });
                form.$setPristine();
            };
            $scope.addRentRollRow = function(form){
                //console.log("rentRollNewRow",$scope.rentRollNewRow);
                // detect if value is date and format it
                angular.forEach($scope.rentRollNewRow, function(itemVal, itemName){
                    var toDate = $filter('date')(itemVal, 'MM/dd/yyyy');
                    if(Date.parse(toDate)>100000){ // in case it's not a date it'll return -xxxxxxx
                        $scope.rentRollNewRow[itemName] = toDate;
                    }
                })
                rentRollServ.addRentRollRow($scope.projectId, $scope.rentRollNewRow).then(function(resp){
                    console.log("addRentRollRow",resp.data);
                    $scope.rentRollData.tenants.push(resp.data);
                    $scope.resetRentRollNewRow(form);
                })
            };
            $scope.updateRentRollCell = function(newValue, cell, tenant){
                var ref = cell.compareTo||cell.ref;
                //console.log("newValue",newValue);
                //console.log("ref",ref, typeof newValue);
                //console.log("cell",cell);
                var toDate = $filter('date')(newValue, 'MM/dd/yyyy');
                if(Date.parse(toDate)>100000){ // in case it's not a date it'll return -xxxxxxx
                    newValue = toDate;
                };
                //console.log("updateRentRollCell",newValue, cell, ref, tenant);
                var data = {};
                data[ref] = newValue;
                rentRollServ.updateRentRollCell($scope.projectId, tenant.id, data).then(function(resp){
                    console.log("updateRentRollCell>>>", resp.data);
                    if(resp.data[ref]){
                        tenant[ref] = resp.data[ref];
                    }
                })
            };
            $scope.showingFoldersForRow = null;
            $scope.new_folder_name = null;
            $scope.editedFolderId = null;
            $scope.handleFoldersModal = function(evt, tenant, folderId, posIdx){
                //console.log("posIdx",posIdx);
                var idx = null;
                if(tenant){
                    idx = tenant.id;
                }
                //console.log("handleFoldersModal", $scope.showingFoldersForRow, evt, idx);
                if($scope.showingFoldersForRow == idx || ($scope.showingFoldersForRow && $scope.showingFoldersForRow.id == idx) || idx==null){
                    $scope.showingFoldersForRow = null;
                    $scope.editedFolderId = null;
                    $("#folders").css({"left": '-5000'});
                    return;
                }
                $scope.showingFoldersForRow = idx=='new'?idx:tenant;
                if(folderId){
                    $scope.editedFolderId = folderId;
                }else{
                    $scope.editedFolderId = null;
                }
                //console.log("posIdx>>",posIdx, idx);
                var closest = $(evt.target).closest("div");
                var pos = closest.position();
                //var height = closest.height();
                var width = closest.width() + 20;

                console.log("rentRollData",$scope.rentRollData.tenants);
                if(posIdx<=8 || $scope.rentRollData.tenants.length<6){
                    $("#folders").addClass("from-top");
                }else{
                    $("#folders").removeClass("from-top");
                }
                //console.log("pos",pos);
                $("#folders").css({"left": pos.left, "top": pos.top, width: width}).show();
            };
            $scope.chooseFolder = function(id, name){
                if($scope.showingFoldersForRow=='new'){
                    $scope.showingFoldersForRow = null;
                    $scope.rentRollNewRow['folder'] = id;
                    $scope.rentRollNewRow['folderName'] = name;
                    $scope.handleFoldersModal(null, null);
                }else{
                    var tenant = $scope.showingFoldersForRow;
                    if(!tenant.tenant_folder || typeof tenant.tenant_folder != 'object'){
                        tenant.tenant_folder = {};
                    }
                    tenant.tenant_folder.id = id;
                    tenant.tenant_folder.name = name;
                    console.log("$scope.showingFoldersForRow",$scope.showingFoldersForRow);
                    console.log("tenant",tenant);
                    $scope.updateRentRollCell(id, {ref:'tenant_folder'}, tenant);
                    $scope.showingFoldersForRow = null;
                    $scope.handleFoldersModal(null, null);
                }
            };
            $scope.addNewFolder = function(){
                $scope.showingFoldersForRow = null;
                $scope.handleFoldersModal(null, null);
                $scope.addingTenant = true;
                $scope.addingFolderErrorIcon = false;
                var parent_type = "root";
                var parent_id = $scope.projectId;
                var folder_name = $scope.new_folder_name;
                uploadServ.uploadFolder(folder_name, parent_id, parent_type).then(function(resp) {
                    $scope.addingTenant = false;
                    if(resp.status == 400){
                        $scope.addingFolderErrorIcon = true;
                        $scope.addingFolderError = resp.data.error;
                    }else {
                        $scope.rentRollNewRow['tenant_folder'] = resp.data.id;
                        $scope.rentRollNewRow['folderName'] = resp.data.name;
                        console.log("newFolder created >>", resp.data);
                    }
                });
            };
            $scope.sortRentrollTable = function(header_cell){
                if(header_cell.sortable){
                    var ref = header_cell.ref;
                    if($scope.sortBy.ref==ref){
                        $scope.sortBy.sortAscent = !$scope.sortBy.sortAscent
                    }else{
                        $scope.sortBy.sortAscent = true;
                    }
                    $scope.sortBy.ref = ref;
                    //console.log("ref", $scope.sortBy,ref);
                }
            };
            $scope.headerInitilized = false;
            $scope.initTableHeaders = function(){
                //console.log("initTableHeaders");
                $scope.headerInitilized = false;
                $timeout(function(){
                    var _w;
                    $('#rent-roll-header > div').each(function(idx) {
                        idx = idx + 1;
                        _w = $(this).outerWidth();
                        $('#rent-roll-header-visible > div:nth-child(' + (idx) + ')').outerWidth(_w + 'px')
                    });
                    $scope.headerInitilized = true;
                }, 1000);
            };
            angular.element($window).bind('resize', function() {
                $scope.initTableHeaders();
            })
            $scope.backToList = function(){
                $state.go('app.clientTenantsView');
            };
            $scope.clickedRentRollRow = function(tenantInfo){
                if(tenantInfo.tenantId>-1){
                    $state.go('app.clientAbstract', {
                        'projectId': $scope.projectId,
                        'tenantId': tenantInfo.tenantId
                    });
                }
            };
            $scope.init();
        }]
    );
});
