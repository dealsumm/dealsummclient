define(function (require) {
    'use strict';

    var app = require('appModule');

    app.register.service('tocServ',
        ['$http', '$timeout','$q', 'DEALSUMM_CONFIG', 'GLOBAL_VARS', '$cookies', '$rootScope',
            function ($http,     $timeout,  $q,   DEALSUMM_CONFIG,   GLOBAL_VARS,   $cookies,   $rootScope) {

                var returnObject = {}, promise, url;
                var basePath = DEALSUMM_CONFIG.DSPROD_REST_PATH;
                var oldBasePath = DEALSUMM_CONFIG.REST_PATH;

                // get the TOC by calling get_toc_json from the server
                returnObject.getTocTree = function (doc_id) {
                    var url = oldBasePath + $rootScope.$stateParams.accountId + '/docs/?func=get_toc_json&doc_id=' + doc_id;
                    var promise = $http.get(url).then(function (response) {
                        return response;
                    });
                    return promise;
                };


                return returnObject;
            }
        ]);
});

