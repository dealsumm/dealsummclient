define(function (require) {
    'use strict';

    var app = require('appModule');

    app.register.service('docServ',
                    ['$http',  'generalServ','$timeout','$q', 'DEALSUMM_CONFIG', 'GLOBAL_VARS', '$cookies', '$rootScope',
            function ($http,    generalServ,  $timeout,  $q,   DEALSUMM_CONFIG,   GLOBAL_VARS,   $cookies,   $rootScope) {

                var returnObject = {}, promise, url;
                var basePath = DEALSUMM_CONFIG.DSPROD_REST_PATH;
                var oldBasePath = DEALSUMM_CONFIG.REST_PATH;

                returnObject.removePulse = function(item, itemStyle, destroyTime){
                    // remove all style of pulse animation
                    $timeout(function(){
                        $(item).removeAttr("style");
                        //console.log("itemStyle",itemStyle);
                        if(itemStyle && itemStyle.length>0){
                            // in case there was a style -> replace it now
                            $(item).attr("style", itemStyle);
                        }
                    }, destroyTime+500)
                };
                returnObject.getDocDetails = function (doc_id, folder_id, scope) {
                    var docDetails = $q.defer();
                    var docLabels = $q.defer();
                    var docPromise = {};
                    docPromise['docDetails'] = docDetails.promise;
                    docPromise['docLabels'] = docLabels.promise;
                    generalServ.http('document', 'getDocDetails',[doc_id]).then(function(resp) {
                        docDetails.resolve(resp);
                    });
                    generalServ.http('document', 'getDocLabels',[folder_id, doc_id]).then(function(resp) {
                        docLabels.resolve(resp);
                    });
                    $q.all(docPromise).then(function(docPromise){
                        //console.log("docPromise",docPromise);
                        scope.docDetails = scope.$parent.docDetails = docPromise.docDetails.data;
                        scope.item_title = scope.docTitle = scope.$parent.item_title = scope.docDetails.doc_title;
                        scope.item_id = scope.$parent.item_id = scope.docDetails.doc_id;
                        scope.docUrl = STATIC_PREFIX + scope.docDetails.doc_url;
                        scope.docClasses = scope.docDetails.classes;
                        scope.nextDoc = scope.docDetails.next_doc!=-1?scope.docDetails.next_doc:null;
                        scope.prevDoc = scope.docDetails.prev_doc!=-1?scope.docDetails.prev_doc:null;
                        console.log("docDetails",scope.docDetails);
                        //$('#doc_container').append("<div id='object_marker' class='object-marker'></div>")
                    })
                };

                returnObject.timesToTryItem = 15; //after that the code will stop looking for the element on DOM
                returnObject.docScroll = function(selector, scope, parentElt) {
                    var item = $(selector);
                    if(!item || !item.position()){
                        returnObject.timesToTryItem--;
                        if(returnObject.timesToTryItem>0){
                            $timeout(function(){
                                console.log("no item found: " + selector);
                                returnObject.docScroll(selector, scope, parentElt);
                            },100)
                        }
                        return;
                    }
                    returnObject.timesToTryItem = 15;// reset the variable to count again
                    var parent = $(parentElt);

                    var offset = item.position().top;
                    //console.log("parent",parent, item, offset);
                    if(item.is("span")){
                        var p = item.closest("p");
                        offset += p.position().top
                    }
                    if (offset != 0) {
                        if($(parent).is($('body'))){
                            offset = offset - 200;
                        }else{
                            offset = offset - 50;
                        }
                        //parent.scrollTop(offset);
                        parent.animate({ scrollTop: offset }, 400);
                    }

                    if(!scope.highlight && typeof scope.splitPage == "undefined") { // in case we are highlighting a section
                        if(scope.token_spans){
                            scope.token_spans.removeClass("highlight highlight-last");
                        }
                        var itemStyle = $(item).attr("style");
                        if (scope.sectionSpecialPulseAnimation) {
                            // currently works in train / predict
                            $(item).pulse({backgroundColor: 'transparent'}, {duration: 400, pulses: 5}, returnObject.removePulse(item, itemStyle, 2000));
                        } else {
                            $(item).pulse({backgroundColor: '#33b5e6'}, {duration: 400, pulses: 5}, returnObject.removePulse(item, itemStyle, 2000));
                        }
                    }
                    if(typeof scope.splitPage != "undefined"){
                        splitAutoMark(scope.splitPage)
                    }
                    scope.splitPage = undefined;
                }
                var splitAutoMark = function(splitPage){
                    $("p#" + splitPage).addClass("split-paragraph-on");
                    $timeout(function(){
                        $("p#" + splitPage).removeClass("split-paragraph-on");
                    }, 5000)
                }
                returnObject.scrollToSplit = function(splitPage, scope, parentElt){
                    if(!splitPage){
                        return;
                    }
                    scope.splitPage = splitPage;
                    returnObject.docScroll('#doc_container p#'+splitPage, scope, parentElt);
                }
                returnObject.timesToTryTokensSpan = 50; //after that the code will stop looking for the element on DOM
                returnObject.highlightPhrase = function(phrase, scope, parentElt){
                    if(!phrase){
                        return;
                    }
                    // make sure we an array of all spans of the document
                    if(!scope.token_spans || scope.token_spans.length==0){
                        scope.token_spans = $("#doc_container span.token");
                        returnObject.timesToTryTokensSpan--;
                        if(returnObject.timesToTryTokensSpan>0){
                            $timeout(function(){
                                console.log("no token span found...(" + returnObject.timesToTryTokensSpan + ")");
                                returnObject.highlightPhrase(phrase, scope, parentElt)
                            },120)
                        }
                        return;
                    }
                    returnObject.timesToTryTokensSpan = 33;// reset the variable to count again
                    var startIndex = phrase.split("-")[0];
                    var endIndex = phrase.split("-")[1];
                    if(startIndex==endIndex){
                        endIndex = (1* endIndex) + 1;
                    }
                    scope.highlight = scope.token_spans.slice(startIndex, endIndex);
                    console.log("startIndex", startIndex, endIndex, scope.highlight);

                    // remove extra whitespace to be able to highlight the whole word
                    var tempElt;
                    angular.forEach(scope.highlight, function(elt){
                        if(elt.nextSibling && elt.nextSibling.nodeType){
                            //console.log("elt",elt, elt.nextSibling, elt.nextSibling.nodeType);
                            tempElt = elt.nextSibling;
                            if(tempElt.nodeType==3 && !/S/.test(tempElt.nodeValue)){
                                elt.nextSibling.remove();
                            }
                        }
                    })
                    scope.token_spans.removeClass("highlight highlight-last");
                    // special treatment for the last item in middle of the line
                    $(scope.highlight[scope.highlight.length-1]).addClass("highlight-last");
                    console.log("highlight", scope.highlight.length);
                    scope.highlight.addClass("highlight");
                    //console.log("doc_container end highlightPhrase");
                    returnObject.docScroll('#doc_container span:eq(' + startIndex + ')', scope, parentElt);
                }


                return returnObject;
            }
        ]);
});

