define(function (require) {
    'use strict';
    var app = require('appModule');

    app.register.controller('docPartialCtrl',
        ['$scope', 'docServ', 'generalServ', '$state', 'GLOBAL_VARS', '$filter', '$cookies', '$dialog', '$timeout', '$stateParams', '$rootScope', '$q',
        function ($scope,   docServ, generalServ,   $state,   GLOBAL_VARS,   $filter,   $cookies,   $dialog,   $timeout,  $stateParams, $rootScope, $q) {


            //console.log("docCtrl");

            var stateParams = $rootScope.$stateParams;

            $scope.projectId = stateParams.projectId;
            $scope.tenantId = stateParams.tenantId;
            $scope.docId = stateParams.docId;
            $scope.phrase = stateParams.phrase;

            $scope.getDocDetails = function(docId){
                $scope.docUrl = "";
                docServ.getDocDetails(docId, $scope.tenantId, $scope);
            };
            $scope.init = function(){
                generalServ.http('general', 'getFolderInfo',[$scope.projectId, $scope.tenantId]).then(function(resp) {
                    $scope.tenantName = resp.data.name;
                })
                generalServ.http('general', 'getProjectInfo',[$scope.projectId]).then(function(resp) {
                    $scope.projectName = resp.data.name;
                })
                $scope.getDocDetails($scope.docId);
                if($state.is('app.clientAbstract.doc')){
                    //$scope.docDrawer.showDrawer = true;
                    $scope.docDrawer.drawerState = 2;
                }
            };
            if($scope.docId){           // && $scope.phrase
                $scope.init();
            }
            $scope.docLoaded = function(){
                docServ.highlightPhrase(stateParams.phrase, $scope, "#doc_container");
            };

            $scope.parentScope = $scope;

            $scope.$on('$stateChangeSuccess',
                function(event, toState, toParams, fromState, fromParams){
                    //console.log("toState.name",toParams.docId, fromParams.docId, fromState, toState);
                    if((toParams.docId && toParams.docId!=fromParams.docId)){                          // || fromStateName!=toStateName
                        $scope.getDocDetails(toParams.docId);
                    }else if(toParams.phrase){
                        docServ.highlightPhrase(toParams.phrase, $scope, "#doc_container");
                    }
                }
            )
        }
    ]);
});
