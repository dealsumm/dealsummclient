define(function (require) {
    'use strict';
    var app = require('appModule');

    app.register.controller('tocCtrl',
        ['$scope', 'tocServ', '$state', 'GLOBAL_VARS', '$filter', '$cookies', '$dialog', '$timeout', '$stateParams', '$rootScope', '$q',
        function ($scope,   tocServ,   $state,   GLOBAL_VARS,   $filter,   $cookies,   $dialog,   $timeout,  $stateParams, $rootScope, $q) {

            var stateParams = $rootScope.$stateParams;
            var docId = stateParams.docId;
            tocServ.getTocTree(docId).then(function(tocTree) {
                console.log("tocTree",tocTree.data);
                $scope.toc_tree = angular.copy(tocTree.data);
                $scope.addFnToItems($scope.toc_tree);
                $scope.toc_tree[0].expanded = true;
            })
            $scope.addFnToItems = function(itemsArr){
                angular.forEach(itemsArr, function(item){
                    item.fn = $scope.onNodeClickFn;
                    if(item.items && item.items.length>0){
                        $scope.addFnToItems(item.items);
                    }
                })
            }
            // isopenAmendments
            // isopenNotes
            // isopenToC
            // isopenTermSheet
            // isopenDefinitions
            $scope.isopenToC = true;

            // for accordion
            $scope.oneAtATime = true;

            $scope.onNodeClickFn = function(item){
                $scope.handleNodeClick(item);
            };
            $scope.handleNodeClick = function(item){
                //console.log("clicked node",item);
                if($state.includes('app.clientDocumentView')){
                    $state.go('app.clientDocumentView.section', {sectionId: item.div});
                } else if($state.includes('app.clientPdfView')){
                    $state.go('app.clientPdfView.toc', {tocItemId: item.id});
                }
            }
            $scope.showHideLabels = function(e){
                //e.preventDefault();
                $scope.hideUnlabled=!$scope.hideUnlabled;
                if($scope.hideUnlabled){
                    $(".legend-tree-holder li").not(".has-label").hide();
                }else{
                    $(".legend-tree-holder li").show();
                }
            }
        }
    ]);
});
