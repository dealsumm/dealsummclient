define(function (require) {
    'use strict';
    var app = require('appModule');

    app.register.controller('docCtrl',
        ['$scope', 'docServ', 'generalServ', '$state', 'GLOBAL_VARS', '$filter', '$cookies', '$dialog', '$timeout', '$stateParams', '$rootScope', '$q', '$window', 'localStorageService',
        function ($scope,   docServ, generalServ,  $state,   GLOBAL_VARS,   $filter,   $cookies,   $dialog,   $timeout,  $stateParams, $rootScope, $q, $window, localStorageService) {


            var stateParams = $rootScope.$stateParams;

            $scope.docId = stateParams.docId;
            $scope.projectId = stateParams.projectId;
            $scope.tenantId = stateParams.tenantId;
            $scope.resizer_force_left_width = 30;

            $scope.labelingMode = false;//localStorageService.get("labelingMode")=="false"?false:true;
            $scope.clickedLabeling = function(force) {
                $scope.labelingMode = angular.isDefined(force)?force:!$scope.labelingMode;
                localStorageService.set("labelingMode", $scope.labelingMode);
                saveDocScrollPosition();
                if($scope.labelingMode){
                    $timeout(
                        function(){
                            //console.log("scrollTop2", $("#doc_container").scrollTop());
                            $("#label_control").height($("#doc_holder").height());
                            /*labelPositioning($scope.objects);
                            $scope.allParagraphs = mapAllParagraphs();
                            applySavedDocScrollPosition();
                            $scope.initLabelMarkers();*/
                        }, 50)
                }else{
                    /*$scope.clearLabelMarkers();
                    $timeout(
                        function(){
                            applySavedDocScrollPosition();
                        }, 250)*/
                }
            }
            var saveDocScrollPosition = function(){
                //console.log("scrollTop1", $("#doc_container").scrollTop());
                /*
                $scope.allParagraphs = mapAllParagraphs();
                var topmostParagraph = $scope.getTopmostParagraph($scope.allParagraphs, $("#doc_container").scrollTop());
                $scope.topmostParagraphId = topmostParagraph.id;
                $scope.topmostGap = topmostParagraph.top - $("#doc_container").scrollTop();
                $scope.topmostDocHeight = topmostParagraph.height;
                */
            };

            //$scope.docView = 'toc';
            $scope.switchToAbstractSingleView = function(){
                if(stateParams.phrase){
                    $state.go('app.clientSingleAbstract.phrase');
                }else if(stateParams.sectionId) {
                    $state.go('app.clientSingleAbstract.section');
                }else{
                    $state.go('app.clientSingleAbstract');
                }
            };

            $scope.getDocDetails = function(docId){
                docServ.getDocDetails(docId, $scope.tenantId, $scope);
            }
            $scope.backToList = function(){
                $state.go('app.property');
            }
            $scope.openAbstractOld = function(){
                var url = "#/org/" + stateParams.accountId + "/termsheet/doc/" + $scope.docId + "?forceView=old";
                $window.open(url);
            }
            $scope.init = function() {
                if ($scope.docId) {
                    $scope.getDocDetails($scope.docId);
                }
                generalServ.http('general', 'getFolderInfo',[$scope.projectId, $scope.tenantId]).then(function(resp) {
                    $scope.tenantName = resp.data.name;
                })
                generalServ.http('general', 'getProjectInfo',[$scope.projectId]).then(function(resp) {
                    $scope.projectName = resp.data.name;
                })
            };
            $scope.docLoaded = function(){
                if(stateParams.phrase){
                    docServ.highlightPhrase(stateParams.phrase, $scope, "#doc_container");
                }
                if(stateParams.sectionId){
                    docServ.docScroll("p#" + stateParams.sectionId, $scope, "#doc_container");
                }
                if($scope.labelingMode){
                    $scope.clickedLabeling(true);
                }
            };
            $scope.init();

            $scope.$on('$stateChangeSuccess',
                function(event, toState, toParams, fromState, fromParams){
                    console.log("$stateChangeSuccess",toParams.docId, fromParams.docId);
                    $scope.highlight = null;
                    $scope.highlightPhraseDone = null;
                    //console.log("toState.name",toState.name);
                    if(toParams.docId && toParams.docId!=fromParams.docId){
                        $scope.getDocDetails(toParams.docId);
                    }else{
                        if(toParams.phrase){
                            docServ.highlightPhrase(toParams.phrase, $scope, "#doc_container");
                        }
                        if(toParams.sectionId){
                            docServ.docScroll("p#" + toParams.sectionId, $scope, "#doc_container");
                        }
                    }
                }
            )
        }
    ]);
});
