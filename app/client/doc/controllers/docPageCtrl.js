define(function (require) {
    'use strict';
    var app = require('appModule');

    app.register.controller('docPageCtrl',
        ['$scope', 'docServ', 'generalServ', '$state', 'GLOBAL_VARS', '$filter', '$cookies', '$dialog', '$timeout', '$stateParams', '$rootScope', '$q', '$window', 'localStorageService',
        function ($scope,   docServ, generalServ,  $state,   GLOBAL_VARS,   $filter,   $cookies,   $dialog,   $timeout,  $stateParams, $rootScope, $q, $window, localStorageService) {
            var stateParams = $rootScope.$stateParams;

            $scope.docId = stateParams.docId;
            $scope.projectId = stateParams.projectId;
            $scope.tenantId = stateParams.tenantId;
            $scope.resizer_force_left_width = 30;

            $scope.switchToAbstractSingleView = function(){
                if(stateParams.phrase){
                    $state.go('app.clientSingleAbstract.phrase');
                }else if(stateParams.sectionId) {
                    $state.go('app.clientSingleAbstract.section');
                }else{
                    $state.go('app.clientSingleAbstract');
                }
            };
            $scope.backToList = function(){
                $state.go('app.property');
            }
            /*
            // OLD - do not remove
            $scope.openAbstractOld = function(){
                var url = "#/org/" + stateParams.accountId + "/termsheet/doc/" + $scope.docId + "?forceView=old";
                $window.open(url);
            }*/
            $scope.init = function() {
                generalServ.http('general', 'getFolderInfo',[$scope.projectId, $scope.tenantId]).then(function(resp) {
                    $scope.tenantName = resp.data.name;
                })
                generalServ.http('general', 'getProjectInfo',[$scope.projectId]).then(function(resp) {
                    $scope.projectName = resp.data.name;
                })
            };
            $scope.init();
        }
    ]);
});
