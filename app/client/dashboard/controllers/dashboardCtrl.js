define(function (require) {
    'use strict';
    var app = require('appModule');
    app.register.controller('dashboardCtrl',
        function ($scope, generalServ, headersServ,  $state,   GLOBAL_VARS,   $filter,   $cookies,   $dialog,   $timeout,  $stateParams, $rootScope, $q) {

            var stateParams = $rootScope.$stateParams;
            console.log("dashboardCtrl");
            var projectsIdPromise = $q.defer();
            var beforeInit = function(){
                var accountId = stateParams.accountId;
                generalServ.http('general', 'getUserInfo',[accountId]).then(function(resp) {
                //generalServ.getUserInfo(accountId).then(function (resp) {
                    //console.log("user Details on init", resp.data.last_project);
                    //resp.data.last_project = null;//:test
                    if(resp.data.last_project){
                        //$scope.currentProjectId = resp.data.last_project;
                        $state.go('app.clientDashboard', {accountId: accountId, projectId:resp.data.last_project});
                    }else{
                        $state.go('app.dashboard', {accountId: accountId});
                    }
                })
            };
            if(stateParams.projectId){
                projectsIdPromise.resolve();
            }else{
                beforeInit();
            }


            $scope.toggleProjects = function(){
                $scope.showProjects=!$scope.showProjects;
            };
            $scope.changeProject = function(proj){
                if(proj.id != $scope.currentProjectId){
                    $state.go('app.clientDashboard', {'projectId': proj.id});
                }
            };
            $scope.clickedDashboardRow = function(tenantId){
                if(tenantId>-1){
                    $state.go('app.clientAbstract', {
                        'projectId': $scope.currentProjectId,
                        'tenantId': tenantId
                    });
                }
            };
            $scope.sortDashboardTable = function(ref){
                if($scope.sortBy.ref==ref){
                    $scope.sortBy.sortAscent = !$scope.sortBy.sortAscent
                }else{
                    $scope.sortBy.sortAscent = true;
                }
                $scope.sortBy.ref = ref;
                // AVI/Chaim:
                // Update server with sortBy object ($scope.sortBy)
            };

            $q.all(projectsIdPromise).then(function(){
                $scope.currentProjectId = stateParams.projectId;
                $scope.showProjects = false;
                $scope.expanded = false;

                //var readyPromise = [];
                var projectsListPromise = $q.defer();
                var projectTenantsPromise = $q.defer();
                var tableHeadersPromise = $q.defer();
                generalServ.http('properties', 'getProperties',[]).then(function(resp) {
                    $scope.projectsList = resp.data;
                    projectsListPromise.resolve();
                    console.log("properties list ",resp.data);
                });
                generalServ.http('directory', 'getProjectFolders',[$scope.currentProjectId]).then(function(resp) {
                    //generalServ.getProjectFolders($scope.currentProjectId).then(function (resp) {
                    console.log("project tenants", resp);
                    var rows = resp.data;
                    $scope.dashboardTableRows = rows;
                    projectTenantsPromise.resolve();
                });
                headersServ.get('dashboard').then(function(data){
                    //console.log("dashboard >>",data);
                    $scope.dashboardTableHeaders = data['headers'];
                    $scope.hiddenHeadersRef = data['hiddenHeadersRef'];// resp.hiddenHeadersRef;
                    $scope.sortBy = data['sortBy'];
                    tableHeadersPromise.resolve();
                });
            });
        },
        ['$scope', 'generalServ', 'headersServ', '$state', 'GLOBAL_VARS', '$filter', '$cookies', '$dialog', '$timeout', '$stateParams', '$rootScope', '$q'
        ]);
});
