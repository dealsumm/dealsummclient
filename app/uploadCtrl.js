define(function (require) {
    'use strict';
    var app = require('appModule');

    app.register.controller('uploadCtrl',
        [           '$scope', 'generalServ', '$state', '$filter', '$timeout', '$stateParams', '$rootScope', '$q', 'Upload',
            function ($scope, generalServ, $state,   $filter,   $timeout,  $stateParams, $rootScope, $q, Upload) {

                $scope.queue = {};
                $scope.queue['project'] = {};
                $scope.queue['directories'] = {};
                $scope.startedUploading = false;
                $scope.showUploadProgress = true;
                $scope.initUpload = function(files, projectDetails, folder) {
                    if (files && files.length>0) {
                        $scope.uploadProjectId = projectDetails.id;
                        $scope.queue['project']['id'] = projectDetails.id;
                        $scope.queue['project']['name'] = projectDetails.name;
                        $scope.uploadedDirectories = [];
                        $scope.directoryIdx = 0;
                        $scope.startedUploading = true;
                        if(folder){
                            //console.log("folder",folder);
                            $scope.uploadedDirectories.push(folder);
                            $scope.queue['directories'][folder.name] = {uploaded:true, progress:100, name:folder.name, files:{}};
                            angular.forEach(files, function(file){
                                file.path = folder.name + "/" + file.name;
                            })
                            $scope.upload(files, "doc");
                        }else{
                            $scope.upload(files, "directory");
                        }

                    }
                };
                $scope.uploadObj = {};
                $scope.directoryUploaded = function(files, data){
                    //console.log("doneUploading",$scope.uploadedDirectories);
                    if(typeof $scope.uploadObj.doneUploadingDirectory == 'function'){
                        $scope.uploadObj.doneUploadingDirectory(data);
                    }
                    if($scope.uploadedDirectories.length == $scope.directoryIdx){
                        if (files && files.length>0) {
                            $scope.upload(files, "doc");
                        }
                        console.log("$scope.queue",$scope.queue);
                    }
                };
                $scope.upload = function (files, uploadType, isRejected) {
                    for (var i = 0; i < files.length; i++) {
                        var file = files[i];
                        var _type = file.type == "directory"?'directory':'doc';
                        console.log("file",file , _type, uploadType, isRejected);
                        var data = {};
                        if(_type == "directory" && uploadType == "directory"){
                            data['url'] = generalServ.getPaths("upload", "directory", [$scope.uploadProjectId]);
                            data['file'] = file;
                            data['fields'] = {'parent_folder_id':$scope.uploadProjectId, 'parent_type':'folder', 'name':file.name, 'bypassErrorsInterceptor':400};
                            data['sendFieldsAs'] = 'form'; // to make it POST
                            data['bypassErrorsInterceptor'] = 400;
                            $scope.queue['directories'][file.name] = {uploaded:false, progress:0, name:file.name, error:null, files:{}};
                            $scope.directoryIdx++;
                        }else{
                            if(_type == "doc" && uploadType == "doc"){
                                if($scope.uploadedDirectories.length>0){
                                    var folder_name = file.path.split("/")[0];
                                    var folder_id = $filter('filter')($scope.uploadedDirectories, {'name':folder_name})[0]['id'];
                                    data['url'] = generalServ.getPaths("upload", "files", [folder_id]);
                                    data['file'] = file;
                                    data['folder_name'] = folder_name;
                                    data['fields'] = {'folder_id':folder_id, 'parent_type':'folder'};
                                    $scope.queue['directories'][folder_name]['files'][file.path] = {uploaded:false, progress:0, name:file.path.split("/")[1]};
                                }else if($scope.uploadedDirectories.length==0){
                                    var errorText = "";

                                    if(isRejected){
                                        errorText = 'File type not supported';
                                    }else if(file && file.type=="directory"){
                                        errorText = 'Directory not uploaded';
                                    }else{
                                        errorText = 'Files should have folders';
                                    }

                                    // create FAKE directory in order to show file which was uploaded directly to root of property
                                    $scope.queue['directories']['temp'] = {uploaded:false, progress:0, name:'temp', error:null, files:{}};
                                    $scope.queue['directories']['temp']['files'][file.name] = {uploaded:false, progress:0, name:file.name, error:errorText};
                                    console.log("ERROR loading>>", file, errorText);
                                }
                            }
                        }

                        if(_type == uploadType && !isRejected) {
                            Upload.upload(data)
                                .progress(function (evt) {
                                    var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                                    if (evt.config.file.type == "directory") {
                                        $scope.queue['directories'][evt.config.file.name].progress = progressPercentage;
                                    } else {
                                        $scope.queue['directories'][evt.config.folder_name]['files'][evt.config.file.path].progress = progressPercentage;
                                    }
                                })
                                .success(function (data, status, headers, config) {
                                    if(status==400){
                                        if (config.file.type == "directory") {
                                            $scope.queue['directories'][config.file.name].error = data.error;
                                        }
                                        console.log("ERROR 400>>", config.file.type,data, status, headers, config);
                                    }else{
                                        console.log("data",data, config);
                                        if (config.file.type == "directory") {
                                            $scope.uploadedDirectories.push(data);
                                            $scope.directoryUploaded(files, data);
                                            $scope.queue['directories'][config.file.name].uploaded = true;
                                        } else {
                                            $scope.queue['directories'][config.folder_name]['files'][config.file.path].uploaded = true;
                                            if(typeof $scope.uploadObj.doneUploadingDocument == 'function'){
                                                $scope.uploadObj.doneUploadingDocument(config.fields.folder_id, config.file.name);
                                            }
                                        }
                                    }
                                })
                                .error(function (data, status, headers, config) {
                                    console.log("ERROR uploading", data, status, headers, config);
                                })
                        }
                    }
                };
            }
        ]);
});
