define(['appModule'], function (app) {
    'use strict';

    app.register.controller('appCtrl',
        [        '$scope', 'DEALSUMM_CONFIG', 'GLOBAL_VARS',  '$q', '$dialog', '$rootScope', '$state', '$stateParams', 'localStorageService', '$timeout', '$location', 'headersServ', '$controller',
        function ($scope,   DEALSUMM_CONFIG,   GLOBAL_VARS,    $q,   $dialog,   $rootScope,   $state,   $stateParams, localStorageService, $timeout, $location, headersServ, $controller) {

            $scope.IMAGES_PREFIX = DEALSUMM_CONFIG.BASE_PATH_IMAGES;
            $scope.STATIC_PREFIX = STATIC_PREFIX;
            $scope.APP_FOLDER = APP_FOLDER;
            $scope.WHAT_CAN_YOU_DO = STATIC_PREFIX + APP_FOLDER_NORMAL + "partials/views/what_can_you_do.html";
            $scope.showBreadCrumbs = DEALSUMM_CONFIG.SHOW_BC;

            $scope.userDetails = window.USER_DETAILS;
            $rootScope.selectionPhrase = {};
            //console.log("$scope.userDetails",$scope.userDetails);

            $scope.fix_id = function(text) { // UZ 090713: convert IDs (e.g., F_123,  D_15, into numbers)
                var ret;
                if (text) {
                    ret = text.replace(/F_/, '');
                    ret = ret.replace(/D_/, '');
                    ret = ret.replace(/O_/, '');
                } else {
                    ret = text;
                }
                return ret
            };

            $scope.helpClicked= function(){
                var url = "#/help";
                window.open(url);
            }

            $scope.getIdNumber = function(text) {
                text = ""+text;
                return text.indexOf("_")>-1?text.split("_")[1]:text;
            }
            $scope.debugMode = localStorageService.get("debugMode");
            $scope.updateDebugMode = function(val){
                $scope.debugMode = val;
                localStorageService.set("debugMode", val);
            };
            var stateParams = $rootScope.$stateParams;

            $scope.$on('$stateChangeSuccess',
                function(event, toState, toParams, fromState, fromParams){
                    //console.log("$stateParams docScroll", toParams.section, fromParams.section);
                    $scope.accountId = stateParams.accountId;
                    if($scope.accountId){
                        $scope.extraTabsHolderData = localStorageService.get("extraTabsHolderAccounts") || {};
                        //console.log("extraTabsHolderAccounts",$scope.extraTabsHolderData);
                        if(!$scope.extraTabsHolderData[$scope.accountId]) {
                            $scope.extraTabsHolderData[$scope.accountId] = [];
                            localStorageService.set("extraTabsHolderAccounts", $scope.extraTabsHolderData);
                        }
                        $scope.extraTabsHolder = $scope.extraTabsHolderData[$scope.accountId];
                    }
                }
            )

            $scope.removeExtraTab = function(property){
                var tabIdx = -1;
                for(var i=0;i<$scope.extraTabsHolder.length;i++){
                    if(angular.equals($scope.extraTabsHolder[i], property)){
                        tabIdx = i;
                    }
                }
                if(tabIdx>-1){
                    $scope.extraTabsHolder.splice(tabIdx, 1);
                }
                $scope.extraTabsHolderData[$scope.accountId] = $scope.extraTabsHolder;
                localStorageService.set("extraTabsHolderAccounts", $scope.extraTabsHolderData);
                if(stateParams.projectId==property.id){
                    $state.go('app.dashboard');
                }
            };
            var uploadCtrl = $controller('uploadCtrl',{$scope : $scope });

            $scope.safeApply = $rootScope.safeApply = function(fn) {
                var phase = this.$root.$$phase;
                if(phase == '$apply' || phase == '$digest') {
                    if(fn && (typeof(fn) === 'function')) {
                        fn();
                    }
                } else {
                    this.$apply(fn);
                }
            };
            $scope.isEmpty = function (obj) {
                return angular.equals({},obj);
            };
            $scope.projectDetailsActions = {};
            $scope.backgroundColorsArray = ["254,163,170","248,184,139","250,248,132","186,237,145","178,206,254","242,162,232","213,62,79","244,109,67","253,174,97","254,224,139","255,255,191","230,245,152","171,221,164","102,194,165","50,136,189",
                "254,163,170","248,184,139","250,248,132","186,237,145","178,206,254","242,162,232","213,62,79","244,109,67","253,174,97","254,224,139","255,255,191","230,245,152","171,221,164","102,194,165","50,136,189",
                "254,163,170","248,184,139","250,248,132","186,237,145","178,206,254","242,162,232","213,62,79","244,109,67","253,174,97","254,224,139","255,255,191","230,245,152","171,221,164","102,194,165","50,136,189"]

            $scope.switchView = function(){
                var newView = '';
                if(VIEW != "client"){
                    newView = 'client';
                }
                localStorageService.set("viewMode", newView);
                $timeout(function(){
                    document.location.reload();
                }, 200);
            }
            $scope.contractRedirectFunc = function(){
                var stateParams = $rootScope.$stateParams;
                var sendParams = {};

                if(stateParams.doc){
                    sendParams.doc = stateParams.doc;
                }else if(stateParams.nodeType=="doc"){
                    sendParams.doc = stateParams.nodeId;
                }
                if(sendParams.doc && stateParams.orgId){
                    if(stateParams.sectionId) {
                        $state.go('app.docView.section', {'doc':stateParams.doc, 'sectionId':stateParams.sectionId});
                    }else if(stateParams.phrase){
                        $state.go('app.docView.phrase', {'doc':stateParams.doc, 'phrase':stateParams.phrase});
                    }else{
                        $state.go('app.docView', {'doc':stateParams.doc});
                    }
                }else{
                    if(stateParams.orgId) {
                        $state.go('app.docView', {'doc':1});
                    }else{
                        $state.go('app.adminView');
                    }
                }
            };
            $scope.trainRedirectFunc = function(){
                var stateParams = $rootScope.$stateParams;
                var storage = localStorageService.get("trainCtrl");
                if(storage && storage.nodeId && storage.nodeType){
                    console.log("train used localStorage", storage);
                    $state.go('app.trainView', {'nodeId':storage.nodeId, 'nodeType':storage.nodeType});
                }else{
                    if(stateParams.orgId) {
                        $state.go('app.treeView');
                    }else{
                        $state.go('app.adminView');
                    }
                }
            };
            $scope.predictRedirectFunc = function(){
                var stateParams = $rootScope.$stateParams;
                var storage = localStorageService.get("predictCtrl");
                if(storage && storage.nodeId && storage.nodeType){
                    console.log("predict used localStorage", storage);
                    $state.go('app.predictView', {'nodeId':storage.nodeId, 'nodeType':storage.nodeType});
                }else{
                    if(stateParams.orgId) {
                        $state.go('app.treeView');
                    }else{
                        $state.go('app.adminView');
                    }
                }
            }
            $scope.searchRedirectFunc = function(){
                var stateParams = $rootScope.$stateParams;
                var storage = localStorageService.get("searchCtrl");
                if(storage && storage.nodeId && storage.nodeType){
                    console.log("search used localStorage", storage);
                    $state.go('app.searchView', {'nodeId':storage.nodeId, 'nodeType':storage.nodeType});
                }else{
                    if(stateParams.orgId) {
                        $state.go('app.treeView');
                    }else{
                        $state.go('app.adminView');
                    }
                }
            }

            $scope.setClassesBackgroundColor = function(docClasses, backgroundColorsArray, elt){
                var thisStyle = "";
                angular.forEach(docClasses, function(object, idx){
                    thisStyle+= elt + " div.label_"+object.cat_id+" .doc-info{background-color: rgba("+backgroundColorsArray[object.idx]+",0.3) !important; }\n";
                })
                thisStyle+= elt + ".hide-labels p[class*=label_]{background-color: transparent; }";
                //console.log("thisStyle\n"+thisStyle);
                return thisStyle;
            }

            $scope.analyseClassesColors = function(data){
                var docClasses = {};
                var counter = 0;
                angular.forEach(data, function(object, idx){
                    //console.log("docClasses[object.class]",docClasses[object.class], typeof(docClasses[object.class]));
                    if(typeof(docClasses[object.class]) == "undefined"){
                        docClasses[object.class] = {cat_id:object.class, idx:counter};
                        counter++;
                    }
                })
                //console.log("docClasses",docClasses);
                return docClasses;
            }

            /*if(!$rootScope.orgId && GLOBAL_VARS.orgId){
                $rootScope.orgId = GLOBAL_VARS.orgId;
                if($rootScope.orgId){
                    //console.log("treeView>>>> --- ", $rootScope.orgId);
                    $state.go('app.treeView', {orgId: $rootScope.orgId});
                }else{
                    //console.log("adminView>>>>");
                    $state.transitionTo('app.adminView');
                }
            }else if($rootScope.orgId){
                $state.go('app.treeView', {orgId: $rootScope.orgId});
            }*/
            //console.log("$stateParams",$rootScope.orgId, $stateParams.orgId);
            /*if(!$rootScope.orgId){
                $state.transitionTo('app.adminView', $stateParams, {location:"replace"});
            }*/
            /*if(!$rootScope.orgId && !$state.includes('app.notFound')){
                $state.go('app.adminView', $stateParams, {location:"replace"});
            }

            $rootScope.$on('$stateChangeSuccess',
                function(event, toState, toParams, fromState, fromParams){
                    //console.log("$stateParams docScroll", toParams.section, fromParams.section);
                    console.log("$rootScope",$rootScope.orgId);
                    if(!$rootScope.orgId){
                        $state.go('app.adminView', $stateParams, {location:"replace"});
                    }
                }
            )*/

        }
    ]);


});