$(function() {
    var urlPrefix = "";
    if(typeof navBarsURLprefix != "undefined"){
        urlPrefix = navBarsURLprefix;
    }
    var topNavItems = [
        {"title": "HOME", "url":"index.html"},
        {"title": "REVIEW", "url":"review.html"},
        {"title": "SOLUTIONS", "url":"solutions.html"},
        {"title": "ABOUT US", "url":"about.html"},
        {"title": "CONTACT", "url":"contact.html"},
        {"title": "REGISTER", "url":"http://54.214.11.206:8000/my_registration", "align": "right"},
        {"title": "LOG-IN", "url":"http://54.214.11.206:8000/accounts/login/", "align": "right"}
    ];
    var footerNavItems = [
        {"title": "Site Map", "url":"siteMap.html"},
        {"title": "Privacy Policy", "url":"privacyPolicy.html"},
        {"title": "Legal Notice", "url":"legalNotice.html"},
        {"title": "Send Feedback", "url":"mailto:feedback@clearforest.com"}
    ]



    // TOP NAVIGATION ITEMS
    var topNavDomItems = "";
    $.each(topNavItems, function(idx, item){
        topNavDomItems += "<li " + (item.align?'class=pull-right':'') + "><a href='" + (item.url.indexOf("http")==-1?urlPrefix:"") + item.url + "'>" + item.title + "</a></li>"
    })
    $("#topNav").append(topNavDomItems);


    // FOOTER NAVIGATION ITEMS
    var footerNavDomItems = "";
    $.each(footerNavItems, function(idx, item){
        footerNavDomItems += "<a href=" + (item.url.indexOf("http")==-1?urlPrefix:"") + item.url + ">" + item.title + "</a>";//&nbsp;&nbsp;|&nbsp;&nbsp;"
        if(idx<footerNavItems.length-1){
            footerNavDomItems += "<span>|</span>";
        }
    })
    $("#footerNav").append(footerNavDomItems);
    // temporary disable all footer links
    $("#footerNav a").bind("click", function(e){
        e.preventDefault()
    });
});
