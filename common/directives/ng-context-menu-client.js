define(function (require) {
    'use strict';
    /**
     * ng-context-menu - An AngularJS directive to display a context menu when a right-click event is triggered
     *
     * @author Ian Kennington Walter (http://www.iankwalter.com)
     */
    var app = require('appModule');
    app.register.directive('contextMenu', ['$window','GLOBAL_VARS', '$timeout', '$rootScope', function($window, GLOBAL_VARS, $timeout, $rootScope) {
        return {
            restrict: 'A',
            link: function($scope, element, attrs) {
                var opened = false,
                    menuElement = angular.element(document.getElementById(attrs.contextMenu)),
                    parentScope = menuElement.scope(),
                    /*init = function(){

                        console.log("$scope",menuElement.scope(), $scope.$parent);
                    },*/
                    open = function open(event, element) {
                        opened = true;
                        var scrollTop = $($window).scrollTop();
                        //console.log(scrollTop, "event",event);
                        //console.log("target",($($(event.target).parents("treeitem")[0]).parents("li")[0]).id);
                        var topPos = event.pageY-scrollTop;
                        element.css('left', event.pageX + 'px');
                        element.css('top', topPos + 'px');

                        parentScope.initRightMenu(event);

                        element.addClass('rcm-open');

                        $timeout(function(){
                            if( (topPos + element[0].scrollHeight) > ($($window).height() - 30) ){
                                var newTop = $($window).height() - element[0].scrollHeight - 30;
                                element.css('top', newTop + 'px');
                            }
                        },200)

                    },
                    close = function close(element) {
                        opened = false;
                        element.removeClass('rcm-open');
                        //element.removeClass('rename-open');
                        //element.removeClass('confirm-open');
                        //element.removeClass('note-open');
                        //element.attr('rel', null);
                    }

                menuElement.css('position', 'fixed'); //absolute
                menuElement.css('z-index', '99999');

                element.bind('contextmenu', function(event) {
                    //init();
                    $scope.$apply(function() {
                        event.preventDefault();
                        open(event, menuElement);
                        startBind();
                    });
                });
                /*function showNameDialog(){
                    menuElement.removeClass('rcm-open');
                    menuElement.addClass('rename-open');
                    angular.element(document.getElementById("newName").focus())
                }
                function showNameConfirm(){
                    menuElement.removeClass('rcm-open');
                    menuElement.addClass('confirm-open');
                }
                function showAddNote(){
                    menuElement.removeClass('rcm-open');
                    menuElement.addClass('note-open');
                }*/
                function unbindMenu(){
                    if(parentScope.onHideRCM){
                        parentScope.onHideRCM();
                    }
                    //console.log("CLOSE");
                    $scope.$apply(function() {
                        event.preventDefault();
                        close(menuElement);
                        angular.element($window).unbind('click');
                        angular.element($window).unbind('mousedown');
                    });
                }
                function startBind(){
                    angular.element($window).bind('click', function(event) {
                        // handle most cases
                        /*if (opened && $rootScope.rcmAction=="") {
                            unbindMenu();
                        }else{
                            if($rootScope.rcmAction!=""){
                                console.log("$rootScope.rcmAction",$rootScope.rcmAction);
                            }
                        }*/
                        if (opened){
                            unbindMenu();
                        }
                    });
                }

            }
        };
    }])
});

