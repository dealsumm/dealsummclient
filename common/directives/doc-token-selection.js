define(function (require) {
    'use strict';

    var app = require('appModule');

    app.register.directive('docTokenSelection', ['$rootScope', '$state', '$compile', '$window', function($rootScope, $state, $compile, $window) {
        var start_token = null;
        var start_select_time = null;
        var just_click_time = 400;
        var end_token = null;
        var startElt, endElt, tempEndElt, range;
        var selectedTextMaxLabelLength = 20;
        return {
            restrict: 'A',
            link: function(scope, element, attr) {
                scope.selectionPhrase = {};
                element.addClass("position-relative");
                var selectionMenu = angular.element('<div class="phrase-selected"><div class="phrase-data">Tokens: <span ng-bind="selectionPhrase.phrase.from_token"></span>-<span ng-bind="selectionPhrase.phrase.to_token"></span>&nbsp;&nbsp;<span class="icon-copy"></span></div></div>');
                //element.append(selectionMenu);
                scope.copyPhraseData = function(){
                    angular.extend($rootScope.selectionPhrase, scope.selectionPhrase);
                    //$rootScope.selectionPhrase = scope.selectionPhrase;
                    console.log("scope.selectionPhrase",scope.selectionPhrase);
                    console.log("$rootScope.selectionPhrase",$rootScope.selectionPhrase);
                    $rootScope.$apply();
                    $(selectionMenu).hide();
                };
                element.on('mousedown', function (event) {
                    if(event.which==1){
                        start_token =  null;
                        end_token = null;
                        if($(event.target).hasClass('phrase-data') || $(event.target).parent().hasClass('phrase-data')){
                            //console.log("selectionMenu",selectionMenu);
                            scope.copyPhraseData();
                            event.preventDefault();
                            return false;
                        } else{
                            scope.selectionPhrase = {};
                            start_select_time = new Date().getTime();
                            $(selectionMenu).hide();
                        }
                    }
                })
                element.on('selectstart', function (event) {
                    //console.log("START",$(event.target).closest("span[m]"));
                    startElt = $(event.target).closest("span[m]");
                    if(!startElt.attr("m")){
                        startElt = $(event.target).next();
                    };
                    if(!startElt.attr("m")){
                        startElt = $(event.target).prev();
                    };
                    start_token = startElt.attr("m");
                })
                element.on('mouseup', function (event) {
                    if(event.which!=1 || start_token==null){
                        return;
                    }
                    var now = new Date().getTime();
                    if(start_select_time + just_click_time - now > 0){
                        console.log("Short click");
                    }else{
                        if(start_token){
                            range = $window.getSelection();
                            //console.log("END",$(event.target).closest("span[m]"));
                            endElt = $(event.target).closest("span[m]");
                            if(!endElt.attr("m")){
                                endElt = $(range.focusNode).prev();
                            };
                            if(!endElt.attr("m")){
                                endElt = $(range.focusNode).next();
                            };
                            if(!endElt.attr("m")){
                                endElt = $(event.target).find("span[m]").last();
                            };
                            end_token = endElt.attr("m");

                            //console.log("endElt",endElt);
                            if(start_token!=null && end_token!=null){
                                //$rootScope.selectionPhrase = {};
                                // switch between first and last in case selection started from end of paragraph
                                if(start_token>end_token){
                                    tempEndElt = end_token;
                                    end_token = start_token;
                                    start_token = tempEndElt;
                                }else if(start_token==end_token){
                                    end_token = (1*end_token) + 1;
                                }
                                var phraseObj = {from_token:Math.min(start_token, end_token), to_token: Math.max(start_token, end_token)};
                                //var phrase = phraseObj.start + "-" + phraseObj.end;
                                scope.selectionPhrase.phrase = phraseObj;
                                scope.selectionPhrase.label = getSelectionText(selectedTextMaxLabelLength)||"";
                                console.log("tokens / phrase: ", phraseObj);
                                //console.log("$(startElt) P",$(startElt).closest("p"));
                                //var phraseTop = $(startElt).parent().position().top;
                                var phraseTop = $(startElt).closest("p")?$(startElt).closest("p").position().top:$(startElt).position().top;// + $(startElt).position().top;
                                //var phraseLeft = $(endElt).parent().position().left + ((($(endElt).parent().width())/2));
                                var phraseLeft = $(element).width()/2;
                                $(selectionMenu).attr("style", "left:" + phraseLeft + "px;top:" + phraseTop + "px;display:block;");
                                var menuElt = $compile(selectionMenu)(scope);
                                //var menuElt = linkFn(scope);
                                element.append(menuElt);
                                scope.$apply();
                                //console.log("selectionMenu", selectionMenu, phraseLeft, phraseTop);
                            }else{
                                console.log("no end token, start:", start_token);
                            }
                        }else{
                            console.log("no start token");
                        }
                    }
                })
                function getSelectionText(maxChars) {
                    var text = "";
                    if ($window.getSelection) {
                        text = $window.getSelection().toString();
                    } else if (document.selection && document.selection.type != "Control") {
                        text = document.selection.createRange().text;
                    }
                    text = text.substring(0,maxChars);
                    return text;
                }
            }
        };
    }]);
});
