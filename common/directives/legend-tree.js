define(function (require) {
    'use strict';
    // http://plnkr.co/edit/6maXLm?p=preview
    var app = require('appModule');
    app.register.directive('legendTree', ['$compile', '$parse', function($compile, $parse) {
        return {
            template: '<ul><legend-tree-node ng-repeat="item in items"></legend-tree-node></ul>',
            restrict: 'E',
            replace: false,
            scope: {
                items: '=items',
                onNodeClickFn:"&",
                topFunc: '='
            },
            link: function(scope, elm, attrs) {
                scope.onNodeClickFn = function(item) {
                    //console.log("scope.item", item);
                    item.fn(item);
                }
            }
        };
    }]);
    app.register.directive('legendTreeNode', ['$compile', '$stateParams', '$state', '$parse', function($compile, $stateParams, $state) {
        var isClientDocumentView = false;
        var isClientPdfView = false;
        return {
            restrict: 'E',
            template: '<li ng-class="{\'has-label\':item.no_of_labels>0}">'+
                            '<i ng-class="getIconClass()" ng-click="toggleItem()"></i>'+
                            '<div class="labels" ng-if="item.no_of_labels>0">[{{item.no_of_labels}}]</div>'+
                            '<div ng-click="changeState(item)"><span>{{item.section}}</span> {{item.label}}</div>'+
                        '</li>',
            link: function(scope, elm, attrs) {
                scope.getIconClass = function(){
                    if (scope.item.items && scope.item.items.length > 0) {
                        if(scope.item.expanded){
                            return 'icon-minus';
                        }
                        return 'icon-plus';
                    }
                    return '';
                }
                scope.changeState = function(clickedItem){
                    if(typeof clickedItem.fn == "function"){
                        scope.onNodeClickFn(clickedItem);
                    }else{
                        //console.log("clickedItem",clickedItem,clickedItem.div,clickedItem.phrase);
                        var section = clickedItem.div;
                        var phrase = clickedItem.phrase;

                        if(section){
                            $state.go('app.docView.section', {sectionId:section});
                        }else if(phrase && phrase.indexOf("-1")!=0){
                            $state.go('app.docView.phrase', {phrase:phrase});
                        }
                    }

                    //scope.docScroll($stateParams.div);
                    //scope.docScrollFn({selector:selector})
                }
                scope.toggleItem = function(){
                    scope.item.expanded =! scope.item.expanded;
                }
                if(scope.item.preSelected){
                    // preSelected:true - disabled for now
                    // not sure it should come from item itself -
                    // or should come from URL (?)
                    // in case that might come from both which one is "stronger" ?
                    // code:
                    // $stateParams.section = scope.item.section;
                    // $state.transitionTo($state.$current, $stateParams);
                }
                if (scope.item.items && scope.item.items.length > 0) {
                    var children = $compile('<legend-tree items="item.items" ng-class="{collapsed:!item.expanded}" doc-scroll-fn="docScroll(selector)"></legend-tree>')(scope);
                    elm.append(children);
                }
            }
        };
    }]);
});