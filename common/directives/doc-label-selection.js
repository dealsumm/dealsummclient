define(function (require) {
    'use strict';

    var app = require('appModule');
    app.register.directive('labelsBars', ['$rootScope', '$state', '$compile', '$window', '$timeout', function($rootScope, $state, $compile, $window, $timeout) {
        return {
            restrict: 'AE',
            replace: false,
            scope: {
                paragraphs:'=paragraphs',
                generatedLabels:'=generatedLabels',
                selectedLabelId:'=selectedLabelId'
            },
            template:   '<div class="limiter snap-to" ng-repeat="para in paragraphs track by $index" ng-style="{\'top\': para.top + \'px\',\'height\': para.height + \'px\'}">'+
                            '<span ng-repeat="(idx, lbl) in para.label_ids"' +
                                'ng-init="brdr=init_marker(idx, lbl, para)"' +
                                'style="right:{{label_bar_width * generatedLabels[lbl].marker_pos + 1 + 2 * generatedLabels[lbl].marker_pos}}px;"' +
                                'ng-class="{active:selectedLabelId==lbl}"' +
                                'brdr="{{brdr}}"' +
                                'class="category__{{para.category_ids[idx]}} border_{{brdr}}" category="{{para.category_ids[idx]}}" label="{{lbl}}"></span>' + //{{lbl}}
                        '</div>',
            link: function(scope, element, attr) {
                scope.label_bar_width = 20;
                /*for(var k=0;k<scope.paragraphs.length;k++){
                    console.log("paragraphs",scope.paragraphs[k].id);
                }
                console.log("paragraphs",scope.paragraphs);*/
                //scope.generatedLabels[lbl].marker_pos = val
                function resetMarkerPos(){
                    for(var k=0;k<scope.generatedLabels.length;k++){
                        scope.generatedLabels[k].marker_pos = 0;
                    }
                    for(var k=0;k<scope.paragraphs.length;k++){
                        scope.paragraphs[k].marker_pos = 0;
                    }

                    //scope.paragraphs[para.id].marker_pos
                }
                resetMarkerPos();
                //console.log("paragraphs",scope.paragraphs);
                //console.log("generatedLabels",scope.generatedLabels);
                scope.init_marker = function(idx, lbl, para, para_idx){
                    var rm_border = 0;
                    if(scope.generatedLabels[lbl].paragraphs.length>1){
                        //console.log("scope.generatedLabels[lbl]",scope.generatedLabels[lbl].paragraphs.indexOf(para.id));
                        if(scope.generatedLabels[lbl].paragraphs.indexOf(para.id) == 0){
                            // remove the bottom border
                            rm_border = 1;
                        }else if(scope.generatedLabels[lbl].paragraphs.indexOf(para.id) == scope.generatedLabels[lbl].paragraphs.length-1){
                            // remove the top border
                            rm_border = 2;
                        }else{
                            // remove both top and bottom borders
                            rm_border = 3;
                        }
                        /*if(scope.generatedLabels[lbl].paragraphs.indexOf(para.id)>0 && scope.generatedLabels[lbl].paragraphs.indexOf(para.id)<scope.generatedLabels[lbl].paragraphs.length-1){
                            console.log("para.id", para.id, scope.paragraphs[para.id]);
                            // remove both top and bottom borders
                            rm_border = 3;
                        }*/
                    }
                    var val = idx >= scope.generatedLabels[lbl].marker_pos ? idx : scope.generatedLabels[lbl].marker_pos;
                    scope.paragraphs[para.id].marker_pos+=val;
                    scope.generatedLabels[lbl].marker_pos = val;
                    //val = val >= scope.paragraphs[para.id].marker_pos ? val : scope.paragraphs[para.id].marker_pos;
                    //console.log("para.id",para.id, "lbl", lbl, scope.generatedLabels[lbl].marker_pos, "p", scope.paragraphs[para.id].marker_pos, ">", val);
                    return rm_border;
                }
                scope.isReady = function(){
                    //console.log("isReady");
                    scope.checkOverlapping();
                };
                scope.paragraphsReady = scope.$watch('paragraphs', function(val){
                    $timeout(function(){
                        scope.isReady();
                    }, 100);
                })
                scope.checkOverlapping = function(){
                    var map = {}, pos, conflicts = [];
                    angular.forEach(scope.generatedLabels, function(lbl){
                        pos = lbl.marker_pos;
                        //console.log("lbl",lbl, pos);
                        angular.forEach(lbl.paragraphs, function(para_id){
                            if(!map[para_id]){
                                map[para_id] = [];
                            }
                            if(map[para_id].indexOf(pos)>-1){
                                conflicts.push(lbl.label_id);
                            }
                            map[para_id].push(pos);
                        })
                    });
                    if(conflicts.length>0){
                        console.log("conflicts",conflicts);
                        angular.forEach(conflicts, function(label_id){
                            //var conflict_label_id = label_id;
                            var label_marker_pos = scope.generatedLabels[label_id].marker_pos;
                            scope.generatedLabels[label_id].marker_pos = label_marker_pos + 1;
                            scope.$apply();
                        })
                        scope.checkOverlapping();
                    }
                }
                scope.$on("$destroy", function handleDestroyEvent(){
                    //console.log("handleDestroyEvent");
                    scope.paragraphsReady();
                });

            }
        }
    }])
    app.register.directive('docLabelSelection', ['$rootScope', '$state', '$compile', '$window', '$timeout', function($rootScope, $state, $compile, $window, $timeout) {
        var startElt, endElt, tempEnd_paragraph, range;
        var selectedTextMaxLabelLength = 20;
        //var limiter = null;
        return {
            restrict: 'A',
            link: function(scope, element, attr) {
                var label_control, top_marker, bottom_marker, dashed_marker, limiterTop, limiterBottom, limiters, limitersHolder;
                scope.initLabelDirective = function(){
                    scope.start_paragraph = null;
                    scope.end_paragraph = null;
                    label_control = angular.element('<div class="label-control" id="label_control"></div>');
                    top_marker = angular.element('<div class="paragraph-marker first-paragraph-marker" ng-style="marker.topMarker"></div>');
                    bottom_marker = angular.element('<div class="paragraph-marker last-paragraph-marker" ng-style="marker.bottomMarker"></div>');
                    dashed_marker = angular.element('<div class="paragraph-dashed-marker category__border_{{selectedCategory.id}}" ng-style="marker.dashedMarker"><div class="right-dash category__border_{{selectedCategory.id}}" ng-style="marker.dashedRightMarker"></div></div>');//<span class="btn-dismiss icon-cancel-circle fadeIn" ng-click="dismissLabelMarker()" ng-show="marker.showDismiss"></span>
                    limiterTop = angular.element('<div class="limiter-marker" id="limiter_top" ng-style="marker.limiterTop"></div>');
                    limiterBottom = angular.element('<div class="limiter-marker" id="limiter_bottom" ng-style="marker.limiterBottom"></div>');
                    limitersHolder = angular.element('<div id="limiters_holder"></div>');
                    label_control.append(top_marker);
                    label_control.append(bottom_marker);
                    label_control.append(dashed_marker);
                    label_control.append(limiterTop);
                    label_control.append(limiterBottom);
                    label_control.append(limitersHolder);
                    element.prepend(label_control);
                    label_control.on('click', function (event) {
                        //console.log("event", $(event.target).attr('label'));
                        var clicked_label = $(event.target).attr('label');
                        if(clicked_label){
                            event.preventDefault();
                            //console.log("scope.selectedLabelId",scope.selectedLabelId, "clicked_label", clicked_label);
                            if(scope.selectedLabelId != clicked_label){
                                scope.selectedLabelId = clicked_label;
                                scope.setCategory($(event.target).attr('category'));
                                var firstAndLast = scope.getFirstAndLast(scope.generatedLabels[clicked_label].paragraphs);
                                scope.start_paragraph = firstAndLast[0];
                                scope.end_paragraph = firstAndLast[1];
                                scope.setFirstParagraphMeasurements(scope.start_paragraph);
                                scope.setLastParagraphMeasurements(scope.end_paragraph);
                                scope.setParagraphMarkers();
                                scope.selectedLabels = checkSelectedLabels();
                            }else{
                                scope.dismissLabelMarker();
                            }
                            scope.$apply();
                        }
                    })
                    scope.marker = {};
                    scope.marker.first = {};
                    scope.marker.last = {};
                    scope.selectedLabels = null;

                }
                scope.initLabelDirective();

                scope.initLabelMarkers = function(){
                    limiters = angular.element('<div labels-bars paragraphs="allParagraphs" generated-labels="generatedLabels" selected-label-id="selectedLabelId"></div>');
                    limitersHolder.append(limiters);
                    $compile(label_control)(scope);
                    scope.$apply();
                };
                scope.clearLabelMarkers = function(){
                    scope.dismissLabelMarker();
                    $("#limiters_holder").empty();
                    $("#label_control").height(1);
                    limiters = null;
                };

                scope.dismissLabelMarker = function(){
                    scope.start_paragraph = null;
                    scope.end_paragraph = null;
                    scope.error400 = null;
                    scope.selectedLabelId = null;
                    scope.selectedLabels = null;
                    scope.showCategories.range = false;
                    scope.showCategories.selected = false;
                    scope.selectedCategory = {};
                    scope.marker = {};
                    scope.marker.first = {};
                    scope.marker.last = {};
                    $('.paragraph-marker').css('top',"-5000px");
                    $('.paragraph-dashed-marker').css('top',"-5000px");
                    //scope.$apply();
                }
                scope.setFirstParagraphMeasurements = function(pid){
                    var elt = $("p#"+pid);
                    scope.marker.first.id = pid;
                    //scope.marker.first.top = elt.position().top;// + element[0].scrollTop;
                    scope.marker.first.top = scope.allParagraphs[pid].top;
                    scope.marker.first.width = elt.outerWidth();
                    //scope.marker.first.height = elt.outerHeight();
                    scope.marker.first.height = scope.allParagraphs[pid].height;
                    scope.marker.first.bottom = scope.marker.first.top;// + scope.marker.first.height;
                    scope.$apply();
                };
                scope.setLastParagraphMeasurements = function(pid){
                    var elt = $("p#"+pid);
                    scope.marker.last.id = pid;
                    //scope.marker.last.top = elt.position().top;// + element[0].scrollTop;
                    scope.marker.last.top = scope.allParagraphs[pid].top;
                    scope.marker.last.width = elt.outerWidth();
                    //scope.marker.last.height = elt.outerHeight();
                    scope.marker.last.height = scope.allParagraphs[pid].height;
                    scope.marker.last.bottom = scope.marker.last.top + scope.marker.last.height;
                    scope.$apply();
                };
                scope.setParagraphMarkers = function(){
                    scope.marker.topMarker = {'top': scope.marker.first.top + "px", 'width': scope.marker.first.width + 106 + "px"};
                    scope.marker.bottomMarker = {'top': scope.marker.last.bottom + "px", 'width': scope.marker.last.width + 106 + "px"};
                    scope.marker.dashedMarker = {'top': scope.marker.first.top + "px", 'height': (scope.marker.last.bottom - scope.marker.first.top) + "px"};//, 'width': scope.marker.last.width + 90 + "px"
                    scope.marker.dashedRightMarker = {'top': "0", 'height': (scope.marker.last.bottom - scope.marker.first.top) + "px", 'left': scope.marker.last.width + 103 + "px"};
                    scope.setLimiters();
                    //$compile(label_control)(scope);
                    scope.$apply();
                };
                scope.setLimiters = function(){
                    scope.marker.limiterTop = {'top': "10px", 'height': (scope.marker.last.top-10) + "px"};
                    scope.marker.limiterBottom = {'bottom': "10px", 'top': scope.marker.first.bottom + "px"};
                    scope.$apply();
                };
                var findMatchingElement = function(pos){
                    for(var i=0;i<scope.allParagraphs.length;i++){
                        //console.log(">>>",scope.allParagraphs[i].top + scope.allParagraphs[i].height, "pos", pos);
                        if(scope.allParagraphs[i].top + scope.allParagraphs[i].height >= pos){
                            return i;
                        }
                    }
                };
                scope.dragTopEnded = function(endPos){
                    var finalIdx = findMatchingElement(endPos);
                    scope.start_paragraph = finalIdx;
                    scope.setFirstParagraphMeasurements(finalIdx);
                    scope.selectedLabels = checkSelectedLabels();
                    scope.setParagraphMarkers();
                };
                scope.dragBottomEnded = function(endPos){
                    var finalIdx = findMatchingElement(endPos);
                    scope.end_paragraph = finalIdx;
                    scope.setLastParagraphMeasurements(finalIdx);
                    scope.selectedLabels = checkSelectedLabels();
                    scope.setParagraphMarkers();
                };
                $("#label_control > .first-paragraph-marker").draggable({
                    axis: "y",
                    snap: ".snap-to",
                    snapMode: "outer",
                    containment: "#limiter_top",
                    opacity: 0.7,
                    //revert: true,
                    helper: "clone",
                    scroll: false,
                    start : function(event, ui) {
                        scope.marker.showDismiss = false;
                    },
                    drag: function(){

                    },
                    stop: function(event, ui){
                        scope.marker.showDismiss = true;
                        scope.dragTopEnded(ui.helper.position().top);
                    }
                });
                $("#label_control > .last-paragraph-marker").draggable({
                    axis: "y",
                    snap: ".snap-to",
                    snapMode: "outer",
                    containment: "#limiter_bottom",
                    opacity: 0.7,
                    helper: "clone",
                    scroll: false,
                    start : function(event, ui) {
                        scope.marker.showDismiss = false;
                    },
                    drag: function(){

                    },
                    stop: function(event, ui){
                        scope.marker.showDismiss = true;
                        scope.dragBottomEnded(ui.helper.position().top);
                    }
                });
                element.on('mousedown', function (event) {
                    if(event.which==1 && scope.labelingMode){
                        scope.marker.showDismiss = false;
                        //console.log("scope.allParagraphs",scope.allParagraphs);
                    }
                })
                element.on('selectstart', function (event) {
                    //console.log("START",$(event.target).closest("p"));
                    if(scope.labelingMode){
                        //console.log("element",element.scrollTop,element[0].scrollTop);

                        startElt = $(event.target).closest("p");

                        if(startElt.attr("id")) {
                            scope.dismissLabelMarker();
                            scope.start_paragraph = startElt.attr("id");
                            scope.setFirstParagraphMeasurements(scope.start_paragraph);
                            scope.setLastParagraphMeasurements(scope.start_paragraph);
                            scope.setParagraphMarkers();
                        }
                    }
                })
                element.on('mousemove', function (event) {
                    if (event.which!=1 || !scope.labelingMode) {
                        return;
                    }
                    endElt = $(event.target).closest("p");

                    if(endElt.attr("id")){
                        scope.end_paragraph = endElt.attr("id");
                        scope.setLastParagraphMeasurements(scope.end_paragraph);
                        scope.setParagraphMarkers();
                    }
                })
                element.on('mouseup', function (event) {
                    if (event.which!=1 || !scope.labelingMode) {
                        return;
                    }
                    //console.log("event.target",$(event.target));
                    endElt = $(event.target).closest("p");

                    if(endElt.attr("id")){
                        scope.end_paragraph = endElt.attr("id");
                        scope.setLastParagraphMeasurements(scope.end_paragraph);
                        scope.setParagraphMarkers();
                    }
                    scope.marker.showDismiss = true;
                    $window.getSelection().removeAllRanges();
                    scope.$apply();
                    if(scope.start_paragraph){
                        $timeout(function(){
                            if(scope.start_paragraph!=null && scope.end_paragraph!=null) {
                                if(scope.end_paragraph*1 < scope.start_paragraph*1){
                                    tempEnd_paragraph = scope.end_paragraph;
                                    scope.end_paragraph = scope.start_paragraph;
                                    scope.start_paragraph = tempEnd_paragraph;
                                    scope.setFirstParagraphMeasurements(scope.start_paragraph);
                                    scope.setLastParagraphMeasurements(scope.end_paragraph);
                                    scope.setParagraphMarkers();
                                }
                                //console.log("start_paragraph", scope.start_paragraph, "end_paragraph", scope.end_paragraph);
                                scope.selectedLabels = checkSelectedLabels();
                                scope.$apply();
                            }
                        }, 300)
                    }
                })
                var checkSelectedLabels = function(){
                    var first = scope.start_paragraph;
                    var last = scope.end_paragraph;
                    //console.log("first, last",first, last);
                    var paragraphsArray = [];
                    var conflictArray = [];
                    var allArray = [];
                    for(var i=first;i<=last;i++){
                        allArray.push(i*1);
                        if(scope.selectedLabelId && scope.allParagraphs[i].label_ids.indexOf(scope.selectedLabelId * 1)>-1){
                            conflictArray.push(i*1);
                        }else{
                            paragraphsArray.push(i*1);
                        }
                    }
                    //console.log("scope.selectedLabels",{paragraphs: paragraphsArray, conflict: conflictArray, merged:allArray});
                    return {paragraphs: paragraphsArray, conflict: conflictArray, merged:allArray};
                };
            }
        };
    }]);

});
