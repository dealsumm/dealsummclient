define(function (require) {
    'use strict';

    var app = require('appModule');

    app.register.directive('ngSglclick', ['$parse', function($parse) {
        return {
            restrict: 'A',
            link: function(scope, element, attr) {
                var fn = $parse(attr['ngSglclick']);
                scope.clicks = 0;
                var delay = 300, timer = null;
                element.on('click', function (event) {
                    scope.clicks++;  //count clicks
                    console.log("scope.clicks",scope.clicks);
                    if(scope.clicks === 1) {
                        timer = setTimeout(function() {
                            scope.$apply(function () {
                                fn(scope, { $event: event });
                            });
                            scope.clicks = 0;             //after action performed, reset counter
                        }, delay);
                    } else {
                        clearTimeout(timer);    //prevent single-click action
                        scope.clicks = 0;             //after action performed, reset counter
                    }
                });
            }
        };
    }]);
});
