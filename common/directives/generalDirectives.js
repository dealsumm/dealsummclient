define(function (require) {
    'use strict';
    var app = require('appModule');

    app.register.directive('dragOverGetId', ['$rootScope', '$timeout', function ($rootScope, $timeout) {
        return {
            replace: false,
            link: function (scope, element, attrs) {
                var isOver = false;
                element.bind('dragover', function (evt) {
                    //console.log("folder-id",attrs.folderId, attrs.propertyId);
                    if (attrs.folderId) {
                        $rootScope.dragOverFolderId = attrs.folderId.split("_")[1];
                    }
                    if (attrs.propertyId) {
                        $rootScope.dragOverPropertyId = attrs.propertyId;
                    }

                    isOver = true;
                });
                element.bind('drop dragleave', function (evt) {
                    console.log("drop");
                    isOver = false;
                    $timeout(function () {
                        if (!isOver) {
                            $rootScope.dragOverFolderId = null;
                            $rootScope.dragOverPropertyId = null;
                        }
                    }, 300);

                });
            }
        }

    }]);

    app.register.directive('mixitup', function ($timeout) {
        // http://plnkr.co/edit/9kxAPSA2aKatFHFD5gET
        // https://mixitup.kunkalabs.com/docs/
        var initialized = false;
        var linker = function (scope, element, attrs) {

            scope.$on("has_properties", function () {
                if (initialized) {
                    try {
                        // in case the mixitup directive was pre-init - destroy it to start with a fresh one
                        $(element).mixItUp('destroy');
                    } catch (err) {
                        console.log("mixItUp > err", err);
                    }

                }
                $timeout(function () {
                    initialized = true;
                    $(element).mixItUp({
                        load: {
                            sort: 'order:asc' /* default:asc */
                        },
                        animation: {
                            animateChangeLayout: true, // Animate the positions of targets as the layout changes
                            animateResizeTargets: true, // Animate the width/height of targets as the layout changes
                            effects: 'fade rotateX(-40deg) translateZ(-100px)',
                            easing: 'ease'
                        },
                        /*callbacks: {
                         onMixFail: function(state){	console.log('No elements found matching ',state); },
                         onMixStart: function(state, futureState){ console.log('Animation starting',state); }
                         },*/
                        layout: {
                            containerClass: 'grid' // Add the class 'list' to the container on load
                        }
                    });
                });
            });

            var layout = 'grid';
            scope.$on("updateGridFilter", function (evt, filter) {
                $timeout(function () {
                    if (filter.length > 1) {
                        var pname, ret, idx;
                        $("#mixitup > div").each(function () {
                            $(this).css("display", "none");
                            pname = $(this).attr("pname");
                            if (pname) {
                                pname = pname.toLowerCase();
                                filter = filter.toString().toLowerCase();
                                idx = pname.indexOf(filter);
                                //console.log("ret", pname, idx, filter);
                                if (idx > -1) {
                                    $(this).css("display", "inline-block");
                                }
                            }
                        });
                    } else {
                        $("#mixitup > div").each(function (item) {
                            $(this).css("display", "inline-block");
                        })
                    }
                }, 100);
            })
            scope.$on("toggleGridSort", function (evt, direction) {
                $timeout(function () {
                    var method = 'asc';
                    if (direction.length > 0) {
                        method = 'desc';
                    }
                    //console.log("sortType",direction, option, method);
                    $(element).mixItUp('sort', 'myorder:' + method);
                }, 100);
            })
            scope.$on("toggleGridList", function (evt, isList) {
                if (isList) {
                    layout = 'list';
                    $(element).mixItUp('changeLayout', {
                        containerClass: layout // Change the container class to 'list'
                    });
                } else {
                    layout = 'grid';
                    $(element).mixItUp('changeLayout', {
                        containerClass: layout // change the container class to "grid"
                    });
                }
            })
        };
        return {
            restrict: 'A',
            link: linker
        };
    });

    //https://github.com/sparkalow/angular-count-to
    app.register.directive('countTo', ['$timeout', function ($timeout) {
        return {
            replace: false,
            scope: true,
            link: function (scope, element, attrs) {

                var e = element[0];
                var num, refreshInterval, duration, steps, step, countTo, value, increment;

                var calculate = function () {
                    refreshInterval = 30;
                    step = 0;
                    scope.timoutId = null;
                    countTo = parseInt(attrs.countTo) || 0;
                    scope.value = parseInt(attrs.value, 10) || 0;
                    duration = (parseFloat(attrs.duration) * 1000) || 0;

                    steps = Math.ceil(duration / refreshInterval);
                    increment = ((countTo - scope.value) / steps);
                    num = scope.value;
                }

                var tick = function () {
                    scope.timoutId = $timeout(function () {
                        num += increment;
                        step++;
                        if (step >= steps) {
                            $timeout.cancel(scope.timoutId);
                            num = countTo;
                            e.textContent = countTo;
                        } else {
                            e.textContent = Math.round(num);
                            tick();
                        }
                    }, refreshInterval);

                }

                var start = function () {
                    if (scope.timoutId) {
                        $timeout.cancel(scope.timoutId);
                    }
                    $timeout(function () {
                        calculate();
                        tick();
                    }, 500);

                }

                attrs.$observe('countTo', function (val) {
                    if (val) {
                        start();
                    }
                });

                attrs.$observe('value', function (val) {
                    start();
                });

                return true;
            }
        }
    }]);

});