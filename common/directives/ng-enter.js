define(function (require) {
    'use strict';

    var app = require('appModule');

    app.register.directive('ngEnter', ['$timeout',function ($timeout) {

        return function(scope, element, attrs) {
            element.bind("keydown keypress", function(event) {
                if(event.which === 13) {
                    scope.$apply(function(){
                        scope.$eval(attrs.ngEnter, {'event': event});
                    });

                    event.preventDefault();
                }
            });
        };

    }]);
});