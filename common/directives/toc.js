define(function (require) {
    'use strict';
    // http://plnkr.co/edit/6maXLm?p=preview
    var app = require('appModule');
    app.register.directive('legendTree', ['$compile', function($compile) {
        return {
            template: '<ol><legend-tree-node ng-repeat="item in items"></legend-tree-node></ol>',
            restrict: 'E',
            replace: false,
            scope: {
                items: '=items'
            }
        };
    }]);
    app.register.directive('legendTreeNode', ['$compile', '$stateParams', '$state', function($compile, $stateParams, $state) {
        return {
            restrict: 'E',
            template: '<li><i ng-class="getIconClass()" ng-click="toggleItem()"></i><span ng-click="changeState(item.section)">{{item.label}}</span></li>',
            link: function(scope, elm, attrs) {
                scope.getIconClass = function(){
                    if (scope.item.items && scope.item.items.length > 0) {
                        if(scope.item.expanded){
                            return 'icon-minus';
                        }
                        return 'icon-plus';
                    }
                    return '';
                }
                scope.changeState = function(selector){
                    //console.log("$stateParams",$stateParams);
                    $stateParams.section = selector;
                    $state.transitionTo('app.docView.section', $stateParams);
                    //scope.docScrollFn({selector:selector})
                }
                scope.toggleItem = function(){
                    scope.item.expanded =! scope.item.expanded;
                }
                if(scope.item.preSelected){
                    // preSelected:true - disabled for now
                    // not sure it should come from item itself -
                    // or should come from URL (?)
                    // in case that might come from both which one is "stronger" ?
                    // code:
                    // $stateParams.section = scope.item.section;
                    // $state.transitionTo($state.$current, $stateParams);
                }
                if (scope.item.items && scope.item.items.length > 0) {
                    var children = $compile('<legend-tree items="item.items" ng-class="{collapsed:!item.expanded}" doc-scroll-fn="docScroll(selector)"></legend-tree>')(scope);
                    elm.append(children);
                }
            }
        };
    }]);
});