define(function (require) {
    'use strict';

    var app = require('appModule');

    app.register.directive('ngTitle', ['$timeout',function ($timeout) {
        return function(scope, element, attrs) {
            var debugMode = scope.debugMode;
            if(debugMode){
                $timeout(function(){
                    element.attr("title", attrs.ngTitle);
                }, 500)
            }
        };
    }]);
});