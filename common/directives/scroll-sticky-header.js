define(function (require) {
    'use strict';

    var app = require('appModule');

    app.register.directive('scrollStickyHeader', ['$rootScope', '$state', '$timeout', '$window', function($rootScope, $state, $timeout, $window) {

        return {
            restrict: 'AE',
            scope:{
                scrollStickyHeader:"="
            },
            link: function(scope, element, attr) {
                scope.childs = [];
                var scroll_top;
                //element.addClass("position-relative");

                scope.init = function(){
                    var allChildren = $(element).children();
                    angular.forEach(allChildren ,function(kid){
                        if(!scope.innerHeaderHeight){
                            scope.innerHeaderHeight = $($(kid).children()[0]).innerHeight();
                        }
                        //console.log("$(kid).first()",$(kid),$(kid).children()[0]);
                        //console.log("kid",$(kid), $(kid).position().top, $(kid).height());
                        scope.childs.push({elt:kid, header:$(kid).children()[0], body:$(kid).children()[1], top:$(kid).position().top, height:$(kid).innerHeight()})
                    })
                    element.bind('scroll', function(event) {
                        scroll_top = element[0].scrollTop;
                        angular.forEach(scope.childs, function(kid){
                             //console.log("kid.top<scroll_top",kid.top, kid.top+kid.height,scroll_top);
                             if(kid.top < scroll_top && kid.top + kid.height - scope.innerHeaderHeight > scroll_top) {
                                 $(kid.elt).addClass("sticky");
                                 $(kid.header).css("top", scroll_top + "px");
                                 $(kid.body).css("paddingTop", (scope.innerHeaderHeight) + "px");
                             }else if(kid.top < scroll_top && kid.top + kid.height > scroll_top){
                                 $(kid.elt).addClass("sticky");
                                 $(kid.header).css("top", (scroll_top + ((((kid.top + kid.height) - scope.innerHeaderHeight) - scroll_top))) + "px");
                                 $(kid.body).css("paddingTop", (scope.innerHeaderHeight) + "px");
                             } else{
                                 $(kid.elt).removeClass("sticky");
                                 $(kid.body).css("paddingTop","0");
                             }
                        });
                    })
                }
                var watchOnce = scope.$watch('scrollStickyHeader', function(val){
                    if(val && val.length>0){
                        $timeout(function(){
                            scope.init();
                            watchOnce();
                        },200);
                    }
                })
            }
        };
    }]);
});
