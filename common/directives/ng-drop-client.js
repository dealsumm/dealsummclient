define(function (require) {
    'use strict';

    var app = require('appModule');

    app.register.directive('ngDrop',
                ['$timeout', 'uploadServ', '$dialog', 'DEALSUMM_CONFIG', 'GLOBAL_VARS', '$rootScope', '$window',
        function ($timeout, uploadServ, $dialog, DEALSUMM_CONFIG, GLOBAL_VARS, $rootScope, $window) {
        var startReadingFiles = function(dataTransfer, parentId, type){
            var parentType = type;
            $rootScope.uploadParentId = parentId;
            //console.log("parentId",parentId,"parentType",parentType);
            if(dataTransfer && dataTransfer.items) {
                $rootScope.files = angular.copy(dataTransfer.items);
                var doneDataTransferItemsPromise = readDataTransferItems(dataTransfer.items, parentId, parentType);

                doneDataTransferItemsPromise.done( function(result) {
                    uploadServ.uploadDone();
                    console.log("IMPORT DONE....")

                });
            }
        }
        var params = [];
        var readDataTransferItems = function(dataTransferItems, parentFolderId, parentType){
            var deferred = $.Deferred();
            var chained = $.Deferred();
            uploadServ.uploadFilesList = [];
            for(var i = 0; i < dataTransferItems.length; i++){
                chained.then(
                    function (num) {
                        return function () {
                            var entry = dataTransferItems[num];
                            if(entry.getAsEntry){  //Standard HTML5 API
                                entry = entry.getAsEntry();
                            } else if(entry.webkitGetAsEntry){  //WebKit implementation of HTML5 API.
                                entry = entry.webkitGetAsEntry();
                            }

                            if(entry.isDirectory){
                                //$rootScope.uploadFilesList.push({name:entry.name, type:"folder"})
                                chained = chained.then(function(){
                                    return readFolderTree(entry, parentFolderId, parentType);
                                });
                            }else if(entry.isFile){
                                //console.log("uploadFilesList push",entry.name);
                                //console.log("parentType 1",parentType);
                                if(parentType=="root"){
                                    // in case dropping a file onto the root of project - prevent it
                                    if(dropParent){
                                        dropParent.addClass("cannot-drop");
                                        $timeout(function(){
                                            dropParent.removeClass("cannot-drop");
                                        }, 3000);
                                    }
                                    return false;
                                }
                                uploadServ.uploadFilesList.push({name:entry.name, type:"file", documentParent:parentFolderId});
                                chained = chained.then(function(){
                                    return readFile(entry, parentFolderId, parentType, i);
                                });
                            }
                            params.push(entry);

                            //console.log(i+"|"+dataTransferItems.length+"!!", entry, "\n", "entry.isFile", entry.isFile, "entry.isDirectory", entry.isDirectory);
                        };
                    }(i)
                );
            }

            chained.resolve();
            chained.done(function () {
                deferred.resolve();
            });

            //return deferred obj promise
            return deferred.promise();
        };
        // Traverse recursively through folder
        var readFolderTree = function(itemEntry, parentFolderId, parentType){
            var deferred = $.Deferred();

            var folderIdPromise = uploadServ.uploadFolder(itemEntry.name, parentFolderId, parentType, deferred);
            folderIdPromise.then(function(promise){
                //console.log("promise.data", promise.data);
                //console.log("folderIdPromise", getIdNumber(promise.data.id).id);
                var folderId = getIdNumber(promise.data.id).id;
                parentType = "folder";
                var dirReader = itemEntry.createReader();
                dirReader.readEntries(function(entries){
                    //console.log("readEntries-", entries);

                    /*angular.forEach(entries, function(entry){
                        $rootScope.uploadFilesList.push({name:entry.name, type:entry.isDirectory?"folder":"file"})
                    })*/

                    var doneChildrenPromise = readChildren(entries, folderId, parentType);
                    doneChildrenPromise.done(function(result){
                        //console.log("doneChildrenPromise");
                        deferred.resolve(folderId);
                    });
                });
            });

            //return deferred obj promise
            return deferred.promise();
        };
        //Read the children
        var readChildren = function(entries, parentFolderId, parentType){
            var deferred = $.Deferred();
            var chained = $.Deferred();

            for(var i = 0; i < entries.length; i++){
                chained.then(
                    function (num) {
                        return function () {
                            if(entries[num].isDirectory){
                                chained = chained.then(function(){
                                    return readFolderTree(entries[num], parentFolderId, parentType);
                                });
                            }else if(entries[num].isFile){
                                //console.log("uploadFilesList children push",entries[num].name);
                                uploadServ.pushUploadFiles({
                                    name: entries[num].name,
                                    type: "file",
                                    documentParent: parentFolderId
                                });
                                chained = chained.then(function(){
                                    return readFile(entries[num], parentFolderId, parentType, i);
                                });
                            }
                        };
                    }(i)
                );
            }
            chained.resolve();
            chained.done(function() {
                deferred.resolve();
            });

            //return deferred obj promise
            return deferred.promise();
        };


        //Read FileEntry to get Native File object.
        var readFile = function(fileEntry, folderId, parentType, idx) {
            var deferred = $.Deferred();
            //Get File object from FileEntry
            fileEntry.file(function(file){
                //console.log("file",file.name, deferred);
                var docIdPromise = uploadServ.uploadFile(file, folderId, parentType, idx, deferred);

                /*console.log("docIdPromise",docIdPromise);
                docIdPromise.then(function(result){

                    console.log("result",result);
                    deferred.resolve(result);
                });*/
            });

            //return deferred obj promise
            return deferred.promise();
        };
        var getIdNumber = function(text) {
            text = ""+text;
            var ret = {};
            ret['type'] = "folder";//id.indexOf("F_")>-1?"folder":id.indexOf("O_")>-1?"org":"document";
            ret['id'] = text.indexOf("_")>-1?text.split("_")[1]:text;//id.replace(/F_/, '').replace(/D_/, '').replace(/O_/, '');
            return ret
        };
        var dropParent;
        return {
            scope: {
                ngUrlDrop: '=',
                originalLabelValue: '='//,
                //itemId: '=', // The item ID to send the server
                //callback: '&' // A callback function that will apply on save
            },
            link: function(scope, element, attrs, ctrl) {
                element
                    .bind('drop', function (event) {
                        var dataTransfer = event.dataTransfer ?
                            event.dataTransfer :
                            event.originalEvent.dataTransfer; // jQuery fix;
                        if (!dataTransfer) return;

                        //var eltId = angular.element(event.target)[0].attributes['id'] || angular.element(event.target).parent()[0].attributes['id'];

                        //console.log("attrs",attrs);
                        event.preventDefault();
                        event.stopPropagation();
                        element.parent().removeClass(attrs.ngFileOver || 'ng-file-over');
                        dropParent = element.parent();
                        if(!attrs.folderId){
                            return;
                        }
                        var droppedOnId = attrs.folderId;//eltId.value;//angular.element(event.target)[0].attributes['id'].value;
                        var eltTypeAndId = getIdNumber(droppedOnId);
                        /*console.log("eltTypeAndId.id",eltTypeAndId);
                        // in case dropping on a document and not on a folder - NO IN USE
                        if(eltTypeAndId.type=="document"){
                            return;
                        }*/

                        if(attrs.isProjectFolder){
                            // handle a case of dropping a file onto the project/root folder
                            console.log("isProjectFolder",attrs.isProjectFolder);
                            eltTypeAndId.type = "root";
                        }

                        startReadingFiles(dataTransfer, eltTypeAndId.id, eltTypeAndId.type);
                    })
                    .bind('dragover', function (event) {
                        event.preventDefault();
                        event.stopPropagation();
                        element.parent().addClass(attrs.ngFileOver || 'ng-file-over');
                    })
                    .bind('dragleave', function () {
                        element.parent().removeClass(attrs.ngFileOver || 'ng-file-over');
                    });


                }
        };
    }]);
});