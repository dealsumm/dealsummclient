define(function (require) {
    'use strict';

    var app = require('appModule');

    app.register.directive('resizer', ['$document', '$window', '$timeout', function($document, $window, $timeout) {

        return function($scope, $element, $attrs) {
            var minSideMargins = 180;
            var waitForRepostioning = 200;
            var resizerMaxY, resizerMinY, resizerMinX, resizerMaxX, y, windowInnerHeight, parentLeft, parentWidth, initLeft, timeForRepostioning = 0;;

            angular.element($window).bind('resize', function() {
                init();
            });
            function init(){
                //console.log("resizer init");
                //parentLeft = $($attrs.resizerParent).offset().left;
                resizerMinX = null;
                resizerMaxX = null;
                initLeft = $($attrs.resizerLeftElement).width() + (1*($attrs.resizerPadding));
                if($attrs.resizerForceLeftWidth){
                    timeForRepostioning = waitForRepostioning;
                }
                $timeout(function(){
                    if($attrs.resizerForceLeftWidth){ // in percentage
                        parentWidth = $($attrs.resizerParent).width();
                        initLeft = (parentWidth/100*(1*($attrs.resizerForceLeftWidth))) + (1*($attrs.resizerPadding));
                        // make sure the left pos is not bigger than minSideMargins
                        if(parentWidth-minSideMargins<initLeft){
                            initLeft = parentWidth-minSideMargins;
                        }
                        if(minSideMargins>initLeft){
                            initLeft = minSideMargins;
                        }
                        if ($attrs.resizer == 'vertical') {
                            $($attrs.resizerLeftElement).css({
                                width: initLeft - (1*($attrs.resizerPadding)) + 'px'
                            });
                            $($attrs.resizerRightElement).css({
                                width: parentWidth + (1*($attrs.resizerPadding)) - initLeft + 'px'
                            });
                        }
                    }
                    $element.css({
                        left: initLeft + 'px'
                    });
                },200);

                if($attrs.resizerHide){
                    $($element).hide();
                }
            }
            init();
            $element.on('mousedown', function(event) {

                event.preventDefault();
                $element.addClass("dragged");

                if ($attrs.resizer == 'vertical') {
                    parentLeft = $($attrs.resizerParent).offset().left;
                    parentWidth = $($attrs.resizerParent).width();
                    if(!resizerMinX && !resizerMaxX){

                        //var posLeft = $($element).position().left;
                        resizerMinX = minSideMargins;
                        resizerMaxX = parentWidth-minSideMargins;

                        //console.log("resizer init >", resizerMinX, resizerMaxX);
                    }
                }else{
                    windowInnerHeight = window.innerHeight;
                    resizerMaxY = windowInnerHeight * ($attrs.resizerMaxY / 100);
                    resizerMinY = 70;
                    $($attrs.resizerBottom).addClass("dragged");
                }
                console.log("$scope",$scope, $scope.$parent);
                if($attrs.resizerCallback && $scope[$attrs.resizerCallback]){
                    $scope[$attrs.resizerCallback]();
                }

                //console.log("parentWidth", parentWidth);
                //leftElement = $($attrs.resizerLeftElement);
                //console.log("$attrs.resizerMaxY",$attrs.resizerMaxY);
                $document.on('mousemove', mousemove);
                $document.on('mouseup', mouseup);
            });

            function mousemove(event) {
                if ($attrs.resizer == 'vertical') {
                    // Handle vertical resizer
                    //console.log("parentLeft",parentLeft);
                    //console.log("event",event);
                    //console.log("event",$($attrs.resizerParent).position().left);
                    var x = event.clientX - parentLeft; //screenX ?   clientX? pageX?

                    if (x > resizerMaxX) {
                        x = parseInt(resizerMaxX);
                    }
                    if (x < resizerMinX) {
                        x = parseInt(resizerMinX);
                    }

                    $element.css({
                        left: x + 'px'
                    });
                    ///console.log("$attrs.resizerLeftElement",x);
                    $($attrs.resizerLeftElement).css({
                        width: x - (1*($attrs.resizerPadding)) + 'px'
                    });
                    $($attrs.resizerRightElement).css({
                        width: parentWidth + (1*($attrs.resizerPadding)) - x + 'px'
                    });

                } else {
                    // Handle horizontal resizer
                    y = windowInnerHeight - event.clientY;

                    if (y > resizerMaxY) {
                        y = resizerMaxY;
                    }
                    if (y < resizerMinY) {
                        y = resizerMinY;
                    }

                    //console.log("y",y, resizerMaxY);
                    $element.css({
                        bottom: y + 'px'
                    });
                    $($attrs.resizerBottom).css({
                        height: y+10 + 'px'
                    });
                    //$scope.drawerHeight = y+10 + 'px'

                    //console.log("$scope.drawerHeight",$scope.drawerHeight);
                }
            }

            function mouseup() {
                $element.removeClass("dragged");
                //$scope.updateDrawerHeight();
                $document.unbind('mousemove', mousemove);
                $document.unbind('mouseup', mouseup);
            }
        };
    }]);
});