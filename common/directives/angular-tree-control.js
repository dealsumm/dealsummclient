define(function (require) {
    'use strict';

    var app = require('appModule');
    app.register.directive('treecontrol', ['$stateParams', '$state', '$compile', function($stateParams, $state, $compile) {
        /**
         * @param cssClass - the css class
         * @param addClassProperty - should we wrap the class name with class=""
         */
        function classIfDefined(cssClass, addClassProperty) {
            if (cssClass) {
                if (addClassProperty)
                    return 'class="' + cssClass + '"';
                else
                    return cssClass;
            }
            else
                return "";
        }

        function ensureDefault(obj, prop, value) {
            if (!obj.hasOwnProperty(prop))
                obj[prop] = value;
        }

        return {
            restrict: 'EA',
            require: "treecontrol",
            transclude: true,
            scope: {
                treeModel: "=",
                selectedNode: "=?",
                onSelection: "&",
                onLoading: "&",
                options: "=?",
                metadataWidth: "=",
                metadata: "="
            },
            controller: function( $scope ) {
                $scope.$stateParams = $stateParams;
                $scope.$state = $state;
                $scope.fix_id = $scope.$parent.fix_id;

                $scope.options = $scope.options || {};
                ensureDefault($scope.options, "nodeChildren", "children");
                ensureDefault($scope.options, "dirSelectable", "true");
                ensureDefault($scope.options, "injectClasses", {});
                ensureDefault($scope.options.injectClasses, "ul", "");
                ensureDefault($scope.options.injectClasses, "li", "");
                ensureDefault($scope.options.injectClasses, "liSelected", "");
                ensureDefault($scope.options.injectClasses, "iExpanded", "");
                ensureDefault($scope.options.injectClasses, "iCollapsed", "");
                ensureDefault($scope.options.injectClasses, "iLeaf", "");
                ensureDefault($scope.options.injectClasses, "label", "");
                ensureDefault($scope.options.injectClasses, "labelSelected", "");
                ensureDefault($scope.options, "equality", function (a, b) {
                    if (a === undefined || b === undefined)
                        return false;
                    a = angular.copy(a); a[$scope.options.nodeChildren] = [];
                    b = angular.copy(b); b[$scope.options.nodeChildren] = [];
                    return angular.equals(a, b);
                });

                $scope.expandedNodes = {};
                $scope.isAmendedOrVersion = function(node){
                    return (node['rel'].indexOf("amended-")>-1) || (node['rel'].indexOf("version-")>-1);
                }
                $scope.isVersion = function(node){
                    return (node['rel'].indexOf("version-")>-1);
                }
                $scope.isFav = function(node) {
                    //console.log("this.node['fav']",this.node['fav']);
                    if (this.node['fav']==true || this.node['fav']=="true"){
                        return "fav active";
                    }else{
                        return "fav";
                    }
                }
                $scope.clickFav = function(node) {
                    this.node['fav'] = !this.node['fav'];
                }
                $scope.clickTermsheet = function(node) {

                    console.log("clickTermsheet");
                }
                $scope.headClass = function(node) {
                    var liSelectionClass = classIfDefined($scope.options.injectClasses.liSelected, false);
                    var injectSelectionClass = "";
                    if (liSelectionClass && (this.$id == $scope.selectedScope)){
                        injectSelectionClass = " " + liSelectionClass;
                    }

                    if (this.node['rel']==="loading"){
                        return "tree-loading";
                    }
                    if (this.node['rel']==="doc" || $scope.isVersion(this.node)){
                        // make sure that "doc" and "version" will behave the same
                        return "tree-leaf" + injectSelectionClass;
                    }
                    if(this.node['expanded']==="true"){
                        $scope.expandedNodes[this.$id] = this.node;
                    }
                    if ($scope.expandedNodes[this.$id]){
                        this.node['expanded'] = true;
                        return "tree-expanded" + injectSelectionClass;
                    }else{
                        this.node['expanded'] = false;
                        return "tree-collapsed" + injectSelectionClass;
                    }
                };

                $scope.iArrowClass = function() {
                    if ($scope.expandedNodes[this.$id]){
                        return classIfDefined($scope.options.injectClasses.iExpanded);
                    }else{
                        return classIfDefined($scope.options.injectClasses.iCollapsed);
                    }
                };
                // on a leaf which is amended - instead of arrow we place L shape
                $scope.iArrowLeafClass = function() {
                    if($scope.isAmendedOrVersion(this.node)){
                        return "icon-L";
                    }
                };
                // once a branch or a leaf are amended use icon font to display the document icon
                // rel: amended-XXXXXX-Y (as folder)   /   version-XXXXXX-Y (as document)
                // XXXXXX = the color hex
                // Y letter to display
                $scope.iAmendedClass = function() {
                    if($scope.isAmendedOrVersion(this.node)){
                        return "icon-file";
                    }
                };
                // once a branch or a leaf are amended color the above document icon according to "rel" color
                // rel: amended-XXXXXX-Y (as folder)   /   version-XXXXXX-Y (as document)
                // XXXXXX = the color hex
                // Y letter to display
                $scope.iAmendedStyle = function() {
                    if($scope.isAmendedOrVersion(this.node)){
                        return { 'color':"#" + (this.node['rel'].split("-")[1]) }
                    }
                };
                // once a branch or a leaf are amended place inside the letter according to "rel" letter
                // rel: amended-XXXXXX-Y (as folder)   /   version-XXXXXX-Y (as document)
                // XXXXXX = the color hex
                // Y letter to display
                $scope.iAmendedLetter = function() {
                    if($scope.isAmendedOrVersion(this.node)){
                        return this.node['rel'].split("-")[2]
                    }
                };

                $scope.nodeExpanded = function() {
                    return !!$scope.expandedNodes[this.$id];
                };

                $scope.selectNodeHead = function() {
                    $scope.expandedNodes[this.$id] = ($scope.expandedNodes[this.$id] === undefined ? this.node : undefined);
                };

                $scope.selectNodeLabel = function( selectedNode ){
                    // folder selection
                    if (selectedNode[$scope.options.nodeChildren] && selectedNode[$scope.options.nodeChildren].length > 0 &&
                        !$scope.options.dirSelectable) {
                        this.selectNodeHead();
                    }
                    else {
                        //$scope.selectedScope = this.$id;
                        //$scope.selectedNode = selectedNode;

                        console.log("selectedNode111",selectedNode);
                        if ($scope.onSelection){
                            $scope.onSelection({node: selectedNode});
                        }
                    }
                };

                $scope.selectedClass = function() {
                    var labelSelectionClass = classIfDefined($scope.options.injectClasses.labelSelected, false);
                    var injectSelectionClass = "";
                    if (labelSelectionClass && (this.$id == $scope.selectedScope))
                        injectSelectionClass = " " + labelSelectionClass;

                    return (this.$id == $scope.selectedScope)?"tree-selected" + injectSelectionClass:"";
                };

                //tree template
                var template =
                    '<ul '+classIfDefined($scope.options.injectClasses.ul, true)+'>' +
                        '<li ng-repeat="node in node.' + $scope.options.nodeChildren+'" ng-class="headClass(node)" '+classIfDefined($scope.options.injectClasses.li, true)+'>' +    // id="{{node.id}}"
                            '<span ng-class="isFav(node)" ng-click="clickFav(node)"></span>' +
                            '<div class="{{node.rel}}" data-index="{{$index}}">'+
                                '<i class="tree-branch-head" ng-click="selectNodeHead(node)" ng-style="iAmendedStyle()" ng-class="iAmendedClass()"><span class="amended_letter">{{iAmendedLetter()}}</span><span ng-class="iArrowClass()"></span></i>' +
                                '<i class="tree-leaf-head '+classIfDefined($scope.options.injectClasses.iLeaf, false)+'" ng-style="iAmendedStyle()" ng-class="iAmendedClass()"><span class="amended_letter">{{iAmendedLetter()}}</span><span ng-class="iArrowLeafClass()"></span></i>' +
                                '<div class="tree-label '+classIfDefined($scope.options.injectClasses.label, false)+'" ng-class="selectedClass()" tree-transclude context-menu data-target="RCMenu"></div>' +
                            '</div>' +
                            '<treeitem ng-if="nodeExpanded()"><div>LOADING</div></treeitem>' +
                        '</li>' +
                    '</ul>';
                return {
                    template: $compile(template)
                }
            },
            compile: function(element, attrs, childTranscludeFn) {
                return function ( scope, element, attrs, treemodelCntr ) {

                    scope.$watch("treeModel", function updateNodeOnRootScope(newValue) {
                        if (angular.isArray(newValue)) {
                            if (angular.isDefined(scope.node) && angular.equals(scope.node[scope.options.nodeChildren], newValue))
                                return;
                            scope.node = {};
                            scope.node[scope.options.nodeChildren] = newValue;
                        }
                        else {
                            if (angular.equals(scope.node, newValue))
                                return;
                            scope.node = newValue;
                        }
                    });

                    //Rendering template for a root node
                    treemodelCntr.template( scope, function(clone) {
                        element.html('').append( clone );
                    });

                    /* Build the header of tree */
                    var treeHeader = angular.element("<div class='tree-header'></div>");
                    treeHeader.append("<div>Name</div>");
                    var metadataWrap = angular.element("<div style='width:"+scope.metadataWidth+"px'></div>");
                    angular.forEach(scope.metadata, function(obj, idx){
                        metadataWrap.append("<span style='width:"+(scope.metadataWidth*(scope.metadata[idx].width)/100)+"px;'>" + obj.display + "</span>");
                    });
                    treeHeader.append(metadataWrap);
                    treeHeader.append("<span class='fav'></span>");
                    /* end build header */

                    var headerPlaceholder = angular.element(document.getElementById("tree-header-placeholder"));
                    if(headerPlaceholder){
                        headerPlaceholder.append(treeHeader)
                    }else{
                        element.prepend(treeHeader)
                    }


                    // save the transclude function from compile (which is not bound to a scope as apposed to the one from link)
                    // we can fix this to work with the link transclude function with angular 1.2.6. as for angular 1.2.0 we need
                    // to keep using the compile function
                    scope.$treeTransclude = childTranscludeFn;
                }
            }
        };
    }]);
    app.register.directive('treeitem', [function() {
        return {
            restrict: 'E',
            require: "^treecontrol",
            link: function( scope, element, attrs, treemodelCntr) {
                // Rendering template for the current node
                treemodelCntr.template(scope, function(clone) {
                    // in case there are no children, apply a loading child
                    if(!scope.node.children || (scope.node.children && scope.node.children.length==0)){
                        // in case there is no "children node" - create one
                        if(!scope.node.children){
                            scope.node.children = [];
                        }
                        scope.node.children.push({"label":"Loading...", "rel":"loading"});
                        scope.onLoading({node: scope.node});
                    }
                    element.html('').append(clone);
                });

            }
        }
    }]);
    app.register.directive('treeTransclude', ['$stateParams','$state',function($stateParams, $state) {
        return {
            link: function(scope, element, attrs, controller) {
                angular.forEach(scope.expandedNodes, function (node, id) {
                    if (scope.options.equality(node, scope.node)) {
                        scope.expandedNodes[scope.$id] = scope.node;
                        scope.expandedNodes[id] = undefined;
                    }
                });
                if (scope.options.equality(scope.node, scope.selectedNode)) {
                    scope.selectNodeLabel(scope.node);
                }

                scope.$treeTransclude(scope, function(clone) {
                    element.empty();
                    element.append(clone);
                    //element.attr("id", scope.node.id);
                    //element.attr("rel", scope.node.rel);
                });
            }
        }
    }])
});
