define(function (require) {
    'use strict';

    var app = require('appModule');

    app.register.directive('ngDrop',
                ['$timeout', 'treeServ', '$dialog', 'DEALSUMM_CONFIG', 'GLOBAL_VARS', '$rootScope',
        function ($timeout, treeServ, $dialog, DEALSUMM_CONFIG, GLOBAL_VARS, $rootScope) {
        var startReadingFiles = function(dataTransfer, id, type){
            var parentId = type=="org"?GLOBAL_VARS.org_id:id;
            var parentType = type;
            $rootScope.uploadParentId = (type=="org"?"O":"F")+"_"+parentId;
            //console.log("parentId",parentId,"parentType",parentType);
            if(dataTransfer && dataTransfer.items) {
                $rootScope.files = angular.copy(dataTransfer.items);
                var doneDataTransferItemsPromise = readDataTransferItems(dataTransfer.items, parentId, parentType);

                doneDataTransferItemsPromise.done( function(result) {
                    treeServ.uploadDone();
                    console.log("IMPORT DONE....")

                });
            }
        }
        var params = [];
        var readDataTransferItems = function(dataTransferItems, parentFolderId, parentType){
            var deferred = $.Deferred();
            var chained = $.Deferred();
            $rootScope.uploadFilesList = [];
            for(var i = 0; i < dataTransferItems.length; i++){
                chained.then(
                    function (num) {
                        return function () {
                            var entry = dataTransferItems[num];
                            if(entry.getAsEntry){  //Standard HTML5 API
                                entry = entry.getAsEntry();
                            } else if(entry.webkitGetAsEntry){  //WebKit implementation of HTML5 API.
                                entry = entry.webkitGetAsEntry();
                            }

                            if(entry.isDirectory){
                                $rootScope.uploadFilesList.push({name:entry.name, type:"folder"})
                                chained = chained.then(function(){
                                    return readFolderTree(entry, parentFolderId, parentType);
                                });
                            }else if(entry.isFile){
                                $rootScope.uploadFilesList.push({name:entry.name, type:"file"})
                                chained = chained.then(function(){
                                    return readFile(entry, parentFolderId, parentType, i);
                                });
                            }
                            params.push(entry)

                            console.log(i+"|"+dataTransferItems.length+"!!", entry, "\n", "entry.isFile", entry.isFile, "entry.isDirectory", entry.isDirectory);
                        };
                    }(i)
                );
            }

            chained.resolve();
            chained.done(function () {
                deferred.resolve();
            });

            //return deferred obj promise
            return deferred.promise();
        };
        // Traverse recursively through folder
        var readFolderTree = function(itemEntry, parentFolderId, parentType){
            var deferred = $.Deferred();

            var folderIdPromise = treeServ.uploadFolder(itemEntry.name, parentFolderId, parentType, deferred);
            // URI: help with serv call - returning 500
            folderIdPromise.then(function(promise){
                console.log("folderIdPromise", fix_drop_id(promise.data.id).id);
                var folderId = fix_drop_id(promise.data.id).id;
                parentType = "folder";
                var dirReader = itemEntry.createReader();
                dirReader.readEntries(function(entries){
                    console.log("readEntries-", entries);

                    angular.forEach(entries, function(entry){
                        $rootScope.uploadFilesList.push({name:entry.name, type:entry.isDirectory?"folder":"file"})
                    })

                    var doneChildrenPromise = readChildren(entries, folderId, parentType);
                    doneChildrenPromise.done(function(result){
                        console.log("doneChildrenPromise");
                        deferred.resolve(folderId);
                    });
                });
            });

            //return deferred obj promise
            return deferred.promise();
        };
        //Read the children
        var readChildren = function(entries, parentFolderId, parentType){
            var deferred = $.Deferred();
            var chained = $.Deferred();

            for(var i = 0; i < entries.length; i++){
                chained.then(
                    function (num) {
                        return function () {
                            if(entries[num].isDirectory){
                                chained = chained.then(function(){
                                    return readFolderTree(entries[num], parentFolderId, parentType);
                                });
                            }else if(entries[num].isFile){
                                chained = chained.then(function(){
                                    return readFile(entries[num], parentFolderId, parentType, i);
                                });
                            }
                        };
                    }(i)
                );
            }
            chained.resolve();
            chained.done(function() {
                deferred.resolve();
            });

            //return deferred obj promise
            return deferred.promise();
        };


        //Read FileEntry to get Native File object.
        var readFile = function(fileEntry, folderId, parentType, idx) {
            var deferred = $.Deferred();
            //Get File object from FileEntry
            fileEntry.file(function(file){
                console.log("file",file.name, deferred);
                var docIdPromise = treeServ.uploadFile(file, folderId, parentType, idx, deferred);

                /*console.log("docIdPromise",docIdPromise);
                docIdPromise.then(function(result){

                    console.log("result",result);
                    deferred.resolve(result);
                });*/
            });

            //return deferred obj promise
            return deferred.promise();
        };
        var fix_drop_id = function(id) {
            var ret = {};
            ret['type'] = id.indexOf("F_")>-1?"folder":id.indexOf("O_")>-1?"org":"document";
            ret['id'] = id.replace(/F_/, '').replace(/D_/, '').replace(/O_/, '');
            return ret
        }
        return {
            scope: {
                ngUrlDrop: '=',
                originalLabelValue: '='//,
                //itemId: '=', // The item ID to send the server
                //callback: '&' // A callback function that will apply on save
            },
            link: function(scope, element, attrs, ctrl) {

                element
                    .bind('drop', function (event) {
                        var dataTransfer = event.dataTransfer ?
                            event.dataTransfer :
                            event.originalEvent.dataTransfer; // jQuery fix;
                        if (!dataTransfer) return;

                        console.log("dataTransfer",dataTransfer, dataTransfer.items, angular.element(event.target)[0].attributes['id'].value );
                        event.preventDefault();
                        event.stopPropagation();
                        element.removeClass(attrs.ngFileOver || 'ng-file-over');

                        var droppedOnId = angular.element(event.target)[0].attributes['id'].value;
                        var eltTypeAndId = fix_drop_id(droppedOnId);
                        if(eltTypeAndId.type=="document"){
                            return;
                        }
                        startReadingFiles(dataTransfer, eltTypeAndId.id, eltTypeAndId.type);
                    })
                    .bind('dragover', function (event) {
                        event.preventDefault();
                        event.stopPropagation();
                        element.addClass(attrs.ngFileOver || 'ng-file-over');
                    })
                    .bind('dragleave', function () {
                        element.removeClass(attrs.ngFileOver || 'ng-file-over');
                    });


                }
        };
    }]);
});