/**
 * Created by CHAIMON on 10-Apr-15.
 */
define(function (require) {
    'use strict';
    var app = require('appModule');
    app.register.directive('updateDataOnField', ['$compile', '$timeout', function($compile, $timeout) {
        var icons = {};
        icons.updated = "icon-checkmark-circle";
        icons.updating = "icon-loop";
        icons.error = "icon-cancel-circle";
        return {
            restrict: 'EA',
            scope: {
                updateRel: "=updateDataOnField",
                server_progress: "=serverProgressCheck"
            },
            link: function ($scope, element, attrs) {

                var icon = angular.element("<span class='update-field-icon {{fieldIcon}}'></span>");
                $scope.$watch('updateRel', function(newVal, oldVal){
                    removeAllClassess();
                    if(newVal!=undefined){
                        if(newVal=="updating"){
                            $scope.server_progress['working'] = true;
                            element.addClass('field-updating');
                        }else{
                            if(newVal=="updated"){
                                element.addClass('field-updated');
                            }else if(newVal=="error"){
                                element.addClass('field-error');
                            }
                            cleanIcons();
                            stopServerWorking();
                        }
                        $scope.fieldIcon = icons[newVal];
                        $compile(icon)($scope);
                        element.append(icon);
                    }
                });
                function stopServerWorking(){
                    if($scope.server_progress['working']){
                        $timeout(function(){
                            $scope.server_progress['working'] = false;
                        },200);
                    }
                }
                function cleanIcons(){
                    $timeout(function(){
                        removeAllClassess();
                    },1200);
                }
                function removeAllClassess(){
                    element.removeClass('field-updating');
                    element.removeClass('field-updated');
                    element.removeClass('field-error');
                }

            }
        }
    }])
    /*app.register.directive('dataUpdateOnField', ['$parse', function($parse) {
        return {
            scope: {
                updateRel: "=dataUpdateOnField"
            },
            link: function($scope, element, attrs) {
                console.log("dataUpdateOnField");
                element.append("<span class='icon-checkmark2'></span>");
                $scope.$watch('updateRel', function(newVal, oldVal){
                    console.log("val",newVal, oldVal);
                    if(newVal){
                        //element.addClass();
                    }
                })
            }
        };
    }]);*/
});

