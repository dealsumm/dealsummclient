define(function (require) {
    'use strict';

    var app = require('appModule');

    app.register.directive('ngBindHtmlUnsafe', ['$window', function ($window) {
        return function(scope, element, attr) {
            element.addClass('ng-binding').data('$binding', attr.ngBindHtmlUnsafe);
            scope.$watch(attr.ngBindHtmlUnsafe, function ngBindHtmlUnsafeWatchAction(value) {
                element.html(value || '');
            });
        }
    }]);

});
