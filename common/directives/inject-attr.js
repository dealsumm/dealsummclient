define(function (require) {
    'use strict';

    var app = require('appModule');

    app.register.directive('injectAttr', ['$parse', '$compile',function ($parse, $compile) {
        return {
            restrict: 'A',
            template: '<div ng-transclude></div>',
            /*template: function( element, attr ) {
                var tag = element[0].nodeName;
                var att = eval(attr.injectAttr);

                console.log(" element, attr", att);
                return "<"+tag+" ng-transclude "+ (attr.injectAttr) +"></"+tag+">";
            },*/
            transclude: true,
            replace: true,
            link: function (scope, element, attr) {
                scope.directive_name = scope[attr.injectAttr];
                console.log("scope.directive_name",scope.directive_name);
                element.attr(scope.directive_name);
                //console.log("$parse", scope.directive_name, scope[attr.injectAttr]);
                $compile()(scope);
            }
        }
    }]);
});