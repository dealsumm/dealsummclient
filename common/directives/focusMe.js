define(function (require) {
    'use strict';

    var app = require('appModule');
    app.register.directive('focusMe', ['$parse', '$timeout', function($parse, $timeout) {
        return {
            //scope: true,   // optionally create a child scope
            link: function(scope, element, attrs) {
                $timeout(function() {
                    element[0].focus();
                });
                /*var model = $parse(attrs.focusMe);
                scope.$watch(model, function(value) {
                    //console.log('value=',value);
                    if(value) {
                        $timeout(function() {
                            element[0].focus();
                        });
                    }
                });*/
                // to address @blesh's comment, set attribute value to 'false'
                // on blur event:
                /*element.bind('blur', function() {
                    console.log('blur');
                    scope.$apply(model.assign(scope, false));
                });*/
            }
        };
    }]);
});
