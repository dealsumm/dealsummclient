angular.module('modal', [])
    .provider("$dialog", function () {

        var defaults = {
            id: null,
            template: null,
            templateUrl: null,
            title: 'Default Title',
            backdrop: true,
            ok: {label: 'OK', fn: null},
            negative: {label: 'cancel', fn: null},
            controller: null, //just like route controller declaration
            footerTemplate: true,
            css: {},
            backdropCancel:true,
            headerTemplate: true,
            initCallback: null,
            animation: true,
            okCss: "Btn__action"
        };

        this.$get = ["$http", "$document", "$compile", "$rootScope", "$controller",
            function ($http, $document, $compile, $rootScope, $controller) {

                var body = $document.find('body');

                var Dialog = function (templateUrl/*optional*/, options, passedInLocals) {
                    // Handle arguments if optional template isn't provided.
                    if (angular.isObject(templateUrl)) {
                        passedInLocals = options;
                        options = templateUrl;
                    } else {
                        options.templateUrl = templateUrl;
                    }

                    options = angular.extend({}, defaults, options); //options defined in constructor

                    var key;
                    var idAttr = options.id ? ' id="' + options.id + '" ' : '';
                    var animation = options.animation ? " slideUp" : "";
                    var defaultFooter =
                            '<div class="text__rev">' +
                            '   <button ng-disabled="$modalButtonsDisabled()" class="Btn '+options.okCss+'" ng-click="$modalOk()">{{$modalOkLabel}}</button>' +
                            (options.negative!==false?'<button ng-disabled="$modalButtonsDisabled()" class="Btn Btn__negative pointer" ng-click="$modalCancel()">{{$modalnegativeLabel}}</button>':'')  +
                            '</div>'
                        ;
                    var headerTemplate =
                        options.headerTemplate ?
                            '<div class="modal-header">' +
                            '	<a class="close-button" ng-click="$modalCancel()"></a>' +
                            '	<div id="modal-title" class="strong fs5">{{$title}}</div>' +
                            '</div>'
                            :
                            ''
                        ;

                    var footerTemplate = options.footerTemplate ? '<div class="modal-footer clearfix">' + defaultFooter + '</div>' : options.footerTemplate;
                    var modalBody = (function () {
                        if (options.template) {
                            if (angular.isString(options.template)) {
                                return '<div class="modal-body padding-small">' + options.template + '</div>';
                            } else {
                                return '<div class="modal-body container">' + options.template.html() + '</div>';
                            }
                        } else {
                            return '<div class="modal-body container" ng-include="\'' + options.templateUrl + '\'"></div>'
                        }
                    })();
                    //We don't have the scope we're gonna use yet, so just get a compile function for modal


                    var modalEl = angular.element([
                        '<div class="modal-wrapper fadeIn">',  // ng-click="$offsetCancel($event)"
                        '	<div class="cc">',
                        '		<div class="ccc">',
                        '			<div class="modal-container fade' + animation + '"' + idAttr + '>',
                        headerTemplate || '',
                        modalBody,
                        footerTemplate || '',
                        '			</div>',
                        '		</div>',
                        '	</div>',
                        '</div>'
                    ].join(''));

                    for (key in options.css) {
                        modalEl.css(key, options.css[key]);
                    }

                    var handleEscPressed = function (event) {
                        if (event.keyCode === 27) {
                            scope.$modalCancel();
                        }
                    };

                    var closeFn = function () {
                        angular.element(body).css({"overflow-y":"auto"});
                        body.unbind('keydown', handleEscPressed);
                        modalEl.remove();
                    };

                    body.bind('keydown', handleEscPressed);

                    var ctrl, locals,
                        scope = options.scope || $rootScope.$new();

                    scope.params = options.params || null;

                    scope.$title = options.title;
                    scope.$offsetCancel = function(ev){
                        if(!options.backdropCancel) {
                            return;
                        }
                      if (ev.currentTarget === ev.target) {
                        closeFn();
                      }
                    };
                    scope.$modalCancel = closeFn;
                    scope.$setModalButtonsStatus = function(val){
                        scope.disableModalButtons = val;
                    }
                    scope.$modalButtonsDisabled = function(){
                        return scope.disableModalButtons;
                    }

                    scope.$modalOk = function () {
                        var callFn = typeof (options.ok.fn) == "function"?options.ok.fn:eval(options.ok.fn) || closeFn;
                        callFn.call(this);
                        //scope.$modalCancel();
                    };
                    scope.$modalOkLabel = options.ok.label;
                    scope.$modalnegativeLabel = options.negative.label;

                    if (options.controller) {
                        locals = angular.extend({$scope: scope}, passedInLocals);
                        ctrl = $controller(options.controller, locals);
                        // Yes, ngControllerController is not a typo
                        modalEl.contents().data('$ngControllerController', ctrl);
                    }

                    $compile(modalEl)(scope);
                    angular.element(body).css({"overflow-y":"hidden"});
                    body.append(modalEl);

                };


                return  {
                    dialog: function (templateUrl/*optional*/, options, passedInLocals) {
                        return new Dialog(templateUrl/*optional*/, options, passedInLocals);
                    }
                };


            }];
    });