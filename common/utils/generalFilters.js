define(function (require) {
    'use strict';
    var app = require('appModule');

    app.register.filter('formats', ['$filter', function ($filter) {
        var statuses = ["Pending", "Uploaded", "Processing", "Processed", "Failed"];
        var stringFormats = {
            date: {name: 'date', format: 'MM/dd/yyyy'},
            currency: {name: 'currency', format: '$'},
            percent: {
                name: 'percent', format: function (input) {
                    return $filter('number')(input, 0) + '%';
                }
            },
            dateTime: {name: 'date', format: 'MM/dd/yyyy HH:mm'},
            toNumber: {
                name: 'toNumber', format: function (input) {
                    return parseInt(input.replace(/,/g, ''));
                }
            },
            status: {
                name: 'status', format: function (input) {
                    return statuses[input];
                }
            },
            removeUnderScore: {
                name: 'removeUnderScore', format: function (input) {
                    return input.replace(/_/g, ' ');
                }
            }
        };
        return function (text, format) {
            if (angular.isDefined(text) && text.toString().length > 0 && format && stringFormats[format]) {
                if (typeof stringFormats[format].format == 'function') {
                    var func = stringFormats[format].format;
                    //console.log("function", func(text));
                    return func(text);
                } else {
                    return $filter(stringFormats[format].name)(text, stringFormats[format].format);
                }
            } else {
                return text;
            }
        }
    }]);
    app.register.filter('capitalize', function () {
        return function (input, scope) {
            if (input != null) {
                return input.substring(0, 1).toUpperCase() + input.substring(1).toLowerCase();
            }
        }
    });

    app.register.filter('getById', function () {
        return function (input, id) {
            var i = 0, len = input.length;
            for (; i < len; i++) {
                //console.log("input[i].id",input[i].id, +input[i].id == +id, input[i].id == id);
                if (input[i].id == id) {
                    return {obj: input[i], idx: i};
                }
                /*
                 if (+input[i].id == +id) {
                 return {obj: input[i], idx: i};
                 }
                 */
            }
            return null;
        }
    });

    app.register.filter("noNone", function() {
        return function(text) {
            if(text=="None"){
                text=" ";
            }
            return text;
        };
    });

})