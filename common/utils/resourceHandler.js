angular.module('resourceHandler', [])
    .factory('resourceHandler', ['$http', '$q', function ($http, $q) {

    function ResourceResourceFactory(url, collectionName) {

        var defaultParams = {};
        var thisUrl = url;
        var thenFactoryMethod = function (httpPromise, successcb, errorcb, isArray) {
            var scb = successcb || angular.noop;
            var ecb = errorcb || angular.noop;

            return httpPromise.then(

                // Success
                function (response) {
                    var responseData = response.data,
                        result = [];

                    if (isArray) {
                        angular.forEach(responseData.data, function (resp, i) {
                            result.push(new Resource(resp));
                        });
                    } else {
                        if (response.status === 404) {
                            return $q.reject({
                                code: 'resource.notfound',
                                collection: collectionName
                            });
                        } else {
                            result = new Resource(responseData.data[0]);
                        }
                    }
                    scb(result, response.status, response.headers, response.config);
                    return result;
                },
                // Fail
                function (response) {
                    ecb(undefined, response.status, response.headers, response.config);
                    return undefined;
                });

        };

        var Resource = function (data) {
            angular.extend(this, data);
        };

        Resource.all = function (cb, errorcb) {
            return Resource.query({}, cb, errorcb);
        };

        Resource.query = function (queryJson, successcb, errorcb) {
            var params = angular.isObject(queryJson) ? {q:JSON.stringify(queryJson)} : {};
            var httpPromise = $http.get(thisUrl, {params:angular.extend({}, defaultParams, params)});
            return thenFactoryMethod(httpPromise, successcb, errorcb, true);
        };

        //Added by Avigad (18.12.13) to support headers in requests
        Resource.queryWithHeaders = function (queryJson, headers, successcb, errorcb) {
            var params = angular.isObject(queryJson) ? {q:JSON.stringify(queryJson)} : {};
            var headersObj = angular.isObject(headers) ? headers : {};
            var httpPromise = $http.get(thisUrl, {params:angular.extend({}, defaultParams, params), headers:headersObj});
            return thenFactoryMethod(httpPromise, successcb, errorcb, true);
        };

        Resource.getById = function (id, successcb, errorcb) {
            var httpPromise = $http.get(thisUrl + '/' + id, {params:defaultParams});
            return thenFactoryMethod(httpPromise, successcb, errorcb);
        };
        // Added by Chaim (05 Nov, 2013) - this function enables us to update the URL of the resource
        Resource.setUrl = function (url) {
            thisUrl = url;
        };


        Resource.getByIds = function (ids, successcb, errorcb) {
            var qin = [];
            angular.forEach(ids, function (id) {
                qin.push({$oid: id});
            });
            return Resource.query({_id:{$in:qin}}, successcb, errorcb);
        };

        //instance methods

        Resource.prototype.$id = function () {
            if (this._id && this._id.$oid) {
                return this._id.$oid;
            }
        };

        Resource.prototype.$save = function (successcb, errorcb) {
            var httpPromise = $http.post(thisUrl, this, {params:defaultParams});
            return thenFactoryMethod(httpPromise, successcb, errorcb);
        };

        Resource.prototype.$update = function (queryMethod, successcb, errorcb) {
            var httpPromise = $http.put(queryMethod || thisUrl , angular.extend({}, this, {_id:undefined}), {params:defaultParams});
            return thenFactoryMethod(httpPromise, successcb, errorcb);
        };

        Resource.prototype.$remove = function (successcb, errorcb) {
            var httpPromise = $http['delete'](thisUrl, {params:defaultParams});
            return thenFactoryMethod(httpPromise, successcb, errorcb);
        };

        Resource.prototype.$saveOrUpdate = function (savecb, updatecb, errorSavecb, errorUpdatecb) {
            if (this.$id()) {
                return this.$update(updatecb, errorUpdatecb);
            } else {
                return this.$save(savecb, errorSavecb);
            }
        };

        return Resource;
    }
    return ResourceResourceFactory;
}]);