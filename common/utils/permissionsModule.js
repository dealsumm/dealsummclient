define(function () {
    'use strict';

    /* Start dealSummPermissions */

    var dealSummPermissions = angular.module('permissionsModule', []);

    dealSummPermissions.value('highLevel', 'level0');
    dealSummPermissions.value('lowLevel', 'level99');
    dealSummPermissions.constant('permissions',
                                        {
                                            level99: ['console', 'title', 'debug'],
                                            level2: ['console', 'title'],
                                            level1: ['console'],
                                            level0: []
                                        }
                                );

    dealSummPermissions.service('permissionsServ', [
        '$rootScope',
        'permissions',
        'highLevel',
        'lowLevel',
        'localStorageService',
        function ($rootScope, permissions, highLevel, lowLevel, localStorageService) {

            var permissionLevel = localStorageService.get("permissionLevel") || "level0";
            var getPermissionLevel = function(){
                return permissionLevel;
            };
            var isPermissionHighLevel = function(){
                return permissionLevel == highLevel;
            };
            var isPermissionLowLevel = function(){
                return permissionLevel == lowLevel;
            };
            var toggleHighLowPermissionLevel = function(){
                if(isPermissionHighLevel()){
                    setPermissionToLowLevel();
                }else if(isPermissionLowLevel()){
                    setPermissionToHighLevel();
                }
                return permissionLevel;
            }
            var setPermissionLevel = function(level){
                if(permissions[level]){
                    permissionLevel = level;
                    localStorageService.set("permissionLevel", permissionLevel);
                }
            };
            var setPermissionToHighLevel = function(){
                permissionLevel = highLevel;
                localStorageService.set("permissionLevel", permissionLevel);
            };
            var setPermissionToLowLevel = function(){
                permissionLevel = lowLevel;
                localStorageService.set("permissionLevel", permissionLevel);
            };
            var isAllowed = function(action){
                return permissions[permissionLevel].indexOf(action)>-1;
            };
            return {
                allowed: isAllowed,
                set: setPermissionLevel,
                toggleHighLow: toggleHighLowPermissionLevel,
                get: getPermissionLevel
            };

        }]);


});