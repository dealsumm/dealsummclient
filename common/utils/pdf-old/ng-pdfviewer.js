/**
 * @preserve AngularJS PDF viewer directive using pdf.js.
 *
 * https://github.com/akrennmair/ng-pdfviewer
 *
 * MIT license
 */

define(function (require) {
    'use strict';

    var app = require('appModule');
    app.register.directive('pdfviewer', ['$parse', '$filter', function ($parse, $filter) {
        var canvas = null;
        var viewerContainer = null;
        var highlightLayer = null;
        var instance_id = null;
        var docScale;// = 1;//1.4;
        var scaleFactor;// = 1;//1.4;
        var startYFromBottom;
        var defaultBgColor = "#33b5e6";
        return {
            restrict: "AE",
            template: '<canvas></canvas>',
            scope: {
                onPageLoad: '&',
                loadProgress: '&',
                src: '@',
                id: '=',
                scale: '=',
                startYFromBottom: '@'
            },
            controller: ['$scope', '$timeout', function ($scope, $timeout) {
                $scope.pageNum = 1;
                $scope.pdfDoc = null;
                //$scope.scale = 1.0;

                $scope.documentProgress = function (progressData) {
                    if ($scope.loadProgress) {
                        $scope.loadProgress({state: "loading", loaded: progressData.loaded, total: progressData.total});
                    }
                };

                $scope.loadPDF = function (path) {
                    PDFJS.getDocument(path, null, null, $scope.documentProgress).then(function (_pdfDoc) {
                        $scope.pdfDoc = _pdfDoc;
                        console.log("$scope.pdfDoc",$scope.pdfDoc);
                        $scope.renderPage($scope.pageNum, function (success) {
                            if ($scope.loadProgress) {
                                $scope.loadProgress({state: "finished", loaded: 0, total: 0});
                            }
                        });
                    }, function (message, exception) {
                        console.log("PDF load error: " + message);
                        if ($scope.loadProgress) {
                            $scope.loadProgress({state: "error", loaded: 0, total: 0});
                        }
                    });
                };

                $scope.renderPage = function (num, callback) {
                    //console.log('renderPage ', num);
                    $scope.pdfDoc.getPage(num).then(function (page) {
                        var viewport = page.getViewport(docScale);
                        var ctx = canvas.getContext('2d');

                        canvas.height = viewport.height;
                        canvas.width = viewport.width;
                        highlightLayer.style.height = Math.ceil(viewport.height) + "px";
                        highlightLayer.style.width = Math.ceil(viewport.width) + "px";
                        $scope.highlightScale();

                        /*highlightLayer.style.zoom = docScale;
                        highlightLayer.style.MozTransform = 'scale('+docScale+')';
                        highlightLayer.style.WebkitTransform = 'scale('+docScale+')';*/

                        page.render({canvasContext: ctx, viewport: viewport}).promise.then(
                            function () {
                                if (callback) {
                                    callback(true);
                                }
                                $scope.$apply(function () {
                                    $scope.onPageLoad({page: $scope.pageNum, total: $scope.pdfDoc.numPages});
                                });
                            },
                            function () {
                                if (callback) {
                                    callback(false);
                                }
                                console.log('page.render failed');
                            }
                        );
                    });
                };
                $scope.highlightScale = function(){
                    var layer = angular.element(highlightLayer);
                    var highlighter = layer.find("div");
                    if(highlighter){
                        highlighter.css("left", Math.ceil(highlighter.attr("data-left")*scaleFactor) + "px");
                        highlighter.css("top", Math.ceil(highlighter.attr("data-top")*scaleFactor) + "px");
                        highlighter.css("width", Math.ceil(highlighter.attr("data-width")*scaleFactor) + "px");
                        highlighter.css("height", Math.ceil(highlighter.attr("data-height")*scaleFactor) + "px");

                        var parentContainerHeight = $(viewerContainer).height();
                        var containerScrollTop = $(viewerContainer).scrollTop();
                        var highlighterTop = Math.ceil(highlighter.attr("data-top")*scaleFactor) + (25*scaleFactor);
                        /*console.log("containerScrollTop",containerScrollTop);
                        console.log("parentContainerHeight",parentContainerHeight);
                        console.log("highlighterTop",highlighterTop);*/
                        if(highlighterTop > parentContainerHeight){
                            var offset = highlighterTop - containerScrollTop;
                            //console.log("offset",offset);
                            $(viewerContainer).animate({ scrollTop: offset }, 400);
                        }
                    }
                };
                $scope.clearHighlight = function(){
                    highlightLayer.innerHTML = "";
                };
                $scope.delayHighlight = function(coordinates){
                    $timeout(function(){
                        $scope.highlight(coordinates)
                    }, 500)
                };
                $scope.highlight = function(coordinates){
                    var layer = angular.element(highlightLayer);
                    var layerHeight = $(layer).height();
                    var top =  Math.ceil(startYFromBottom? (layerHeight/docScale)-coordinates.top : coordinates.top);
                    layer.append("<div></div>");
                    var highlighter = layer.find("div");
                    highlighter.attr("data-left", Math.ceil(coordinates.left));
                    highlighter.attr("data-top", top);
                    highlighter.attr("data-width", Math.ceil(coordinates.width));
                    highlighter.attr("data-height", Math.ceil(coordinates.height));
                    highlighter.css("background-color", coordinates.bgColor||defaultBgColor);
                    $scope.highlightScale();
                }
                $scope.$on('pdfviewer.nextPage', function (evt, id) {
                    if (id !== instance_id) {
                        return;
                    }

                    if ($scope.pageNum < $scope.pdfDoc.numPages) {
                        $scope.clearHighlight();
                        $scope.pageNum++;
                        $scope.renderPage($scope.pageNum);
                    }
                });

                $scope.$on('pdfviewer.prevPage', function (evt, id) {
                    if (id !== instance_id) {
                        return;
                    }

                    if ($scope.pageNum > 1) {
                        $scope.clearHighlight();
                        $scope.pageNum--;
                        $scope.renderPage($scope.pageNum);
                    }
                });

                $scope.$on('pdfviewer.gotoPage', function (evt, id, page) {
                    if (id !== instance_id) {
                        return;
                    }

                    if (page >= 1 && page <= $scope.pdfDoc.numPages) {
                        $scope.pageNum = page;
                        $scope.renderPage($scope.pageNum);
                    }
                });
                $scope.$on('pdfviewer.zoomIn', function (evt, id, scale) {
                    scaleFactor = scale;
                    docScale = $filter('number')(scale, 2);
                    $scope.renderPage($scope.pageNum);
                });
                $scope.$on('pdfviewer.zoomOut', function (evt, id, scale) {
                    scaleFactor = scale;
                    docScale = $filter('number')(scale, 2);
                    $scope.renderPage($scope.pageNum);
                });
                $scope.$on('pdfviewer.clearhighlight', function (evt, id) {
                    $scope.clearHighlight();
                })
                $scope.$on('pdfviewer.highlight', function (evt, id, coordinates) {
                    if(!coordinates.page){
                        console.log("no page number");
                        return;
                    }
                    $scope.clearHighlight();
                    if(coordinates.page != $scope.pageNum){
                        $scope.pageNum = coordinates.page;
                        $scope.renderPage($scope.pageNum, $scope.delayHighlight(coordinates));
                    }else{
                        $scope.highlight(coordinates);
                    }

                })
            }],
            link: function (scope, iElement, iAttr) {
                docScale = scope.scale;
                scaleFactor = scope.scale;
                startYFromBottom = (scope.startYFromBottom === "true");
                //console.log("docScale",docScale);
                //console.log("scaleFactor",scaleFactor);
                canvas = iElement.find('canvas')[0];
                instance_id = iAttr.id;

                iElement.append("<div class='highlight-layer'></div>");
                highlightLayer = iElement.find('div')[0];

                viewerContainer = iElement.parent();
                //console.log("viewerContainer",$(viewerContainer).height());

                iAttr.$observe('src', function (v) {
                    console.log('src attribute changed, new value is', v);
                    if (v !== undefined && v !== null && v !== '') {
                        scope.pageNum = 1;
                        scope.loadPDF(scope.src);
                    }
                });
            }
        };
    }])

    app.register.service("PDFViewerService", ['$rootScope', function ($rootScope) {

        var svc = {};
        svc.nextPage = function () {
            $rootScope.$broadcast('pdfviewer.nextPage');
        };

        svc.prevPage = function () {
            $rootScope.$broadcast('pdfviewer.prevPage');
        };

        svc.Instance = function (id) {
            var instance_id = id;

            return {
                prevPage: function () {
                    $rootScope.$broadcast('pdfviewer.prevPage', instance_id);
                },
                nextPage: function () {
                    $rootScope.$broadcast('pdfviewer.nextPage', instance_id);
                },
                gotoPage: function (page) {
                    $rootScope.$broadcast('pdfviewer.gotoPage', instance_id, page);
                },
                zoomIn: function (scale) {
                    $rootScope.$broadcast('pdfviewer.zoomIn', instance_id, scale);
                },
                zoomOut: function (scale) {
                    $rootScope.$broadcast('pdfviewer.zoomOut', instance_id, scale);
                },
                highlight: function (coordinates) {
                    $rootScope.$broadcast('pdfviewer.highlight', instance_id, coordinates);
                },
                clearHighlight: function () {
                    $rootScope.$broadcast('pdfviewer.clearhighlight', instance_id);
                }
            };
        };

        return svc;
    }]);
})


