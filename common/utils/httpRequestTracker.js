angular.module('services.httpRequestTracker', []);
angular.module('services.httpRequestTracker').factory('httpRequestTracker', ['$cacheFactory',function ($cacheFactory) {

	var cache = $cacheFactory('cacheServicePending',{});

	return {
		addRequest: function(){
			var reqs = cache.get("pendingRequests");
			if(reqs){
				reqs = reqs <= 0 ? 1 : reqs +1;
				cache.put("pendingRequests", reqs);
			} else {
				cache.put("pendingRequests", 1);
			}
		},
		removeRequest: function(){
			var reqs = cache.get("pendingRequests");
			if(reqs){
				reqs = reqs -1 ;
				cache.put("pendingRequests", reqs);
			}
		},
		getNumberOfRequests: function(){
			return cache.get("pendingRequests") || 0;
		}
	}
}]);
