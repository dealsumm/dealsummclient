define(function (require) {
    'use strict';
    /*
        pk-scroll directive - extended
        currently supported and tested with y scroll only - x scroll should be supported too but not tested

        setup + parameters + optional parameters
        # pk-scroll="y" (or "x") / must
        # pk-border="true" / optional
                Adds a border to top and bottom of the scrollable container
        # pk-infinite="<function>" + can-load="<value>"
                Once scrolling and passing the percentage of threshold executes the <function> only in case the condition <value> is true
        # pk-scroll-track-padd="0" / optional
                Defines the padding of scroll track from container (top+right+bottom)
                Default is 5
        # pk-scroll-max-height="400" / pk-scroll-max-height="40vh" / optional
                Determine a dynamic height container, in case the container height is smaller than "max-height" value -
                the container height will be set according to it's content,
                Once the content height passes "max-height" value the container maximum height will be "max-height" value + scroll.
                vh - view-height - an option to set the maximum height according to the browsers height in percentage instead of in pixels.
                For example: setting 40vh will have max-height of 40% of the browser's height

     */
    var app = require('appModule');
    app.register.directive('pkScroll', ['$timeout', '$compile', function($timeout, $compile) {
        return {
            restrict: 'A',
            transclude: true,
            template:   "<div class='pk-scroll-content' ng-transclude></div>" +
                        "<div class='pk-scroll-trackY'><div class='pk-scroll-floatY'></div></div>" +
                        "<div class='pk-scroll-trackX'><div class='pk-scroll-floatX'></div></div>" +
                        "<div class='pk-scroll-top-border'></div>" +
                        "<div class='pk-scroll-bottom-border'></div>" +
                        "<div class='pk-scroll-left-border'></div>"+
                        "<div class='pk-scroll-right-border'></div>",
            link: function (scope, el, attr) {
                el.on("mouseenter", function(){
                    el.addClass('pk-scroll-active');
                    scope.$apply();
                });
                el.on("mouseleave", function(){
                    el.removeClass('pk-scroll-active');
                    scope.$apply();
                });
                var autoMaxHeight = false;
                var theEl = el[0];
                if(attr.pkScrollMaxHeight){
                    autoMaxHeight = attr.pkScrollMaxHeight;
                    el.addClass('pk-max-height');
                };
                var autoMinHeight = false;
                if(attr.pkScrollMinHeight){
                    autoMinHeight = attr.pkScrollMinHeight;
                };
                if(attr.pkBorder){
                    el.addClass('pk-border');
                }
                if(attr.pkLrBorder){
                    el.addClass('pk-lr-border');
                }
                if(attr.pkScrollTrackPadd && attr.pkScrollTrackPadd!='5'){
                    var axis = theEl.getAttribute('pk-scroll');
                    var trackPaddInCaseBothXandY = attr.pkScrollTrackPadd;
                    if(axis.indexOf("x")>-1 && axis.indexOf("y")>-1){
                        trackPaddInCaseBothXandY = ((attr.pkScrollTrackPadd*1)+10);
                    }
                    if(axis.indexOf("y")>-1){
                        theEl.children[1].setAttribute('style', 'right:'+attr.pkScrollTrackPadd + 'px;top:'+attr.pkScrollTrackPadd + 'px;bottom:' + trackPaddInCaseBothXandY + 'px;');
                    }
                    if(axis.indexOf("x")>-1){
                        theEl.children[2].setAttribute('style', 'bottom:'+attr.pkScrollTrackPadd + 'px;left:'+attr.pkScrollTrackPadd + 'px;right:' + trackPaddInCaseBothXandY + 'px;');
                    }
                }
                if(attr.pkId){
                    theEl.children[0].setAttribute('id', attr.pkId);
                }
                if(attr.pkPadd){
                    var padds = attr.pkPadd.split(",");
                    if(padds.length<4){
                        padds = [0,0,0,0];
                    }
                    var verticalPadd = parseInt(padds[0]) + parseInt(padds[2]);
                    theEl.children[0].setAttribute('style', 'top:'+ padds[0] + 'px;right:'+ padds[1] + 'px;bottom:'+ padds[2] + 'px;left:' + padds[3] + 'px;');
                }
                var checkForAutoScroll = false;
                if(angular.isDefined(attr.pkScrollTo)){
                    checkForAutoScroll = true;
                    //console.log("scope",scope);
                }

                el.addClass('pk-scroll-container');
                pk.scroll({
                    element: el[0],
                    axis: el[0].getAttribute('pk-scroll'),
                    scope:scope,
                    maxHeight:autoMaxHeight,
                    minHeight:autoMinHeight,
                    checkForAutoScroll:checkForAutoScroll,
                    verticalPadd:verticalPadd||0
                });

            }
        };
    }]);
});
