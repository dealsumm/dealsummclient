/* pk-core */
// Javascript Polyfill

// Production steps of ECMA-262, Edition 5, 15.4.4.14
// Reference: http://es5.github.io/#x15.4.4.14
// indexOf method

if (!Array.prototype.indexOf) {
    Array.prototype.indexOf = function(searchElement, fromIndex) {

        var k;

        if (this == null) {
            throw new TypeError('"this" is null or not defined');
        }

        var O = Object(this);
        var len = O.length >>> 0;

        if (len === 0) {
            return -1;
        }
        var n = +fromIndex || 0;

        if (Math.abs(n) === Infinity) {
            n = 0;
        }
        if (n >= len) {
            return -1;
        }
        k = Math.max(n >= 0 ? n : len - Math.abs(n), 0);
        while (k < len) {
            if (k in O && O[k] === searchElement) {
                return k;
            }
            k++;
        }
        return -1;
    };
}

// Pocketknife Core

var pk = pk || {};
(function (pk) {
    pk.preventBubble = function (e) {
        if (e.preventDefault) {e.preventDefault();}
        if (e.stopPropagation) {e.stopPropagation();}
        e.cancelBubble = true;
        e.returnValue = false;
        return false;
    };
    pk.toggleClass = function(el, c, t){
        if(t === true){
            pk.addClass(el,c);
            return;
        }else if(t===false){
            pk.removeClass(el,c);
            return;
        }
        pk.toggleClass(el, c, !pk.hasClass(el,c));
    };
    pk.hasClass=function(el, c){
        var ca = el.getAttribute('class') || '';
        return (ca && ca.indexOf(c) > -1) ? true : false;
    };
    pk.center = function (el) {
        el.style.top = el.parentNode.clientHeight / 2 - (el.offsetHeight / 2) + 'px';
        el.style.left = el.parentNode.clientWidth / 2 - (el.offsetWidth / 2) + 'px';
    };
    pk.getStyle = function (el, p) {
        return window.getComputedStyle(el).getPropertyValue(p);
    };
    pk.addClass = function (el, c) {
        if (pk.hasClass(el,c)){ return;}
        var ca = el.getAttribute('class') || '';
        el.setAttribute('class', (ca ? ca + ' ' : '') + c);
        return el;
    };
    pk.removeClass = function (el, c) {
        var ca = el.getAttribute('class');
        if (!ca){return;}
        el.setAttribute('class', ca.replace(c, ''));
        return el;
    };
    pk.bindEvent = function (e, el, fn) {
        if(e==="mousewheel"){
            e = (/Firefox/i.test(navigator.userAgent)) ? "DOMMouseScroll" : "mousewheel"; //FF doesn't recognize mousewheel as of FF3.x
        }
        if (el.addEventListener) {
            el.addEventListener(e, fn, false);
        } else {
            // fix for IE8 which does not detect window.onmousemove nor window.onmouseup events - need to attach these events to document
            if(e==="mousemove" || e==="mouseup"){
                document.attachEvent("on" + e, fn);
            }else{
                el.attachEvent("on" + e, fn);
            }

        }
    };
    pk.layout = function (el, offset) {
        var t = offset ? el.offsetTop : el.getBoundingClientRect().top,
            l = offset ? el.offsetLeft : el.getBoundingClientRect().left,
            h = el.offsetHeight,
            w = el.offsetWidth;
        return {
            top: t,
            left: l,
            right: l + w,
            bottom: t + h,
            height: h,
            width: w
        };
    };
    pk.bindListeners=function(l, el){
        for(var e in l){
            pk.bindEvent(e, el, l[e]);
        }
    };
    pk.getRand = function (min, max) {
        return Math.floor(Math.random() * (max - min)) + min;
    };

    pk.wrapEl = function(el, str){
        var helperEl = pk.createEl(str);
        el.parentNode.insertBefore(helperEl, el);
        helperEl.appendChild(el);
        return helperEl;
    };
    pk.createEl=function(str){
        var el = document.createElement('div');
        el.innerHTML=str;
        return el.children[0];
    };
    pk.getIndex=function(el){
        if(!el){return null;}
        var prop = document.body.previousElementSibling ? 'previousElementSibling' : 'previousSibling';
        var i = 1;
        while (el = el[prop]) { ++i; }
        return i-1;
    };
    pk.replaceEl = function(el,str) {
        var newEl = pk.createEl(str);
        for(var i = 0; i < el.attributes.length; i++) {
            newEl.setAttribute(el.attributes[i].nodeName, el.attributes[i].nodeValue);
        }
        while (el.firstChild) {
            newEl.appendChild(el.firstChild);
        }
        el.parentNode.replaceChild(newEl, el);
        return newEl;
    };
    pk.toArr = function(v) {
        var a=[];
        if (v && typeof v !== "object") {
            if(v.indexOf(',') !== -1){
                a = v.split(',');
            }else{
                a.push(v);
            }
        }else{
            a=v;
        }
        return a;
    };
    pk.collide = function(a1, a2, s){
        s = s || 0;
        a1=pk.toArr(a1);
        a2=pk.toArr(a2);
        /*
         s = switch
         0 (default) = replace a1 with a2
         1 = add a2 to a1
         2 = remove a2 from a1
         3 = toggle a2 in a1 and add/remove items if not/found
         */
        if(s===0){return a2;}
        for(var i in a2){
            var f = a1.indexOf(a2[i]) !== -1 ? true : false;
            if(!f && (s ===1 || s === 3)){
                a1.push(a2[i]);
            }else if(f && (s ===2 || s === 3)){
                a1.splice(parseInt(a1.indexOf(a2[i]), 0), 1);
            }
        }
        return a1;
    };
    pk.attribute = function (el, attr, val){

        attr = el.hasAttribute(attr) ? attr : el.hasAttribute('data-'+attr) ? 'data-'+attr : attr;
        if(val===undefined){
            return (attr==='selected' || attr==='disabled' || attr==='checked') ? (el.hasAttribute(attr) ? true : false) : el.getAttribute(attr);
        }
        if(val===false && (attr==='selected' || attr==='disabled' || attr==='checked')){
            el.removeAttribute(attr);
        }else{
            el.setAttribute(attr,val);
        }
    };
})(pk);

/* pk-draggable */
//var pk = pk || {};
(function (pk) {
    pk.draggable = function (opt) {
        var el = opt.element;
        var handle = opt.handle || opt.element;
        var handleTrack = el;//handle.parentNode;
        /*if(!pk.hasClass(handleTrack, 'pk-scroll-trackY') && !pk.hasClass(handleTrack, 'pk-scroll-trackX')){
            handleTrack = null;
        }*/
        var container = {
            element: opt.container && opt.container.element ? opt.container.element : document.body,
            style: opt.container && opt.container.style ? opt.container.style : 'restrict'
        };

        pk.addClass(handle, 'pk-drag-draggable');
        var fn = opt.listeners;
        var m = opt.move;
        if (m && typeof m !== 'object') {
            m= {
                x: true,
                y: true
            };
        }
        var dragging = false;
        var dragStart = {};
        var startOffset;
        var containerD = {};
        var elD = {};

        function augmentEvent(e){

            e.dragStart = dragStart;
            e.dragOffset = startOffset;

            e.dragEnd = {
                x: e.clientX,
                y: e.clientY
            };
            e.dragDist = {
                x:e.dragEnd.x - e.dragStart.x,
                y:e.dragEnd.y - e.dragStart.y
            };
            e.dragPerc = {
                x:(pk.layout(el).left + e.dragDist.x + e.dragOffset.x) / pk.layout(container.element).width,
                y:(pk.layout(el).top + e.dragDist.y + e.dragOffset.y) / pk.layout(container.element).height
            };
            return e;
        }

        pk.bindEvent("mousedown", handle, function (e) {
            dragging = true;
            dragStart = {
                x: e.clientX,
                y: e.clientY
            };
            startOffset = {
                x: e.clientX - el.getBoundingClientRect().left,
                y: e.clientY - el.getBoundingClientRect().top
            };
            e=augmentEvent(e);
            pk.addClass(handle, 'pk-drag-dragging');
            pk.addClass(handleTrack, 'pk-track-dragging');
            pk.addClass(document.body, 'pk-noselect');
            document.onselectstart = function () {
                return false;
            };
            containerD = pk.layout(container.element);
            elD = pk.layout(el);
            if (fn && fn.dragstart){ fn.dragstart(el, e);}
        });
        pk.bindEvent("mouseup", window, function (e) {
            if (!dragging){ return;}
            dragging = false;
            e=augmentEvent(e);
            pk.removeClass(handle, 'pk-drag-dragging');
            pk.removeClass(handleTrack, 'pk-track-dragging');
            pk.removeClass(document.body, 'pk-noselect');
            document.onselectstart = function () {
                return true;
            };
            if (m && container.style === "snap") {
                contain();
            }
            if (fn && fn.dragend){ fn.dragend(el, e);}
        });

        function contain() {
            var h=container.element.tagName ==="BODY" ? document.documentElement.clientHeight : container.element.offsetHeight,
                w=container.element.tagName ==="BODY" ? document.documentElement.clientWidth : container.element.offsetWidth;

            if (m.x && el.offsetLeft < 0) {
                el.style.left = 0 + 'px';
            } else if (m.x && el.offsetLeft > w - el.offsetWidth) {
                el.style.left = w - el.offsetWidth + 'px';
            }
            if (m.y && el.offsetTop < 0) {
                el.style.top = 0 + 'px';
            } else if (m.y && el.offsetTop > h - el.offsetHeight) {
                el.style.top = h - el.offsetHeight + 'px';
            }
        }
        pk.bindEvent("mousemove", window, function (e) {
            if (!dragging){ return;}
            e=augmentEvent(e);
            if (m.x){ el.style.left = el.offsetLeft + (e.dragEnd.x - el.getBoundingClientRect().left) - e.dragOffset.x + 'px';}
            if (m.y){ el.style.top = el.offsetTop + (e.dragEnd.y - el.getBoundingClientRect().top) - e.dragOffset.y + 'px';}
            if (container.style === "restrict"){ contain();}
            if (fn && fn.dragging){ fn.dragging(el, e);}
        });
    };
})(pk);

/* pk-scroll */
//var pk = pk || {};
(function (pk) {
    pk.scroll = function (opt) {
        var threshold = 0.85; // for "infinite scroll" feature
        var scroll_gain = 14; // in case the scrollable content is bigger than container - when should we show scrollbar
        var el = opt.element;
        var scroll_drag_min_height = 30;
        var mouse_scroll_delta = 40;
        var scope = opt.scope;
        var maxHeight = opt.maxHeight;
        var minHeight = opt.minHeight;
        var verticalPadd = opt.verticalPadd;
        var checkForAutoScroll = opt.checkForAutoScroll;
        if(maxHeight){
            // add support for VH
            //console.log("maxHeight",maxHeight, maxHeight.indexOf("vh"));
            if(maxHeight.indexOf("vh")>-1){
                maxHeight = parseInt(maxHeight)/100;
                var w = window,
                    d = document,
                    e = d.documentElement,
                    g = d.getElementsByTagName('body')[0],
                    pageHeight = w.innerHeight|| e.clientHeight|| g.clientHeight;
                maxHeight = Math.round(pageHeight*maxHeight);
                //console.log("maxHeight",maxHeight);
            }
            //el.style.height = 10 + "px";
        }
        // INIT SCROLL STRUCTURE
        var container = el.children[0],
            trackY = el.children[1],
            floatY = trackY.children[0],
            trackX = el.children[2],
            floatX = trackX.children[0];

        //pk.addClass(el, 'pk-scroll-top-0');
        // INIT VARIABLES
        var floatYh = 0,
            floatXw = 0,
            allowY = false,
            allowX = false,
            percY = 0,
            percX = 0,
            contentH = 0,
            contentW = 0,
            containerH = 0,
            containerW = 0,
            contentWidth = 0,
            contentHeight = 0,
            containerWidth = 0,
            containerHeight = 0,
            saveAllowY,
            saveAllowX,
            saveScrollTop = null,
            fetchingData = false,
            _top,
            scrollDir = opt.axis ? opt.axis.toLowerCase() : (pk.attribute(el, 'pk-scroll') ?  pk.attribute(el, 'pk-scroll') : "y");
        if (pk.getStyle(el, 'position') === "static") {el.style.position = "relative";}

        function fetchDataCallback(abort){
            setTimeout(function(){
                //console.log("fetchDataCallback",saveScrollTop, container.scrollHeight);
                fetchingData = false;
                if (!abort){
                    contentH = container.scrollHeight;
                    contentW = container.scrollWidth;
                    container.scrollTop = saveScrollTop;
                    saveScrollTop = null;
                    updateOnScroll();
                }
            }, 150);
        }
        function updateOnScroll(){
            percY = container.scrollTop / (contentH - containerH);
            percX = container.scrollLeft / (contentW - containerW);
            percY = percY < 0 ? 0 : percY > 1 ? 1 : percY;
            percX = percX < 0 ? 0 : percX > 1 ? 1 : percX;
            // IE8 hack
            if(isNaN(percX)){
                percX = 0;
            }
            if(isNaN(percY)){
                percY = 0;
            }
            _top = (containerH - floatYh) * percY;
            floatY.style.top = _top + 'px';
            floatX.style.left = ((containerW - floatXw) * percX) + 'px';

            //console.log("percY", percY, "---- can-load",scope.$eval(pk.attribute(el, 'can-load')));
            if(pk.attribute(el, 'can-load') && percY>threshold && percY<=1 && !fetchingData){
                if (scope.$eval(pk.attribute(el, 'can-load'))) {
                    saveScrollTop = container.scrollTop;
                    fetchingData = true;
                    //console.log("container.scrollTop start",saveScrollTop, container.scrollHeight);
                    scope.$apply(scope[pk.attribute(el, 'pk-infinite')](fetchDataCallback));
                }
            }
        }
        pk.bindEvent("scroll", container, function (e) {
            updateOnScroll();
        });

        function resolveDimensions(_w, _h) {
            contentH = container.scrollHeight;
            contentW = container.scrollWidth;
            containerH = Math.max(_h-verticalPadd, el.offsetHeight-verticalPadd);
            containerW = Math.max(_w, el.offsetWidth);
            saveAllowY = allowY;
            saveAllowX = allowX;
            //console.log("resolveDimensions",contentH, containerH);
            if (scrollDir.indexOf("y") > -1 && contentH > containerH) {
                allowY = true;
                pk.addClass(el, 'pk-scroll-enableY');
                //console.log("2222 >>contentH", contentH, ">>containerH", containerH, ":::", containerH*(containerH/contentH), scroll_drag_min_height);
                var floatYcalcHeight = Math.max(containerH*(containerH/contentH), scroll_drag_min_height);
                floatY.style.height = Math.round(floatYcalcHeight) + "px";
                floatYh = floatYcalcHeight;
                container.scrollTop = (contentH - containerH) * percY;
            } else {
                allowY = false;
                pk.removeClass(el, 'pk-scroll-enableY');
                container.scrollTop = 0;
            }
            if (scrollDir.indexOf("x") > -1 && contentW > containerW) {
                allowX = true;
                pk.addClass(el, 'pk-scroll-enableX');
                floatXw = floatX.offsetWidth;
                container.scrollLeft = (contentW - containerW) * percX;
            } else {
                allowX = false;
                pk.removeClass(el, 'pk-scroll-enableX');
                container.scrollLeft = 0;
            }
            // make it draggable, will go in upon allowY will change from false to true (Chaim)
            if((allowY && saveAllowY!=allowY) || (allowX && saveAllowX!=allowX)){
                addDragToXY();
            }
        }
        resolveDimensions();

        function getChildNodes(node) {
            var children = [];
            var children_height = 0;
            var currentChild;
            for(var child in node.childNodes) {
                currentChild = node.childNodes[child];
                if(currentChild.nodeType == 1) {
                    children.push(currentChild);
                    children_height += currentChild.scrollHeight;
                }
            }
            return {children:children, children_height:children_height};
        }

        setInterval(function () {
            var widthContainer = el.offsetWidth,
                heightContainer = el.offsetHeight,
                widthContent = container.scrollWidth,
                heightContent = container.scrollHeight,
                needToScroll = null;

            if(maxHeight){
                var heightContentUpdated = false;
                if (heightContent == heightContainer && heightContent != 0) {
                    //console.log("********************", heightContainer, heightContent);
                    // for cases with a high content height (scroll) turning to short content height (no scroll + decrease the container size)
                    var childs = getChildNodes(container);
                    heightContent = childs.children_height;// hack - to prevent looping over heights
                    if(Math.max(heightContent, heightContainer) - Math.min(heightContent, heightContainer) < scroll_gain){
                        heightContent = heightContainer;
                    }
                    heightContentUpdated = true;
                }
                if (heightContainer < maxHeight || heightContentUpdated) {
                    var newHeight = (heightContent < maxHeight ? heightContent : maxHeight);
                    //console.log("heightContainer/heightContent", heightContainer, heightContent, "maxHeight:", maxHeight, "newHeight", newHeight);
                    el.style.height = newHeight + "px";
                    heightContainer = newHeight;
                }
                //console.log("heightContent",heightContent, heightContainer);
            }
            if(minHeight){
                //console.log("minHeight",heightContent, heightContainer);
                if (heightContent == heightContainer && heightContent != 0 && heightContent<minHeight) {
                    el.style.height = minHeight + "px";
                    heightContainer = minHeight;
                }
            }
            if(checkForAutoScroll){
                needToScroll = scope.scrollValueFunc(pk.attribute(el, 'pk-scroll-to'));
                // make sure there is a value before scrolling.
                // in case of 'bottom' on init state the height equals to zero - so i'll pass this condition and return next time.
                if(needToScroll!=null && needToScroll.length>0 && !(needToScroll=='bottom' && heightContent==0)){
                    saveScrollTop = needToScroll=='bottom'?heightContent:needToScroll=='top'?0:parseInt(needToScroll);
                    //console.log("saveScrollTop", needToScroll,saveScrollTop, heightContent);
                    if(!isNaN(saveScrollTop)){
                        scope.$apply(scope.scrollValueFunc(pk.attribute(el, 'pk-scroll-to'), 'null'));
                        scrollEaseTo(container, saveScrollTop, fetchDataCallback, 700);
                    }
                }
            }


            if (widthContainer !== containerWidth || heightContainer !== containerHeight || widthContent !== contentWidth || heightContent !== contentHeight) {
                contentWidth = widthContent;
                contentHeight = heightContent;
                containerWidth = widthContainer;
                containerHeight = heightContainer;
                resolveDimensions(containerWidth, containerHeight);
            }
        }, 500);

        // easing functions http://goo.gl/5HLl8
        function easeInOutQuad(t, b, c, d) {
            t /= d/2;
            if (t < 1) {
                return c/2*t*t + b
            }
            t--;
            return -c/2 * (t*(t-2) - 1) + b;
        };

        function easeInCubic(t, b, c, d) {
            var tc = (t/=d)*t*t;
            return b+c*(tc);
        };

        function inOutQuintic(t, b, c, d) {
            var ts = (t/=d)*t,
                tc = ts*t;
            return b+c*(6*tc*ts + -15*ts*ts + 10*tc);
        };



        function scrollEaseTo(elt, to, callback, duration) {
            // requestAnimationFrame for Smart Animating http://goo.gl/sx5sts
            var requestAnimFrame = (function(){
                return  window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || function( callback ){ window.setTimeout(callback, 1000 / 60); };
            })();

            function move(amount) {
                elt.scrollTop = amount;
            }
            function position() {
                return elt.scrollTop;
            }
            var start = position(),
                change = to - start,
                currentTime = 0,
                increment = 20;
            duration = (typeof(duration) === 'undefined') ? 500 : duration;
            var animateScroll = function() {
                // increment the time
                currentTime += increment;
                // find the value with the quadratic in-out easing function
                var val = easeInOutQuad(currentTime, start, change, duration);
                move(val);
                // do the animation unless its over
                if (currentTime < duration) {
                    requestAnimFrame(animateScroll);
                } else {
                    if (callback && typeof(callback) === 'function') {
                        // the animation is done so lets callback
                        callback();
                    }
                }
            };
            animateScroll();
        }

        // DRAG HANDLERS
        function addDragToXY(){
            if(allowY){
                pk.draggable({
                    element: floatY,
                    move: {
                        y: true
                    },
                    container: {
                        element: trackY
                    },
                    listeners: {
                        dragging: function () {
                            container.scrollTop = (contentH - containerH) * (floatY.offsetTop / (trackY.offsetHeight - floatY.offsetHeight));
                        }
                    }
                });
                //trackY.addClass("");
            }
            if(allowX){
                pk.draggable({
                    element: floatX,
                    move: {
                        x: true
                    },
                    container: {
                        element: trackX
                    },
                    listeners: {
                        dragging: function () {
                            container.scrollLeft = (contentW - containerW) * (floatX.offsetLeft / (trackX.offsetWidth - floatX.offsetWidth));
                        }
                    }
                });
            }
        }
        addDragToXY();

        pk.bindEvent("click", floatY, function (e) {
            pk.preventBubble(e);
        });
        pk.bindEvent("click", floatX, function (e) {
            pk.preventBubble(e);
        });

        // TRACK CLICKING HANDLERS
        pk.bindEvent("click", trackY, function (e) {
            //console.log("click > contentH:", contentH, "containerH:", containerH, "containerTop", container.scrollTop);
            container.scrollTop = ((e.pageY - el.getBoundingClientRect().top) / containerH * (contentH - containerH));
        });
        pk.bindEvent("click", trackX, function (e) {
            container.scrollLeft = ((e.pageX - el.getBoundingClientRect().left) / containerW * (contentW - containerW));
        });

        // MOUSE WHEEL HANDLERS
        function mouseScroll(e) {
            var offset = Math.min((contentH - containerH - container.scrollTop), mouse_scroll_delta);//0.1; // currently tested for Y scroll
            if (e.wheelDelta > 0 || e.detail < 0) {
                if(offset==0){
                    offset=mouse_scroll_delta;
                }
                offset = offset * -1;
            }
            //console.log("offset", offset, (contentH - containerH - container.scrollTop), container.scrollTop, containerH);
            if (allowY) {
                //container.scrollTop = Math.round(container.scrollTop + (contentH - containerH) * offset);
                container.scrollTop = Math.round(container.scrollTop + offset);
            } else {
                //container.scrollLeft = Math.round(container.scrollLeft + (contentW - containerW) * offset);
                container.scrollLeft = Math.round(container.scrollLeft + offset);
            }
            // in case the current container is not scrollable
            // - let the event bubble up to check if there are other scrollable containers (parents)
            // for example: conversation container on support - once it's content is short - mouse wheel will scroll the whole list
            // in case of inner scroll while scroll reached it's final step, the event IS bubbling to allow it's parent container to continue and scroll
            if ((allowY || allowX) && !(offset<=0 || container.scrollTop==0)) {
                /* Stop wheel propagation (prevent parent scrolling) */
                pk.preventBubble(e);
            }

        }

        pk.bindEvent("mousewheel", container, mouseScroll);

        // TOUCH EVENT HANDLERS

        function getXy(e) {
            // touch event
            if (e.targetTouches && (e.targetTouches.length >= 1)) {
                return {
                    x: e.targetTouches[0].clientX,
                    y: e.targetTouches[0].clientY
                };
            }
            // mouse event
            return {
                x: e.clientX,
                y: e.clientY
            };
        }

        var pressed = false,
            startPos = {};

        function tap(e) {
            pressed = true;
            startPos = getXy(e);
            e.preventDefault();
            e.stopPropagation();
            return false;
        }

        function release(e) {
            pressed = false;
            e.preventDefault();
            e.stopPropagation();
            return false;
        }

        function drag(e) {
            var endPos, deltaX, deltaY;
            if (pressed) {
                endPos = getXy(e);
                deltaY = startPos.y - endPos.y;
                deltaX = startPos.x - endPos.x;
                if (deltaY > 2 || deltaY < -2) {
                    startPos.y = endPos.y;
                    container.scrollTop += deltaY;

                }
                if (deltaX > 2 || deltaX < -2) {
                    startPos.x = endPos.x;
                    container.scrollLeft += deltaX;
                }
            }
            e.preventDefault();
            e.stopPropagation();
            return false;
        }
        if (typeof window.ontouchstart !== 'undefined') {
            pk.bindEvent('touchstart', container[0], tap);
            pk.bindEvent('touchmove', container[0], drag);
            pk.bindEvent('touchend', window, release);
        }

        // KEYBOARD HANDLERS    
        container.setAttribute("tabindex", 0);
        // keydown was disabled - it disabled clicking on forms
        /*
        pk.bindEvent('keydown', container, function (e) {
            if(allowY){                
                switch (e.keyCode) {
                    case 38: //up cursor
                        container.scrollTop -= containerH * 0.1;
                        break;
                    case 40: //down cursor
                    case 32: //spacebar
                        container.scrollTop += containerH * 0.1;
                        break;
                    case 33: //page up
                        container.scrollTop -= containerH;
                        break;
                    case 34: //page down
                        container.scrollTop += containerH;
                        break;
                    case 36: //home
                        container.scrollTop = 0;
                        break;
                    case 35: //end
                        container.scrollTop = contentH;
                        break;
                }                
            }
            if(allowX){                
                switch (e.keyCode) {
                    case 37: //left cursor
                        container.scrollLeft -= containerW * 0.1;
                        break;
                    case 39: //right cursor
                        container.scrollLeft += containerW * 0.1;
                        break;
                }                
            }
            pk.preventBubble(e);
        });*/
    };
    return pk;
})(pk);


if (!window.getComputedStyle) {
    window.getComputedStyle = function(el, pseudo) {
        this.el = el;
        this.getPropertyValue = function(prop) {
            var re = /(\-([a-z]){1})/g;
            if (prop == 'float') prop = 'styleFloat';
            if (re.test(prop)) {
                prop = prop.replace(re, function () {
                    return arguments[2].toUpperCase();
                });
            }
            return el.currentStyle[prop] ? el.currentStyle[prop] : null;
        }
        return this;
    }
}