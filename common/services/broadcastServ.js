define(function (require) {
    'use strict';

    var app = require('appModule');

    app.register.factory('broadcastServ', ['$rootScope', function ($rootScope) {
        var sharedService = {};
        sharedService.broadcastData;


        sharedService.prepForBroadcast = function(elt) {

            this.broadcastData = elt;
            this.broadcastItem();
        };

        sharedService.broadcastItem = function() {
            $rootScope.$broadcast('handleBroadcast');
        };

        return sharedService;
    }]);

});