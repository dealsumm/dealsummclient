define(function (require) {
	'use strict';

	var app = require('appModule');

	app.factory('cacheServ', ['$cacheFactory',function ($cacheFactory) {
		var cache = $cacheFactory('cacheService',{});
		return cache;
	}]);

});