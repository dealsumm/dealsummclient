define(function (require) {
    'use strict';

    var app = require('appModule');

    app.register.service('generalServ',
                ['$http','$timeout','$q', 'DEALSUMM_CONFIG', 'GLOBAL_VARS', '$cookies', '$rootScope',
        function ($http,  $timeout,  $q,   DEALSUMM_CONFIG,   GLOBAL_VARS,   $cookies,   $rootScope) {

            var returnObject = {}, promise, url, _this, dataToSend, requestVars;
            var stateParams = $rootScope.$stateParams;
            var basePath = DEALSUMM_CONFIG.DSPROD_REST_PATH;
            var oldBasePath = DEALSUMM_CONFIG.REST_PATH;
            var urls = {};
            urls['oldBasePath'] = oldBasePath;
            urls['new_url_prefix'] = basePath;// + (stateParams.accountId || stateParams.orgId);
            urls['old_url_prefix'] = oldBasePath;// + (stateParams.accountId || stateParams.orgId);
            function getUrlPrefix(type){
                if(type=='oldBasePath'){
                    return urls[type];
                }else{
                    return urls[type] + (stateParams.accountId || stateParams.orgId);
                }
            }

            var servicesMap = {
                "general": {
                    "getFolderInfo": {
                        "params": ['project_id', 'folder_id'],
                        "method": "GET",
                        "url": '/projects/{project_id}/folders/{folder_id}/',
                        "url_prefix": 'new_url_prefix'
                    },
                    "getProjectInfo": {
                        "params": ['project_id'],
                        "method": "GET",
                        "url": '/projects/{project_id}/',
                        "url_prefix": 'new_url_prefix'
                    },
                    "getUserInfo": {
                        "params": ['user_id'],
                        "method": "GET",
                        "url": '/users/{user_id}/',
                        "url_prefix": 'new_url_prefix'
                    }
                },
                "properties": {
                    "getProperties": {
                        "params": [],
                        "method": "get",
                        "url": '/projects/',
                        "url_prefix": 'new_url_prefix'
                    },
                    "createProperty": {
                        "params": [],
                        "method": "POST",
                        "url": '/projects/',
                        "url_prefix": 'new_url_prefix'
                    }
                },
                "abstract": {
                    "updateScore": {
                        "params": ['chart_id'],
                        "method": "POST",
                        "url": '/charts/{chart_id}/',
                        "url_prefix": 'old_url_prefix',
                        "bypassErrorsInterceptor":400
                    },
                    "makeCellPrimary": {
                        "params": ['chart_id', 'property_id'],
                        "method": "POST",
                        "url": '/charts/{chart_id}/properties/{property_id}/',
                        "url_prefix": 'old_url_prefix'
                    },
                    "toggleIgnoreRow": {
                        "params": ['chart_id', 'row_id'],
                        "method": "PATCH",
                        "url": '/charts/{chart_id}/periods/{row_id}/',
                        "url_prefix": 'old_url_prefix'
                    },
                    "toggleIgnoreColumn": {
                        "params": ['chart_id', 'column_no'],
                        "method": "PATCH",
                        "url": '/charts/{chart_id}/columns/{column_no}/',
                        "url_prefix": 'old_url_prefix'
                    },
                    "addNewRow": {
                        "params": ['chart_id'],
                        "method": "POST",
                        "url": '/charts/{chart_id}/periods/',
                        "url_prefix": 'old_url_prefix'
                    },
                    "saveCellValue":{
                        "params": ['cell_id'],
                        "method": "POST",
                        "url": '/projects/1/tenants/1/annotation_periods/1/properties/{cell_id}/annotations/',
                        "url_prefix": 'new_url_prefix'
                    },
                    "saveHeaderValue":{
                        "params": ['chart_id', 'column_no'],
                        "method": "PATCH",
                        "url": '/charts/{chart_id}/columns/{column_no}/',
                        "url_prefix": 'old_url_prefix'
                    },
                    "addNewColumn":{
                        "params": ['chart_id'],
                        "method": "POST",
                        "url": '/charts/{chart_id}/columns/',
                        "url_prefix": 'old_url_prefix'
                    }
                },
                "document": {
                    "getDocDetails": {
                        "params": ['doc_id'],
                        "method": "GET",
                        "url": '/docs/?func=get_contract&id={doc_id}',
                        "url_prefix": 'old_url_prefix'
                    },
                    "getDocLabels": {
                        "params": ['folder_id', 'doc_id'],
                        "method": "GET",
                        "url": '/folders/{folder_id}/documents/{doc_id}/labels/',
                        "url_prefix": 'old_url_prefix'
                    },
                    "getDocWarnings": {
                        "params": ['folder_id', 'doc_id'],
                        "method": "GET",
                        "url": '/folders/{folder_id}/documents/{doc_id}/uploadwarnings/',
                        "url_prefix": 'old_url_prefix'
                    },
                    "getDocAbstract": {
                        "params": ['doc_id'],
                        "method": "GET",
                        "url": '/docs/?func=get_ts_table_json&doc_id={doc_id}',
                        "url_prefix": 'old_url_prefix'
                    }
                },
                "category": {
                    "getClassCategories": {
                        "params": [],
                        "method": "GET",
                        "url": '/classcategories/',
                        "url_prefix": 'old_url_prefix'
                    },
                    "newCategory": {
                        "params": [],
                        "method": "PUT",
                        "url": '/classcategories/',
                        "url_prefix": 'old_url_prefix'
                    },
                    "updateCategoryName": {
                        "params": ['category_id'],
                        "method": "PATCH",
                        "url": '/classcategories/{category_id}/',
                        "url_prefix": 'old_url_prefix'
                    },
                    "setCategoryToParagraphs": {
                        "params": ['category_id'],
                        "method": "PUT",
                        "url": '/classcategories/{category_id}/labels/',
                        "url_prefix": 'old_url_prefix',
                        "bypassErrorsInterceptor":400
                    },
                    "updateCategoryToParagraphs": {
                        "params": ['category_id', 'label_id'],
                        "method": "PATCH",
                        "url": '/classcategories/{category_id}/labels/{label_id}/',
                        "url_prefix": 'old_url_prefix'
                    },
                    "removeLabelFromParagraphs": {
                        "params": ['folder_id', 'doc_id', 'label_id'],
                        "method": "DELETE",
                        "url": '/folders/{folder_id}/documents/{doc_id}/labels/{label_id}',
                        "url_prefix": 'old_url_prefix'
                    }
                },
                "admin": {
                    "sysConfig": {
                        "params": [],
                        "method": "GET",
                        "url": '/sysconfig/items/',
                        "url_prefix": ''
                    },
                    "updateSysConfigItem": {
                        "params": ['item_id'],
                        "method": "PATCH",
                        "url": '/sysconfig/items/{item_id}/',
                        "url_prefix": ''
                    },
                    "getOrganizationsList": {
                        "params": [],
                        "method": "GET",
                        "url": '1/docs/?func=show_organizations',
                        "url_prefix": 'oldBasePath'
                    }
                },
                "split":{
                    "getDocDetails":{
                        "params": ['doc_id'],
                        "method": "GET",
                        "url": '/docs/?func=get_contract&id={doc_id}',
                        "url_prefix": 'old_url_prefix'
                    },
                    "getSplits":{
                        "params": ['project_id', 'folder_id', 'doc_id'],
                        "method": "GET",
                        "url": '/folders/{folder_id}/documents/{doc_id}/splits/',       
                        "url_prefix": 'old_url_prefix'
                    },
                    "applySplit":{
                        "params": ['project_id', 'folder_id', 'doc_id'],
                        "method": "POST",
                        "url": '/folders/{folder_id}/documents/{doc_id}/splits/',   
                        "url_prefix": 'old_url_prefix',
                        "bypassErrorsInterceptor":400
                    },
                    "updateSplit":{
                        "params": ['project_id', 'folder_id', 'doc_id', 'split_id'],
                        "method": "PATCH",
                        "url": '/folders/{folder_id}/documents/{doc_id}/splits/{split_id}',       
                        "url_prefix": 'old_url_prefix',
                        "bypassErrorsInterceptor":400
                    },
                    "addSplit":{
                        "params": ['project_id', 'folder_id', 'doc_id'],
                        "method": "PATCH",
                        "url": '/folders/{folder_id}/documents/{doc_id}/splits/',       
                        "url_prefix": 'old_url_prefix',
                        "bypassErrorsInterceptor":400
                    }
                },
                "directory":{
                    "getProjectFolders":{
                        "params": ['project_id'],
                        "method": "GET",
                        "url": '/projects/{project_id}/folders/',
                        "url_prefix": 'new_url_prefix'
                    },
                    "getFolderDocs":{
                        "params": ['project_id', 'folder_id'],
                        "method": "GET",
                        "url": '/folders/{folder_id}/documents/',    
                        "url_prefix": 'old_url_prefix'
                    },
                    "removeDoc":{
                        "params": ['project_id', 'folder_id', 'document_id'],
                        "method": "DELETE",
                        "url": '/folders/{folder_id}/documents/{document_id}/', 
                        "url_prefix": 'old_url_prefix'
                    },
                    "removeFolder":{
                        "params": ['folder_id'],
                        "method": "DELETE",
                        "url": '/folder/{folder_id}',
                        "url_prefix": 'old_url_prefix'
                    },
                    "reprocessDoc":{
                        "params": ['folder_id', 'document_id', 'async'],
                        "method": "POST",
                        "url": '/folders/{folder_id}/documents/{document_id}/?async={async}',
                        "url_prefix": 'old_url_prefix'
                    }
                },
                "pdf":{
                     "getPdfDetails":{
                         "params": ['project_id', 'folder_id', 'document_id'],
                         "method": "GET",
                         "url": '/folders/{folder_id}/documents/{document_id}/', 
                         "url_prefix": 'old_url_prefix'
                     }
                },
                "search":{
                    "getSearchHistory":{
                        "params": [],
                        "method": "GET",
                        "url": '/searchhistory/',
                        "url_prefix": 'old_url_prefix'
                    },
                    "getSearchHistoryItem":{
                        "params": ['item_id'],
                        "method": "GET",
                        "url": '/searchhistory/{item_id}/',
                        "url_prefix": 'old_url_prefix'
                    },
                    "setSearchHistoryItem":{
                        "params": ['item_id'],
                        "method": "PATCH",
                        "url": '/searchhistory/{item_id}/',
                        "url_prefix": 'old_url_prefix'
                    },
                    "runSearch":{
                        "params": ['node_type', 'node_id', 'search_page'],
                        "method": "POST",
                        "url": '/{node_type}/{node_id}/{search_page}',
                        "url_prefix": 'old_url_prefix'
                    },
                    "getQueryTemplates":{
                        "params": [],
                        "method": "GET",
                        "url": '/querytemplates/',
                        "url_prefix": 'old_url_prefix'
                    },
                    "setNewQueryTemplateName":{
                        "params": [],
                        "method": "PUT",
                        "url": '/querytemplates/',
                        "url_prefix": 'old_url_prefix'
                    },
                    "updateQueryTemplateName":{
                        "params": ['cat_id'],
                        "method": "PATCH",
                        "url": '/querytemplates/{cat_id}/',
                        "url_prefix": 'old_url_prefix'
                    }
                },
                "upload":{
                    "files":{
                        "params": ['folder_id'],
                        "url": '/folders/{folder_id}/documents/?async=true',
                        "url_prefix": 'old_url_prefix'
                    },
                    "directory":{
                        "params": ['project_id'],
                        "url": '/projects/{project_id}/folders/',
                        "url_prefix": 'new_url_prefix'
                    }
                }
            };
            returnObject.http = function(module, func, params, data){
                _this = servicesMap[module][func];
                url = (getUrlPrefix(_this.url_prefix)||'') + _this.url;
                for(var i=0;i<_this.params.length;i++){
                    url = url.replace('{' + _this.params[i] + '}', params[i]||'');
                }
                requestVars = { method: _this.method, url: url};
                if(data){
                    requestVars.data = angular.toJson(data);
                }
                if(_this.bypassErrorsInterceptor){
                    requestVars.bypassErrorsInterceptor = _this.bypassErrorsInterceptor;
                }
                promise = $http(requestVars).then(function (data) {
                    return data;
                });
                return promise;
            };
            returnObject.getPaths = function(module, func, params){
                _this = servicesMap[module][func];
                url = (getUrlPrefix(_this.url_prefix)||'') + _this.url;
                for(var i=0;i<_this.params.length;i++){
                    url = url.replace('{' + _this.params[i] + '}', params[i]||'');
                }
                return url;
            }
            returnObject.updateLastFolderAndDoc = function (folder_id, doc_id, user_id) {
                // http://<host:port>/dsprod/account/<id>/users/<user_id>/
                var dataToSend = {};
                dataToSend["last_folder"] = folder_id;
                if(doc_id){
                    dataToSend["last_doc"] = doc_id;
                }
                url = basePath + $rootScope.$stateParams.accountId + '/users/' + user_id + "/";
                promise = $http({ method: 'PATCH', url: url, data: angular.toJson(dataToSend)});
                promise.success(function (data, status) {
                         return data;
                    });
                return promise;
            };
            return returnObject;
        }
    ]);
    app.register.service('headersServ', function ($http, GLOBAL_VARS, $q) {
        var ret;
        var _url = STATIC_PREFIX + 'common/services/headers.json';
        var deferred;
        this.get = function (module) {
            deferred = $q.defer();
            if (GLOBAL_VARS.HEADERS) {
                ret = module ? GLOBAL_VARS.HEADERS[module] : GLOBAL_VARS.HEADERS;
                deferred.resolve(ret);
            } else {
                $http.get(_url).then(function (resp) {
                    GLOBAL_VARS.HEADERS = resp.data;
                    ret = module ? resp.data[module] : resp.data;
                    deferred.resolve(ret);
                })
            }
            return deferred.promise;
        }
    });
});

