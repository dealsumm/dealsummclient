define(function (require) {
    'use strict';

    var app = require('appModule');

    app.register.service('uploadServ',
                ['$http', '$timeout','$q', 'DEALSUMM_CONFIG', 'GLOBAL_VARS', '$cookies', '$rootScope', '$dialog',
        function ($http,     $timeout,  $q,   DEALSUMM_CONFIG,   GLOBAL_VARS,   $cookies,   $rootScope, $dialog) {

            var returnObject = {}, promise, url;
            var basePath = DEALSUMM_CONFIG.DSPROD_REST_PATH;
            var oldBasePath = DEALSUMM_CONFIG.REST_PATH;

            // from drag & drop
            returnObject.folderUpload = null;
            returnObject.uploadFilesList = [];
            //$rootScope.uploadFilesCounter = 0;
            returnObject.uploadDone = function(){
                returnObject.folderUpload = null;
            }

            returnObject.pushUploadFiles = function (content) {
                returnObject.uploadFilesList.push(content);
            };
            returnObject.uploadFolder = function (itemEntryName, parentFolderId, parentType, deferred) {

                //returnObject.uploadProgressUpdate(itemEntryName);

                url = basePath + $rootScope.$stateParams.accountId + '/projects/' + parentFolderId + '/folders/';
                var data = {};
                data['name'] = itemEntryName;
                data['parent_folder_id'] = parentFolderId;
                data['parent_type'] = parentType;

                console.log("uploadFolder-data",data);
                return $http({ method: 'POST', url: url, data: data, bypassErrorsInterceptor:400}).then(function (response) {
                    if(response.status != 400){
                        $rootScope.safeApply(function () {
                            returnObject.folderUpload = {obj:response.data};
                        });
                    }
                    return response;
                });
                /*return $http.post(url, data).then(function(response){
                    //console.log("response returned +++", response);
                    $rootScope.safeApply(function () {
                        returnObject.folderUpload = {obj:response.data};
                    })
                    return response;
                })*/
            };
            returnObject.uploadFile = function (file, folderId, parentType, idx, deferred) {

                //console.log("uploadFile...");
                //returnObject.uploadDocumentProgressUpdate(file.name, folderId);
                //returnObject.uploadProgressUpdate(file.name);

                var accountId = $rootScope.$stateParams.accountId;
                var projectId = $rootScope.$stateParams.projectId;
                var tenantId = folderId;


                // temp old bypass path
                //url = DEALSUMM_CONFIG.REST_PATH + accountId + '/adddoc/';

                // working path for new flow - DO NOT REMOVE!
                //url = basePath + accountId + '/projects/' + projectId + '/tenants/' + tenantId + '/documents/';
                url = oldBasePath + accountId + '/folders/' + tenantId + '/documents/?async=true';

                var xhr = new XMLHttpRequest();
                xhr.open('POST', url, true);
                /*function updateListItem(obj){

                    $timeout(function(){
                        returnObject.folderUpload = obj;
                        //console.log("returnObject.folderUpload",returnObject.folderUpload);
                    }, 100)
                }*/
                var formData = new FormData();
                formData.append('file', file);
                formData.append('folder_id', folderId);
                formData.append('parent_type', parentType);
                var csrftoken = $cookies['csrftoken'];
                xhr.setRequestHeader("X-CSRFToken", csrftoken);

                xhr.onreadystatechange=function() {
                    //console.log("xhr.readyState",xhr.readyState, xhr.status, xhr);
                    if (xhr.readyState==4 && (xhr.status==200 || xhr.status==201)) {
                        var obj = JSON.parse(xhr.responseText);
                        $rootScope.safeApply(function () {
                            // TEMP!!
                            if (!obj.name) {
                                obj.name = obj.label;
                            }
                            obj.id = ""+obj.id;
                            if (obj.id.indexOf("D_")>-1) {
                                obj.id = obj.id.split("_")[1];
                            }
                            // END TEMP!!
                            //console.log("document - obj",obj, tenantId);
                            //returnObject.folderUpload = {obj:obj, documentParent:tenantId};
                        })
                        //updateListItem({obj:obj, documentParent:tenantId});
                        deferred.resolve(obj.id);
                        //return obj.doc_id;
                    }
                }
                xhr.send(formData);
            };


            return returnObject;
        }
    ]);
});

