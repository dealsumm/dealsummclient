
////////////////////////////////////////////////////////////////////////
//																	  //
//                                                                    //
//																	  //
////////////////////////////////////////////////////////////////////////

require.config({
    baseUrl: '',
    // Add angular modules that does not support AMD out of the box, put it in a shim
    paths: {
        // utils
        appModule: STATIC_PREFIX+'app/appModule',
        bootCtrl: STATIC_PREFIX+'app/bootCtrl',
        angularAMD: STATIC_PREFIX+'common/utils/angularAMD',
        ngLoad: STATIC_PREFIX+'common/utils/ngLoad',
        httpRequestTracker: STATIC_PREFIX+'common/utils/httpRequestTracker',
        localStorageModule: STATIC_PREFIX+'common/utils/localStorageModule',
        notifications: STATIC_PREFIX+'common/utils/notifications',
        angularFileUpload: STATIC_PREFIX+'common/utils/angularFileUpload',
        smoothScroll: STATIC_PREFIX+'common/utils/smoothScroll',
        browserDetect: STATIC_PREFIX+'common/utils/browserDetect',
        'ncy-angular-breadcrumb': STATIC_PREFIX+'common/utils/breadcrumbs/angular-breadcrumb',
        generalServ: STATIC_PREFIX+'common/services/generalServ',
        ngFileUpload: STATIC_PREFIX+'common/directives/ng-file-upload/ng-file-upload',

        // app directives
        uiModal: STATIC_PREFIX+'common/directives/modal/modal2',
        googlechart: STATIC_PREFIX+'common/directives/ng-google-chart',

        // vendor
        'jquery': STATIC_PREFIX+'vendor/jquery/jquery',
        'angular': STATIC_PREFIX+'vendor/angular/angular',
        'angular-animate': STATIC_PREFIX+'vendor/angular-animate/angular-animate',
        'angular-mocks': STATIC_PREFIX+'vendor/angular-mocks/angular-mocks',
        'angular-resource': STATIC_PREFIX+'vendor/angular-resource/angular-resource',
        'angular-cookies': STATIC_PREFIX+'vendor/angular-cookies/angular-cookies',
        'angular-sanitize': STATIC_PREFIX+'vendor/angular-sanitize/angular-sanitize',
        'angular-ui-router': STATIC_PREFIX+'vendor/angular-ui-router/angular-ui-router',
        'angular-ui-bootstrap-collapse': STATIC_PREFIX+'vendor/angular-ui-bootstrap/collapse',
        'angular-ui-bootstrap-progressbar': STATIC_PREFIX+'vendor/angular-ui-bootstrap/progressbar',
        'angular-ui-bootstrap-transition': STATIC_PREFIX+'vendor/angular-ui-bootstrap/transition',
        'angular-ui-bootstrap-dropdownToggle': STATIC_PREFIX+'vendor/angular-ui-bootstrap/dropdownToggle',
        'angular-ui-bootstrap-bindHtml': STATIC_PREFIX+'vendor/angular-ui-bootstrap/bindHtml',
        'angular-ui-bootstrap-typeahead': STATIC_PREFIX+'vendor/angular-ui-bootstrap/typeahead',
        'angular-ui-bootstrap-position': STATIC_PREFIX+'vendor/angular-ui-bootstrap/position',
        'angular-ui-bootstrap-tpls': STATIC_PREFIX+'vendor/angular-ui-bootstrap-bower/ui-bootstrap-tpls',
        'angular-ui-utils-scrollfix': STATIC_PREFIX+'vendor/angular-ui-utils/scrollfix',
        'angular-ui-utils-keypress': STATIC_PREFIX+'vendor/angular-ui-utils/keypress',
        'angular-ui-utils-unique': STATIC_PREFIX+'vendor/angular-ui-utils/unique',

        // modules
        'dealsumm.dashboard': STATIC_PREFIX+'app/client/_pages/dashboard/page_dashboard',
        'dealsumm.property': STATIC_PREFIX+'app/client/_pages/property/page_property'
	},
    shim: {
        'appModule': ['angular'],
        'angularAMD': ['angular'],
        'angular-animate': ['angular']
    },
    modules: [
        //First set up the common build layer.
        {
            //module names are relative to baseUrl
            name: 'app/appBootstrap',
            include: [
                'appModule'
            ]
        }
    ],
    waitSeconds: 30
});



require(['angularAMD', 'appModule', 'browserDetect', 'ngLoad', STATIC_PREFIX+'app/bootCtrl.js'], function ( angularAMD,   app) {

    'use strict';
/*    if (window.BrowserDetect.browser === "Explorer" && window.BrowserDetect.version < 8) { document.location.href("oldBrowser.html"); }
    if (window.BrowserDetect.browser === "Chrome") { require(['smoothScroll']); }
    if (window.BrowserDetect.browser === "Explorer" && window.BrowserDetect.version === 8) { document.body.className = 'ie8'; }*/
    // Bootstrap Angular when DOM is ready
    angularAMD.bootstrap(app);

});

