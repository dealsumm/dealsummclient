'use strict';

module.exports = function(grunt) {
	// Unified Watch Object
	var watchFiles = {
		clientViews: ['app/**/**/views/**/*.html' ,'app/**/**/**/*.html', '*.html'],
		clientJS: ['app/**/**/**/*.js', 'app/*.js'],
		clientCSS: ['styles/*.css'],
		mochaTests: ['app/tests/**/*.js']
	};

	// Project Configuration
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		watch: {
			clientViews: {
				files: watchFiles.clientViews,
				options: {
					livereload: true
				}
			},
			clientJS: {
				files: watchFiles.clientJS,
				tasks: ['jshint'],
				options: {
					livereload: true
				}
			},
			clientCSS: {
				files: watchFiles.clientCSS,
				tasks: ['csslint'],
				options: {
					livereload: true
				}
			}
		},
		jshint: {
			all: {
				src: watchFiles.clientJS,
				options: {
					jshintrc: true
				}
			}
		},
		csslint: {
			options: {
				csslintrc: '.csslintrc'
			},
			all: {
				src: watchFiles.clientCSS
			}
		},
		ngtemplates: {
			// Change `angular` namespace to something else
			appModule: {
				src: ['**/*.html'],
				dest: 'dist/angularTemplate.js',
				options: {
					htmlmin: {
						collapseBooleanAttributes:      true,
						collapseWhitespace:             true,
						removeAttributeQuotes:          true,
						removeComments:                 true, // Only if you don't use comment directives!
						removeEmptyAttributes:          true,
						removeRedundantAttributes:      true,
						removeScriptTypeAttributes:     true,
						removeStyleLinkTypeAttributes:  true
					}
				}
			}
		},
		image: {
			dynamic: {
				files: [{
					expand: true,
					src: ['styles/img/**/*.{png,jpg,gif,svg}', 'html_resources/images/*.{png,jpg,gif,svg}'],
					dest: 'dist/'
				}]
			}
		},
		uglify: {
			min: {
				files: grunt.file.expandMapping(['app/**/*.js', '**/*.js','!node modules/**/*.js','!node modules/grunt-concurrent/**/*.js','!dist/**/*.js','!gruntfile.js'], 'dist/', {
					rename: function(destBase, destPath) {
						return destBase+destPath;
					}
				})
			}
		},
		cssmin: {
			combine: {
				files: {
					'dist/styles/mainclient.css': watchFiles.clientCSS
				}
			}
		},
		concurrent: {
			default: ['nodemon', 'watch'],
			debug: ['nodemon', 'watch', 'node-inspector'],
			options: {
				logConcurrentOutput: true,
				limit: 10
			}
		},
		env: {
			test: {
				NODE_ENV: 'test'
			},
			secure: {
				NODE_ENV: 'secure'
			}
		},
		mochaTest: {
			src: watchFiles.mochaTests,
			options: {
				reporter: 'spec',
				require: 'server.js'
			}
		},
		karma: {
			unit: {
				configFile: 'karma.conf.js'
			}
		}
	});

	// Load NPM tasks
	require('load-grunt-tasks')(grunt);

	// Making grunt default to force in order not to break the project.
	grunt.option('force', true);

	// Lint task(s).
	grunt.registerTask('lint', ['jshint', 'csslint']);
	grunt.loadNpmTasks('grunt-angular-templates');
	// Build task(s).
	grunt.registerTask('build', ['uglify', 'cssmin']);
	grunt.registerTask('build-images',['image']);
	grunt.registerTask('build-angularTemplates',['ngtemplates']);
	// Test task.
	grunt.registerTask('test', ['env:test', 'mochaTest', 'karma:unit']);
};